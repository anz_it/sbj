<?php
class CategoryController extends CategoryControllerCore
{
    public function init()
    {
        $id_category = (int) Tools::getValue('id_category');
        $listFeaturedProducts = array();
        if($id_category == 23) {
            $listFeaturedProducts = Product::getFeaturedProducts(Configuration::get('JEWELRY_FEATURED_CAT'), $this->context->language->id);
        }
        if($id_category == 22) {
            $listFeaturedProducts = Product::getFeaturedProducts(Configuration::get('GIFT_FEATURED_CAT'), $this->context->language->id);
        } 
         
        $this->category = new Category(
            $id_category,
            $this->context->language->id
        );
        $parentCats = $this->category->getParentsCategories();
        $parentCatsCount = count($parentCats);
        if($parentCatsCount > 1) {
            $relatedCat = $parentCats[$parentCatsCount-2];
            $new_product_id_category = $relatedCat['id_category'];
        } else {
            $new_product_id_category = NEW_PRODUCT_CATEGORY_ID;
        }
        $category_tree = Category::getNestedCategories($new_product_id_category, $this->context->language->id);
        $category_tree =array_values($category_tree);
        if(isset($category_tree[0]) && $parentCatsCount > 3) {
            $category_tree = $category_tree[0]['children'];
        }
        $this->context->smarty->assign(array(
                'category_tree' => $category_tree,
            ));
        
        

        if (!Validate::isLoadedObject($this->category) || !$this->category->active) {
            Tools::redirect('index.php?controller=404');
        }

        parent::init();

        if (!$this->category->checkAccess($this->context->customer->id)) {
            header('HTTP/1.1 403 Forbidden');
            header('Status: 403 Forbidden');
            $this->errors[] = $this->trans('You do not have access to this category.', array(), 'Shop.Notifications.Error');
            $this->setTemplate('errors/forbidden');

            return;
        }

        $categoryVar = $this->getTemplateVarCategory();

        $filteredCategory = Hook::exec(
            'filterCategoryContent',
            array('object' => $categoryVar),
            $id_module = null,
            $array_return = false,
            $check_exceptions = true,
            $use_push = false,
            $id_shop = null,
            $chain = true
        );
        if (!empty($filteredCategory['object'])) {
            $categoryVar = $filteredCategory['object'];
        }
        $this->context->smarty->assign(array(
            'category' => $categoryVar,
            'subcategories' => $this->getTemplateVarSubCategories(),
            'featuredProducts' => $listFeaturedProducts,
        ));
    }
    /**
     * @inheritdoc
     */
    public function initContent()
    {
        parent::initContent();
        $hasSubCategory = $this->category->getSubCategories($this->context->language->id);
        $viewDetail = Tools::getValue('selected_filters');
        if(empty($viewDetail) || $viewDetail !='xem-chi-tiet') {
            $category_products = array();
            foreach ($hasSubCategory as $key=> $category){
                $cate = new Category($category['id_category'], $this->context->language->id);
                $listProduct = $cate->getProducts($this->context->language->id,1,7);
                $category_products[$key]['category'] = $cate;
                $category_products[$key]['products'] = $listProduct;
            }
            $variables = $this->getProductSearchVariables();
            $this->context->smarty->assign(array(
                'listing' => $variables,
                'category_products'=>$category_products,
                'total_sub_category'=>count($hasSubCategory),
            ));
            $params = [
                'entity' => 'category',
                'id' => $this->category->id,
            ];
            $this->setTemplate('catalog/listing/category', $params);
        } else {
            $this->setTemplate('catalog/listing/product-list');
        }

    }
}
