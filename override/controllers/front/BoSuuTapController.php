<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
class BoSuuTapController extends FrontController
{
    public $ssl = false;

    /**
    * Assign template vars related to page content
    * @see FrontController::initContent()
    */
    public function initContent()
    {
        parent::initContent();
        $trungtamgiamdinh_link = Configuration::get('trungtamgiamdinh_IMG', $this->context->language->id);
        $dichvukimhoan_link = Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_img', $this->context->language->id);
        $dichvugiamdinh_link = Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_img', $this->context->language->id);
        if ($trungtamgiamdinh_link) {
            $trungtamgiamdinh_link = $this->context->link->protocol_content . Tools::getMediaServer($trungtamgiamdinh_link) . '/modules/ps_trungtamgiamdinh/' . 'img/' . $trungtamgiamdinh_link;
        }
        if ($dichvukimhoan_link) {
            $dichvukimhoan_link = $this->context->link->protocol_content . Tools::getMediaServer($dichvukimhoan_link) . '/modules/ps_trungtamgiamdinh/' . 'img/' . $dichvukimhoan_link;
        }
        if ($dichvugiamdinh_link) {
            $dichvugiamdinh_link = $this->context->link->protocol_content . Tools::getMediaServer($dichvugiamdinh_link) . '/modules/ps_trungtamgiamdinh/' . 'img/' . $dichvugiamdinh_link;
        }

        $categoryId = Configuration::get('trungtamgiamdinh_tin_tuc_kien_thuc', $this->context->language->id);
        $posts = $this->getAllPostsByCategory($categoryId,$this->context->language->id);
        $this->context->smarty->assign(
            array(
                'trungtamgiamdinh_link' => $trungtamgiamdinh_link,
                'trungtamgiamdinh_dich_vu_kim_hoan_img' => $dichvukimhoan_link,
                'trungtamgiamdinh_dich_vu_giam_dinh_img' =>$dichvugiamdinh_link,
                'trungtamgiamdinh_whisper' => Configuration::get('trungtamgiamdinh_whisper', $this->context->language->id),
                'trungtamgiamdinh_whisper_desc' => Configuration::get('trungtamgiamdinh_whisper_desc', $this->context->language->id),
                'trungtamgiamdinh_dich_vu_kim_hoan' => Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan', $this->context->language->id),
                'trungtamgiamdinh_dich_vu_kim_hoan_desc' => Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_desc', $this->context->language->id),
                'trungtamgiamdinh_dich_vu_giam_dinh' => Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh', $this->context->language->id),
                'trungtamgiamdinh_dich_vu_giam_dinh_desc' => Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_desc', $this->context->language->id),
                'trungtamgiamdinh_tieu_chuan_chat_luong' => Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong', $this->context->language->id),
                'trungtamgiamdinh_tieu_chuan_chat_luong_desc' => Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_desc', $this->context->language->id),
                'trungtamgiamdinh_tieu_chuan_chat_luong_img' => Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_img', $this->context->language->id),
                'trungtamgiamdinh_tin_tuc_kien_thuc' => Configuration::get('trungtamgiamdinh_tin_tuc_kien_thuc', $this->context->language->id),
                'posts' => $posts,
            )
        );
        $this->setTemplate('bo-suu-tap');
    }
    private function updateUrl($link)
    {
        if (substr($link, 0, 7) !== "http://" && substr($link, 0, 8) !== "https://") {
            $link = "http://" . $link;
        }

        return $link;
    }
    public function getAllPostsByCategory($id_category, $id_lang)
    {
        $sql = 'SELECT p.*, pl.*
                FROM '._DB_PREFIX_.'jxblog_post p
                LEFT JOIN '._DB_PREFIX_.'jxblog_post_lang pl
                ON(p.`id_jxblog_post` = pl.`id_jxblog_post` AND pl.`id_lang` = '.(int)$id_lang.')
                LEFT JOIN '._DB_PREFIX_.'jxblog_post_category pc
                ON(p.`id_jxblog_post` = pc.`id_jxblog_post`)
                WHERE p.`date_start` <= NOW()
                AND pc.`id_jxblog_category` = '.(int)$id_category.'
                AND p.`active` = 1
                LIMIT 5
                ';

        return Db::getInstance()->executeS($sql);
    }
}
