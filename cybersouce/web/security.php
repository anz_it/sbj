<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', 'e4203fbac03c4ce3b8b3488772420d009643f580068f4399907633a7213304326372ea9782fd407a913228c649cfc68216524ba6ac87480e9d343b1b6a998620dcdd6a62b85f498b8de91a71fffbc9c28b964efb08054238af778fd9b1561fbd79d9c26306904fef8fc1f7e6fc93aaad4ce5cde7ca1244f4a8141d94fb672108');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
