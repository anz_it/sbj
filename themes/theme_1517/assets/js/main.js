(function ($) {
  'use strict';
  // **********************************************************************//
  // ! Init variables global
  // **********************************************************************//
  var pageSection = $('.slide-bg-image');
  pageSection.each(function (indx) {
    if ($(this).attr('data-background-img')) {
      $(this).css('background-image', 'url(' + $(this).data('background-img') + ')');
    }
    if ($(this).attr('data-bg-position-x')) {
      $(this).css('background-position', $(this).data('bg-position-x'));
    }
    if ($(this).attr('data-height')) {
      $(this).css('height', $(this).data('height') + 'px');
    }
  });

  function slide_home() {
    if ($('.slide-home').length) {
      $('.slide-home').slick({
        fade: true,
        autoplay: true,
        dots: true,
        arrows: false
      })
    }
  }

  function banner_inner() {
    if ($('.banner-inner .item-img').length == 1) {
      $('.banner-inner').slick({
        fade: true,
        autoplay: true,
        dots: false,
        arrows: false
      })
    } else {
      $('.banner-inner').slick({
        fade: true,
        autoplay: true,
        dots: false,
        arrows: false
      })
    }
  }



  function slide_products() {
    if ($('.slide-products').length) {
      $('.slide-products').slick({
        arrows: true,
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        cssEase: 'linear',
        variableWidth: true,
        activateRows: true,
        responsive: [{
          breakpoint: 320,
          settings: {
            rows: 2,
            slidesPerRow: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: false
          }
        }]

      });
    }
  }

  function slide_other_products() {
    if ($('.slide-other-products').length) {
      $('.slide-other-products').slick({
        arrows: true,
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        cssEase: 'linear',
        variableWidth: true,
        activateRows: true,
        responsive: [{
          breakpoint: 320,
          settings: {
            slidesPerRow: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: false
          }
        }]

      });
    }
  }

  function slide_prodcuts_sale() {
    if ($('.slide-prodcuts-sale').length) {
      $('.slide-prodcuts-sale').slick({
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        cssEase: 'linear',
        variableWidth: true,
        responsive: [{
          breakpoint: 320,
          settings: {
            rows: 2,
            slidesPerRow: 1,
            slidesToScroll: 1,
            variableWidth: false
          }
        }]

      });
    }
  }


  function slide_products_small() {
    $('.slide-product-small').slick({
      arrows: true,
      dots: false,
      rows: 2,
      slidesPerRow: 3,
      slidesToScroll: 1,
      infinite: true,
      cssEase: 'linear',
      variableWidth: false,
      responsive: [{
        breakpoint: 767,
        settings: {
          slidesPerRow: 2
        }
      },
      {
        breakpoint: 320,
        settings: {
          rows: 2,
          slidesPerRow: 1
        }
      }
      ]

    });
  }

  function slide_collection() {
    $('.slide-collection').slick({
      arrows: false,
      dots: true,
      slidesPerRow: 1,
      slidesToScroll: 1,
      infinite: true,
      cssEase: 'linear',
      autoplay: true,
      fade: true
    });
  }


  function slide_gifts() {
    $('.slide-gifts').slick({
      arrows: true,
      dots: false,
      rows: 2,
      slidesPerRow: 3,
      slidesToScroll: 1,
      infinite: true,
      cssEase: 'linear',
      variableWidth: false,
      responsive: [{
        breakpoint: 767,
        settings: {
          slidesPerRow: 2
        }
      },
      {
        breakpoint: 567,
        settings: {
          rows: 2,
          slidesPerRow: 1
        }
      }
      ],
      nextArrow: '.custom .slick-next',
      prevArrow: '.custom .slick-prev'
    });
  }


  var init_header = function () {
    var largeScreen = matchMedia('only screen and (min-width: 992px)').matches;
    if (largeScreen) {
      if ($().sticky) {
        $('header.header-sticky').sticky();
      }
    }
  };

  //Menu
  var ResponsiveMenu = {
    menuType: 'desktop',
    initial: function (winWidth) {
      ResponsiveMenu.menuWidthDetect(winWidth);
      ResponsiveMenu.menuBtnClick();
      ResponsiveMenu.parentMenuClick();
    },
    menuWidthDetect: function (winWidth) {
      var currMenuType = 'desktop';
      if (matchMedia('only screen and (max-width: 991px)').matches) {
        currMenuType = 'mobile';
      }
      if (currMenuType !== ResponsiveMenu.menuType) {
        ResponsiveMenu.menuType = currMenuType;
        if (currMenuType === 'mobile') {
          var $mobileMenu = $('#mainnav').attr('id', 'mainnav-mobi').hide();
          $('#header').find('.header-wrap').after($mobileMenu);
          var hasChildMenu = $('#mainnav-mobi').find('li:has(ul)');
          hasChildMenu.children('ul').hide();
          hasChildMenu.children('a').after('<span class="btn-submenu"></span>');
          $('.navbar-toogle').removeClass('active');
        } else {
          var $desktopMenu = $('#mainnav-mobi').attr('id', 'mainnav').removeAttr('style');
          $desktopMenu.find('.sub-menu').removeAttr('style');
          $('#header').find('.navbar-toogle').after($desktopMenu);
          $('.btn-submenu').remove();
        }
      } // clone and insert menu
    },
    menuBtnClick: function () {
      $('.navbar-toogle').on('click', function () {
        $('#mainnav-mobi').slideToggle(300);
        $(this).toggleClass('active');
      });
    }, // click on moblie button
    parentMenuClick: function () {
      $(document).on('click', '#mainnav-mobi li .btn-submenu', function (e) {
        if ($(this).has('ul')) {
          e.stopImmediatePropagation()
          $(this).next('ul').slideToggle(300);
          $(this).toggleClass('active');
        }
      });
    } // click on sub-menu button
  };

  //getValue Qty
  var getValue = function () {
    $('.qty_inc').on('click', function () {
      var $qty = $(this).closest('.qty-holder').find('.input-qty');
      var currentVal = parseInt($qty.val());
      if (!isNaN(currentVal)) {
        $qty.val(currentVal + 1);
      }
    });

    $('.qty_dec').on('click', function () {
      var $qty = $(this).closest('.qty-holder').find('.input-qty');
      var currentVal = parseInt($qty.val());
      if (!isNaN(currentVal) && currentVal > 0) {
        $qty.val(currentVal - 1);
      }
    });
  };
  function checkStepPayment() {
    if ($('.checkout-step').length > 0) {
      $(".checkout-step").each(function () {
        var pos = $(this).attr('data-pos');
        if ($(this).hasClass('-complete')) {
          $('.nav-tabs .' + pos).addClass('reachable');
        }
        if ($(this).hasClass('show')) {
          $('.nav-tabs .' + pos).addClass('reachable').addClass('active');
        }
      });
    }
  }
  // **********************************************************************//
  // ! Window ready
  // **********************************************************************//
  $(document).ready(function () {
    init_header();
    ResponsiveMenu.initial($(window).width());
    $(window).resize(function () {
      ResponsiveMenu.menuWidthDetect($(this).width());
    });

    slide_home();
    slide_products();
    slide_products_small();
    // banner_inner();
    slide_gifts();

    slide_prodcuts_sale();

    slide_collection();

    slide_other_products();

    $('.dotdotdot').dotdotdot();

    getValue();
    checkStepPayment();
    //banner news
    $('.banner-2 ').slick({
      fade: true,
      autoplay: true,
      dots: false,
      arrows: false
    })


    $('.tree-toggle').click(function () {
      $(this).parent().children('ul.tree').toggle(200);
    });

    $(function () {
      $('.tree-toggle').parent().children('ul.tree').toggle(200);
    });

    $('.toggle-nav-pro').click(function () {
      $(this).parents('.nav-collsape').toggleClass('open-sub');
    });

    $('.toogle-menu-ft').click(function () {
      $(this).parent().toggleClass('show');
    });

    // $('.header-search-icon').click(function(){
    //   $('.top-search').addClass('show');
    // });

    $('.closeSearch').click(function () {
      $('.top-search').removeClass('show');
    });


    $(window).scroll(function () {
      if ($(this).scrollTop() > 300) {
        $('.back-top').show();
      } else {
        $('.back-top').hide();
      }
    });

    $('.back-top').on('click', function () {
      $('html,body').animate({
        scrollTop: 0
      }, 700);
    });

    //open menu
    $('.header-cart').click(function (e) {
      e.stopPropagation();
      $('body').addClass('open-menu');
    });

    $('.closeSlidebar').click(function () {
      $('body').removeClass('open-menu');
    });

    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      vertical: true,
      //- centerMode: true,
      focusOnSelect: true,
    });
    if ($('.zoom').length > 0) {
      $('.zoom').zoom();
    }

    var linkForgotPassword = $('a.forgot-password');
    if (linkForgotPassword.length) {
      // var loginForm = $('#login-form');
      // var forgotForm = $('#forgot-password-form')
      // linkForgotPassword.on('click', function(){
      //   loginForm.addClass('hidden');
      //   forgotForm.removeClass('hidden');
      // });
    }

    if (matchMedia('only screen and (max-width: 991px)').matches) {
      var el = document.getElementById("language-selector");
      if (el) {
        el.addEventListener("touchstart", function () {
          var btn = $(el);
          var menu = btn.next();
          menu.toggleClass('show');
          btn.parent().toggleClass('show');
        }, false);
      }
      // el.addEventListener("touchend", function(){
      // }, false);
      document.addEventListener('touchend', function (e) {
        if (!$(e.target).parents('#language-selector').length && !$(e.target).parents('.dropdown-menu').length) {
          var btn = $(el);
          var menu = btn.next();
          if (menu.hasClass('show')) {
            menu.removeClass('show');
            btn.parent().removeClass('show');
          }
        }
      });
    }
    if ($('.masonry').length) {
      var $masonry = $('.masonry').isotope({
        layoutMode: 'packery',
        itemSelector: '.article-news'
      })
    }
    if ($('.fillter-day input[name="date"]').length) {
      $('.fillter-day input[name="date"]').datepicker();
    }

    if ($('.checkout .nav-tabs').length) {
      if ($('.checkout .nav-tabs a.active').length) {
        $('.checkout .nav-tabs a.active')
        var tabContentId = $('.checkout .nav-tabs a.active').attr('href');
        $(tabContentId).addClass('active');
      }
      $('.checkout .nav-tabs a').on('show.bs.tab', function (e) {
        // e.target // newly activated tab
        // e.relatedTarget // previous active tab
        var tabContentId = $(e.target).attr('href');
        if (!$(tabContentId).hasClass('-complete')) {
          if (!$(tabContentId).hasClass('-current')) {
            return e.preventDefault()
          }
        }
      })
    }
    if ($('.dropdown').length) {
      $('.dropdown').unbind();
      $('.dropdown').on('mouseenter', function (e) {
        var cont = $(e.currentTarget);
        var menu = cont.find('.dropdown-menu');
        if (menu.length) {
          menu.css('display', 'block')
        }
      });
      $('.dropdown').on('mouseleave', function (e) {
        var cont = $(e.currentTarget);
        var menu = cont.find('.dropdown-menu');
        if (menu.length) {
          menu.css('display', 'none')
        }
      });

      $('.dropdown').on('click', function (e) {
        var cont = $(e.currentTarget);
        var menu = cont.find('.dropdown-menu');
        if (menu.length) {
          menu.toggle()
        }
      });
    }
  });


  // **********************************************************************//
  // ! Window load
  // **********************************************************************//
  $(window).on('load', function () {

  });

  // **********************************************************************//
  // ! Window resize
  // **********************************************************************//
  $(window).on('resize', function () {

  });

})(jQuery);

// **********************************************************************//
// Milestone
// **********************************************************************//
(function ($) {
  'use strict';

  var dot, line, wrapper, lastScrollTop,
    firstDotOffset, lastCircle, lastCircleOffset,
    breakpointDown, breakpointUp, contentBlock;

  var milestoneDot = function () {
    wrapper = document.getElementById('milestone-achievements');
    dot = document.getElementById('milestone-dot');
    line = document.getElementById('line');
    lastCircle = document.getElementById('last-circle');
    contentBlock = $(wrapper).find('.block--milestone');
    breakpointDown = $(window).height() / 3;
    breakpointUp = $(window).height() / 4;

    init();
    initScroll(wrapper);
  };

  function init() {
    // milestones = getMilestones();
    firstDotOffset = $(dot).offset();
    lastCircleOffset = $(lastCircle).offset();

    var activeBlockId = getActiveBlock();
    var $activeBlock = $(contentBlock[activeBlockId]);
    var top;

    $(contentBlock).removeClass('active');
    $activeBlock.addClass('active');

    // console.log('Active: ' + activeBlockId + ' position ' + $activeBlock.position().top);
    top = $activeBlock.position().top;

    if (top < 0) {
      top = 0;
    }
    dot.style.top = top + 'px';
    // console.log('dot top', dot.style.top);
    // dot.classList.add('animate');
  }

  function initScroll() {
    window.onscroll = function () {
      // console.log('scroll');

      if (!$(dot).hasClass('animate')) {
        dot.classList.add('animate');
      }

      var scrollTop = $(window).scrollTop();
      scrollHandle();

      lastScrollTop = scrollTop;
    };
  }

  function scrollHandle() {
    var scrollTop = $(window).scrollTop();
    var positionTop, $nextMile, top;
    var dotPosition = $(dot).position();
    var contentBlock = $(wrapper).find('.block--milestone:not(.list-hide)');

    if (dotPosition.top < 0) {
      // first milestone
      dot.style.top = 0;
    }

    $nextMile = getNextMile(scrollTop);
    var direction = getDirection();

    if ($nextMile.length && $nextMile.is(':visible')) {
      positionTop = $nextMile.offset().top - scrollTop;

      if ((direction === 'down' && positionTop <= breakpointDown) || (direction === 'up' && positionTop >= breakpointUp)) {
        $(contentBlock).removeClass('active');
        $nextMile.addClass('active');
        // console.log('moving-----');
        top = $nextMile.position().top;

        if (top < 0) {
          top = 0;
        }

        if (!isIE8()) {
          dot.style.top = top + 'px';
        } else {
          $(dot).animate({
            top: top + 'px'
          }, 1000, 'linear');
        }

        return;
      }
    }
    // console.log('NEXT', nextMile);
  }

  function getActiveBlock() {
    var scrollTop = $(window).scrollTop();
    var positionTop;
    var milestonesOffset = getOffsetMilestone();

    for (var i = 0; i < milestonesOffset.length; i++) {
      positionTop = milestonesOffset[i] - scrollTop;

      if (positionTop >= 0 && positionTop <= breakpointDown) {
        // console.log('founded ' + i);
        return i;
      }
    }

    return 0;

  }

  function getOffsetMilestone() {
    var milestonesBlock = $(contentBlock);
    var result = [];

    for (var i = 0; i < milestonesBlock.length; i++) {
      result.push($(milestonesBlock[i]).offset().top);
    }

    return result;
  }

  function getScrollDist() {
    var scrollTop = $(window).scrollTop();

    return scrollTop - lastScrollTop;
  }

  function getDirection() {
    var dist = getScrollDist();

    if (dist > 0 && Math.abs(dist) >= 1) {
      return 'down';
    }

    if (dist < 0 && Math.abs(dist) >= 1) {
      return 'up';
    }

    return '';
  }

  function getNextMile(/*top*/) {
    var direction = getDirection();

    var $currBlock = $('.block--milestone.active');
    var $nextBlock = $currBlock.next();
    var $prevBlock = $currBlock.prev();

    if (direction === 'up') {
      // up
      return $prevBlock;
    } else {
      // down
      return $nextBlock;
    }
  }

	/**
	 * check IE
	 */
  function isIE8() {
    return $('html').hasClass('lt-ie9');
  }

  $(document).ready(function () {
    var x = 0;
    if($("#hello .block--milestone").length){
      $("#hello .block--milestone").each(function () {
        x++;
        
        if (x == 1) {
          $(this).removeClass('block--milestone').addClass('block--milestone first active');
        }
  
        milestoneDot();
      });
    }
  })

})(jQuery);

$(function () {
  if($('#banner-ads-home').length > 0) {
    $('#banner-ads-home').firstVisitPopup({
            cookieName : 'homepage',
            showAgainSelector: '#show-message'
    });

  }
  if($('#day-frontend').length > 0) {
    $("#day-frontend").datepicker({
        dateFormat: 'dd/mm/yy'
    });
  }
});