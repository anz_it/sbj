
{extends file='page.tpl'}

{block name='page_content'}
  <div id="main-content">
        <!-- banner inner-->
        <section class="banner-inner">
          <div class="item-img" style="background-image:url({$urls.img_url}upload/banner-about-us.jpg)"></div>
        </section>
        <!-- e: banner inner-->
        <div class="container">
          <!-- about-us-->
          <section class="about-us section-general">
            <h2 class="title text-center">GIỚI THIỆU</h2>
            <div class="desc">
              <p>Established in 2011, BlueStone is India's leading destination for high quality fine jewellery with strikingly exquisite designs. We aim at revolutionizing the fine jewellery and lifestyle segment in India with a firm focus on craftsmanship, quality and customer experience. In a short span of time, BlueStone has built a large family of loyal consumers in India and abroad.</p>
              <p>We house more than 5000 unique designs for you to choose from. All these designs are crafted to perfection with utmost care giving you the flexibility to customize the product's gold purity and colour or diamond clarity to suit your needs.</p>
              <p>With an award-winning design team that pays great attention to detail, each of our products are a symbol of perfection. With cutting edge innovation and latest technology, we make sure the brilliance is well reflected in all our jewellery.</p>
              <p>We also offer a 30 Day Money Back guarantee, Certified Jewellery and Lifetime Exchange. You can also experience luxury shopping from the comfort of your home with our complimentary Try At Home service.</p>
            </div>
          </section>
          <!-- e: about-us-->
          <!-- development-process-->
          <section class="development-process section-general">
            <h2 class="title text-center">QUÁ TRÌNH PHÁT TRIỂN</h2>
            <div class="text-center"><img src="{$urls.img_url}upload/about-us.jpg" alt="QUÁ TRÌNH PHÁT TRIỂN"></div>
          </section>
          <!-- e: development-process-->
          <!-- core values-->
          <section class="core-values section-general">
            <h2 class="title text-center">GIÁ TRỊ CỐT LÕI</h2>
            <div class="row">
              <div class="col-lg-6 offset-lg-3">
                <div class="text-center">
                  <h3>TẦM NHÌN</h3>
                  <p>Sacombank-SBJ phát triển nhanh, hiệu quả nhưng an toàn và bền vững, hướng đến mục tiêu trở thành một trong những Công ty hàng đầu Việt Nam trong lĩnh vực kinh doanh vàng bạc đá quý.    </p>
                  <h3>SỨ MỆNH</h3>
                  <p>Sacombank-SBJ không ngừng phát triển nhằm tối ưu hóa nhu cầu của khách hàng bằng các sản phẩm, dịch vụ tốt nhất với chính sách chăm sóc khách hàng chuyên nghiệp; cam kết mang lại giá trị, sự thịnh vượng cho nhân viên; đồng thời đóng góp vào sự phát triển chung của xã hội, cộng đồng.</p>
                  <h3>GIÁ TRỊ CỐT LÕI</h3>
                  <p>
                    Cam kết đạt đến sự hoàn hảo<br>
                    Luôn luôn đổi mới<br>
                    Tinh thần trách nhiệm cao<br>
                    Chuyên nghiệp và đủ năng lực<br>
                    Tôn trọng giá trị đạo đức và nhân văn
                  </p>
                </div>
              </div>
            </div>
          </section>
          <!-- e: core values-->
          <!-- awards-->
          <section class="awards section-general">
            <h2 class="title text-center">HỆ THỐNG GIẢI THƯỞNG</h2>
            <div class="row">
              <div class="col-lg-4 col-sm-6">
                      <div class="tile-award">
                        <figure class="effect-1"><img src="{$urls.img_url}upload/award-1.jpg" alt="Thương hiệu mạnh Việt Nam 2009"></figure>
                        <h3>Thương hiệu mạnh Việt Nam 2009</h3>
                      </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                      <div class="tile-award">
                        <figure class="effect-1"><img src="{$urls.img_url}upload/award-2.jpg" alt="Thương hiệu mạnh Việt Nam 2009"></figure>
                        <h3>Thương hiệu mạnh Việt Nam 2009</h3>
                      </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                      <div class="tile-award">
                        <figure class="effect-1"><img src="{$urls.img_url}upload/award-3.jpg" alt="Thương hiệu mạnh Việt Nam 2009"></figure>
                        <h3>Thương hiệu mạnh Việt Nam 2009</h3>
                      </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                      <div class="tile-award">
                        <figure class="effect-1"><img src="{$urls.img_url}upload/award-1.jpg" alt="Thương hiệu mạnh Việt Nam 2009"></figure>
                        <h3>Thương hiệu mạnh Việt Nam 2009</h3>
                      </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                      <div class="tile-award">
                        <figure class="effect-1"><img src="{$urls.img_url}upload/award-2.jpg" alt="Thương hiệu mạnh Việt Nam 2009"></figure>
                        <h3>Thương hiệu mạnh Việt Nam 2009</h3>
                      </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                      <div class="tile-award">
                        <figure class="effect-1"><img src="{$urls.img_url}upload/award-3.jpg" alt="Thương hiệu mạnh Việt Nam 2009"></figure>
                        <h3>Thương hiệu mạnh Việt Nam 2009</h3>
                      </div>
              </div>
            </div>
          </section>
          <!-- e: awards-->
        </div>
      </div>
{/block}

{block name='right_column'}
  <div class="right-column col-12 col-md-3">
    {widget name="ps_contactinfo" hook='displayLeftColumn'}
  </div>
{/block}
