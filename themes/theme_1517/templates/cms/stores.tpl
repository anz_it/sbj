{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='notifications'} 
{/block}
{block name='page_content_container'}
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6-s3NiR3hILq17d9tlksN8qGfXPOCA3w&callback.js"></script>
  <section id="content" class="page-content page-stores">
    <div class="search-shop">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="list-shop">
              {block name='page_title'}
                <h2 class="title-2 text-center">{l s='Hệ thống cửa hàng' d='Shop.Theme.Global'}</h2>
              {/block} 
              <div class="accordion accordion-map" id="accordionMap">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                      <button class="btn btn-red btn-block" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      {l s='Ho Chi Minh City' d='Shop.Theme.Actions'}
                      </button>
                    </h5>
                  </div>
                  <div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionMap">
                    <div class="card-body">
                      <div class="list-shop-address mCustomScrollbar">
                        <ul class="store-addresses">
                          {foreach $stores as $store}
                          {if $store.address.city =='HCM' ||  $store.address.city =='hcm' }
                            <li>
                              <h4><a data-latitude="{$store.latitude}" data-longitude="{$store.longitude}" href="javascript:;">{$store.name}</a></h4>
                              <address><i class="fa fa-address-card"></i> {$store.address.address1}</address>
                              <ul class="card-block">
                                {if $store.phone}
                                  <li><i class="fa fa-phone"></i> {$store.phone}</li>
                                {/if}
                                {if $store.email}
                                  <li><i class="fa fa-envelope"></i> {$store.email}</li>
                                {/if}
                              </ul>
                              <div class="card-block card-note">
                                {if $store.note}
                                  {$store.note} 
                                {/if}
                              </div>
                            </li>
                          {/if}
                          {/foreach}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!--BEGIN HANOI-->
                  <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-red btn-block" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        {l s='Ha Noi City' d='Shop.Theme.Actions'}
                        </button>
                      </h5>
                    </div>
                    <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionMap">
                      <div class="card-body">
                        <div class="list-shop-address mCustomScrollbar">
                          <ul class="store-addresses">
                            {foreach $stores as $store}
                            {if $store.address.city =='HANOI' ||  $store.address.city =='hanoi' }
                              <li>
                                <h4><a data-latitude="{$store.latitude}" data-longitude="{$store.longitude}" href="javascript:;">{$store.name}</a></h4>
                                <address><i class="fa fa-address-card"></i> {$store.address.address1}</address>
                                <ul class="card-block">
                                  {if $store.phone}
                                    <li><i class="fa fa-phone"></i> {$store.phone}</li>
                                  {/if}
                                  {if $store.email}
                                    <li><i class="fa fa-envelope"></i> {$store.email}</li>
                                  {/if}
                                </ul>
                                <div class="card-block card-note">
                                  {if $store.note}
                                     {$store.note} 
                                  {/if}
                                </div>
                              </li>
                            {/if}
                            {/foreach}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                <!-- END HANOI -->

                <!--BEGIN OTHER-->
                  <div class="card">
                    <div class="card-header" id="headingOther">
                      <h5 class="mb-0">
                        <button class="btn btn-red btn-block" type="button" data-toggle="collapse" data-target="#collapseOther" aria-expanded="false" aria-controls="collapseOther">
                        {l s='Other City' d='Shop.Theme.Actions'}
                        </button>
                      </h5>
                    </div>
                    <div class="collapse" id="collapseOther" aria-labelledby="headingOther" data-parent="#accordionMap">
                      <div class="card-body">
                        <div class="list-shop-address mCustomScrollbar">
                          <ul class="store-addresses">
                            {foreach $stores as $store}
                            {if $store.address.city == 'OTHER' ||  $store.address.city =='other' }
                              <li>
                                <h4><a data-latitude="{$store.latitude}" data-longitude="{$store.longitude}" href="javascript:;">{$store.name}</a></h4>
                                <address><i class="fa fa-address-card"></i> {$store.address.address1}</address>
                                <ul class="card-block">
                                  {if $store.phone}
                                    <li><i class="fa fa-phone"></i> {$store.phone}</li>
                                  {/if}
                                  {if $store.email}
                                    <li><i class="fa fa-envelope"></i> {$store.email}</li>
                                  {/if}
                                </ul>
                                <div class="card-block card-note">
                                  {if $store.note}
                                     {$store.note} 
                                  {/if}
                                </div>
                              </li>
                            {/if}
                            {/foreach}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                <!-- END HANOI -->
              </div>
            </div>
          </div>
          <div class="col-lg-8">
            <!-- map item-->
            <div class="map-item">
              <div class="maps" id="map_canvas"></div>
              <script>
                /* ==================================================
                    initGMap
                ================================================== */
                window.initGMap = function(strLocation) {

                    if(strLocation == undefined){
                        strLocation = '10.76398,106.6953683';
                    }

                    var pos = strLocation.split(',');
                    var myLatLng = new google.maps.LatLng(pos[0], pos[1]);
                    var mapOptions = {
                        zoom: 14,
                        center: myLatLng,
                        scrollwheel: false,
                        streetViewControl: false,
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.DEFAULT
                        }
                        
                    };

                    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                    var image = '/themes/theme_1517/assets/img/marker.png';

                    var beachMarker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        icon: image
                    });
                };
                $(document).ready(function(){
                  var initTcpLink = function(){
                  var links = $('.store-addresses li a');
                  links.each(function(index, btn){
                        $(btn).unbind().bind('click', function(e){
                            e.preventDefault();
                            initGMap($(this).attr('data-latitude') +','+ $(this).attr('data-longitude'));
                        });
                    })
                  }
                  initGMap();
                  initTcpLink();
                });
              </script>
              <!-- e: map-->
            </div>
          </div>
        </div>
         
      </div>
    </div>
  </section>
{/block}
