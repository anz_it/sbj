{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_title'}
  {l s='Forgot your password?' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <div class="container">
    <form action="{$urls.pages.password}" class="forgotten-password" method="post">
      <ul class="ps-alert-error">
        {foreach $errors as $error}
          <li class="item">
            <p>{$error}</p>
          </li>
        {/foreach}
      </ul>
      <p class="text-center">{l s='Please enter the email address you used to register. You will receive a temporary link to reset your password.' d='Shop.Theme.Customeraccount'}</p>
      <div class="row">
        <div class="col-md-5 offset-md-3">
          <input placeholder="{l s='Email address *' d='Shop.Forms.Labels'}" type="email" name="email" id="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes}{/if}" class="form-control" required>
        </div>
        <div class="col-md-4">
          <button class="form-control-submit btn btn-primary" name="submit" type="submit">
            {l s='Send reset link' d='Shop.Theme.Actions'}
          </button>
        </div>
         
      </div>
      <div class="row">
        <div class="col-md-12 text-center pt-40">
          <a href="{$urls.pages.my_account}" class="btn btn-secondary">
            {l s='Back to login' d='Shop.Theme.Actions'}
          </a>
          <br/>
        </div>
      </div>
    </form>
  </div>
{/block}
