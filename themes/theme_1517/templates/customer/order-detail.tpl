{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_content'}
  <div class="container pt-40">
  <div class="row">
     <div class="col-12 col-lg-3">
        {include file='customer/_partials/left-account-links.tpl'}
      </div>
      <div class="col-12 col-lg-9">
        {block name='page_title'}
          <h4>{l s='Order details' d='Shop.Theme.Customeraccount'}</h4>
        {/block}
        <div class="account-form-wrapper">
          {block name='order_infos'}
            <div id="order-infos">
              <div class="row">
                <div class="col-{if $order.details.reorder_url}9{else}12{/if}">
                  <strong>
                    {l
                      s='Order Reference %reference% - placed on %date%'
                      d='Shop.Theme.Customeraccount'
                      sprintf=['%reference%' => $order.details.reference, '%date%' => $order.details.order_date]
                    }
                  </strong>
                </div>
                {if $order.details.reorder_url}
                  <div class="col-3 text-sm-right">
                    <a href="{$order.details.reorder_url}" class="button-primary">{l s='Reorder' d='Shop.Theme.Actions'}</a>
                  </div>
                {/if}
                <div class="clearfix"></div>
                </div>

                <ul>
                    <li><strong>{l s='Carrier' d='Shop.Theme.Checkout'}</strong> {$order.carrier.name}</li>
                    <li><strong>{l s='Payment method' d='Shop.Theme.Checkout'}</strong> {$order.details.payment}</li>

                    
                    {if $order.details.gift_message}
                      <li>{l s='You have requested gift wrapping for this order.' d='Shop.Theme.Customeraccount'}</li>
                      <li>{l s='Message' d='Shop.Theme.Customeraccount'} {$order.details.gift_message nofilter}</li>
                    {/if}
                  </ul>
            </div>
          {/block}

          {block name='addresses'}
            <div class="row">
              {if $order.addresses.delivery}
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <article id="delivery-address" class="box">
                    <strong class="text-uppercase">{l s='Delivery address' d='Shop.Theme.Checkout'}</strong>
                    <address>{$order.addresses.delivery.formatted nofilter}</address>
                  </article>
                </div>
              {/if}

              <div class="col-lg-6 col-md-6 col-sm-6">
                <article id="invoice-address" class="box">
                  <strong class="text-uppercase">{l s='Invoice address' d='Shop.Theme.Checkout'}</strong>
                  <address>{$order.addresses.invoice.formatted nofilter}</address>
                </article>
              </div>
              <div class="clearfix"></div>
            </div>
          {/block}

          {$HOOK_DISPLAYORDERDETAIL nofilter}

          {block name='order_detail'}
            {if $order.details.is_returnable}
              {include file='customer/_partials/order-detail-return.tpl'}
            {else}
              {include file='customer/_partials/order-detail-no-return.tpl'}
            {/if}
          {/block}
          <a id="history-link" href="{$urls.pages.history}">
            <strong><< {l s='Quay lại' d='Shop.Theme.Customeraccount'}</strong>
          </a>
        </div>
      </div>
</div>
{/block}
