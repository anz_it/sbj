{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_content'}
<div class="change-body">
  <div class="container pt-40">
    <div class="row">
     <div class="col-12 col-lg-3">
        {include file='customer/_partials/left-account-links.tpl'}
      </div>
      <div class="col-12 col-lg-9">
        {block name='page_title'}
            <h4>{l s='Your addresses' d='Shop.Theme.Customeraccount'}</h4>
        {/block}
        <div class="account-form-wrapper add-address pt-40">
          <a class="btn-link btn-link-primary" href="{$urls.pages.address}" data-link-action="add-address">
            <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
            <div>{l s='Create new address' d='Shop.Theme.Actions'}</div>
          </a>
        </div>
        
          <div class="">
            {foreach $customer.addresses as $address}
              {block name='customer_address'}
                <div class="account-form-wrapper" style="margin-bottom:20px">
                  {include file='customer/_partials/block-address.tpl' address=$address}
                </div>
              {/block}
            {/foreach}
          </div> 
      </div>
    </div>
  </div>
</div>
{/block}
