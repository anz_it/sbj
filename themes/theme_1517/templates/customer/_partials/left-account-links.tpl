<h2 class="headline-my-account">Tài khoản của tôi</h2>
<ul class="my-account-links">
  <li class="ma-link-item {if $smarty.get.controller =='identity'}active{/if}">
    <a id="identity-link" href="{$urls.pages.identity}">
      <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
      {l s='Information' d='Shop.Theme.Customeraccount'}
    </a>
  </li>
  <li class="ma-link-item {if $smarty.get.controller =='addresses' ||  $smarty.get.controller =='address'}active{/if}">
    {if $customer.addresses|count}
      <a id="addresses-link" href="{$urls.pages.addresses}">
        <i class="fa fa-address-card fa-2x" aria-hidden="true"></i>
        {l s='Addresses' d='Shop.Theme.Customeraccount'}
      </a>
    {else}
      <a id="address-link" href="{$urls.pages.address}">
        <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
        {l s='Add first address' d='Shop.Theme.Customeraccount'}
      </a>
    {/if}
  </li>

  {if !$configuration.is_catalog} 
    <li class="ma-link-item {if $smarty.get.controller =='history' || $smarty.get.controller =='orderdetail'}active{/if}">
      <a id="history-link" href="{$urls.pages.history}">
        <i class="fa fa-shopping-basket fa-2x" aria-hidden="true"></i>
        {l s='Order history and details' d='Shop.Theme.Customeraccount'}
      </a>
    </li>
  {/if}
  {if $configuration.voucher_enabled && !$configuration.is_catalog}
    <li class="ma-link-item">
      <a id="discounts-link" href="{$urls.pages.discount}">
        <i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
        {l s='Vouchers' d='Shop.Theme.Customeraccount'}
      </a>
    </li>
  {/if} 
</ul>