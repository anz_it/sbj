{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='customer_form'}
  {block name='customer_form_errors'}
    {include file='_partials/form-errors.tpl' errors=$errors['']}
  {/block}
  <div class="change-body">
    <div class="container">
      <div class="panel-information">
    <form action="{block name='customer_form_actionurl'}{$action}{/block}" id="customer-form" class="js-customer-form mx-auto" method="post">
      <section>
        {block "form_fields"}
          {foreach from=$formFields item="field"}
            {if $field.name != 'optin' && $field.name != 'newsletter'}
            {block "form_field"}
              {form_field field=$field}
            {/block}
            {/if}
          {/foreach}
          {$hook_create_account_form nofilter}
        {/block}
      </section>
      {block name='customer_form_footer'}
        <div class="form-footer clearfix">
          <input type="hidden" name="submitCreate" value="1">
          {block "form_buttons"}
            <button class="continue btn btn-red text-uppercase radius-0" data-link-action="save-customer" type="submit">
              {l s='Continue' d='Shop.Theme.Actions'}
            </button>
          {/block}
        </div>
      {/block}

    </form>
    </div>
  </div>
</div>
{/block}
