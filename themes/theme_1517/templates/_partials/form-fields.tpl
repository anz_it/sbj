{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 
{if $field.type == 'hidden'}

  {block name='form_field_item_hidden'}
    <input type="hidden" name="{$field.name}" value="{$field.value}">
  {/block}

{else}

  <div class="form-group {if !empty($field.errors)}has-error{/if}">
    {if $field.type !== 'checkbox'}
      <label class="form-control-label{if $field.required} required{/if}">
        {$field.label}
      </label>
    {/if}
    <div class="form-control-content">

      {if $field.type === 'select'}

        {block name='form_field_item_select'}
          <select class="custom-select" name="{$field.name}" {if $field.required}required{/if}>
            <option value disabled selected>{l s='-- please choose --' d='Shop.Forms.Labels'}</option>
            {foreach from=$field.availableValues item="label" key="value"}
              <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
            {/foreach}
          </select>
        {/block}
      {elseif $field.name === 'alias'}
        {block name='form_field_item_alias'}
          <div class="input-group">
            <input
              class="form-control"
              name="{$field.name}"
              type="text"
              value="{if $field.value == "" ||  $field.value == "Delivery Address"}Địa chỉ của bạn{/if}"
              {if $field.required}required{/if}
            >
          </div>
        {/block}
      {elseif $field.name === 'city'}
        {block name='form_field_item_city'}
          <select
          class="custom-select js-city"
          name="{$field.name}"
          {if $field.required}required{/if}
          >
            <option value disabled selected>{l s='-- please choose --' d='Shop.Forms.Labels'}</option>
            <option value="Hồ Chí Minh" {if 'Hồ Chí Minh' eq $field.value} selected {/if}>Hồ Chí Minh</option>
            <option value="Hà Nội" {if 'Hà Nội' eq $field.value} selected {/if}>Hà Nội</option>
            <option value="Đà Nẵng" {if 'Đà Nẵng' eq $field.value} selected {/if}>Đà Nẵng</option>
            <option value="An Giang" {if 'An Giang' eq $field.value} selected {/if}>An Giang</option>
            <option value="Bà Rịa - Vũng Tàu" {if 'Bà Rịa - Vũng Tàu' eq $field.value} selected {/if}>Bà Rịa - Vũng Tàu</option>
            <option value="Bắc Giang" {if 'Bắc Giang' eq $field.value} selected {/if}>Bắc Giang</option>
            <option value="Bắc Kạn" {if 'Bắc Kạn' eq $field.value} selected {/if}>Bắc Kạn</option>
            <option value="Bạc Liêu" {if 'Bạc Liêu' eq $field.value} selected {/if}>Bạc Liêu</option>
            <option value="Bắc Ninh" {if 'Bắc Ninh' eq $field.value} selected {/if}>Bắc Ninh</option>
            <option value="Bến Tre" {if 'Bến Tre' eq $field.value} selected {/if}>Bến Tre</option>
            <option value="Bình Dương" {if 'Bình Dương' eq $field.value} selected {/if}>Bình Dương</option>
            <option value="Bình Phước" {if 'Bình Phước' eq $field.value} selected {/if}>Bình Phước</option>
            <option value="Bình Thuận" {if 'Bình Thuận' eq $field.value} selected {/if}>Bình Thuận</option>
            <option value="Bình Định" {if 'Bình Định' eq $field.value} selected {/if}>Bình Định</option>
            <option value="Cà Mau" {if 'Cà Mau' eq $field.value} selected {/if}>Cà Mau</option>
            <option value="Cần Thơ" {if 'Cần Thơ' eq $field.value} selected {/if}>Cần Thơ</option>
            <option value="Cao Bằng" {if 'Cao Bằng' eq $field.value} selected {/if}>Cao Bằng</option>
            <option value="Đắk Lắk" {if 'Đắk Lắk' eq $field.value} selected {/if}>Đắk Lắk</option>
            <option value="Đắk Nông" {if 'Đắk Nông' eq $field.value} selected {/if}>Đắk Nông</option>
            <option value="Điện Biên" {if 'Điện Biên' eq $field.value} selected {/if}>Điện Biên</option>
            <option value="Đồng Nai" {if 'Đồng Nai' eq $field.value} selected {/if}>Đồng Nai</option>
            <option value="Đồng Tháp" {if 'Đồng Tháp' eq $field.value} selected {/if}>Đồng Tháp</option>
            <option value="Gia Lai" {if 'Gia Lai' eq $field.value} selected {/if}>Gia Lai</option>
            <option value="Hà Giang" {if 'Hà Giang' eq $field.value} selected {/if}>Hà Giang</option>
            <option value="Hà Nam" {if 'Hà Nam' eq $field.value} selected {/if}>Hà Nam</option>
            <option value="Hà Tĩnh" {if 'Hà Tĩnh' eq $field.value} selected {/if}>Hà Tĩnh</option>
            <option value="Hải Dương" {if 'Hải Dương' eq $field.value} selected {/if}>Hải Dương</option>
            <option value="Hải Phòng" {if 'Hải Phòng' eq $field.value} selected {/if}>Hải Phòng</option>
            <option value="Hậu Giang" {if 'Hậu Giang' eq $field.value} selected {/if}>Hậu Giang</option>
            <option value="Hoà Bình" {if 'Hoà Bình' eq $field.value} selected {/if}>Hoà Bình</option>
            <option value="Hưng Yên" {if 'Hưng Yên' eq $field.value} selected {/if}>Hưng Yên</option>
            <option value="Khánh Hòa" {if 'Khánh Hòa' eq $field.value} selected {/if}>Khánh Hòa</option>
            <option value="Kiên Giang" {if 'Kiên Giang' eq $field.value} selected {/if}>Kiên Giang</option>
            <option value="Kon Tum" {if 'Kon Tum' eq $field.value} selected {/if}>Kon Tum</option>
            <option value="Lai Châu" {if 'Lai Châu' eq $field.value} selected {/if}>Lai Châu</option>
            <option value="Lâm Đồng" {if 'Lâm Đồng' eq $field.value} selected {/if}>Lâm Đồng</option>
            <option value="Lạng Sơn" {if 'Lạng Sơn' eq $field.value} selected {/if}>Lạng Sơn</option>
            <option value="Lào Cai" {if 'Lào Cai' eq $field.value} selected {/if}>Lào Cai</option>
            <option value="Long An" {if 'Long An' eq $field.value} selected {/if}>Long An</option>
            <option value="Nam Định" {if 'Nam Định' eq $field.value} selected {/if}>Nam Định</option>
            <option value="Nghệ An" {if 'Nghệ An' eq $field.value} selected {/if}>Nghệ An</option>
            <option value="Ninh Bình" {if 'Ninh Bình' eq $field.value} selected {/if}>Ninh Bình</option>
            <option value="Ninh Thuận" {if 'Ninh Thuận' eq $field.value} selected {/if}>Ninh Thuận</option>
            <option value="Phú Thọ" {if 'Phú Thọ' eq $field.value} selected {/if}>Phú Thọ</option>
            <option value="Phú Yên" {if 'Phú Yên' eq $field.value} selected {/if}>Phú Yên</option>
            <option value="Quảng Bình" {if 'Quảng Bình' eq $field.value} selected {/if}>Quảng Bình</option>
            <option value="Quảng Nam" {if 'Quảng Nam' eq $field.value} selected {/if}>Quảng Nam</option>
            <option value="Quảng Ngãi" {if 'Quảng Ngãi' eq $field.value} selected {/if}>Quảng Ngãi</option>
            <option value="Quảng Ninh" {if 'Quảng Ninh' eq $field.value} selected {/if}>Quảng Ninh</option>
            <option value="Quảng Trị" {if 'Quảng Trị' eq $field.value} selected {/if}>Quảng Trị</option>
            <option value="Sóc Trăng" {if 'Sóc Trăng' eq $field.value} selected {/if}>Sóc Trăng</option>
            <option value="Sơn La" {if 'Sơn La' eq $field.value} selected {/if}>Sơn La</option>
            <option value="Tây Ninh" {if 'Tây Ninh' eq $field.value} selected {/if}>Tây Ninh</option>
            <option value="Thái Bình" {if 'Thái Bình' eq $field.value} selected {/if}>Thái Bình</option>
            <option value="Thái Nguyên" {if 'Thái Nguyên' eq $field.value} selected {/if}>Thái Nguyên</option>
            <option value="Thanh Hóa" {if 'Thanh Hóa' eq $field.value} selected {/if}>Thanh Hóa</option>
            <option value="Thừa Thiên Huế" {if 'Thừa Thiên Huế' eq $field.value} selected {/if}>Thừa Thiên Huế</option>
            <option value="Tiền Giang" {if 'Tiền Giang' eq $field.value} selected {/if}>Tiền Giang</option>
            <option value="Trà Vinh" {if 'Trà Vinh' eq $field.value} selected {/if}>Trà Vinh</option>
            <option value="Tuyên Quang" {if 'Tuyên Quang' eq $field.value} selected {/if}>Tuyên Quang</option>
            <option value="Vĩnh Long" {if 'Vĩnh Long' eq $field.value} selected {/if}>Vĩnh Long</option>
            <option value="Vĩnh Phúc" {if 'Vĩnh Phúc' eq $field.value} selected {/if}>Vĩnh Phúc</option>
            <option value="Yên Bái" {if 'Yên Bái' eq $field.value} selected {/if}>Yên Bái</option> 
          </select>
        {/block}

      {elseif $field.type === 'countrySelect'}

        {block name='form_field_item_country'}
          <select
          class="custom-select js-country"
          name="{$field.name}"
          {if $field.required}required{/if}
          >
            <option value disabled selected>{l s='-- please choose --' d='Shop.Forms.Labels'}</option>
            {foreach from=$field.availableValues item="label" key="value"}
              <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
            {/foreach}
          </select>
        {/block}

      {elseif $field.type === 'radio-buttons'}

        {block name='form_field_item_radio'}
          {foreach from=$field.availableValues item="label" key="value"}
            <div class="custom-control custom-radio {if $field.name == "id_gender"}custom-control-inline{/if}">
              <label>
                <input class="custom-control-input" name="{$field.name}" type="radio" value="{$value}"{if $field.required} required{/if}{if $value eq $field.value} checked{/if}>
                <span class="custom-control-label">{$label}</span>
              </label>
            </div>
          {/foreach}
        {/block}

      {elseif $field.type === 'checkbox'}

        {block name='form_field_item_checkbox'}
          <div class="custom-control custom-checkbox">
            <label>
              <input class="custom-control-input" name="{$field.name}" type="checkbox" value="1"{if $field.value} checked="checked"{/if}{if $field.required} required{/if}>
              <span class="custom-control-label">{$field.label nofilter}</span>
            </label>
          </div>
        {/block}

      {elseif $field.type === 'date'}

        {block name='form_field_item_date'}
          <input name="{$field.name}" class="form-control" type="date" value="{$field.value}" placeholder="{if isset($field.availableValues.placeholder)}{$field.availableValues.placeholder}{/if}">
          {if isset($field.availableValues.comment)}
            <span class="form-control-comment">
              {$field.availableValues.comment}
            </span>
          {/if}
        {/block}

      {elseif $field.type === 'birthday'}

        {block name='form_field_item_birthday'}
          <div class="js-parent-focus">
            {html_select_date
            field_order=DMY
            time={$field.value}
            field_array={$field.name}
            prefix=false
            reverse_years=true
            field_separator='<br>'
            day_extra='class="custom-select"'
            month_extra='class="custom-select"'
            year_extra='class="custom-select"'
            day_empty={l s='-- day --' d='Shop.Forms.Labels'}
            month_empty={l s='-- month --' d='Shop.Forms.Labels'}
            year_empty={l s='-- year --' d='Shop.Forms.Labels'}
            start_year={'Y'|date}-100 end_year={'Y'|date}
            }
          </div>
        {/block}

      {elseif $field.type === 'password'}

        {block name='form_field_item_password'}
          <div class="input-group">
            <input
              class="form-control js-visible-password"
              name="{$field.name}"
              type="password"
              value=""
              pattern=".{literal}{{/literal}5,{literal}}{/literal}"
              {if $field.required}required{/if}
            >
          </div>
        {/block}
      {elseif $field.type === 'textarea'}

        {block name='form_field_item_textarea'}
          <div class="input-group">
            <textarea
              class="form-control"
              name="{$field.name}" 
              {if $field.required}required{/if}
              rows="4" cols="50"
            >{$field.value}</textarea>
          </div>
        {/block}
      {else}

        {block name='form_field_item_other'}
          <input
            class="form-control"
            name="{$field.name}"
            type="{$field.type}"
            value="{$field.value}"
            {if isset($field.availableValues.placeholder)}placeholder="{$field.availableValues.placeholder}"{/if}
            {if $field.maxLength}maxlength="{$field.maxLength}"{/if}
            {if $field.required}required{/if}
          >
          {if isset($field.availableValues.comment)}
            <span class="form-control-comment">
              {$field.availableValues.comment}
            </span>
          {/if}
        {/block}

      {/if}

      {block name='form_field_errors'}
        {include file='_partials/form-errors.tpl' errors=$field.errors}
      {/block}

    </div>
  </div>

{/if}
