
<div class="pagging d-flex justify-content-center align-items-center">
  <nav aria-label="Page navigation example">
    {block name='pagination_page_list'}
     {if $pagination.should_be_displayed}
        <ul class="pagination">
          {foreach from=$pagination.pages item="page"}

            <li class="page-item" {if $page.current} class="page-active" {/if}>
              {if $page.type === 'spacer'}
                <span class="spacer">&hellip;</span>
              {else}
                <a
                  rel="{if $page.type === 'previous'}prev{elseif $page.type === 'next'}next{else}nofollow{/if}"
                  href="{$page.url}"
                  class="page-link {if $page.type === 'previous'}previous {elseif $page.type === 'next'}next {/if}{['disabled' => !$page.clickable, 'js-search-link' => true]|classnames}"
                >
                  {if $page.type === 'previous'}
                    <i class="fa fa-angle-left  mr-1" aria-hidden="true"></i>{l s='Previous' d='Shop.Theme.Actions'}
                  {elseif $page.type === 'next'}
                    {l s='Next' d='Shop.Theme.Actions'}<i class="fa fa-angle-right ml-1" aria-hidden="true"></i>
                  {else}
                    {$page.page}
                  {/if}
                </a>
              {/if}
            </li>
          {/foreach}
        </ul>
      {/if}
    {/block}  
  </nav>
  
</div>