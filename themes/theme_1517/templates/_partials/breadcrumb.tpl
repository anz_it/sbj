{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{if $breadcrumb.count > 1}
  <div class="container">
   <nav data-depth="{$breadcrumb.count}" class="{if $page.page_name == 'category'} my-4 {/if} mb-4 ">
    <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
      {assign var=tmpIdx value=0}
      {foreach from=$breadcrumb.links item=path name=breadcrumb}
        {block name='breadcrumb_item'}
          <li data-idx="{$tmpIdx}" class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            {if !$smarty.foreach.breadcrumb.last}
              {if $tmpIdx > 1}
                <a itemprop="item" href="{$path.url}?selected_filters=xem-chi-tiet"><span itemprop="name">{$path.title}</span></a>
              {else}
                <a itemprop="item" href="{$path.url}"><span itemprop="name">{$path.title}</span></a>
              {/if}
            {else}
              <span itemprop="item"><span itemprop="name">{$path.title}</span></span>
            {/if}
            <meta itemprop="position" content="{$smarty.foreach.breadcrumb.iteration}">
          </li>
        {/block}
        {assign var=tmpIdx value=$tmpIdx+1}
      {/foreach}
    </ol>
   </nav>
  </div>
{/if}
