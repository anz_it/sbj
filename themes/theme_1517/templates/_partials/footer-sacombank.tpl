<!-- footer-->
<footer>
  <div class="footer-top">
    <div class="container">
      <div class="row">
        {block name='hook_footer'}
          {hook h='displayFooter'}
        {/block}
         <div class="col-lg-3">
            <div class="footer-box">
              <h3>{l s='Contact Info' d='Admin.Global'}</h3>
              <div class="footer-contact-info text-white">
                <p><strong>{l s='Saigon Thuong Tin Jewelry Co., Ltd' d='Admin.Global'}</strong></p>
                <p>Điện thoại: (+84.28)3 9329 001</p>
                <p>Fax: (+84.28)3 9329 002</p>
                <p>Hotline: 090 954 3466 - 090 362 6242</p>
                <p>Email: <a href="mailto:cskhsbj@sacombank-sbj.com">cskhsbj@sacombank-sbj.com</a></p>
                <p>{l s='Address: 278 Nam Ky Khoi Nghia, Ward 8, D.3, Dist. Ho Chi Minh City, VN' d='Admin.Global'}</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-3">
            <div class="footer-box">
              <h3><a href="#">{l s='Payment methods' d='Admin.Global'}</a></h3>
              <ul class="list-inline">
                <li class="list-inline-item"><img src="{$urls.img_url}upload/money-bill.png" alt="Image"></li>
                <li class="list-inline-item"><img src="{$urls.img_url}upload/visa.png" alt="Image"></li>
                <li class="list-inline-item"><img src="{$urls.img_url}upload/mastercard.png" alt="Image"></li>
                <li class="list-inline-item"><img src="{$urls.img_url}upload/jcb.png" alt="Image"></li>
              </ul>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- e: footer-top-->
  <div class="footer-bottom">
    <div class="container">
      <div class="footer-bottom-wrapper">
        <div class="copyright">Copyright &copy; 2017 SBJ - Design &amp; Development by ZiniMedia Ltd. All rights reserved.</div>
      </div>
    </div>
  </div>
</footer>
<!-- e: footer-->
<!-- ManyChat -->
<script src="//widget.manychat.com/406253999406613.js" async="async">
</script>

<div class="toolbox">
  <ul>
    <li class="clickable back-top"><a href="javascript:;" title="Lên đầu trang"><i class="fa fa-arrow-up"></i></a></li>
    <li><a target="_blank" href="https://www.facebook.com/trangsuc.sbj"><i class="fa fa-facebook"></i></a></li> 
    <li><a target="_blank" href="https://www.youtube.com/channel/UChCMoE1Uuix7Q4mnarTa52w"><i class="fa fa-youtube"></i></a></li> 
    <li><a target="_blank" href="https://zalo.me/3390043933752112470"><img src="/img/zalo.png"/></a></li> 
  </ul>
</div> 