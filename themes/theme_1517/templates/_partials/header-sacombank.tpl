{hook h='displaySearch'}

<div class="header-top">
  <div class="container">
    <div class="top d-flex flex-wrap justify-content-end justify-content-md-between">
      <ul class="nav-left d-none d-md-flex align-items-center">
        <li>
          {hook h='displayNav1'}
        </li>
        <li><a class="find-location" href="{$urls.pages.stores}">
          <i class="fa fa-map-marker"></i><span>{l s='Hệ thống cửa hàng' d='Shop.Theme.Actions'}</span></a>
          </li>
      </ul>
      <ul class="nav-right d-flex align-items-center">
        {hook h='displayNav2'}
        <li class="d-none d-lg-block">
          <div class="header-search">
            <form method="get" action="{$search_controller_url}">
              <input type="hidden" name="controller" value="search">
              <input  class="form-control" placeholder="Tìm kiếm" type="text" name="s" value="{$search_string}"/>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- header middle-->
<div class="header-middle">
  <div class="container">
    <div class="d-flex justify-content-center align-items-center">
      <ul class="brand d-flex brand-left">
        <li><a href="/23-trang-suc-sbj"><img src="{$urls.img_url}logo-small-2.png" width="71" height="40"></a></li>
        <li><a href="/22-qua-tang-cao-cap"><img src="{$urls.img_url}logo-2.png" width="77" height="40"></a></li>
      </ul>
      <ul class="brand d-flex brand-center">
        <li><a href="{$urls.base_url}"><img src="{$urls.img_url}logo@2x.png" width="315" height="54"></a></li>
      </ul>
      <ul class="brand d-flex brand-right">
        <li><a href="/trung-tam-giam-dinh"><img src="{$urls.img_url}logo-3.png" width="56" height="56"></a></li>
        <li><a href="javascript:void(0)" target="_blank"><img src="{$urls.img_url}logo-4.png" width="56" height="56"></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="header-middle-mobile">
  <ul>
    <li><a href="/23-trang-suc-sbj"><img src="{$urls.img_url}logo-small-2.png" width="71" height="40"></a></li>
    <li><a href="/22-qua-tang-cao-cap"><img src="{$urls.img_url}logo-2.png" width="77" height="40"></a></li>
    <li><a href="/trung-tam-giam-dinh"><img src="{$urls.img_url}logo-3.png" width="56" height="56"></a></li>
    <li><a href="javascript:void(0)" target="_blank"><img src="{$urls.img_url}logo-4.png" width="56" height="56"></a></li>
  </ul>
</div>
<!-- header main-->
<header class="header header-sticky" id="header">
  <div class="header-wrap">
    <div class="header-nav">
      <div class="container d-flex d-lg-block justify-content-between align-items-center">
        <div class="header-mainnav">
          <button class="collapsed navbar-toogle" type="button" data-toggle="collapse" data-target="#main-nav" aria-expanded="true"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          {hook h='displayTop'}    
        </div>
        <div class="header-brand"><a class="logo" href="/"><img class="img-fluid" src="{$urls.img_url}logo@2x.png" alt="Image"></a></div>
        <div class="header-right d-lg-none">
          <div class="d-flex align-items-center">
            <div class="header-search">
            <a class="header-search-icon icon-wrapper clickable" data-toggle="modal" data-target="#searchModal" href="javascript:void(0)"><i class="fa fa-search"></i></a>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- e: header-->