
<!-- news and price gold-->
<div class="news-pricegold">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mb-5 mb-lg-0">
        <!-- news-->
        <div class="news">
          <h2 class="title text-center">TIN TỨC VÀ KHUYẾN MÃI</h2>
          <ul class="list-unstyled">
            <li class="tile-news-home">
              <h3><a href="#">Khuyến mãi tháng 05.2018 - CƠ HỘI VÀNG – BÁN HÀNG GIÁ VỐN</a></h3>
              <p class="time">14/05/2018</p>
              <div class="desc">Nhằm tri ân đến những khách hàng thân thiết đã luôn đồng hành cùng Sacombank trong thời gian qua và giới thiệu đến quý khách hàng những bộ sưu tập trang sức,</div>
            </li>
            <li class="tile-news-home">
              <h3><a href="#">Chương trình “Cơ Hội Vàng - Bán Hàng Giá Vốn – Tháng 04.2018” - SACOMBANK CN SÓC TRĂNG & PGD VĨNH CHÂU</a></h3>
              <p class="time">16/04/2018</p>
              <div class="desc">Dưới bàn tay khéo léo của những người thợ nhiều năm kinh nghiệm, Sacombank-SBJ không ngừng sáng tạo và cho ra đời những thiết kế trang sức tuyệt đẹp, những</div>
            </li>
            <li class="tile-news-home">
              <h3><a href="#">Khuyến mãi tháng 05.2018 - CƠ HỘI VÀNG – BÁN HÀNG GIÁ VỐN” - SACOMBANK CN SÓC TRĂNG & PGD VĨNH CHÂU</a></h3>
              <p class="time">16/04/2018</p>
              <div class="desc">Nhằm tri ân đến những khách hàng thân thiết đã luôn đồng hành cùng Sacombank trong thời gian qua và giới thiệu đến quý khách hàng những bộ sưu tập trang sức, quà tặng cao cấp mới nhất, Sacombank-SBJ thực hiện chương trình “Cơ Hội Vàng</div>
            </li>
          </ul>
          <div class="text-center"><a class="btn btn-red btn-lg text-uppercase" href="#">{l s='View more' d='Shop.Theme.Actions'}</a></div>
        </div>
        <!-- e: news-->
      </div>
      <div class="col-lg-6">
        <!-- price-gold-->
        <div class="price-gold">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th class="text-center" colspan="3">GIÁ VÀNG SBJ</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td class="text-center" colspan="3">
                <p>09:43:40 AM 16/05/2018</p>
                <p>(Đơn vị: ngàn đồng/chỉ)</p>
              </td>
            </tr>
            <tr>
              <td class="text-center">LOẠI VÀNG</td>
              <td class="text-center">MUA VÀO</td>
              <td class="text-center">BÁN RA</td>
            </tr>
            <tr>
              <td>Vàng SJC</td>
              <td>3.656.000</td>
              <td>3.677.00</td>
            </tr>
            <tr>
              <td>Vàng 24K ép vỉ</td>
              <td>3.593.000</td>
              <td>3.673.000</td>
            </tr>
            <tr>
              <td>Vàng 24K</td>
              <td>3.580.000</td>
              <td>3.660.000</td>
            </tr>
            <tr>
              <td>Bóng đổi 99.99%</td>
              <td>0</td>
              <td>3.632.000</td>
            </tr>
            <tr>
              <td>Vàng 18K (75%)</td>
              <td>2.605.000</td>
              <td>2.770.000</td>
            </tr>
            <tr>
              <td>Vàng 14K (58.3%)</td>
              <td>2.017.000 </td>
              <td>2.159.000</td>
            </tr>
            <tr>
              <td>Vàng 10K (41.7%)</td>
              <td>1.398.000</td>
              <td>1.541.000</td>
            </tr>
            <tr>
              <td>Vàng (68%)</td>
              <td>2.339.000</td>
              <td>2.519.000</td>
            </tr>
            <tr>
              <td colspan="3"><a class="btn btn-block btn-red text-uppercase" href="#">Xem chi tiết</a></td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- e: price-gold-->
      </div>
    </div>
  </div>
</div>
<!-- e: news and price gold-->