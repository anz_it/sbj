<!DOCTYPE html>
<html lang="{$language.iso_code}">
<head> 
    <!-- Place favicon.ico in the root directory-->
    <script src="{$urls.js_url}jquery.min.js"></script>
    <script src="{$urls.js_url}vendor/modernizr.js"></script>
    {block name='head'}
        {include file='_partials/head.tpl'}
    {/block}
    <link rel="stylesheet" href="{$urls.css_url}font-awesome.min.css">
    <link rel="stylesheet" href="{$urls.css_url}jquery-ui.min.css">
    <link rel="stylesheet" href="{$urls.css_url}jquery.mCustomScrollbar.css">
    <!-- Google Tag Manager -->
    <script>{literal}(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PW76ZGP');{/literal}</script>
    <!-- End Google Tag Manager -->
</head>
<body id="{$page.page_name}" class="{$page.body_classes|classnames}">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PW76ZGP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="wrapper" id="wrapper">
    {block name='product_activation'}
        {include file='catalog/_partials/product-activation.tpl'}
    {/block}
    {include file='_partials/header-sacombank.tpl'}
    {block name='notifications'}
        {include file='_partials/notifications.tpl'}
    {/block}
    <div id="main-content">
        {block name='content_wrapper'}
            {block name="content"}

            {/block}
        {/block}
        {include file='_partials/footer-sacombank.tpl'}
    </div>

    {block name='javascript_bottom'}
        {*{include file="_partials/javascript.tpl" javascript=$javascript.bottom}*}
    {/block}
    <!-- script -->
    <script type="text/javascript" src="/themes/core.js" ></script>
    <script src="{$urls.js_url}vendor/popper.js"></script>
    <script src="{$urls.js_url}vendor/bootstrap.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.dotdotdot.js"></script>
    <script src="{$urls.js_url}vendor/jquery.etalage.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.zoom.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.sticky.js"></script>
    <script src="{$urls.js_url}vendor/slick.min.js"></script>
    <script src="{$urls.js_url}jquery-ui.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.mCustomScrollbar.js"></script>
    <script src="{$urls.js_url}vendor/isotope.pkgd.min.js"></script>
    <script src="{$urls.js_url}vendor/packery-mode.pkgd.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.firstVisitPopup.min.js"></script>
    <script src="{$urls.js_url}main.js"></script>
</body>