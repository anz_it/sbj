<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    {block name='head_seo'}
      <title>{block name='head_seo_title'}{$page.meta.title}{/block}</title>
      <meta name="description" content="{block name='head_seo_description'}{$page.meta.description}{/block}">
      <meta name="keywords" content="{block name='head_seo_keywords'}{$page.meta.keywords}{/block}">
      {if $page.meta.robots !== 'index'}
        <meta name="robots" content="{$page.meta.robots}">
      {/if}
      {if $page.canonical}
        <link rel="canonical" href="{$page.canonical}">
      {/if}
    {/block}
    <link rel="apple-touch-icon" href="{$urls.img_url}apple-touch-icon.png">
    {block name='head'}
        {*{include file='_partials/head.tpl'}*}
    {/block}
    <!-- Place favicon.ico in the root directory-->
    <link rel="stylesheet" href="{$urls.css_url}bootstrap.min.css">
    <link rel="stylesheet" href="{$urls.css_url}font-awesome.min.css">
    <link rel="stylesheet" href="{$urls.css_url}slick.css">
    <link rel="stylesheet" href="{$urls.css_url}main.css">
    <script src="{$urls.js_url}jquery.min.js"></script>
    <script src="{$urls.js_url}vendor/modernizr.js"></script>

</head>
<body>
    
    <div class="wrapper" id="wrapper">
    {block name='product_activation'}
        {include file='catalog/_partials/product-activation.tpl'}
    {/block}
    {include file='_partials/header-sacombank.tpl'}
    {block name='notifications'}
        {include file='_partials/notifications.tpl'}
    {/block}
    
    <div id="main-content">
        <div class="">
        {block name='content_wrapper'}
            {hook h="displayContentWrapperTop"}
            {block name="content"}

            {/block}
            {hook h="displayContentWrapperBottom"}
        {/block}
        </div>
    </div>
    {include file='_partials/footer-sacombank.tpl'}    
        
    {block name='javascript_bottom'}
        {*{include file="_partials/javascript.tpl" javascript=$javascript.bottom}*}
    {/block}
    <!-- script -->
    <script src="{$urls.js_url}vendor/popper.js"></script>
    <script src="{$urls.js_url}vendor/bootstrap.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.dotdotdot.js"></script>
    <script src="{$urls.js_url}vendor/jquery.etalage.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.sticky.js"></script>
    <script src="{$urls.js_url}vendor/slick.min.js"></script>

    <script src="{$urls.js_url}main.js"></script>
</body>
