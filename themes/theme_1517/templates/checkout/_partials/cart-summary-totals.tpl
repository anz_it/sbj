{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="cart-subtotals">
        {foreach from=$cart.subtotals item="subtotal"}
            {if isset($subtotal) && $subtotal}
                <div class="cart-{$subtotal.type} d-flex flex-wrap justify-content-between">
                    <span class="label">{$subtotal.label}</span>
                    <span class="value">{$subtotal.value|replace:',':'.'}</span>
                    {if $subtotal.type == 'discount'}
                        {if $cart.vouchers.added}
                            <ul class="list-group mb-2 w-100">
                                {foreach from=$cart.vouchers.added item='voucher'}
                                    <li class="list-group-item d-flex flex-wrap justify-content-between">
                                        <span>{$voucher.name}({$voucher.reduction_formatted})</span><a
                                                data-link-action="remove-voucher" href="{$voucher.delete_url}"
                                                class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </li>
                                {/foreach}
                            </ul>
                        {/if}
                    {/if}
                </div>
            {/if}
        {/foreach}
    </div>
<div class="card-block cart-summary-totals">

  {block name='cart_summary_total'}
    <div class="cart-summary-line cart-total">
      <strong class="label">{$cart.totals.total.label}</strong>
      <strong class="value">{$cart.totals.total.value|replace:',':'.'}</strong>
    </div>
  {/block}

  {block name='cart_summary_tax'}
    <div class="cart-summary-line">
      <span class="label sub">{$cart.subtotals.tax.label}</span>
      <span class="value sub">{$cart.subtotals.tax.value}</span>
    </div>
  {/block}

</div>
