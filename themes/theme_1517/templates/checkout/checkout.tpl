{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!doctype html>
<html lang="{$language.iso_code}">

  <head>
    {block name='head'}
      {include file='_partials/head.tpl'}
    {/block}
  </head>
  <!-- Place favicon.ico in the root directory-->
  <link rel="stylesheet" href="{$urls.css_url}bootstrap.min.css">
  <link rel="stylesheet" href="{$urls.css_url}font-awesome.min.css">
  <link rel="stylesheet" href="{$urls.css_url}slick.css">
  <link rel="stylesheet" href="{$urls.css_url}jquery-ui.min.css">
  <link rel="stylesheet" href="{$urls.css_url}jquery.mCustomScrollbar.css"> 
  <link rel="stylesheet" href="{$urls.css_url}main.css">
  <script src="{$urls.js_url}jquery.min.js"></script>
  <script src="{$urls.js_url}vendor/modernizr.js"></script>
  <body id="{$page.page_name}" class="{$page.body_classes|classnames}">
 
    {include file='_partials/header-sacombank.tpl'}
    {block name='notifications'}
      {include file='_partials/notifications.tpl'}
    {/block}

    <section id="wrapper">
      {hook h="displayWrapperTop"}
      <div class="container">

      {block name='content'}
        <section id="content">
          <div class="row">
            <div class="col-md-6 offset-md-1">
              <div class="checkout">
                {block name='cart_summary'}
                  {render file='checkout/checkout-process.tpl' ui=$checkout_process}
                {/block}
              </div>
            </div>
            <div class="col-md-4">
              <div class="checkout">
              {block name='cart_summary'}
                {include file='checkout/_partials/cart-summary.tpl' cart = $cart}
              {/block} 
              </div>
            </div>
          </div>
        </section>
      {/block}
      </div>
      {hook h="displayWrapperBottom"}
    </section>

    {include file='_partials/footer-sacombank.tpl'}

    {block name='javascript_bottom'}
      {*{include file="_partials/javascript.tpl" javascript=$javascript.bottom}*}
    {/block}

    {block name='hook_before_body_closing_tag'}
      {hook h='displayBeforeBodyClosingTag'}
    {/block}
    <!-- script -->
    <script type="text/javascript" src="/themes/core.js" ></script>
    <script src="{$urls.js_url}vendor/popper.js"></script>
    <script src="{$urls.js_url}vendor/bootstrap.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.dotdotdot.js"></script>
    <script src="{$urls.js_url}vendor/jquery.etalage.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.zoom.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.sticky.js"></script>
    <script src="{$urls.js_url}vendor/slick.min.js"></script>
    <script src="{$urls.js_url}jquery-ui.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{$urls.js_url}vendor/jquery.mCustomScrollbar.js"></script>
    <script src="{$urls.js_url}main.js"></script>
  </body>

</html>
