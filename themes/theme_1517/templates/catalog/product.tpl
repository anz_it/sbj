{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='head_seo' prepend}
    <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
    <meta property="og:type" content="product">
    <meta property="og:url" content="{$urls.current_url}">
    <meta property="og:title" content="{$page.meta.title}">
    <meta property="og:site_name" content="{$shop.name}">
    <meta property="og:description" content="{$page.meta.description}">
    <meta property="og:image" content="{$product.cover.large.url}">
    <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
    <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
    <meta property="product:price:amount" content="{$product.price_amount}">
    <meta property="product:price:currency" content="{$currency.iso_code}">
    {if isset($product.weight) && ($product.weight != 0)}
        <meta property="product:weight:value" content="{$product.weight}">
        <meta property="product:weight:units" content="{$product.weight_unit}">
    {/if}
{/block}

{block name='content'}
    <!-- breadcrumb-->
    <div class="breadcrumb-site">
        <div class="container">
            {include file='_partials/breadcrumb.tpl'}
        </div>
    </div>
    <!-- e: breadcrumb-->
    <div class="container">
        <!-- .product-view-->
        <div class="product-view">
            <div class="product-essential">
                {include file='catalog/_partials/product-images-sacombank.tpl'}
            </div>
            <div class="product-summary">
                <!-- product-shop-->
                {include file='catalog/_partials/product-details-sacombank.tpl'}
                <!-- e: product-shop-->
            </div>
        </div>
        <div class="product-description">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                                Mô tả
                            </a>
                        </li>
                        {hook h='displayProductCustomTab'}
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane fade in custom-tab active show">
                             <div class="long-desc"> 
                                {$product.description nofilter}
                            </div>
                        </div>
                        {hook h='displayProductCustomTabContent'}
                    </div>
                </div>
            </div>
        </div>
        <!-- e: .product-view-->
    </div>

    <!-- .relate-product-->
    <div class="relate-product bg-light">
        {block name='product_accessories'}
            {if $accessories}
                {block name='product_miniature'}
                    {include file='catalog/_partials/product-relate-sacombank.tpl' products=$accessories}
                {/block}
            {/if}
        {/block}
    </div>

    <!-- e: .relate-product-->
{/block}
