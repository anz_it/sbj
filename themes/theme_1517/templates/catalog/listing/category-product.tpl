
{extends file=$layout}

{block name='breadcrumb'}
  {include file='_partials/breadcrumb.tpl'}
  {block name='product_list_header'}
    <h2 class="section-title">{$listing.label}</h2>
  {/block}
{/block}
{block name='content'}
  <section id="main">
      <!-- banner inner-->
      <div class="banner-inner">
          <div class="item"><img src="{$category.image.large.url}" alt="Image"></div>
      </div>
      <!-- e: banner inner--> 
      <!-- featuredProducts -->
        <div class="new-product bg-light space-global">
            <div class="container">
                <h2 class="title text-center">{l s='Featured product' d='Shop.Theme.Catalog'}</h2>
                <div class="slide-products arrows-red">
                {foreach from=$featuredProducts item="product"} 
                    <div class="item">
                    <div class="tile-product">
                    {foreach from=$product.flags item=flag}
                        {if $flag.type == 'on-sale'}
                        {if $product.has_discount && $product.discount_type === 'percentage'}
                            <div class="sale lbl-pro">{$product.discount_percentage}</div>
                        {/if}
                        {/if}
                    {/foreach}
                    <div class="img effect-1"><a href="{$link->getProductLink($product.id_product, $product.link_rewrite, null,  $product.id_product_attribute, null, null)|escape:'html':'UTF-8'}">
                        <img
                                src = "{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
                                alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                data-full-size-image-url = "{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
                        >
                        </a></div>
                    <div class="text">
                        <h3 class="dotdotdot">
                            <a href="{$link->getProductLink($product.id_product, $product.link_rewrite, null,  $product.id_product_attribute, null, null)|escape:'html':'UTF-8'}">{$product.name|truncate:30:'...'}</a>
                        </h3>
                        <div class="price"> 
                        {if $product.show_price}
                            {if $product.has_discount}
                                <div class="old-price">{$product.regular_price|intval|number_format:0|replace:',':'.'}</div>
                            {/if} 
                            <div class="new-price">{$product.price|intval|number_format:0|replace:',':'.'}</div>
                        {/if}
                        </div>
                        {if $product.show_price}
                        {if $product.add_to_cart_url && !$configuration.is_catalog && ({$product.minimal_quantity} < {$product.quantity})}
                            <a class="btn btn-red btn-sm text-uppercase" href="{$product.add_to_cart_url}" rel="nofollow" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" data-link-action="add-to-cart">
                            {l s='Add to cart' d='Shop.Theme.Actions'}</a>
                        {else}
                            <a href="{$link->getProductLink($product.id_product, $product.link_rewrite, null,  $product.id_product_attribute, null, null)|escape:'html':'UTF-8'}" class="btn btn-red btn-sm text-uppercase">
                                {l s='View product' d='Shop.Theme.Actions'}
                            </a>
                        {/if}
                        {else}
                        <a class="btn btn-red btn-sm text-uppercase" href="{$urls.pages.contact}" rel="nofollow">
                            {l s='Contact' d='Shop.Theme.Actions'}</a>
                        {/if}
                    </div>
                    </div>
                </div>
                {/foreach}
                </div>
            </div>
        </div>
        <!-- e: new product-->
      {if $total_sub_category == 0}
          {hook h='displayCategoryCustom'}
      {/if}

  </section>
    {block name='subCategory'}{/block}
{/block}
