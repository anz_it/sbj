{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='catalog/listing/category-product.tpl'}


{block name="subCategory"}
    {foreach from=$category_products key=k item=category}
        <!-- category box sbj-->
        <div class="category-box-sbj {if $k % 2 ==1}bg-light{/if} space-global">
            {if $category.products|count}
                <div class="container">
                    <h2 class="title text-center">{$category.category->name}</h2>
                    <div class="row">
                        <div class="col-lg-6">
                            <a class="img" href="{$link->getCategoryLink($category.category->id)|escape:'html':'UTF-8'}?selected_filters=xem-chi-tiet">
                                <img src="{$link->getCatImageLink($category.category->link_rewrite, $category.category->id, '580_default')}">
                            </a>
                        </div>
                        {foreach from=$category.products key=i item='product'}
                            {if $i > 5}
                                {break}
                            {/if}
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                    <div class="tile-product">
                                        <div class="img effect-1"><a href="{$link->getProductLink($product.id_product, $product.link_rewrite, null,  $product.id_product_attribute, null, null)|escape:'html':'UTF-8'}">
                                                <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, '250x250')}" alt="{$product.name}"></a></div>
                                        <div class="text">
                                            <h3 class="dotdotdot">
                                                <a href="{$link->getProductLink($product.id_product, $product.link_rewrite, null,  $product.id_product_attribute, null, null)|escape:'html':'UTF-8'}">{$product.name}</a>
                                            </h3>
                                            <div class="price">
                                                {if $product.show_price}
                                                    {if $product.reduction} 
                                                        <div class="old-price">{$product.price_without_reduction|number_format:0:",":"."} {$currency.iso_code}</div> 
                                                    {/if}
                                                    {if $product.show_price}
                                                    <div class="normal-price">{$product.price|number_format:0:",":"."} {$currency.iso_code}</div>
                                                    {/if}
                                                    {hook h='displayProductPriceBlock' product=$product type='unit_price'}
                                                {/if}
                                            </div>
                                            {if $product.show_price}
                                                {if $link->getProductLink($product.id_product,$product.id_product_attribute) && !$configuration.is_catalog && ({$product.minimal_quantity} < {$product.quantity})}
                                                    <a href="{$link->getAddToCartURL($product.id_product,$product.id_product_attribute)}" class="btn btn-red btn-sm text-uppercase">{l s='Add to cart' d='Shop.Theme.Actions'}</a>
                                                {else}
                                                    <a href="{$link->getProductLink($product.id_product, $product.link_rewrite, null,  $product.id_product_attribute, null, null)|escape:'html':'UTF-8'}" class="btn btn-red btn-sm text-uppercase">
                                                        XEM SẢN PHẨM
                                                    </a>
                                                {/if}
                                            {else}
                                                <a class="btn btn-red btn-sm text-uppercase" href="{$urls.pages.contact}" rel="nofollow">
                                                {l s='Contact' d='Shop.Theme.Actions'}</a>
                                            {/if}
                                        </div>
                                    </div>
                                </div>
                        {/foreach}
                    </div>
                    <div class="text-center view-all">
                        <a class="btn btn-outline-red text-uppercase min-132 rounded-0" href="{$link->getCategoryLink($category.category->id_category, $category.category->link_rewrite)|escape:'html':'UTF-8'}?selected_filters=xem-chi-tiet">{l s='Xem thêm' d='Shop.Theme.Actions'}</a>
                        </div>
                </div>
            {/if}
        </div>
        <!-- e: category box sbj-->
    {/foreach}
{/block}



