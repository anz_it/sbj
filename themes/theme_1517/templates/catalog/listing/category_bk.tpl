{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{extends file='catalog/listing/product-list.tpl'}


{block name='product_list_header'}
  <div class="block-category mb-5" {if $category.image.large.url} style="background-image:url({$category.image.large.url})"{/if}>
    <div class="container">
      <div class="cat_desc">
        {if $category.description}
          <div id="category-description">
            <h2 class="section-title text-left">{$category.name}</h2>
            {if $category.description|count_characters > 350}
              <div class="category-description-wrap text-muted mb-2">
                <button type="button" class="btn btn-secondary btn-sm" data-toggle="button" aria-pressed="false" autocomplete="off">
                  <span>{l s='More' d='Shop.Theme.Actions'}</span>
                  <span>{l s='Hide' d='Shop.Theme.Actions'}</span>
                </button>
                <div class="category-description-short">{$category.description|truncate:"400" nofilter}</div>
                <div class="category-description-full">{$category.description nofilter}</div>
              </div>
            {else}
              {$category.description nofilter}
            {/if}
          </div>
        {/if}

        {block name='product_list_subcategories'}
          {if isset($subcategories) && $subcategories}
            <!-- Subcategories -->
            <ul id="subcategories">
              {foreach from=$subcategories item=subcategory}
                <li>
                  <a class="subcategory-name text-uppercase" href="{$subcategory.url}">{$subcategory.name|truncate:20:'...'}</a>
                </li>
              {/foreach}
            </ul>
          {/if}
        {/block}
      </div>
    </div>
  </div>
{/block}


