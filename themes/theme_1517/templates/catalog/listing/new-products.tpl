{*
 * This file allows you to customize your new-product page.
 * You can safely remove it if you want it to appear exactly like all other product listing pages
 *}
{* {extends file='catalog/listing/product-list.tpl'} *}
{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
{extends file=$layout}

{block name='content'}
    {block name='breadcrumb'}
        {include file='_partials/breadcrumb-sacombank.tpl'}
    {/block}
    <div class="container">
        <div class="row"> 
            <div class="col-lg-3 mb-2 mb-lg-0">
                {if empty($category)}
                    {assign var=currentCat value=0}
                {else}
                    {assign var=currentCat value=$category.id}
                {/if}
                <!-- nav left-->
                <div class="sidebar-left">
                        {foreach from=$category_tree item="category"}
                            <h3 class="title-3">{$category.name}</h3>
                            {if isset($category.children) and $category.children|count}
                            <div class="nav-pro nav-collsape">
                                {foreach from=$category.children item="category_child"}
                                    <h4 class="toggle-nav-pro {if $currentCat == $category_child.id_category} active{/if}">
                                        <a href="{url entity='category' id=$category_child.id_category id_lang=$language.id}?selected_filters=xem-chi-tiet">{$category_child.name}</a>
                                        <i class="fa fa-angle-down d-lg-none"></i>
                                    </h4>
                                    {if isset($category_child.children) and $category_child.children|count}
                                    <div class="collsape-pro">
                                        <ul class="nav-list list-unstyled">
                                        {foreach from=$category_child.children item="category_child_item"}
                                            {if isset($category_child_item.children) and $category_child_item.children|count}
                                                <li {if $currentCat == $category_child_item.id_category}class="active"{/if}>
                                                <a href="{url entity='category' id=$category_child_item.id_category id_lang=$language.id}?selected_filters=xem-chi-tiet">{$category_child_item.name}</a></li>
                                                <ul class="nav-list tree">
                                                    {foreach from=$category_child_item.children item="category_child_item_lv1"}
                                                        <li {if $currentCat == $category_child_item_lv1.id_category}class="active"{/if}>
                                                        <a href="{url entity='category' id=$category_child_item_lv1.id_category id_lang=$language.id}?selected_filters=xem-chi-tiet">{$category_child_item_lv1.name}</a></li>
                                                    {/foreach}
                                                </ul>
                                            {else}
                                                <li class="nav-item"><a class="nav-header" href="{url entity='category' id=$category_child_item.id_category id_lang=$language.id}?selected_filters=xem-chi-tiet">{$category_child_item.name}</a></li>
                                            {/if}
                                        {/foreach}    
                                        </ul>
                                    </div>
                                    {/if}
                                {/foreach}
                            </div>
                            {/if}
                        {/foreach}
                        {block name='right_column'}{/block}
                    </div>
            <!-- e: nav left-->
            <!-- e: slidebar-product-sale-->
            {block name="left_column"}
            {assign var=original_hook_name value="displayLeftColumn" scope="global"}
            <div class="slidebar-product-sale">
              {if $page.page_name == 'product'}
                {hook h='displayLeftColumnProduct'}
              {else}
                {hook h="displayLeftColumn"}
              {/if}
            </div>
          {/block}
          <!-- e: slidebar-product-sale-->
        </div>
        <div class="col-lg-9">
            <div class="list-product space-global">
                
                {block name='product_list_subcategories'}{/block}
                <div class="product-list-top">
                    {block name='product_list_top'}
                        {include file='catalog/_partials/products-top.tpl' listing=$listing}
                    {/block}
                </div>
                {if $listing.products|count}
                {block name='product_list_header'}
                    <h2 class="title-2">{$listing.label|replace:'Category: ':''}</h2>
                {/block}
                {block name='product_list_active_filters'}
                    <div class="mb-2 mb-md-3">
                        {$listing.rendered_active_filters nofilter}
                    </div>
                {/block}

                <div id="products-wrapper" class="">
                    {block name='product_list'}
                        {include file='catalog/_partials/products.tpl' listing=$listing}
                    {/block}
                </div>

                <div id="js-product-list-bottom">
                    {block name='product_list_bottom'}
                        {include file='catalog/_partials/products-bottom.tpl' listing=$listing}
                    {/block}
                </div>

            {else}
                {include file='errors/not-found.tpl'}
            {/if}
        </div>
    </div>
</div>
</div>
{/block}
