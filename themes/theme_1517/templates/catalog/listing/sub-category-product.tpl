<div class="catagory-gifts space-global">
    {if $listing.products|count}
        <div class="container">
            <h2 class="title text-center">{$category.name}</h2>
            <div class="row">
                {foreach from=$listing.products key=i item='product'}
                    {if $i == 0}
                        <div class="col-lg-4 d-none d-lg-block">
                            <a class="img" href="#">
                                <img
                                        src="{$product.cover.medium.url}"
                                        alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                        data-full-size-image-url="{$product.cover.large.url}"
                                >
                            </a>
                        </div>
                    {/if}
                {/foreach}
                <div class="col-lg-8">
                    <div class="arrows-red-v2">
                        <div class="custom">
                            <button class="slick-prev slick-arrow">Previous</button>
                            <button class="slick-next slick-arrow">Next</button>
                        </div>
                        <div class="slide-gifts">
                            {foreach from=$listing.products item="product"}
                                <div class="item">
                                    <div class="tile-gift">
                                        <div class="img effect-2">
                                            <a href="{$product.url}">
                                                <img
                                                        class="img-fluid"
                                                        src="{$product.cover.bySize.home_default.url}"
                                                        alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                                        data-full-size-image-url="{$product.cover.large.url}"
                                                >
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {else}
        {include file='errors/not-found.tpl'}
    {/if}
</div>