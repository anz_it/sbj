<div class="product-shop">
  <div class="row">
    <div class="col-xl-12">
      <h1 class="product-name">{$product.name}</h1>
      <div class="product-code">{$product.reference}</div>
    </div>
  </div>
  <div class="price">
    
    <span class="new-price">
      {if $product.show_price}
        {$product.price|replace:',':'.'}
      {/if}
    </span><span class="unit"></span>
    {if $product.has_discount}
        <span class="old-price">{$product.regular_price|replace:',':'.'}</span>
    {/if}
    </div>

  <div class="short-desc"> 
    <div class="std">{$product.description_short nofilter}
    </div>
  </div>
  <div class="buy">
        {if $product.add_to_cart_url && !$configuration.is_catalog && ({$product.minimal_quantity} < {$product.quantity})}
          <a class="btn btn-red btn-sm text-uppercase" href="{$product.add_to_cart_url}" rel="nofollow" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" data-link-action="add-to-cart">
            {l s='Buy now' d='Shop.Theme.Actions'}</a>
        {else}
          <a class="btn btn-red btn-sm" href="{$urls.pages.contact}" rel="nofollow">
                     {l s='Liên hệ' d='Shop.Theme.Actions'}</a>
        {/if}
      
  </div>
  <div class="product-info">
    <div class="row">
      <div class="col-md-6">
        <div class="buy-telephone"><span class="icon-phone"><i class="fa fa-phone"></i></span>
          <p class="text-uppercase text-title"><strong>{l s='Buy product via phone' d='Shop.Theme.Actions'}</strong></p>
          <p class="text-uppercase">TP.HCM: 028.3932.5752</p>
          <p class="text-uppercase">Hà nội: 0903.62.62.42</p>
        </div>
        <div class="find-location">
          <span class="icon-store"><i class="fa fa-search"></i></span>
          <a href="{$urls.pages.stores}" target="_blank">
          <span>{l s='Hệ thống cửa hàng' d='Shop.Theme.Actions'}</span></a>
        </div>
      </div>
      <div class="col-md-6">
        {block name='product_additional_info'}
          {include file='catalog/_partials/product-additional-info.tpl'}
        {/block}
        
        <div class="find-customer-care">
           <span class="icon-user"><i class="fa fa-user"></i></span>
          <span class="text-uppercase">Chăm sóc khách hàng</span> 
        </div>
        <div class="find-customer-care-contact">
        <a href="mailto:cskhsbj@sacombank-sbj.com">cskhsbj@sacombank-sbj.com</a>
        </div>
      </div>
    </div>
    
  </div>
  
</div>