{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='product_miniature_item'}
    <article class="product-miniature product-miniature-small js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    <div class="col-lg-12 col-md-4 col-sm-6">
        <div class="tile-product">
          {foreach from=$product.flags item=flag}
            {if $flag.type == 'on-sale'}
              {if $product.has_discount && $product.discount_type === 'percentage'}
                <div class="sale lbl-pro">{$product.discount_percentage}</div>
              {/if}
            {/if}
          {/foreach}
          <div class="img effect-1"><a href="{$product.url}"><img src="{$product.cover.bySize.large_default.url}" 
            alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                data-full-size-image-url = "{$product.cover.large.url}"></a></div>
          <div class="text">
            <h3 class="dotdotdot"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h3>
            <div class="price">
              {if $product.has_discount}
                <div class="old-price">{$product.regular_price|replace:',':'.'}</div>
              {/if}
              <div class="new-price">
                {if $product.show_price}
                  {$product.price|replace:',':'.'}
                {else}
                  <a class="btn btn-red btn-sm text-uppercase" href="{$urls.pages.contact}" rel="nofollow">
                    {l s='Contact' d='Shop.Theme.Actions'}</a>
                {/if}
              </div>
            </div>
          </div>
        </div>
    </div>
    </article>
{/block}
