{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='product_miniature_item'}
  <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    <div class="product-miniature-container">
      <div class="product-miniature-thumbnail">
        <div class="product-thumbnail">
          {block name='product_thumbnail'}
            <a href="{$product.url}" class="product-thumbnail-link">
              {capture name='displayProductListGallery'}{hook h='displayProductListGallery' product=$product}{/capture}
              {if $smarty.capture.displayProductListGallery}
                {hook h='displayProductListGallery' product=$product}
              {else}
                <img
                  class="img-fluid"
                  src="{$product.cover.bySize.home_default.url}"
                  alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                  data-full-size-image-url="{$product.cover.large.url}"
                >
              {/if}
            </a>
          {/block}
          <div class="top_info">
            {block name='product_flags'}
              <ul class="product-flags">
                {foreach from=$product.flags item=flag}
                  <li class="{$flag.type}">
                    <span>{$flag.label}</span>
                  </li>
                {/foreach}
                {if $product.show_price && $product.has_discount}
                  {if $product.discount_type === 'percentage'}
                    <li class="discount-percentage">
                      <span>{$product.discount_percentage}</span>
                    </li>
                  {/if}
                {/if}
              </ul>
            {/block}
          </div>
        </div>
      </div>

      <div class="product-miniature-information mt-2">

        {block name='product_flags'}
          <ul class="product-flags">
            {foreach from=$product.flags item=flag}
              <li class="{$flag.type}">
                <span>{$flag.label}</span>
              </li>
            {/foreach}
            {if $product.show_price && $product.has_discount}
              {if $product.discount_type === 'percentage'}
                <li class="discount-percentage">
                  <span>{$product.discount_percentage}</span>
                </li>
              {/if}
            {/if}
          </ul>
        {/block}

        {block name='product_variants'}
          {if $product.main_variants}
            {include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}
          {/if}
        {/block}

        {block name='product_name'}
          <h1 class="product-title mt-1" itemprop="name"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h1>
        {/block}

        {block name='product_description_short'}
          <div class="product-description-short">{$product.description_short|truncate:130:'...' nofilter}</div>
        {/block}

        {block name='product_price_and_shipping'}
          {if $product.show_price && !$configuration.is_catalog}
            <div class="product-prices-md{if $product.has_discount} with-discount{/if}">
              {if $product.has_discount}
                {hook h='displayProductPriceBlock' product=$product type="old_price"}
                <span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
                <span class="regular-price">{$product.regular_price}</span>
                {if $product.discount_type === 'percentage'}
                  <span class="discount discount-percentage">{$product.discount_percentage}</span>
                {/if}
              {/if}

              {hook h='displayProductPriceBlock' product=$product type="before_price"}

              <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
              <span itemprop="price" class="price">{$product.price}</span>

              {hook h='displayProductPriceBlock' product=$product type='unit_price'}

              {hook h='displayProductPriceBlock' product=$product type='weight'}
            </div>
          {/if}
        {/block}

        {block name='product_reviews'}
          {hook h='displayProductListReviews' product=$product}
        {/block}

        <div class="product-buttons">
          {block name='quick_view'}
            <a class="quick-view btn-link" href="#" data-link-action="quickview" data-img-cover="{$product.cover.large.url}" data-rippleria="" data-loading-text="{l s='Loading product info...' d='Shop.Theme.Actions'}">
              <i class="fa fa-eye"></i>
            </a>
          {/block}

          {hook h='displayProductListFunctionalButtons' product=$product}

          {if $product.add_to_cart_url && !$configuration.is_catalog && ({$product.minimal_quantity} < {$product.quantity})}
            <a class="add-to-cart btn-link btn-link-primary" href="{$product.add_to_cart_url}" rel="nofollow" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" data-link-action="add-to-cart">
              <i class="fa fa-shopping-basket" aria-hidden="true"></i>
              <span>{l s='Add to cart' d='Shop.Theme.Actions'}</span>
            </a>
          {else}
            {if $product.customizable == 0}
              <a itemprop="url" class="view-product btn-link" href="{$product.url}" title="{l s='View product' d='Shop.Theme.Actions'}">
                <i class="fa fa-info" aria-hidden="true"></i>
                <span>{l s='View product' d='Shop.Theme.Actions'}</span>
              </a>
            {else}
              <a itemprop="url" class="customize btn-link" href="{$product.url}" title="{l s='Customize' d='Shop.Theme.Actions'}">
                <i class="fa fa-cog" aria-hidden="true"></i>
                <span>{l s='Customize' d='Shop.Theme.Actions'}</span>
              </a>
            {/if}
          {/if}

        </div>

      </div>
    </div>
    <script id="quickview-template-{$product.id}-{$product.id_product_attribute}" type="text/template">
      <div id="quickview-modal-{$product.id}-{$product.id_product_attribute}" class="quickview modal fade modal-close-inside" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg mx-auto" role="document">
          <div class="modal-content">
            <button type="button" class="close fa fa-times" data-dismiss="modal" aria-label="Close" aria-hidden="true"></button>
            <div class="modal-body">
              <div class="row m-0 align-items-stretch">
                <div class="col-md-6 bg-white p-0 d-flex flex-direction-column align-items-center align-content-center">
                  {block name='product_cover_thumbnails'}
                    {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                  {/block}
                </div>
                <div class="col-md-6 d-flex flex-column px-3">
                  <h1 class="h3 product-name pt-3 mt-2 mt-sm-0">{$product.name}</h1>
                  {block name='product_description_short'}
                    <div id="product-description-short" itemprop="description">{$product.description_short nofilter}</div>
                  {/block}
                  <div id="quickview-product-prices"></div>
                  {block name='product_buy'}
                    <div id="quickview-product-addToCart" class="product-actions"></div>
                  {/block}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </script>
  </article>
{/block}
