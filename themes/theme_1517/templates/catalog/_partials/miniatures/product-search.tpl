<div class="col-lg-3 col-md-4 col-sm-6">
    <div class="tile-product">
      <div class="img effect-1">
          <a href="{$product.url}">
              <img src="{$product.cover.bySize.home_default.url}" 
               alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                  data-full-size-image-url="{$product.cover.large.url}"     
              >
          </a>
      </div>
      <div class="text">
        <h3 class="dotdotdot"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a></h3>
        <div class="price">
          {if $product.has_discount} 
            <div class="old-price">{$product.regular_price}</div>
          {/if}
          {if $product.show_price}
            <div class="normal-price">{$product.price}</div>
          {/if}
        </div>
                {if $product.add_to_cart_url && !$configuration.is_catalog && ({$product.minimal_quantity} < {$product.quantity})}
            <a class="btn btn-red btn-sm text-uppercase" href="{$product.add_to_cart_url}" rel="nofollow" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" data-link-action="add-to-cart">
              <span>{l s='Add to cart' d='Shop.Theme.Actions'}</span>
            </a>
          {/if}
      </div>
    </div>
</div>
<script id="quickview-template-{$product.id}-{$product.id_product_attribute}" type="text/template">
      <div id="quickview-modal-{$product.id}-{$product.id_product_attribute}" class="quickview modal fade modal-close-inside" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg mx-auto" role="document">
          <div class="modal-content">
            <button type="button" class="close fa fa-times" data-dismiss="modal" aria-label="Close" aria-hidden="true"></button>
            <div class="modal-body">
              <div class="row m-0 align-items-stretch">
                <div class="col-md-6 bg-white p-0 d-flex flex-direction-column align-items-center align-content-center">
                  {block name='product_cover_thumbnails'}
                    {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                  {/block}
                </div>
                <div class="col-md-6 d-flex flex-column px-3">
                  <h1 class="h3 product-name pt-3 mt-2 mt-sm-0">{$product.name}</h1>
                  {block name='product_description_short'}
                    <div id="product-description-short" itemprop="description">{$product.description_short nofilter}</div>
                  {/block}
                  <div id="quickview-product-prices"></div>
                  {block name='product_buy'}
                    <div id="quickview-product-addToCart" class="product-actions"></div>
                  {/block}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </script>
