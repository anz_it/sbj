{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='pack_miniature_item'}
  <article class="pack-miniature col-6 col-sm-4 col-lg-4 col-xl-4 col-xxl-3">
    <div class="card text-center">
      <h2 class="card-header">{$product.pack_quantity} x {$product.name|truncate:'15'}</h2>
      <div class="card-block p-2">
        <img
          class="img-fluid"
          src="{$product.cover.small.url}"
          alt="{$product.cover.legend}"
          data-full-size-image-url="{$product.cover.large.url}"
          title="{$product.name}"
        >
      </div>
      <div class="card-footer">
        <span class="price">{$product.price}</span>
      </div>
    </div>
  </article>
{/block}
