<!-- .product-img-box-->

<div class="product-img-box">
    <div class="etalage">
        <div class="slider-nav">
            {foreach from=$product.images item=image}
            <div class="slider-nav-item ">
                <img src="{$image.bySize.medium_default.url}" alt="Image">
            </div> 
            {/foreach}
        </div>
        <div class="slider-for">
            {foreach from=$product.images item=image}
            <div class="slider-for-item zoom">
                <img src="{$image.bySize.large_default.url}" alt="Image">
            </div>
            {/foreach}
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<!-- e: .product-img-box-->