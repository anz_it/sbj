<div class="container">
    <h2 class="title text-center">{l s='Related product' d='Shop.Theme.Actions'}</h2>
    <div class="slide-products arrows-red">
        {foreach $products as $item}
            <div class="item">
                <div class="tile-product">
                    {foreach from=$item.flags item=flag}
                        {if $flag.type == 'on-sale'}
                        {if $item.has_discount && $item.discount_type === 'percentage'}
                            <div class="sale lbl-pro">{$item.discount_percentage}</div>
                        {/if}
                        {/if}
                    {/foreach}
                    <div class="img effect-1"><a href="{$item.url}"><img src="{$item.cover.large.url}" alt="{$product.name|truncate:30:'...'}"></a>
                    </div>
                    <div class="text">
                        <h3 class="dotdotdot"><a href="{$item.url}">{$item.name|truncate:30:'...'}</a></h3>
                        {if $item.show_price}
                            <div class="price">
                                {if $item.has_discount}
                                    <div class="old-price">{$item.discount_amount_to_display}</div>
                                {/if}
                                <div class="new-price">
                                {if $item.show_price}
                                    {$item.price|replace:',':'.'}
                                {/if}
                                </div>
                            </div>
                        {/if}
                        {if $item.show_price}
                            {if $item.add_to_cart_url && !$configuration.is_catalog && ({$item.minimal_quantity} < {$item.quantity})}
                                <a class="btn btn-red btn-sm text-uppercase" href="{$item.add_to_cart_url}" rel="nofollow" data-id-product="{$item.id_product}" data-id-product-attribute="{$item.id_product_attribute}" data-link-action="add-to-cart">
                                    {l s='Add to cart' d='Shop.Theme.Actions'}</a>
                            {/if}
                        {else}
                            <a class="btn btn-red btn-sm text-uppercase" href="{$urls.pages.contact}" rel="nofollow">
                            {l s='Contact' d='Shop.Theme.Actions'}</a>
                        {/if}
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>