
{extends file=$layout}

{block name='content'}
    <div id="main-content">
        <!-- banner-3-->
        <div class="banner-3" style="background-image:url({$trungtamgiamdinh_link})">
            <div class="container">
                <div class="whisper-wrap">
                    <div class="whisper">
                        <div class="whisper-head">
                            <h3>{l s='Trung tâm giám định & dịch vụ kim hoàn' d='Shop.Theme.Actions'}</h3>
                            <p class="sub">{$trungtamgiamdinh_whisper}</p>
                        </div>
                        <div class="whisper-body">
                            <div class="desc-content dotdotdot">{$trungtamgiamdinh_whisper_desc}</div>
                        </div>
                    </div>
                    <div class="text-center"><a class="btn btn-red text-uppercase min-width-195"
                                                href="{$trungtamgiamdinh_whisper_link}">
                            {l s='View More' d='Shop.Theme.Actions'}</a></div>
                </div>
            </div>
        </div>
        <!-- e: banner-3-->
        <div class="inspect-logo">
            <img src="{$urls.img_url}logo_giamdinh.png" alt="Image" width="250" height="250"/>
        </div>
        <div class="services">
            <div class="container">
                <h3 class="title text-center service-title">{l s='Service' d='Shop.Theme.Actions'} </h3>
                <div class="service-grid row">
                    <div class="col-12 col-md-6">
                        <div class="single-srv-wrap"><a class="single-srv-img"
                                                        href="/news/category/15/dich-vu-giam-dinh">
                                <img src="{$trungtamgiamdinh_dich_vu_giam_dinh_img}" alt="Image" width="565" height="674"></a>
                            <div class="single-srv">
                                <h2 class="title text-center">{l s='EXPERIMENTAL SERVICES' d='Admin.Global'}</h2>
                                <p class="short-text dotdotdot">
                                    {$trungtamgiamdinh_dich_vu_giam_dinh_desc}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="single-srv-wrap"><a class="single-srv-img" href="/news/category/16/dich-vu-kim-hoan">
                                <img src="{$trungtamgiamdinh_dich_vu_kim_hoan_img}" alt="Image" width="565" height="674"></a>
                            <div class="single-srv">
                                <h2 class="title text-center">{l s='KIM HOAN SERVICE' d='Admin.Global'}</h2>
                                <p class="short-text dotdotdot">
                                    {$trungtamgiamdinh_dich_vu_kim_hoan_desc}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- e: services-->
        {hook h='displayFeaturedProductsTTGD'}
        <div class="container">
            <!-- quality-standard-->
            <div class="quality-standard">
                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <div class="quality-column text-center">
                            <h3>{l s='QUALITY STANDARDS' d='Shop.Theme.Actions'} </h3>
                            <div class="quality-short-text short-text">
                                {$trungtamgiamdinh_tieu_chuan_chat_luong_desc}
                            </div>
                            <a class="btn btn-red min-width-195 text-uppercase"
                               href="{$trungtamgiamdinh_tieu_chuan_chat_luong_link}">
                                {l s='Xem thêm' d='Shop.Theme.Actions'}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- e: quality-standard-->

            <!-- knowledge-->
            <div class="knowledge">
                <h2 class="title lg text-center">{l s='News - Knowledge' d='Admin.Global'}</h2>
                <div class="row">

                    {foreach $posts as $key => $item}
                        {if {$key} > 1}
                            {break}
                        {/if}
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="box-thumnail">
                                    <a class="figure-img" href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $item.id_jxblog_post, 'rewrite' => $item.link_rewrite]}">
                                        <img src="{JXBlogImageManager::getImage('post', $item.id_jxblog_post, 'post_default')}" alt="{$item.name}" alt="Image" width="368" height="380">
                                    </a>
                                    <div class="box-thumnail-desc">
                                        <h3>{$item.name}</h3>
                                        <p class="short-text dotdotdot">
                                            {$item.short_description|strip_tags|truncate:100:'...'}
                                        </p>
                                    </div>
                                </div>
                            </div>
                    {/foreach}

                    <div class="col-12 col-md-4">
                        <ul class="knowledge-list">
                            {foreach $posts as $key => $item}
                                {if $key >1}
                                    <li>
                                        <h3><a href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $item.id_jxblog_post, 'rewrite' => $item.link_rewrite]}">{$item.name}</a></h3>
                                        <div>{$item.short_description|strip_tags|truncate:100:'...'}</div>
                                    </li>
                                {/if}
                            {/foreach}
                        </ul>
                        <div class="knowledge-button text-center">
                            <a class="btn btn-outline-red text-uppercase min-132" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => 6, 'rewrite' => 'tin-tuc-kien-thuc']}">{l s='Xem thêm' d='Shop.Theme.Actions'}</a></div>
                    </div>
                </div>
            </div>
            <!-- e: knowledge-->
        </div>
        <!-- center-support-->
        <!-- <div class="center-support">
            <div class="container">
                <div class="box-support bg-white">
                    <h3 class="sp-title text-center">{l s='Customer Support Center' d='Admin.Global'}</h3>
                    <div class="row row-support">
                        <div class="col-md-4">
                            <div class="sm-faq text-center"><a class="direction" href="/content/13-giai-dap-nhanh">
                                    <h3>{l s='Frequently Asked Questions' d='Admin.Global'}</h3>
                                    <p>{l s='Frequently asked questions related to SBJ products and services' d='Admin.Global'}</p></a></div>
                        </div>
                        <div class="col-md-4">
                            <div class="sm-hotline text-center"><a class="direction" href="javascript:;">
                                    <h3>{l s='Hotline: 1800 54 54 57' d='Admin.Global'}</h3>
                                    <p>{l s='Sẵn sàng hỗ trợ mọi thắc mắc của bạn 24/7' d='Admin.Global'}</p></a></div>
                        </div>
                        <div class="col-md-4">
                            <div class="sm-chat text-center"><a class="direction" href="javascript:;">
                                    <h3>{l s='Online support' d='Admin.Global'}</h3>
                                    <p>{l s='Câu hỏi của bạn sẽ được nhân viên SBJ tư vấn tốt nhất' d='Admin.Global'}</p></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- e: center-support-->
    </div>

{/block}
