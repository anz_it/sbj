{*
 @vothienhoa
*}

{extends file=$layout}

{block name='head_seo_description'}{$page.meta.description}{/block}
{block name='head_seo_keywords'}{$page.meta.keywords}{/block}

{block name='content'}
    <div id="main-content" class="category-news">
        <!-- list-girds-->
        <div class="list-girds bg-darkgrey">
            <div class="container">
                <div class="page-title page-title-news">
                    <h2 class="title text-center">{$category_name}</h2>
                    {if !empty($category_id) && ($category_id == 1 || $category_id == 6)  }
                        <div class="news-filter">
                            <div class="dropdown">
                                <button class="btn btn-danger dropdown-toggle" type="button">
                                    {l s='Categories' mod='jxblog'}
                                </button>
                                <div class="dropdown-menu">
                                    {if !empty($cate_tintuc_sukien->id)}
                                        <a class="dropdown-item"
                                           href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_tintuc_sukien->id_jxblog_category, 'rewrite' => $cate_tintuc_sukien->link_rewrite]}">
                                            {l s='NEWS - EVENT SBJ' mod='jxblog'}
                                        </a>
                                    {/if}

                                    {if !empty($cate_khuyen_mai->id)}
                                        <a class="dropdown-item" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_khuyen_mai->id_jxblog_category, 'rewrite' => $cate_khuyen_mai->link_rewrite]}">
                                            {l s='NEWS - PROMOTION' mod='jxblog'}
                                        </a>
                                    {/if}

                                    {if !empty($cate_tintuc_chuyennganh->id)}
                                        <a class="dropdown-item" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_tintuc_chuyennganh->id_jxblog_category, 'rewrite' => $cate_tintuc_chuyennganh->link_rewrite]}">
                                            {l s='PROFESSIONAL NEWS' mod='jxblog'}
                                        </a>
                                    {/if}

                                    {if !empty($cate_tuyen_dung->id)}
                                        <a class="dropdown-item" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_tuyen_dung->id_jxblog_category, 'rewrite' => $cate_tuyen_dung->link_rewrite]}">
                                            {l s='RECRUITMENT' mod='jxblog'}
                                        </a>
                                    {/if}
                                    
                                    {if !empty($cate_hot_news->id)}
                                        <a class="dropdown-item" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_hot_news->id_jxblog_category, 'rewrite' => $cate_hot_news->link_rewrite]}">
                                            {l s='HOT NEWS' mod='jxblog'}
                                        </a>
                                    {/if}
                                    
                                    {if !empty($cate_specialized_knowledge->id)}
                                        <a class="dropdown-item" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_specialized_knowledge->id_jxblog_category, 'rewrite' => $cate_specialized_knowledge->link_rewrite]}">
                                            {l s='SPECIALIZED KNOWLEDGE' mod='jxblog'}
                                        </a>
                                    {/if}

                                    {if !empty($cate_gems_jewelry->id)}
                                        <a class="dropdown-item" href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_gems_jewelry->id_jxblog_category, 'rewrite' => $cate_gems_jewelry->link_rewrite]}">
                                            {l s='GEMS AND JEWELRY' mod='jxblog'}
                                        </a>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    {/if}
                </div>
                <div class="row">
                    {if $posts}
                        {include file="module:jxblog/views/templates/front/_partials/post-miniature-news.tpl"}

                    {else}
                        <div class="col-12"><p class="text-center">{l s='There are no posts in the category' mod='jxblog'}</p></div>
                    {/if}
                </div>
                {if $pagination}
                    {include file="module:jxblog/views/templates/front/_partials/pagination-news.tpl"}
                {/if}
            </div>
        </div>
        
    </div>
{/block}
