{*
* 2017 Zemez
*
* JX Blog
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez (Alexander Grosul)
*  @copyright 2017 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{extends file=$layout}

{block name='head_seo_description'}{$post.meta_description}{/block}
{block name='head_seo_keywords'}{$post.meta_keyword}{/block}

{block name='content'}
    {if $post}
        <div id="main-content">
            <div class="page-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                        <section class="single-blog">
                            <div class="back-redirect"><a href="{$smarty.server.HTTP_REFERER}"><i class="ico-back-redirect"></i>Trở lại</a></div>
                            <div class="header-blog text-center">
                                <h2 class="text-uppercase">{$post.name}</h2>
                                <p class="time">{$post.date_start|date_format:'%d/%m/%y'}</p>
                            </div>
                            <div class="content-blog">
                                {if $post.description}
                                        {$post.description nofilter}
                                {/if}
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <!-- e: title page-->
        </div>
    {else}
      <p>{l s='Post doesn\'t exist or you don\'t have permissions to access it.' mod='jxblog'}</p>
    {/if}
{/block}
