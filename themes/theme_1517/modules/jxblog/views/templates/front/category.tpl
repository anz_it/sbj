{*
* 2017 Zemez
*
* JX Blog
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez (Alexander Grosul)
*  @copyright 2017 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{extends file=$layout}

{block name='head_seo_description'}{$page.meta.description}{/block}
{block name='head_seo_keywords'}{$page.meta.keywords}{/block}

{block name='content'}
    <!-- banner inner-->
    <section class="banner-inner">
      <div class="item-img" alt="{$category.name}" style="background-image:url({JXBlogImageManager::getImage('category', $category.id_jxblog_category, 'category_info')})"></div>
    </section>
    <!-- e: banner inner-->
    <!-- sub collection-->
        <section class="sub-collection">
          <div class="container">
            <h2 class="title text-center">{l s='BỘ SƯU TẬP' mod='jxblog'}</h2>
            <div class="row">
            {if $posts}
              {include file="module:jxblog/views/templates/front/_partials/post-miniature.tpl"}
            {else}
              <p>{l s='There are no posts in the category' mod='jxblog'}</p>
            {/if}
            </div>
            {if $pagination}
                {include file="module:jxblog/views/templates/front/_partials/pagination-category-child.tpl"}
            {/if}
          </div>
        </section>
        <!-- e: sub collection-->
{/block}
