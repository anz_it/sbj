{*
* 2017 Zemez
*
* JX Blog
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez (Alexander Grosul)
*  @copyright 2017 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{extends file=$layout}

{block name='head_seo_description'}{$post.meta_description}{/block}
{block name='head_seo_keywords'}{$post.meta_keyword}{/block}

{block name='content'}
    
<div class="page-inner">
<div class="container">
    {if $post}
  <section class="single-blog">
    <div class="back-redirect"><a onclick="window.history.back()" href="#"><i class="ico-back-redirect"></i>{l s='Trở lại' mod='jxblog'}</a></div>
    <div class="header-blog text-center">
      <h2 class="text-uppercase">{$post.name}</h2>
      <p class="time">{$post.date_start|date_format}</p>
    </div>
    <div class="post-image mb-2">
        <img class="img-fluid" src="{JXBlogImageManager::getImage('post', $post.id_jxblog_post, 'post_default')}" alt="{$post.name}"/>
      </div>
    <div class="content-blog">
     {if $post.description}
          {$post.description nofilter}
      {/if}
    </div>
  </section>
    {else}
      <p>{l s='Post doesn\'t exist or you don\'t have permissions to access it.' mod='jxblog'}</p>
    {/if}
</div>
</div>
{/block}
