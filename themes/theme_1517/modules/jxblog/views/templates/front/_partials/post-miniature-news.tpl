{*
 vothienhoa
*}
{foreach from=$posts item='post'}
    {block name='blog_post_miniature'}
      <div class="col-md-6 col-lg-4 article-news">
        <div class="masonry-item">
          <a class="figure-thumb" href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}">
            <img src="{JXBlogImageManager::getImage('post_thumb', $post.id_jxblog_post, 'post_listing')}" alt="{$post.name}" width="370" height="280">
          </a>
          <div class="lower-content">
            <span class="timepost">{$post.date_start|date_format:'%d/%m/%Y'}</span>
            <h3><a href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}">{$post.name}</a></h3>
            <div class="desc">
                {$post.short_description|strip_tags|truncate:120:'...'}
            </div>
          </div>
        </div>
      </div>
    {/block}
{/foreach}
