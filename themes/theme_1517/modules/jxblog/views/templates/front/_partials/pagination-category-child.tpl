{if isset($pagination.steps) && $pagination.steps}
<div class="pagging d-flex justify-content-center align-items-center">
<nav aria-label="Page navigation example">
<ul class="pagination">
        {foreach from=$pagination.steps item='step'}
            {if $step.active}
                <li class="page-item {if $step.active}active{/if}">
                    <a class="page-link" href="{$step.url}">
                        {if $step.name == 'Previous'}
                            &lsaquo;
                        {elseif $step.name == 'Next'}
                            &rsaquo;
                        {else}
                            {$step.name}
                        {/if}
                    </a>
                </li>
            {else}
                <li class="page-item">
                    <a class="page-link" href="{$step.url}">
                        {if $step.name == 'Previous'}
                            &lsaquo;
                        {elseif $step.name == 'Next'}
                            &rsaquo;
                        {else}
                            {$step.name}
                        {/if}
                    </a>
                </li>
            {/if}

        {/foreach}
    </ul>
</nav>
</div>
    
{/if}

