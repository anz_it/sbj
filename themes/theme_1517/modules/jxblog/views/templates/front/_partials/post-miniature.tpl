{*
 vothienhoa
*}
{foreach from=$posts item='post'}
    {block name='blog_post_miniature'}
    <div class="col-lg-4 col-sm-6">
        <div class="tile-sub-collection">
            <figure class="effect-1"><a href="{url entity='module' name='jxblog' controller='post' params = ['id_jxblog_post' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}"><img src="{JXBlogImageManager::getImage('post_thumb', $post.id_jxblog_post, 'post_listing')}" alt="{$post.name}"></a></figure>
            <h3><a href="{url entity='module' name='jxblog' controller='post' params = ['id_jxblog_post' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}">{$post.name}</a></h3>
        </div>
    </div>
    {/block}
{/foreach}
