{*
    @vothienhoa
*}
<div class="paging">
    {if isset($pagination.steps) && $pagination.steps}
        <ul class="pagination justify-content-center">
            {foreach from=$pagination.steps item='step'}
                {if $step.active}
                    <li class="page-item {if $step.active}active{/if}">
                        <a class="page-link" href="{$step.url}">
                            {if $step.name == 'Previous'}
                                &lsaquo;
                            {elseif $step.name == 'Next'}
                                &rsaquo;
                            {else}
                                {$step.name}
                            {/if}
                        </a>
                    </li>
                {else}
                    <li class="page-item">
                        <a class="page-link" href="{$step.url}">
                            {if $step.name == 'Previous'}
                                &lsaquo;
                            {elseif $step.name == 'Next'}
                                &rsaquo;
                            {else}
                                {$step.name}
                            {/if}
                        </a>
                    </li>
                {/if}

            {/foreach}
        </ul>
    {/if}
</div>
