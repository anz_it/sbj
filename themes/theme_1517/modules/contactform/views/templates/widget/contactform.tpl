{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{block name="page_title"} 
  <div class="title-page text-center contact-title" style="background: none;">
    <div class="container">
        <h1>Liên hệ</h1>
    </div>
    </div>
{/block}

<!-- contact-us-->
<section class="contact-us">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <section class="login-form">
                    <form class="contact-form" action="{$urls.pages.contact}" method="post" {if $contact.allow_file_upload}enctype="multipart/form-data"{/if}>

                        {if $notifications}
                        <div class="notification {if $notifications.nw_error}notification-error{else}notification-success{/if}">
                            <ul>
                            {foreach $notifications.messages as $notif}
                                <li>{$notif}</li>
                            {/foreach}
                            </ul>
                        </div>
                        {/if}

                        {if !$notifications || $notifications.nw_error}
                        <section class="form-fields">
                            <div class="form-group">
                            <input type="hidden" name="id_contact" value="2"/>
                                <input class="form-control" placeholder="{l s='Họ và tên' d='Shop.Forms.Help'}" type="text"
                                    name="contact_name" value=""/>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="email" name="from" value="{$contact.email}" placeholder="{l s='Email của bạn' d='Shop.Forms.Help'}" />
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="{l s='Điện thoại' d='Shop.Forms.Help'}" type="text"
                                    name="contact_phone" value=""/>
                            </div>
                            
                            {if $contact.orders}
                            <!--<div class="form-group">
                                <select name="id_order" class="form-control">
                                    <option value="">{l s='Select reference' d='Modules.Contactform.Shop'}</option>
                                    {foreach from=$contact.orders item=order}
                                        <option value="{$order.id_order}">{$order.reference}</option>
                                    {/foreach}
                                </select>
                            </div>-->
                            {/if}

                            {if $contact.allow_file_upload}
                            <label>
                                <span>{l s='Attach File' d='Modules.Contactform.Shop'}</span>
                                <input type="file" name="fileUpload" />
                            </label>
                            {/if}
                            <div class="form-group">
                                <textarea class="form-control" cols="67" rows="3" name="message" placeholder="{l s='Nội dung cần liên hệ' d='Shop.Forms.Help'}">{if $contact.message}{$contact.message}{/if}</textarea>
                            </div>
                            <label>
                            
                            {hook h='displayGDPRConsent' id_module=$id_module}

                        </section>

                        <div class="btn-form">
                            <style>
                            input[name=url] {
                                display: none !important;
                            }
                            </style>
                            <input type="text" name="url" value=""/>
                            <input type="hidden" name="token" value="{$token}" />
                            <button class="btn btn-red btn-block text-uppercase" type="submit" name="submitMessage">
                            {l s='Send' d='Modules.Contactform.Shop'}
                            </button>
                        </div>
                        {/if}
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
