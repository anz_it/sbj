{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{if isset($original_hook_name) && ($original_hook_name == 'displayLeftColumn' || $original_hook_name == 'displayRightColumn')}
<section>
  <h3 class="title-3">{l s='On sale' d='Modules.Specials.Shop'}</h3>
  <div class="row">
    {foreach from=$products item="product"}
      {include file="catalog/_partials/miniatures/product-special.tpl" product=$product}
    {/foreach}
  </div>
  <div class="text-center"><a class="btn btn-red text-uppercase min-132 rounded-0" href="{$allSpecialProductsLink}">{l s='All sale products' d='Modules.Specials.Shop'}</a></div>
</section>
{else}
  <section class="featured-products grid clearfix">
    <h1 class="h3 products-section-title">
      {l s='On sale' d='Shop.Theme.Catalog'}
    </h1>
    <div class="products row">
      {foreach from=$products item="product"}
        {include file="catalog/_partials/miniatures/product.tpl" product=$product}
      {/foreach}
    </div>
    <a class="btn btn-secondary d-md-none" href="{$allSpecialProductsLink}">
      {l s='All special products' d='Shop.Theme.Catalog'}
    </a>
  </section>
{/if}