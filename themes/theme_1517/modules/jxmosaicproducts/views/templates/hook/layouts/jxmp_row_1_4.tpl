{*
* 2002-2017 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2017 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
<div class="row">
    <div class="col-lg-4 d-none d-lg-block">
        {foreach from=$row_content key=k item=items name=loop}
            {if $k < 2}
                {if $items}
                    {if isset($item_types[$k])}
                        {include file=$partial_path[$item_types[$k]] data=$item_datas[$k]}
                    {/if}
                {/if}
            {/if}
        {/foreach}
    </div>

    <div class="col-lg-8">
        <div class="arrows-red-v2">
            <div class="custom">
                <button class="slick-prev slick-arrow">Previous</button>
                <button class="slick-next slick-arrow">Next</button>
            </div>
            <div class="slide-gifts">
                {foreach from=$row_content key=k item=items name=loop}
                    {if $k > 1}
                            {if $items}
                                {if isset($item_types[$k])}
                                    {include file=$partial_path[$item_types[$k]] data=$item_datas[$k]}
                                {/if}
                            {/if}
                    {/if}
                {/foreach}
            </div>
        </div>
    </div>
</div>