{*
* 2002-2017 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2017 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
<div class="row mb-20">
    {foreach from=$row_content key=k item=items name=loop}
        <div class="col-lg-4 col-sm-6">
            {if $items}
                {if isset($item_types[$k])}
                    {include file=$partial_path[$item_types[$k]] data=$item_datas[$k]}
                {/if}
            {/if}
        </div>
    {/foreach}
</div>