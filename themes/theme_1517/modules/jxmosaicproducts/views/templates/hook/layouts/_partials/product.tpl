{*
* 2002-2018 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2018 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{assign var='product' value=$data}
<div class="item">
    <div class="tile-gift-v2">
        <div class="img effect-2"><a href="{$product.url}"><img src="{$product.cover.bySize.large_default.url}" title="{$product.name}"></a></div>
        <h4 class="text-uppsercase">
            <a href="{$product.url}">{$product.name}</a></h4>
    </div>
</div>
