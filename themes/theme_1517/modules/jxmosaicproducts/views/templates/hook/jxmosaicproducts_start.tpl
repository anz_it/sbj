{*
* 2002-2017 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2017 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}


<div class="space-global">
  <div class="container">
    <h2 class="title text-center">
        {if isset($data.custom_name_status) && $data.custom_name_status}
            {$data.custom_name}
        {else}
          <a href="javascript:void(0)" title="{$data.name}">{$data.name}</a>
        {/if}
    </h2>
