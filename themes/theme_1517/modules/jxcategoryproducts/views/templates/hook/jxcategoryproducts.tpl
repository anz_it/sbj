{if isset($blocks) && $blocks}
    {foreach from=$blocks key=k  item='block' name='block'}
        {assign var="block_identificator" value="{$block.id}"}
        <!-- category box-->
        <div class="category-box {if $k %2 != 0}bg-light{/if} {if $k < ($block@total-1)} space-global {/if}">
            <div class="container">
                
                <h2 class="title text-center"><a href="{url entity='category' id=$block.id}">{$block.name}</a></h2>
                {if isset($block.products) && $block.products}
                    <div class="row">
                        {assign var='products' value=$block.products}
                        <div class="col-lg-5 d-none d-lg-block">
                            <a class="img" href="{$link->getCategoryLink($block.id,'','')|escape:'html':'UTF-8'}?selected_filters=xem-chi-tiet">
                                <img src="{$link->getCatImageLink($block.name, $block.id, 'thumb_default')}">
                            </a>
                        </div>
                        <div class="col-lg-7">
                            <div class="slide-product-small arrows-red">
                                {foreach from=$products key=i item='product'}
                                        <div class="item">
                                            <div class="tile-product small">
                                                {foreach from=$product.flags item=flag}
                                                    {if $flag.type == 'on-sale'}
                                                        {if $product.has_discount && $product.discount_type === 'percentage'}
                                                            <div class="sale lbl-pro">{$product.discount_percentage}</div>
                                                        {/if}
                                                    {/if}
                                                {/foreach}
                                                <div class="img effect-1"><a href="{$product.url}">
                                                        <img
                                                                src="{$product.cover.bySize.home_default.url}"
                                                                alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                                                data-full-size-image-url="{$product.cover.large.url}"
                                                        >
                                                    </a>
                                                </div>
                                                <div class="text">
                                                    <h3 class="dotdotdot"><a href="{$product.url}">{$product.name|truncate:30:'...'}</a>
                                                    </h3>
                                                    <div class="price"> 
                                                        {if $product.has_discount}
                                                            <div class="old-price">{$product.discount_amount_to_display}</div>
                                                        {/if}
                                                        {if $product.show_price}
                                                        <div class="new-price">{$product.price|replace:',':'.'}</div>
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                {else}
                    <p class="alert alert-warning">{l s='No products in this category.' mod='jxcategoryproducts'}</p>
                {/if}
                <div class="text-center">
                    {if $block.id != 124}
                        <a class="btn btn-outline-red text-uppercase min-132 {$block.id}"
                            href="{$link->getCategoryLink($block.id)|escape:'html':'UTF-8'}">{l s='Xem thêm' d='Shop.Theme.Actions'}</a>
                    {else}
                        <a class="btn btn-outline-red text-uppercase min-132 {$block.id}"
                            href="{$link->getCategoryLink(22)|escape:'html':'UTF-8'}">{l s='Xem thêm' d='Shop.Theme.Actions'}</a>
                       {/if}
                </div>
            </div>
        </div>
        <!-- e: category box-->
    {/foreach}
{/if}