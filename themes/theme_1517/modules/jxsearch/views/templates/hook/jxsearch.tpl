{*
* 2017 Zemez
*
* JX Search
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     Zemez (Alexander Grosul)
* @copyright  2017 Zemez
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
<!-- header-->
<div class="top-search">
  <button class="closeSearch fa fa-times"></button>
  <div class="container">
    <form method="get" action="{Jxsearch::getJXSearchLink('jxsearch')|escape:'htmlall':'UTF-8'}">
      {if !Configuration::get('PS_REWRITING_SETTINGS')}
        <input type="hidden" name="fc" value="module"/>
        <input type="hidden" name="controller" value="jxsearch"/>
        <input type="hidden" name="module" value="jxsearch"/>
      {/if}
      <input class="form-control" type="text" name="search_query" placeholder="Search">
      <button type="submit" name="jx_blog_submit_search" class="btn btn-red"><i class="fa fa-search"></i> Search</button>
    </form>
  </div>
</div>
