{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<li>
  <div class="header-account">
    {if $logged}      
      <a id="header-account-link" href="javascript:;" data-toggle="modal" data-target="#form-modal" class="account" title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}" rel="nofollow">
        <span><span>Xin Chào, </span>{$customerName}</span>
      </a>
    {else}
      <a id="header-account-link" href="javascript:;" data-toggle="modal" data-target="#form-modal">
      {l s='Sign in' d='Shop.Theme.Actions'}</a>
      <span class="divider">|</span>
      <a href="{$urls.pages.register}">{l s='Sign up' d='Shop.Theme.Actions'}</a>
    {/if}
    <div class="modal fade modal-close-inside show" id="form-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-width-sm" role="document">
        <div class="modal-content">
          <button class="close fa fa-times" type="button" data-dismiss="modal" aria-label="Close" aria-hidden="true"></button>
          <div class="modal-body">
            <div class="account-wrapper">
              <div class="login-content" id="login-content">
                {if $logged}
                <div class="login-content logined-content" id="logined">
                    Xin chào, <h2>{$customerName}</h2>
                    <div class="main-help-block">
                      <ul>
                        <li><a href="{$link->getPageLink('history', true)|escape:'htmlall':'UTF-8'}">{l s='Lịch sử đơn hàng' mod='Shop.Theme.Customeraccount'}</a></li>
                        <li><a href="{$link->getPageLink('addresses', true)|escape:'htmlall':'UTF-8'}">{l s='Sổ địa chỉ' mod='Shop.Theme.Customeraccount'}</a></li>
                        <li><a href="{$link->getPageLink('identity', true)|escape:'htmlall':'UTF-8'}">{l s='Thông tin tài khoản' mod='Shop.Theme.Customeraccount'}</a></li>
                      </ul>
                    </div>
                    <hr>
                    <div class="text-center">
                      <a class="logout mr-3" href="{$logout_url}" rel="nofollow">
                        <button class="btn btn-red btn-logout">{l s='Sign out' d='Shop.Theme.Actions'}</button>
                      </a>
                    </div>
                </div>
                {else}
                <form id="login-form" action="/dang-nhap" method="post">
                  <div class="main-help-block">
                    {block name='login_form_errors'}
                    {/block}
                  </div>
                  <section>
                    <input type="hidden" name="back" value="">
                    <div class="form-group">
                      <label class="form-control-label required">Số điện thoại</label>
                      <div class="form-control-content">
                        <input class="form-control" name="phone" type="text" value="" required="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="form-control-label required">Mật khẩu</label>
                      <div class="form-control-content">
                        <div class="input-group">
                          <input class="form-control js-visible-password" name="password" type="password" value="" required="">
                        </div>
                      </div>
                    </div>
                  </section>
                  <div class="header-login-footer">
                    <div class="nav d-block">
                      <a class="btn-link forgot-password" href="{$urls.pages.password}" rel="nofollow">
                        {l s='Quên mật khẩu?' d='Shop.Theme.Customeraccount'}
                      </a>
                    </div>
                    <input type="hidden" name="submitLogin" value="1">
                    {block name='form_buttons'}
                      <button id="submit-login" class="btn btn-red btn-sm" data-link-action="sign-in" type="submit" class="form-control-submit">
                        {l s='Sign in' d='Shop.Theme.Actions'}
                      </button>
                    {/block}
                    <hr>
                    <div class="clearfix social-login-buttons">
                      {hook h='displaySocialLoginButtons'}
                    </div>
                  </div>
                </form>
                {/if}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</li>