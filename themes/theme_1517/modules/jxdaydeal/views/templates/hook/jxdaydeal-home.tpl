{*
* 2002-2018 Jetimpex
*
* Jetimpex Deal of Day
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Jetimpex (Sergiy Sakun)
*  @copyright 2002-2018 Jetimpex
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<section class="daydeal-products mt-3 mt-md-5 list clearfix">
  <h1 class="section-title">{l s='Deal of the day' mod='jxdaydeal'}</h1>
  <div class="products">
    {if isset($daydeal_products) && $daydeal_products}
      {foreach from=$daydeal_products item=product name=product}
        <article class="product-miniature" data-id-product="{$product.info.id_product}" data-id-product-attribute="{$product.info.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
          <div class="product-miniature-container">
            {block name='product_thumbnail'}
              <div class="product-miniature-thumbnail">
                <a class="product-thumbnail" href="{$product.info.url}">
                  <img class="img-fluid" src="{$product.info.cover.bySize.home_default.url}" alt = "{$product.info.cover.legend}" data-full-size-image-url = "{$product.info.cover.large.url}" />
                </a>
              </div>
            {/block}
            <div class="product-miniature-information">
              {if $product.info.has_discount}
                <div class="label-discount-wrap">
                  {if isset($daydeal_products_extra[$product.info.id_product]["label"]) && $daydeal_products_extra[$product.info.id_product]["label"]}
                    <span class="label-daydeal">{$daydeal_products_extra[$product.info.id_product]["label"]|escape:'htmlall':'UTF-8'}</span>
                  {/if}
                  {if $product.info.discount_type === 'percentage'}
                    <span class="discount-daydeal">{$product.info.discount_percentage}</span>
                  {/if}
                </div>
              {/if}
              {block name='product_name'}
                <h1 class="h4" itemprop="name"><a href="{$product.info.url}">{$product.info.name|truncate:25:'...'}</a></h1>
              {/block}
              {block name='product_description_short'}
                <div class="product-description-short">{$product.info.description_short|truncate:130:'...' nofilter}</div>
              {/block}
              {block name='product_price_and_shipping'}
                {if $product.info.show_price && !$configuration.is_catalog}
                  <div class="product-prices-lg with-discount">
                    {hook h='displayProductPriceBlock' product=$product.info type="before_price"}
                    <span itemprop="price" class="price">{$product.info.price}</span>
                    {if $product.info.has_discount}
                      {hook h='displayProductPriceBlock' product=$product.info type="old_price"}
                      <span class="regular-price">{$product.info.regular_price}</span>
                    {/if}
                    {hook h='displayProductPriceBlock' product=$product.info type='unit_price'}
                    {hook h='displayProductPriceBlock' product=$product.info type='weight'}
                  </div>
                {/if}
              {/block}
            </div>
           </div>
        </article>
      {/foreach}
    {else}
      <p class="alert alert-info">{l s='No special products at this time.' mod='jxdaydeal'}</p>
    {/if}
  </div>
</section>