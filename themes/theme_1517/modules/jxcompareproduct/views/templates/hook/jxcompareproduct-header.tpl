{**
* 2002-2018 Jetimpex
*
* JX Compare Product
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Jetimpex
*  @copyright 2002-2018 Jetimpex
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div id="_desktop_compare">
  <div class="compare-header"
    data-refresh-url="{$jxcompareproduct_url}"
    data-compare-max="{$jxcompareproduct_max}"
    data-empty-text="{l s='No products to compare' mod='jxcompareproduct'}"
    data-max-alert-message="{l s='Only' mod='jxcompareproduct'} {$jxcompareproduct_max} {l s="products can be compared" mod='jxcompareproduct'}">
    <a href="#" class="compare-products d-flex align-items-center"><i class="fa fa-clone mr-1" aria-hidden="true"></i><span>{l s="Compare" mod="jxcompareproduct"}(</span><span class="compare-counter"></span><span>)</span></a>
  </div>
</div>
