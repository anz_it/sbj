<div class="mainnav" id="mainnav">
  {assign var=_counter value=0}
  {function name="menu" nodes=[] depth=0 parent=null}
  {if $nodes|count}
  <ul {if $depth == 0} class="menu" id="main-menu" {/if} {if $depth == 1} class="sub-menu" {/if} data-depth="{$depth}">
    {foreach from=$nodes item=node key=index}
        <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}" style="{if $depth == 0 && $index == 4} max-width: 192px; {/if} {if $depth == 0 && $index == 6} max-width: 98px; {/if}">
        {assign var=_counter value=$_counter+1}
          <a
            class="{if $depth === 1} dropdown-submenu{/if}"
            {if $node.children|count} href="javascript:;" {else} href="{$node.url}" {/if}
            data-depth="{$depth}"
            {if $node.open_in_new_window} target="_blank" {/if}
          >
            {$node.label nofilter}
            {if $node.children|count}
              {* Cannot use page identifier as we can have the same page several times *}
              {assign var=_expand_id value=10|mt_rand:100000}
              <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse">
                <i class="fa fa-angle-down ml-1" aria-hidden="true"></i>
              </span>
            {/if}
          </a>
          {if $node.children|count}
          <div {if $depth === 0} class="popover collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
            {menu nodes=$node.children depth=$node.depth parent=$node}
          </div>
          {/if}
        </li>
    {/foreach}
  </ul>
  {/if}
  {/function}
  
  {menu nodes=$menu.children}
</div>  
