<div id="blockcart-modal" class="modal fade modal-close-inside" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <button type="button" class="close fa fa-times" data-dismiss="modal" aria-label="Close" aria-hidden="true"></button>
      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-lg-6 pt-2">
            <h4 class="products-section-title text-left"><i class="fa fa-check rtl-no-flip" aria-hidden="true"></i>{l s='Product successfully added to your shopping cart' d='Shop.Theme.Checkout'}</h4>
            <div class="product-thumbnail">
              <img class="img-fluid" src="{$product.cover.bySize.home_default.url}" alt="{$product.name}"/>
            </div>
            <div class="media-body">
              <h3 class="product-title"><a href="{$product.url}" title="{$product.name}">{$product.name}</a></h3>
              <div class="list-inline-separated">
                {foreach from=$product.attributes item="property_value" key="property"}
                  <small>{$property}: {$property_value}</small>
                {/foreach}
              </div>
              <div class="product-quantity">{l s='Quantity' d='Shop.Theme.Checkout'}: {$product.cart_quantity}</div>
              <div class="product-prices-md"><span class="price">{$product.total}</span></div>
            </div>

          </div>
          <div class="col-12 pt-2 col-lg-6 d-flex flex-column justify-content-between align-content-between">
            <div>
              {if $cart.products_count > 1}
                <h4 class="products-section-title text-left">{l s='There are %products_count% items in your cart.' sprintf=['%products_count%' => $cart.products_count] d='Shop.Theme.Checkout'}</h4>
              {else}
                <h4 class="products-section-title text-left">{l s='There is %product_count% item in your cart.' sprintf=['%product_count%' =>$cart.products_count] d='Shop.Theme.Checkout'}</h4>
              {/if}
              {foreach from=$cart.subtotals item="subtotal"}
                <div class="modal-cart-{$subtotal.type} d-flex justify-content-between my-1">
                  <span class="label text-uppercase">{$subtotal.label}</span>
                  <span class="value">{$subtotal.value}</span>
                </div>
              {/foreach}
              <hr>
              <div class="modal-cart-total d-flex justify-content-between">
                <span class="label text-uppercase">{$cart.totals.total.label}</span>
                <strong class="value">{$cart.totals.total.value}
                  <small>{$cart.labels.tax_short}</small>
                </strong>
              </div>
            </div>
            <div class="d-flex justify-content-center my-2">
              <button type="button" class="btn btn-secondary d-none d-md-block mx-1" data-dismiss="modal">{l s='Continue shopping' d='Shop.Theme.Actions'}</button>
              <a class="btn btn-primary mx-1" href="{$cart_url}" title="{l s='Proceed to checkout' d='Shop.Theme.Actions'}">{l s='Proceed to checkout' d='Shop.Theme.Actions'}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
