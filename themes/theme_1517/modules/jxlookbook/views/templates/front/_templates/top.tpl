{**
* 2002-2018 Zemez
*
* JX Header Account Block
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez
*  @copyright 2002-2018 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{if count($tabs) > 0}
  <div class="lookbook-default jx-lookbook-block mt-3 jx-lookbook-slider-top swiper-container">
    <div class="swiper-wrapper">
      {foreach from=$tabs item=tab name=tab}
        <div class="lookbook-tab swiper-slide" data-id="{$tab.id_tab|escape:'htmlall':'UTF-8'}">

          <div class="descr">
            <p>{$tab.description nofilter}</p>
          </div>

          <div class="hotSpotWrap d-block hotSpotWrap_{$tab.id_tab|escape:'htmlall':'UTF-8'}_{$smarty.foreach.tab.iteration|escape:'htmlall':'UTF-8'}">
            <img class="img-fluid w-100" src="{$base_url|escape:'htmlall':'UTF-8'}{$tab.image|escape:'htmlall':'UTF-8'}" style="max-width:100%" alt="">

            <div class="caption">
              <h3>{$tab.name|escape:'html':'UTF-8'}</h3>
            </div>

          </div>

          <div class="row">
            <div class="col-sm-12">
              {if isset($tab.products) && $tab.products}
                {assign var=products value=$tab.products}
                {include '../product-list.tpl'}
              {/if}
            </div>
          </div>

        </div>
      {/foreach}
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
{else}
  <div class="alert alert-warning" role="alert">
    {l s='No one tabs added'}
  </div>
{/if}