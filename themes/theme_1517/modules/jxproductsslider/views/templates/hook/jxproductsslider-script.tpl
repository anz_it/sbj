{*
* 2002-2018 Zemez
*
* JX Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     Zemez
* @copyright  2002-2018 Zemez
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $settings.slider_type == standard}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          {if ($settings.standard_extended_settings && $settings.standard_slider_autoplay) || !$settings.standard_extended_settings}
          autoplay: {
            delay: 8000,
          },
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_loop) || !$settings.standard_extended_settings}
          loop: true,
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_loop && $settings.standard_slider_thumbnails) || !$settings.standard_extended_settings}
          loopedSlides: 5,
          {elseif $settings.standard_extended_settings && !$settings.standard_slider_loop && $settings.standard_slider_thumbnails}
          initialSlide: 1,
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_navigation) || !$settings.standard_extended_settings}
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_pagination) || !$settings.standard_extended_settings}
          pagination: {
            el: '.swiper-pagination',
            renderBullet: function (index, className) {
              return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
            clickable: true
          },
          {/if}
          preloadImages: true,
          lazy: true,
          effect: 'fade',
          fadeEffect: {
            crossFade: true
          },
          speed: {$settings.standard_slider_duration|escape:'htmlall':'UTF-8'},
          breakpoints: {
            1200: {
              autoHeight: true
            }
          }
        });
        {if ($settings.standard_extended_settings && $settings.standard_slider_autoplay) || !$settings.standard_extended_settings}
        $(this).parent().hover(function () {
          jx_products_slider.autoplay.stop();
        }, function () {
          jx_products_slider.autoplay.start();
        });
        {/if}
        {if ($settings.standard_extended_settings && $settings.standard_images_gallery)}
        $(this).find('.jxpr-inner-slider').each(function () {
          var jx_products_slider_inner = new Swiper($(this), {
            loop: true,
            loopedSlides: 5,
            observer: true,
            nested: true,
            lazy: true,
            onSlideChangeStart: function () {
              var activeSlide = this.activeIndex;
              var activeParentSlide = this.$el.parents('.swiper-slide').attr('data-swiper-slide-index');
              this.$el.parents('.swiper-wrapper').children('.swiper-slide[data-swiper-slide-index="' + activeParentSlide + '"]').find('.jxpr-inner-slider').attr('data-init-slide', activeSlide);
            },
            onObserverUpdate: function (swiper) {
              var goToSlide = swiper.container.attr('data-init-slide');
              swiper.slideTo(goToSlide, 500, false);
            }
          });
          var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
            slidesPerView: 5,
            spaceBetween: 10,
            touchRatio: 0.2,
            slideToClickedSlide: true,
            nested: true,
            lazy: true,
          });
          jx_products_slider_inner.controller.control = galleryThumbsInner;
          galleryThumbsInner.controller.control = jx_products_slider_inner;
        });
        {/if}
        {if ($settings.standard_extended_settings && $settings.standard_slider_thumbnails)}
        var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails'), {
          slideToClickedSlide: true,
          grabCursor: true,
          touchRatio: 0.2,
          slidesPerView: 3,
          spaceBetween: 20,
          lazy: true,
          {if ($settings.standard_extended_settings && $settings.standard_slider_loop && $settings.standard_slider_thumbnails) || !$settings.standard_extended_settings}
          loop: true,
          loopedSlides: 5
          {elseif $settings.standard_extended_settings && !$settings.standard_slider_loop && $settings.standard_slider_thumbnails}
          initialSlide: 1,
          centeredSlides: true
          {/if}
        });
        jx_products_slider.controller.control = galleryThumbs;
        galleryThumbs.controller.control = jx_products_slider;
        {/if}
      });
    });
  </script>
{elseif $settings.slider_type == list}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          effect: 'fade',
          fadeEffect: {
            crossFade: true
          },
          {if ($settings.list_extended_settings && $settings.list_slider_autoplay) || $settings.list_extended_settings}
          autoplay: {
            delay: 8000,
          },
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_loop) || !$settings.list_extended_settings}
          loop: true,
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_loop && $settings.list_slider_thumbnails) || !$settings.list_extended_settings}
          loopedSlides: 3,
          {elseif $settings.list_extended_settings && !$settings.list_slider_loop && !$settings.list_slider_thumbnails}
          initialSlide: 1,
          {/if}
          {if ($settings.list_extended_settings || $settings.list_slider_navigation) && !$settings.list_extended_settings}
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_pagination) && $settings.list_extended_settings}
          pagination: {
            el: '.swiper-pagination',
            renderBullet: function (index, className) {
              return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
            clickable: true
          },
          {/if}
          speed: {$settings.list_slider_duration|escape:'htmlall':'UTF-8'}
        });
        {if ($settings.list_extended_settings && $settings.list_slider_autoplay) || !$settings.list_extended_settings}
        $(this).parent().hover(function () {
          jx_products_slider.autoplay.stop();
        }, function () {
          jx_products_slider.autoplay.start();
        });
        {/if}
        {if ($settings.list_extended_settings || $settings.list_images_gallery) && $settings.list_extended_settings}
        $(this).find('.jxpr-inner-slider').each(function () {
          var jx_products_slider_inner = new Swiper($(this), {
            loop: true,
            loopedSlides: 8,
            observer: true,
            nested: true,
            onSlideChangeStart: function (swiper) {
              var activeSlide = swiper.activeIndex;
              var activeParentSlide = swiper.container.parents('.swiper-slide').attr('data-swiper-slide-index');
              swiper.container.parents('.swiper-wrapper').children('.swiper-slide[data-swiper-slide-index="' + activeParentSlide + '"]').find('.jxpr-inner-slider').attr('data-init-slide', activeSlide);
            },
            onObserverUpdate: function (swiper) {
              var goToSlide = swiper.container.attr('data-init-slide');
              swiper.slideTo(goToSlide, 500, false);
            }
          });
          var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
            slidesPerView: 4,
            spaceBetween: 10,
            touchRatio: 0.2,
            loop: true,
            loopedSlides: 8,
            slideToClickedSlide: true,
            nested: true,
          });
          jx_products_slider_inner.controller.control = galleryThumbsInner;
          galleryThumbsInner.controller.control = jx_products_slider_inner;
        });
        {/if}
        {if ($settings.list_extended_settings && $settings.list_slider_thumbnails) || !$settings.list_extended_settings}
        var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails').find('.swiper-container'), {
          slideToClickedSlide: true,
          grabCursor: true,
          mousewheelControl: true,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          direction: 'vertical',
          spaceBetween: 20,
          touchRatio: 0.2,
          slidesPerView: 3,
          autoHeight     : true,
          freeMode       : false,
          loop: true,
          on             : {
            init   : function() {
              var swiper = this;
              $(swiper.$wrapperEl[0]).css('min-height', 0).children('.swiper-slide').css('min-height', 0);
              var verticalHeightArray = $(swiper.$wrapperEl[0]).children('.swiper-slide').map(function() {
                return $(this).height();
              }).get();
              var maxVerticalHeight   = Math.max.apply(null, verticalHeightArray);
              $(swiper.$wrapperEl[0]).css('min-height', (maxVerticalHeight * 3)).children('.swiper-slide').css('min-height', (maxVerticalHeight));
              swiper.update();
            },
            resize : function() {
              var swiper = this;
              $(swiper.$wrapperEl[0]).css('min-height', 0).children('.swiper-slide').css('min-height', 0);
              var verticalHeightArray = $(swiper.$wrapperEl[0]).children('.swiper-slide').map(function() {
                return $(this).height();
              }).get();
              var maxVerticalHeight   = Math.max.apply(null, verticalHeightArray);
              $(swiper.$wrapperEl[0]).css('min-height', (maxVerticalHeight * 3)).children('.swiper-slide').css('min-height', (maxVerticalHeight));
              swiper.update();
            }
          }
        });
        jx_products_slider.controller.control = galleryThumbs;
        galleryThumbs.controller.control = jx_products_slider;
        {/if}
      });
    });
  </script>
{elseif $settings.slider_type == grid}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          preloadImages: true,
          lazy: true,
          {if ($settings.grid_extended_settings && $settings.grid_slider_autoplay) || !$settings.grid_extended_settings}
          autoplay: {
            delay: 8000,
          },
          {/if}
          {if ($settings.grid_extended_settings && $settings.grid_slider_loop && !$settings.grid_slider_thumbnails)}
          loop: true,
          {/if}
          {if ($settings.grid_extended_settings || $settings.grid_slider_pagination) || !$settings.grid_extended_settings}
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          {/if}
          {if ($settings.grid_extended_settings || $settings.grid_slider_pagination) || !$settings.grid_extended_settings}
          pagination: {
            el: '.swiper-pagination',
            renderBullet: function (index, className) {
              return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
            clickable: true
          },
          {/if}
          speed: {$settings.grid_slider_duration|escape:'htmlall':'UTF-8'},
          {if ($settings.grid_extended_settings && $settings.grid_slider_thumbnails) || !$settings.grid_extended_settings}
          on:{
            slideChangeTransitionEnd: function(){
              if (galleryThumbs) {
                console.log(galleryThumbs)
                galleryThumbs.$el.find('.swiper-slide').removeClass('grid-swiper-active').eq(jx_products_slider.activeIndex).addClass('grid-swiper-active');
                galleryThumbs.slideTo(this.activeIndex);
              }
            }
          },
          {/if}
        });
        {if ($settings.grid_extended_settings && $settings.grid_slider_autoplay) || !$settings.grid_extended_settings}
        $(this).parent().hover(function () {
          jx_products_slider.autoplay.stop();
        }, function () {
          jx_products_slider.autoplay.start();
        });
        {/if}
        {if $settings.grid_extended_settings || $settings.grid_images_gallery}
        $(this).find('.jxpr-inner-slider').each(function () {
          var jx_products_slider_inner = new Swiper($(this), {
            loop: true,
            preloadImages: true,
            lazy: true,
            loopedSlides: 8,
            observer: true,
            nested: true,
            on:{
              slideChangeTransitionStart: function () {
                var activeSlide = this.activeIndex;
                var activeParentSlide = this.$el.parents('.swiper-slide').attr('data-swiper-slide-index');
                this.$el.parents('.swiper-wrapper').children('.swiper-slide[data-swiper-slide-index="' + activeParentSlide + '"]').find('.jxpr-inner-slider').attr('data-init-slide', activeSlide);
              }
            },

            onObserverUpdate: function (swiper) {
              var goToSlide = swiper.$el.attr('data-init-slide');
              swiper.slideTo(goToSlide, 500, false);
            }
          });
          var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
            slidesPerView: 4,
            spaceBetween: 10,
            touchRatio: 0.2,
            loop: true,
            loopedSlides: 8,
            slideToClickedSlide: true,
            nested: true,
            preloadImages: true,
            lazy: true
          });
          jx_products_slider_inner.controller.control = galleryThumbsInner;
          galleryThumbsInner.controller.control = jx_products_slider_inner;
        });
        {/if}
        {if ($settings.grid_extended_settings && $settings.grid_slider_thumbnails) || !$settings.grid_extended_settings}
        var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails'), {
          slidesPerView: 3,
          direction: 'vertical',
          spaceBetween: 0,
          slidesPerColumn: 3,
          slidesPerColumnFill: 'column',
          slidesPerGroup: 3,
          on: {
            click: function () {
               jx_products_slider.slideTo(galleryThumbs.clickedIndex);
            },
          },
        });
        {/if}
      });
    });
  </script>
{elseif $settings.slider_type == fullwidth}
   <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_autoplay) && $settings.fullwidth_extended_settings}
          autoplay: {
            delay: 8000,
          },
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop) || !$settings.fullwidth_extended_settings}
          loop: true,
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails) || !$settings.fullwidth_extended_settings}
          loopedSlides: 5,
          {elseif $settings.fullwidth_extended_settings && !$settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails}
          initialSlide: 1,
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_navigation) || !$settings.fullwidth_extended_settings}
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_pagination) || !$settings.fullwidth_extended_settings}
          pagination: {
            el: '.swiper-pagination',
            renderBullet: function (index, className) {
              return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
            clickable: true
          },
          {/if}
          speed: {$settings.fullwidth_slider_duration|escape:'htmlall':'UTF-8'}
        });

        {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_autoplay) || !$settings.fullwidth_extended_settings}
           $(this).parent().hover(function () {
             jx_products_slider.autoplay.stop();
           }, function () {
            jx_products_slider.autoplay.start();
           });
        {/if}

        {if ($settings.fullwidth_extended_settings && $settings.fullwidth_images_gallery) || !$settings.fullwidth_extended_settings}
           $(this).find('.jxpr-inner-slider').each(function () {
          let jx_products_slider_inner = new Swiper($(this), {
            loop: true,
            observer: true,
            nested: true,
            loopedSlides: 4,
            onSlideChangeStart: function (swiper) {
              var activeSlide = swiper.activeIndex;
              var activeParentSlide = swiper.container.parents('.swiper-slide').attr('data-swiper-slide-index');
              swiper.container.parents('.swiper-wrapper').children('.swiper-slide[data-swiper-slide-index="' + activeParentSlide + '"]').find('.jxpr-inner-slider').attr('data-init-slide', activeSlide);
            },
            onObserverUpdate: function (swiper) {
              var goToSlide = swiper.container.attr('data-init-slide');
              swiper.slideTo(goToSlide, 500, false);
            }
          });

          let galleryThumbsInner = new Swiper($(this).closest('.swiper-slide').find('.swiper-thumbnails-gallery'), {
            slidesPerView: 4,
            spaceBetween: 20,
            touchRatio: 0.2,
            slideToClickedSlide: true,
            loop: true,
            nested: true,
            loopedSlides: 4
          });

          jx_products_slider_inner.controller.control = galleryThumbsInner;
          galleryThumbsInner.controller.control = jx_products_slider_inner;
        });
        {/if}

        {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_thumbnails) && $settings.fullwidth_extended_settings}
          let galleryThumbs = new Swiper($(this).next('.swiper-thumbnails'), {
           slideToClickedSlide: true,
           grabCursor: true,
           touchRatio: 0.2,
           slidesPerView: 3,
           spaceBetween: 20,
           {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails) || !$settings.fullwidth_extended_settings}
           loop: true,
           loopedSlides: 4
           {elseif $settings.fullwidth_extended_settings && !$settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails}
           initialSlide: 1,
           centeredSlides: true
           {/if}
          });
          jx_products_slider.controller.control = galleryThumbs;
          galleryThumbs.controller.control = jx_products_slider;
        {/if}
      });
    });
  </script>
{/if}