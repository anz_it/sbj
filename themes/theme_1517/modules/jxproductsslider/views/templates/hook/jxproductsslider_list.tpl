{*
* 2002-2018 Zemez
*
* JX Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     Zemez
* @copyright  2002-2018 Zemez
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{*-------Information list-------*}
{$settings.new_sale_labels = true}             {*display "New Sale labels" in slide info*}
{$settings.product_name = true}                 {*display "Product name" in slide info*}
{$settings.description_short = false}           {*display "Description short" in slide info*}
{$settings.description = true}                  {*display "Description" in slide info*}
{$settings.features = false}                    {*display "Features" in slide info*}
{$settings.prices = true}                       {*display "Prices" in slide info*}
{$settings.cart_button = true}                  {*display "Add to cart button" in slide info*}
{$settings.more_button = false}                  {*display "Read more button" in slide info*}

{*$hook_name|@var_dump*}
{if isset($slides) && $slides}
  <div class="jx-products-slider-wrapper list mt-2 mb-3 py-3 container">
    <div class="jx-products-slider swiper-container">
      <div class="swiper-wrapper">
        {foreach from=$slides item=slide}
          <div class="swiper-slide">
            <div class="row no-gutters">
              {if isset($slide.info_array.images) && $slide.info_array.images}
                <div class="slide-image col-45 col-md-5">
                  <div class="slide-image-wrap">
                    {if ($settings.list_extended_settings || $settings.list_images_gallery) && $settings.list_extended_settings}
                      {if isset($slide.info_array.images) && $slide.info_array.images}
                        <div class="jxpr-inner-slider swiper-container" data-init-slide="0">
                          <div class="swiper-wrapper">
                            {foreach from=$slide.images item=img}
                              <div class="swiper-slide">
                                <img class="img-fluid w-100" src="{$link->getImageLink($slide.info_array.name, $img.id_image, 'large_default')|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}"/>
                              </div>
                            {/foreach}
                          </div>
                        </div>
                        <div class="swiper-thumbnails swiper-container mt-1">
                          <div class="swiper-wrapper">
                            {foreach from=$slide.images item=img}
                              <div class="swiper-slide ">
                                <img class="img-fluid w-100"
                                     src="{$link->getImageLink($slide.info_array.name, $img.id_image, 'medium_default')|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}"/>
                              </div>
                            {/foreach}
                          </div>
                        </div>
                      {/if}
                    {else}
                      <a class="slide-image" href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" title="{$slide.info_array.name|escape:'htmlall':'UTF-8'}">
                        <img class="img-fluid w-100"
                           src="{$slide.info_array.cover.bySize.large_default.url|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}"/>
                      </a>
                    {/if}
                  </div>
                </div>
              {/if}
              <div class="slide-info align-items-center d-flex {if $slide.info_array.images}col-12 col-md-7{else}col-12{/if}">
                <div class="slide-info-wrap js-product-miniature" data-id-product="{$slide.info_array.id_product}" data-id-product-attribute="{$slide.info_array.id_product_attribute}">

                  {if $settings.product_name}
                    <h2 class="mt-0 h2 text-capitalize"><a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}">{$slide.info_array.name|escape:'htmlall':'UTF-8'}</a></h2>{/if}
                  {if $slide.info_array.description_short && $settings.description_short}
                    <p class="slide-description des-short">{$slide.info_array.description_short|strip_tags:true|truncate:130:'...':false|escape:'htmlall':'UTF-8'}</p>
                  {/if}
                  {if $slide.info_array.description && $settings.description}
                    <p class="slide-description d-none d-lg-block">{$slide.info_array.description|strip_tags:true|truncate:230:'...':false|escape:'htmlall':'UTF-8'}</p>
                  {/if}
                  {if isset($slide.info_array.features) && $slide.info_array.features && $settings.features}
                    <div class="slide-product-features mb-1">
                      {foreach from=$slide.info_array.features item=feature}
                        <small class="mr-1"><span>{$feature.name|escape:'htmlall':'UTF-8'}:</span> {$feature.value|escape:'htmlall':'UTF-8'}</small>
                      {/foreach}
                    </div>
                  {/if}
                  {if $slide.info_array.price && $slide.info_array.show_price && $settings.prices && !isset($restricted_country_mode)}
                    <div class="product-price mb-2">
                      {block name='product_price_and_shipping'}
                        <div class="product-price-and-shipping">
                          {if $slide.info_array.has_discount}
                            {hook h='displayProductPriceBlock' product=$slide.info_array type="old_price"}
                          {/if}
                          {hook h='displayProductPriceBlock' product=$slide.info_array type="before_price"}
                          <span class="product-price h5">{$slide.info_array.price}</span>
                          {if $slide.info_array.has_discount}
                            <span class="product-price product-price-old">{$slide.info_array.regular_price}</span>
                            {if $slide.info_array.discount_type === 'percentage'}
                              <span class="product-price product-price-reduction">{$slide.info_array.discount_percentage}</span>
                            {/if}
                          {/if}
                          {hook h='displayProductPriceBlock' product=$slide.info_array type='unit_price'}
                          {hook h='displayProductPriceBlock' product=$slide.info_array type='weight'}
                        </div>
                      {/block}
                    </div>
                  {/if}
                  {if $settings.more_button || $settings.cart_button}
                    <div class="buttons-container">
                      {if $slide.info_array.embedded_attributes.available_for_order && !isset($restricted_country_mode) && $settings.cart_button}
                        {if (!isset($slide.info_array.embedded_attributes.customization_required) || !$slide.info_array.embedded_attributes.customization_required) && $slide.info_array.embedded_attributes.quantity_all_versions > 0}
                          <a class="ajax_add_to_cart_button btn btn-primary btn-lg cart-button"
                             href="{$slide.info_array.add_to_cart_url}"
                             title="{l s='Add to cart' mod='jxproductsslider'}"
                             data-minimal_quantity="{$slide.info_array.embedded_attributes.minimal_quantity|intval}"
                             data-id-product-attribute="{$slide.info_array.id_product_attribute}"
                             data-link-action="add-to-cart">
                            <span>{l s='Add to cart' mod='jxproductsslider'}</span>
                          </a>
                        {else}
                          <a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" class="btn btn-primary btn-lg cart-button">
                            <span>{l s='Add to cart' mod='jxproductsslider'}</span>
                          </a>
                        {/if}
                      {/if}
                      {if $settings.more_button}
                        <a href="{$slide.info_array.url|escape:'htmlall':'UTF-8'}" class="btn lnk_view btn btn-secondary">
                          <span>{l s='Read More' mod='jxproductsslider'}</span>
                        </a>
                      {/if}
                    </div>
                  {/if}
                </div>
              </div>
            </div>
          </div>
        {/foreach}
      </div>
      {if ($settings.list_extended_settings || $settings.list_slider_navigation) && !$settings.list_extended_settings}
        <div class="swiper-button-prev d-xl-none"></div>
        <div class="swiper-button-next d-xl-none"></div>
      {/if}
      {if ($settings.list_extended_settings || $settings.list_slider_pagination) && $settings.list_extended_settings}
        <div class="swiper-pagination my-1"></div>
      {/if}
    </div>
    {if ($settings.list_extended_settings && $settings.list_slider_thumbnails) || !$settings.list_extended_settings}
      <div class="swiper-thumbnails d-none d-xl-block">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            {foreach from=$slides item=slide}
              <div class="swiper-slide">
                <div class="product-thumbnail">
                  <img class="img-fluid mr-lg-2"  src="{$slide.info_array.cover.bySize.small_default.url|escape:'htmlall':'UTF-8'}" alt="{$slide.info_array.name|escape:'htmlall':'UTF-8'}"/>
                  <div>
                    {if $settings.product_name}
                      <h6 class="text-capitalize mb-0">{$slide.info_array.name|escape:'htmlall':'UTF-8'}</h6>
                    {/if}
                    {block name='product_price_and_shipping'}
                      <div class="product-price-and-shipping">
                        {hook h='displayProductPriceBlock' product=$slide.info_array type="before_price"}
                        <span class="product-price h5">{$slide.info_array.price}</span>
                        {hook h='displayProductPriceBlock' product=$slide.info_array type='unit_price'}
                        {hook h='displayProductPriceBlock' product=$slide.info_array type='weight'}
                      </div>
                    {/block}
                  </div>
                </div>
              </div>
            {/foreach}
          </div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </div>
    {/if}
  </div>
{/if}

