{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 
<li class="d-none d-lg-block desktop_language_selector">
  <div class="language-selector dropdown">
    <a id="language-selector" data-toggle="dropdown" class="d-none d-md-block dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false" aria-label="{l s='Language dropdown' d='Shop.Theme.Global'}">
      <span class="expand-more icon-wrapper"><i class="fa fa-globe"></i></span>
    </a>
    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="language-selector">
      {foreach from=$languages item=language}
        <li class="dropdown-item">
          <a href="{url entity='language' id=$language.id_lang}{if 'selected_filters'|array_key_exists:$smarty.get}?selected_filters=xem-chi-tiet{/if}" class="dropdown-item{if $language.id_lang == $current_language.id_lang} active{/if}">
          <img src="/img/{$language.iso_code}.png" alt="{$language.name_simple}"> {$language.name_simple}
          </a>
        </li>
      {/foreach}
    </ul>
  </div>
</li>
