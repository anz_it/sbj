<div class="header-cart">
    <a class="header-cart-icon" href="javascript:void(0)">{l s='Cart' d='Shop.Theme.Checkout'} <span>({$cart.products_count})</span>
    </a>
</div>

<div class="cart-box blockcart-slidebar">
    <button class="closeSlidebar fa fa-times" type="button"></button>
    <h4 class="cart-summary-header mb-4">{l s='Cart' d='Shop.Theme.Checkout'}</h4>
    <ul class="cart-product-list">
        {foreach from=$cart.products item=product}
            <li class="cart-product-item">
                <div class="media"><a class="cart-product-thumbnail mr-3" href="{$product.url}">
                        <img class="img-fluid" src="{$product.cover.bySize.small_default.url}"
                             alt="{$product.name}"></a>
                    <div class="media-body">
                        <a class="remove-from-cart close" href="{$product.remove_from_cart_url}">
                            <span aria-hidden="true">×</span>
                        </a>
                        <h3 class="h6 cart-product-title">{$product.name}</h3>
                        {if $product.attributes}
                            <div class="product-attributes">
                                {foreach from=$product.attributes name='myloop' item='attribute'}
                                    <small>{if $smarty.foreach.myloop.iteration > 1}-{/if}{$attribute}</small>{/foreach}
                            </div>
                        {/if}
                        <div class="price"><span class="new-price">{$product.price|replace:',':'.'} </span></div>
                        <div class="product-quantity">{l s='Quantity' d='Shop.Theme.Checkout'}: {$product.quantity}</div>
                    </div>
                </div>
            </li>
            <hr>
        {/foreach}

    </ul>
    <div class="cart-subtotals">
        {foreach from=$cart.subtotals item="subtotal"}
            {if isset($subtotal) && $subtotal}
                <div class="cart-{$subtotal.type} d-flex flex-wrap justify-content-between">
                    <span class="label">{$subtotal.label}</span>
                    <span class="value">{$subtotal.value|replace:',':'.'}</span>
                    {if $subtotal.type == 'discount'}
                        {if $cart.vouchers.added}
                            <ul class="list-group mb-2 w-100">
                                {foreach from=$cart.vouchers.added item='voucher'}
                                    <li class="list-group-item d-flex flex-wrap justify-content-between">
                                        <span>{$voucher.name}({$voucher.reduction_formatted})</span><a
                                                data-link-action="remove-voucher" href="{$voucher.delete_url}"
                                                class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </a>
                                    </li>
                                {/foreach}
                            </ul>
                        {/if}
                    {/if}
                </div>
            {/if}
        {/foreach}
    </div>
    <hr>
    <div class="cart-total d-flex flex-wrap justify-content-between my-3">
        <strong class="label">{$cart.totals.total.label}</strong>
        <strong class="value">{$cart.totals.total.value|replace:',':'.'}</strong>
    </div>
    <div class="cart-footer py-3"><a class="btn btn-red d-md-block text-uppercase" href="{$cart_url}"
                                     title="Thanh toán">{l s='Thanh toán' d='Shop.Theme.Checkout'}</a></div>
</div>