{*
* 2018 Zemez
*
* JX Blog Post Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez (Alexander Grosul)
*  @copyright 2018 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $related_posts}
  <div class="row m-0">
    {foreach from=$related_posts item='post'}
      <div class="col-6 my-3">
        <div class="row align-items-center">
          <div class="col-12 col-md-4">
            <div class="img-wrapper mr-md-2">
              <img class="img-fluid" src="{JXBlogImageManager::getImage('post_thumb', $post.id_jxblog_post, 'post_listing')}" alt="{$post.name}">
            </div>
          </div>
          <div class="col-12 col-md-8 py-1">
            <a href="{$post.url}" title="{$post.name}">{$post.name}</a>
          </div>
        </div>
      </div>
    {/foreach}
  </div>
{/if}

