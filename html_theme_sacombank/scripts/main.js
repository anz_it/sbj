(function ($) {
    'use strict';
    // **********************************************************************//
    // ! Init variables global
    // **********************************************************************//
    var pageSection = $('.slide-bg-image');
    pageSection.each(function (indx) {
      if ($(this).attr('data-background-img')) {
        $(this).css('background-image', 'url(' + $(this).data('background-img') + ')');
      }
      if ($(this).attr('data-bg-position-x')) {
        $(this).css('background-position', $(this).data('bg-position-x'));
      }
      if ($(this).attr('data-height')) {
        $(this).css('height', $(this).data('height') + 'px');
      }
    });

    function slide_home(){
      $('.slide-home').slick({
        fade:true,
        autoplay:true,
        dots:true,
        arrows:true
      })
    }

    function banner_inner(){
      if ($('.banner-inner .item-img').length ==1){           
        $('.banner-inner').slick({
          fade:true,
          autoplay:true,
          dots:false,
          arrows:false        
        })
      }
      else{         
        $('.banner-inner').slick({
          fade:true,
          autoplay:true,
          dots:true,
          arrows:false        
        })
      }
    }

    

    function slide_products(){
      $('.slide-products').slick({
          arrows: true,
          dots: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          cssEase: 'linear',
          variableWidth:true,
          activateRows: true,
           responsive: [
                {
                  breakpoint: 360,
                  settings: {
                    rows:2,
                    slidesPerRow: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth:false
                  }
                }
            ]

      });
    }

    function slide_other_products(){
      $('.slide-other-products').slick({
          arrows: true,
          dots: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          cssEase: 'linear',
          variableWidth:true,
          activateRows: true,
           responsive: [
                {
                  breakpoint: 360,
                  settings: {
                    slidesPerRow: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth:false
                  }
                }
            ]

      });
    }

    function slide_prodcuts_sale(){
      $('.slide-prodcuts-sale').slick({
          arrows: true,
          dots: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          cssEase: 'linear',
          variableWidth:true,
           responsive: [
                {
                  breakpoint: 360,
                  settings: {
                    rows:2,
                    slidesPerRow: 1,
                    slidesToScroll: 1,
                    variableWidth:false
                  }
                }
            ]

      });
    }
    

    function slide_products_small(){
      $('.slide-product-small').slick({
          arrows: true,
          dots: false,
          rows:2,
          slidesPerRow: 3,
          slidesToScroll: 1,
          infinite: true,
          cssEase: 'linear',
          variableWidth:false,
           responsive: [
                {
                  breakpoint: 767,
                  settings: {
                    slidesPerRow: 2
                  }
                },
                {
                  breakpoint: 360,
                  settings: {
                    rows:2,
                    slidesPerRow: 1
                  }
                }
            ]

      });
    }

    function slide_collection(){
      $('.slide-collection').slick({
          arrows: false,
          dots: true,
          slidesPerRow: 1,
          slidesToScroll: 1,
          infinite: true,
          cssEase: 'linear',
          autoplay:true,
          fade:true
      });
    }
    

    function slide_gifts(){
      $('.slide-gifts').slick({
          arrows: true,
          dots: false,
          rows:2,
          slidesPerRow: 3,
          slidesToScroll: 1,
          infinite: true,
          cssEase: 'linear',
          variableWidth:false,
          responsive: [
                {
                  breakpoint: 767,
                  settings: {
                    slidesPerRow: 2
                  }
                },
                {
                  breakpoint: 567,
                  settings: {
                    rows:2,
                    slidesPerRow: 1
                  }
                }
            ],
        nextArrow: '.custom .slick-next',
        prevArrow: '.custom .slick-prev'
      });
    }
    
    
    var init_header = function() {
      var largeScreen = matchMedia('only screen and (min-width: 992px)').matches;
      if( largeScreen ) {
          if( $().sticky ) {
              $('header.header-sticky').sticky();
          }
      }
    };
    
    //Menu
    var ResponsiveMenu =  {
        menuType: 'desktop',
        initial: function(winWidth) {
            ResponsiveMenu.menuWidthDetect(winWidth);
            ResponsiveMenu.menuBtnClick();
            ResponsiveMenu.parentMenuClick();
        },
        menuWidthDetect: function(winWidth) {
            var currMenuType = 'desktop';
            if (matchMedia('only screen and (max-width: 991px)').matches) {
                currMenuType = 'mobile';
            }
            if ( currMenuType !== ResponsiveMenu.menuType ) {
                ResponsiveMenu.menuType = currMenuType;
                if ( currMenuType === 'mobile' ) {
                    var $mobileMenu = $('#mainnav').attr('id', 'mainnav-mobi').hide();
                    $('#header').find('.header-wrap').after($mobileMenu);
                    var hasChildMenu = $('#mainnav-mobi').find('li:has(ul)');
                    hasChildMenu.children('ul').hide();
                    hasChildMenu.children('a').after('<span class="btn-submenu"></span>');
                    $('.navbar-toogle').removeClass('active');
                } else {
                    var $desktopMenu = $('#mainnav-mobi').attr('id', 'mainnav').removeAttr('style');
                    $desktopMenu.find('.sub-menu').removeAttr('style');
                    $('#header').find('.navbar-toogle').after($desktopMenu);
                    $('.btn-submenu').remove();
                }
            } // clone and insert menu
        },
        menuBtnClick: function() {
            $('.navbar-toogle').on('click', function() {
                $('#mainnav-mobi').slideToggle(300);
                $(this).toggleClass('active');
            });
        }, // click on moblie button
        parentMenuClick: function() {
            $(document).on('click', '#mainnav-mobi li .btn-submenu', function(e) {
                if ( $(this).has('ul') ) {
                    e.stopImmediatePropagation()
                    $(this).next('ul').slideToggle(300);
                    $(this).toggleClass('active');
                }
            });
        } // click on sub-menu button
    };

    //getValue Qty
    var getValue = function () {
      $('.qty_inc').on('click', function () {
        var $qty = $(this).closest('.qty-holder').find('.input-qty');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal)) {
          $qty.val(currentVal + 1);
        }
      });

      $('.qty_dec').on('click', function () {
        var $qty = $(this).closest('.qty-holder').find('.input-qty');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal) && currentVal > 0) {
          $qty.val(currentVal - 1);
        }
      });
    };

    // **********************************************************************//
    // ! Window ready
    // **********************************************************************//
    $(document).ready(function () {
      init_header();
      ResponsiveMenu.initial($(window).width());
      $(window).resize(function() {
          ResponsiveMenu.menuWidthDetect($(this).width());
      });
      
      slide_home();
      slide_products();
      slide_products_small();
      banner_inner();
      slide_gifts();

      slide_prodcuts_sale();

      slide_collection();

      slide_other_products();

      $('.dotdotdot').dotdotdot();

      getValue();

    //banner news
      $('.banner-2 ').slick({
        fade:true,
        autoplay:true,
        dots:true,
        arrows:false
      })


      $('.tree-toggle').click(function () { 
        $(this).parent().children('ul.tree').toggle(200);
      });

      $(function(){
        $('.tree-toggle').parent().children('ul.tree').toggle(200);
      });

      $('.toggle-nav-pro').click(function(){
        $(this).parents('.nav-collsape').toggleClass('open-sub');
      });

      $('.toogle-menu-ft').click(function(){
        $(this).parent().toggleClass('show');
      });

      $('.header-search-icon').click(function(){
        $('.top-search').addClass('show');
      });

      $('.closeSearch').click(function(){
        $('.top-search').removeClass('show');
      });

      //masonry News
      // var $masonry = $('.masonry').isotope({
      //   layoutMode: 'packery',
      //   itemSelector: '.article-news'
      // })


      $(window).scroll(function(){
        if ($(this).scrollTop()>300){
          $('.back-top').show();
        }
        else{
          $('.back-top').hide();
        }
      });

      $('.back-top').on('click', function () {
        $('html,body').animate({
            scrollTop: 0
        }, 700);
      });

      //open menu
      $('.header-cart').click(function(e){
          e.stopPropagation();
          $('body').addClass('open-menu');
        });

        $('.closeSlidebar').click(function(){
          $('body').removeClass('open-menu');
        });

    });
  
    // **********************************************************************//
    // ! Window load
    // **********************************************************************//
    $(window).on('load', function () {
  
    });
  
    // **********************************************************************//
    // ! Window resize
    // **********************************************************************//
    $(window).on('resize', function () {
      
    });

   
  })(jQuery);
  
  
  
  