<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('bv_collection.php');

$main_datas = new Bv_Collection();
$datas = array();

if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $main_datas->secure_key || !Tools::getValue('action'))
	die(1);

if (Tools::getValue('action') == 'updateDatasPosition' && Tools::getValue('datas'))
{
	$datas = Tools::getValue('datas');

	foreach ($datas as $position => $id_data)
		$res = Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'collection_datas` SET `position` = '.(int)$position.'
			WHERE `id_collection_datas` = '.(int)$id_data
		);

	$main_datas->clearCache();
}
