<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */


class Bv_CollectionListModuleFrontController extends ModuleFrontController
{
    /**
	 * @see FrontController::initContent()
	 */
    public function __construct()
    {
        $this->name = 'bv_collection';
        parent::__construct();

    }
	public function initContent()
	{
		parent::initContent(); 
		$collectionDatas = new Bv_Collection();
        $datas = $collectionDatas->getDatas(true);
        
        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'datas' => $datas,
                'image_baseurl' => '/modules/bv_collection/images/',
                'link' => $this->context->link->getModuleLink($this->name, 'detail', array(), true)
            )
        ); 
        $this->setTemplate('module:bv_collection/views/templates/front/list.tpl');
	} 
}
