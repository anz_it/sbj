<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since   1.5.0
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;

include_once(_PS_MODULE_DIR_.'bv_collection/Bv_MainCollection.php');

class Bv_Collection extends Module implements WidgetInterface
{
    protected $_html = '';
    protected $default_width = 779;
    protected $default_speed = 5000;
    protected $default_pause_on_hover = 1;
    protected $default_wrap = 1;
    protected $promo_cat_id = 29; //Default catId promotion is 29
    protected $templateFile;

    public function __construct()
    {
        $this->name = 'bv_collection';
        $this->tab = 'front_office_features';
        $this->version = '2.0.0';
        $this->author = 'PrestaShop';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->getTranslator()->trans('ECollection', array(), 'Modules.Collection.Admin');
        $this->description = $this->getTranslator()->trans('Adds an collection promotion to your site.', array(), 'Modules.Collection.Admin');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:bv_collection/views/templates/hook/slider.tpl';
    }

    /**
     * @see Module::install()
     */
    public function install()
    {
        /* Adds Module */
        if (parent::install()) {
            $shops = Shop::getContextListShopID();
            $shop_groups_list = array();

            /* Setup each shop */
            foreach ($shops as $shop_id) {
                $shop_group_id = (int)Shop::getGroupFromShop($shop_id, true);

                if (!in_array($shop_group_id, $shop_groups_list)) {
                    $shop_groups_list[] = $shop_group_id;
                }

                /* Sets up configuration */
                $res = Configuration::updateValue('COLLECTION_SPEED', $this->default_speed, false, $shop_group_id, $shop_id);
                $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', $this->default_pause_on_hover, false, $shop_group_id, $shop_id);
                $res &= Configuration::updateValue('COLLECTION_WRAP', $this->default_wrap, false, $shop_group_id, $shop_id);
            }

            /* Sets up Shop Group configuration */
            if (count($shop_groups_list)) {
                foreach ($shop_groups_list as $shop_group_id) {
                    $res &= Configuration::updateValue('COLLECTION_SPEED', $this->default_speed, false, $shop_group_id);
                    $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', $this->default_pause_on_hover, false, $shop_group_id);
                    $res &= Configuration::updateValue('COLLECTION_WRAP', $this->default_wrap, false, $shop_group_id);
                }
            }

            /* Sets up Global configuration */
            $res &= Configuration::updateValue('COLLECTION_SPEED', $this->default_speed);
            $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', $this->default_pause_on_hover);
            $res &= Configuration::updateValue('COLLECTION_WRAP', $this->default_wrap);

            /* Creates tables */
            $res &= $this->createTables();

            /* Adds samples */
            if ($res) {
                //$this->installSamples();
            }

            // Disable on mobiles and tablets
            $this->disableDevice(Context::DEVICE_MOBILE);

            return (bool)$res;
        }

        return false;
    }

    /**
     * Adds samples
     */
    protected function installSamples()
    {
        $languages = Language::getLanguages(false);
        for ($i = 1; $i <= 4; ++$i) {
            $data = new Bv_MainCollection();
            $data->position = $i;
            $data->active = 1;
            foreach ($languages as $language) {
                $data->title[$language['id_lang']] = 'Sample '.$i;
                $data->description[$language['id_lang']] = '<h2>EXCEPTEUR OCCAECAT</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique in tortor et dignissim. Quisque non tempor leo. Maecenas egestas sem elit</p>';
                $data->legend = '0';
                $data->url[$language['id_lang']] = 'http://bvmedia.com/?utm_source=back-office&utm_medium=v17_collection'
                    .'&utm_campaign=back-office-'.Tools::strtoupper($this->context->language->iso_code)
                    .'&utm_content='.(defined('_PS_HOST_MODE_') ? 'ondemand' : 'download');
                $data->image[$language['id_lang']] = 'sample-'.$i.'.jpg';
            }
            $data->add();
        }
    }

    /**
     * @see Module::uninstall()
     */
    public function uninstall()
    {
        /* Deletes Module */
        if (parent::uninstall()) {
            /* Deletes tables */
            $res = $this->deleteTables();

            /* Unsets configuration */
            $res &= Configuration::deleteByName('COLLECTION_SPEED');
            $res &= Configuration::deleteByName('COLLECTION_PAUSE_ON_HOVER');
            $res &= Configuration::deleteByName('COLLECTION_WRAP');

            return (bool)$res;
        }

        return false;
    }

    /**
     * Creates tables
     */
    protected function createTables()
    {
        /* Datas */
        $res = (bool)Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'collection` (
                `id_collection_datas` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_shop` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_collection_datas`, `id_shop`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');

        /* Datas configuration */
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'collection_datas` (
              `id_collection_datas` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `position` int(10) unsigned NOT NULL DEFAULT \'0\',
              `active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
              PRIMARY KEY (`id_collection_datas`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');

        /* Datas lang configuration */
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'collection_datas_lang` (
              `id_collection_datas` int(10) unsigned NOT NULL,
              `id_lang` int(10) unsigned NOT NULL,
              `title` varchar(255) NOT NULL,
              `legend` varchar(255) NOT NULL,
              `description` text NOT NULL,
              `url` varchar(255) NOT NULL,
              `image` varchar(255) NOT NULL,
              `fileDownload` varchar(255) NOT NULL,
              PRIMARY KEY (`id_collection_datas`,`id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');

        return $res;
    }

    /**
     * deletes tables
     */
    protected function deleteTables()
    {
        $datas = $this->getDatas();
        foreach ($datas as $data) {
            $to_del = new Bv_MainCollection($data['id_data']);
            $to_del->delete();
        }

        return Db::getInstance()->execute('
            DROP TABLE IF EXISTS `'._DB_PREFIX_.'collection`, `'._DB_PREFIX_.'collection_datas`, `'._DB_PREFIX_.'collection_datas_lang`;
        ');
    }

    public function getContent()
    {
        $this->_html .= $this->headerHTML();

        /* Validate & process */
        if (Tools::isSubmit('submitCollection') || Tools::isSubmit('delete_id_data') ||
            Tools::isSubmit('submitCollectionr') ||
            Tools::isSubmit('changeStatus')
        ) {
            if ($this->_postValidation()) {
                $this->_postProcess(); 
                $this->_html .= $this->renderList();
            } else {
                $this->_html .= $this->renderAddForm();
            }

            $this->clearCache();
        } elseif (Tools::isSubmit('addSlide') || (Tools::isSubmit('id_data') && $this->slideExists((int)Tools::getValue('id_data')))) {
            if (Tools::isSubmit('addSlide')) {
                $mode = 'add';
            } else {
                $mode = 'edit';
            }

            if ($mode == 'add') {
                if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL) {
                    $this->_html .= $this->renderAddForm();
                } else {
                    $this->_html .= $this->getShopContextError(null, $mode);
                }
            } else {
                $associated_shop_ids = Bv_MainCollection::getAssociatedIdsShop((int)Tools::getValue('id_data'));
                $context_shop_id = (int)Shop::getContextShopID();

                if ($associated_shop_ids === false) {
                    $this->_html .= $this->getShopAssociationError((int)Tools::getValue('id_data'));
                } elseif (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL && in_array($context_shop_id, $associated_shop_ids)) {
                    if (count($associated_shop_ids) > 1) {
                        $this->_html = $this->getSharedSlideWarning();
                    }
                    $this->_html .= $this->renderAddForm();
                } else {
                    $shops_name_list = array();
                    foreach ($associated_shop_ids as $shop_id) {
                        $associated_shop = new Shop((int)$shop_id);
                        $shops_name_list[] = $associated_shop->name;
                    }
                    $this->_html .= $this->getShopContextError($shops_name_list, $mode);
                }
            }
        } else {
            $this->_html .= $this->getWarningMultishopHtml().$this->getCurrentShopInfoMsg();

            if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL) {
                $this->_html .= $this->renderList();
            }
        }

        return $this->_html;
    }

    protected function _postValidation()
    {
        $errors = array();

        /* Validation for Slider configuration */
        if (Tools::isSubmit('submitCollectionr')) {
            if (!Validate::isInt(Tools::getValue('COLLECTION_SPEED'))) {
                $errors[] = $this->getTranslator()->trans('Invalid values', array(), 'Modules.Collection.Admin');
            }
        } elseif (Tools::isSubmit('changeStatus')) {
            if (!Validate::isInt(Tools::getValue('id_data'))) {
                $errors[] = $this->getTranslator()->trans('Invalid slide', array(), 'Modules.Collection.Admin');
            }
        } elseif (Tools::isSubmit('submitCollection')) {
            /* Checks state (active) */
            if (!Validate::isInt(Tools::getValue('active_data')) || (Tools::getValue('active_data') != 0 && Tools::getValue('active_data') != 1)) {
                $errors[] = $this->getTranslator()->trans('Invalid slide state.', array(), 'Modules.Collection.Admin');
            }
            /* Checks position */
            if (!Validate::isInt(Tools::getValue('position')) || (Tools::getValue('position') < 0)) {
                $errors[] = $this->getTranslator()->trans('Invalid slide position.', array(), 'Modules.Collection.Admin');
            }
            /* If edit : checks id_data */
            if (Tools::isSubmit('id_data')) {
                if (!Validate::isInt(Tools::getValue('id_data')) && !$this->slideExists(Tools::getValue('id_data'))) {
                    $errors[] = $this->getTranslator()->trans('Invalid slide ID', array(), 'Modules.Collection.Admin');
                }
            }
            /* Checks title/url/legend/description/image */
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                if (Tools::strlen(Tools::getValue('title_' . $language['id_lang'])) > 255) {
                    $errors[] = $this->getTranslator()->trans('The title is too long.', array(), 'Modules.Collection.Admin');
                }
                if (Tools::strlen(Tools::getValue('legend_' . $language['id_lang'])) > 255) {
                    $errors[] = $this->getTranslator()->trans('The caption is too long.', array(), 'Modules.Collection.Admin');
                }
                if (Tools::strlen(Tools::getValue('url_' . $language['id_lang'])) > 255) {
                    $errors[] = $this->getTranslator()->trans('The URL is too long.', array(), 'Modules.Collection.Admin');
                }
                if (Tools::strlen(Tools::getValue('description_' . $language['id_lang'])) > 4000) {
                    $errors[] = $this->getTranslator()->trans('The description is too long.', array(), 'Modules.Collection.Admin');
                }
                if (Tools::strlen(Tools::getValue('url_' . $language['id_lang'])) > 0 && !Validate::isUrl(Tools::getValue('url_' . $language['id_lang']))) {
                    $errors[] = $this->getTranslator()->trans('The URL format is not correct.', array(), 'Modules.Collection.Admin');
                }
                if (Tools::getValue('image_' . $language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_' . $language['id_lang']))) {
                    $errors[] = $this->getTranslator()->trans('Invalid filename.', array(), 'Modules.Collection.Admin');
                }
                if (Tools::getValue('image_old_' . $language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_old_' . $language['id_lang']))) {
                    $errors[] = $this->getTranslator()->trans('Invalid filename.', array(), 'Modules.Collection.Admin');
                }
            }

            /* Checks title/url/legend/description for default lang */
            $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
            // if (Tools::strlen(Tools::getValue('url_' . $id_lang_default)) == 0) {
            //     $errors[] = $this->getTranslator()->trans('The URL is not set.', array(), 'Modules.Collection.Admin');
            // }
            if (!Tools::isSubmit('has_picture') && (!isset($_FILES['image_' . $id_lang_default]) || empty($_FILES['image_' . $id_lang_default]['tmp_name']))) {
                $errors[] = $this->getTranslator()->trans('The image is not set.', array(), 'Modules.Collection.Admin');
            }
            if (Tools::getValue('image_old_'.$id_lang_default) && !Validate::isFileName(Tools::getValue('image_old_'.$id_lang_default))) {
                $errors[] = $this->getTranslator()->trans('The image is not set.', array(), 'Modules.Collection.Admin');
            }
        } elseif (Tools::isSubmit('delete_id_data') && (!Validate::isInt(Tools::getValue('delete_id_data')) || !$this->slideExists((int)Tools::getValue('delete_id_data')))) {
            $errors[] = $this->getTranslator()->trans('Invalid slide ID', array(), 'Modules.Collection.Admin');
        }

        /* Display errors if needed */
        if (count($errors)) {
            $this->_html .= $this->displayError(implode('<br />', $errors));

            return false;
        }

        /* Returns if validation is ok */

        return true;
    }

    protected function _postProcess()
    {
        $errors = array();
        $shop_context = Shop::getContext();

        /* Processes Slider */
        if (Tools::isSubmit('submitCollectionr')) {
            $shop_groups_list = array();
            $shops = Shop::getContextListShopID();

            foreach ($shops as $shop_id) {
                $shop_group_id = (int)Shop::getGroupFromShop($shop_id, true);

                if (!in_array($shop_group_id, $shop_groups_list)) {
                    $shop_groups_list[] = $shop_group_id;
                }

                $res = Configuration::updateValue('COLLECTION_SPEED', (int)Tools::getValue('COLLECTION_SPEED'), false, $shop_group_id, $shop_id);
                $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', (int)Tools::getValue('COLLECTION_PAUSE_ON_HOVER'), false, $shop_group_id, $shop_id);
                $res &= Configuration::updateValue('COLLECTION_WRAP', (int)Tools::getValue('COLLECTION_WRAP'), false, $shop_group_id, $shop_id);
            }

            /* Update global shop context if needed*/
            switch ($shop_context) {
                case Shop::CONTEXT_ALL:
                    $res &= Configuration::updateValue('COLLECTION_SPEED', (int)Tools::getValue('COLLECTION_SPEED'));
                    $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', (int)Tools::getValue('COLLECTION_PAUSE_ON_HOVER'));
                    $res &= Configuration::updateValue('COLLECTION_WRAP', (int)Tools::getValue('COLLECTION_WRAP'));
                    if (count($shop_groups_list)) {
                        foreach ($shop_groups_list as $shop_group_id) {
                            $res &= Configuration::updateValue('COLLECTION_SPEED', (int)Tools::getValue('COLLECTION_SPEED'), false, $shop_group_id);
                            $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', (int)Tools::getValue('COLLECTION_PAUSE_ON_HOVER'), false, $shop_group_id);
                            $res &= Configuration::updateValue('COLLECTION_WRAP', (int)Tools::getValue('COLLECTION_WRAP'), false, $shop_group_id);
                        }
                    }
                    break;
                case Shop::CONTEXT_GROUP:
                    if (count($shop_groups_list)) {
                        foreach ($shop_groups_list as $shop_group_id) {
                            $res &= Configuration::updateValue('COLLECTION_SPEED', (int)Tools::getValue('COLLECTION_SPEED'), false, $shop_group_id);
                            $res &= Configuration::updateValue('COLLECTION_PAUSE_ON_HOVER', (int)Tools::getValue('COLLECTION_PAUSE_ON_HOVER'), false, $shop_group_id);
                            $res &= Configuration::updateValue('COLLECTION_WRAP', (int)Tools::getValue('COLLECTION_WRAP'), false, $shop_group_id);
                        }
                    }
                    break;
            }

            $this->clearCache();

            if (!$res) {
                $errors[] = $this->displayError($this->getTranslator()->trans('The configuration could not be updated.', array(), 'Modules.Collection.Admin'));
            } else {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=6&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
            }
        } elseif (Tools::isSubmit('changeStatus') && Tools::isSubmit('id_data')) {
            $data = new Bv_MainCollection((int)Tools::getValue('id_data'));
            if ($data->active == 0) {
                $data->active = 1;
            } else {
                $data->active = 0;
            }
            $res = $data->update();
            $this->clearCache();
            $this->_html .= ($res ? $this->displayConfirmation($this->getTranslator()->trans('Configuration updated', array(), 'Admin.Notifications.Success')) : $this->displayError($this->getTranslator()->trans('The configuration could not be updated.', array(), 'Modules.Collection.Admin')));
        } elseif (Tools::isSubmit('submitCollection')) {
            /* Sets ID if needed */
            if (Tools::getValue('id_data')) {
                $data = new Bv_MainCollection((int)Tools::getValue('id_data'));
                if (!Validate::isLoadedObject($data)) {
                    $this->_html .= $this->displayError($this->getTranslator()->trans('Invalid slide ID', array(), 'Modules.Collection.Admin'));
                    return false;
                }
            } else {
                $data = new Bv_MainCollection();
            }
            /* Sets position */
            $data->position = (int)Tools::getValue('position');
            /* Sets active */
            $data->active = (int)Tools::getValue('active_data');

            /* Sets each langue fields */
            $languages = Language::getLanguages(false);

            foreach ($languages as $language) {
                $data->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
                $data->url[$language['id_lang']] = Tools::getValue('url_'.$language['id_lang']);
                $data->legend[$language['id_lang']] = Tools::getValue('legend_'.$language['id_lang']);
                $data->description[$language['id_lang']] = Tools::getValue('description_'.$language['id_lang']);
                
                $allowedExts = array("zip");
                
                $temp = explode(".", $_FILES['fileDownload']['name']);
                $extension = end($temp);
                if (($_FILES['fileDownload']['type'] < 200000)
                && in_array($extension, $allowedExts))
                {  
                    $salt = sha1(microtime());
                    $store_path =  dirname(__FILE__).'/collections/' . date('Y-m-d-H-i-s');
                    if(!is_dir($store_path)) mkdir($store_path); 
                    if(!move_uploaded_file($_FILES['fileDownload']['tmp_name'], $store_path . '/'. $_FILES["fileDownload"]["name"])) {
                        return false;
                    }  
                  
                    $zip = new ZipArchive;
                    $res = $zip->open($store_path . '/'. $_FILES["fileDownload"]["name"]);
                    if ($res === TRUE) {
                        // extract it to the path we determined above
                        if(!is_dir($store_path. '/images')) mkdir($store_path. '/images');
                        $zip->extractTo($store_path. '/images');
                        $zip->close(); 
                    }  
                    $data->fileDownload[$language['id_lang']] = $store_path . '/'. $_FILES['fileDownload']['name'];
                    
                } elseif (Tools::getValue('fileDownload_old_'.$language['id_lang']) != '') {
                    $data->fileDownload[$language['id_lang']] = Tools::getValue('fileDownload_old_' . $language['id_lang']);
                }
                
                /* Uploads image and sets slide */
                $type = Tools::strtolower(Tools::substr(strrchr($_FILES['image_'.$language['id_lang']]['name'], '.'), 1));
                $imagesize = @getimagesize($_FILES['image_'.$language['id_lang']]['tmp_name']);
                if (isset($_FILES['image_'.$language['id_lang']]) &&
                    isset($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
                    !empty($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
                    !empty($imagesize) &&
                    in_array(
                        Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)), array(
                            'jpg',
                            'gif',
                            'jpeg',
                            'png'
                        )
                    ) &&
                    in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                ) {
                    $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                    $salt = sha1(microtime());
                    if ($error = ImageManager::validateUpload($_FILES['image_'.$language['id_lang']])) {
                        $errors[] = $error;
                    } elseif (!$temp_name || !move_uploaded_file($_FILES['image_'.$language['id_lang']]['tmp_name'], $temp_name)) {
                        return false;
                    } elseif (!ImageManager::resize($temp_name, dirname(__FILE__).'/images/'.$salt.'_'.$_FILES['image_'.$language['id_lang']]['name'], null, null, $type)) {
                        $errors[] = $this->displayError($this->getTranslator()->trans('An error occurred during the image upload process.', array(), 'Admin.Notifications.Error'));
                    }
                    if (isset($temp_name)) {
                        @unlink($temp_name);
                    }
                    $data->image[$language['id_lang']] = $salt.'_'.$_FILES['image_'.$language['id_lang']]['name'];
                } elseif (Tools::getValue('image_old_'.$language['id_lang']) != '') {
                    $data->image[$language['id_lang']] = Tools::getValue('image_old_' . $language['id_lang']);
                }
            }

            /* Processes if no errors  */
            if (!$errors) {
                /* Adds */
                if (!Tools::getValue('id_data')) {
                    if (!$data->add()) {
                        $errors[] = $this->displayError($this->getTranslator()->trans('The slide could not be added.', array(), 'Modules.Collection.Admin'));
                    }
                } elseif (!$data->update()) {
                    $errors[] = $this->displayError($this->getTranslator()->trans('The slide could not be updated.', array(), 'Modules.Collection.Admin'));
                }
                $this->clearCache();
            }
        } elseif (Tools::isSubmit('delete_id_data')) {
            $data = new Bv_MainCollection((int)Tools::getValue('delete_id_data'));
            $res = $data->delete();
            $this->clearCache();
            if (!$res) {
                $this->_html .= $this->displayError('Could not delete.');
            } else {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=1&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
            }
        }

        /* Display errors if needed */
        if (count($errors)) {
            $this->_html .= $this->displayError(implode('<br />', $errors));
        } elseif (Tools::isSubmit('submitCollection') && Tools::getValue('id_data')) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=4&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
        } elseif (Tools::isSubmit('submitCollection')) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=3&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name);
        }
    }

    public function hookdisplayHeader($params)
    {
        $this->context->controller->registerStylesheet('modules-collection', 'modules/'.$this->name.'/css/collection.css', ['media' => 'all', 'priority' => 150]);
        $this->context->controller->registerJavascript('modules-responsivedatas', 'modules/'.$this->name.'/js/responsivedatas.min.js', ['position' => 'bottom', 'priority' => 150]);
        $this->context->controller->registerJavascript('modules-collection', 'modules/'.$this->name.'/js/collection.js', ['position' => 'bottom', 'priority' => 150]);
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId())) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
        }

        return $this->fetch($this->templateFile, $this->getCacheId());
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $datas = $this->getDatas(true);
        if (is_array($datas)) {
            foreach ($datas as &$data) {
                $data['sizes'] = @getimagesize((dirname(__FILE__) . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $data['image']));
                if (isset($data['sizes'][3]) && $data['sizes'][3]) {
                    $data['size'] = $data['sizes'][3];
                }
            }
        }

        $config = $this->getConfigFieldsValues();

        return [
            'collection' => [
                'speed' => $config['COLLECTION_SPEED'],
                'pause' => $config['COLLECTION_PAUSE_ON_HOVER'] ? 'hover' : '',
                'wrap' => $config['COLLECTION_WRAP'] ? 'true' : 'false',
                'datas' => $datas,
            ],
        ];
    }

    public function clearCache()
    {
        $this->_clearCache($this->templateFile);
    }

    public function hookActionShopDataDuplication($params)
    {
        Db::getInstance()->execute('
            INSERT IGNORE INTO '._DB_PREFIX_.'collection (id_collection_datas, id_shop)
            SELECT id_collection_datas, '.(int)$params['new_id_shop'].'
            FROM '._DB_PREFIX_.'collection
            WHERE id_shop = '.(int)$params['old_id_shop']
        );
        $this->clearCache();
    }

    public function headerHTML()
    {
        if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name) {
            return;
        }

        $this->context->controller->addJqueryUI('ui.sortable');
        /* Style & js for fieldset 'datas configuration' */
        $html = '<script type="text/javascript">
            $(function() {
                var $myDatas = $("#datas");
                $myDatas.sortable({
                    opacity: 0.6,
                    cursor: "move",
                    update: function() {
                        var order = $(this).sortable("serialize") + "&action=updateDatasPosition";
                        $.post("'.$this->context->shop->physical_uri.$this->context->shop->virtual_uri.'modules/'.$this->name.'/ajax_'.$this->name.'.php?secure_key='.$this->secure_key.'", order);
                        }
                    });
                $myDatas.hover(function() {
                    $(this).css("cursor","move");
                    },
                    function() {
                    $(this).css("cursor","auto");
                });
            });
        </script>';

        return $html;
    }

    public function getNextPosition()
    {
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
            SELECT MAX(hss.`position`) AS `next_position`
            FROM `'._DB_PREFIX_.'collection_datas` hss, `'._DB_PREFIX_.'collection` hs
            WHERE hss.`id_collection_datas` = hs.`id_collection_datas` AND hs.`id_shop` = '.(int)$this->context->shop->id
        );

        return (++$row['next_position']);
    }

    public function getDatas($active = null)
    {
        $this->context = Context::getContext();
        $id_shop = $this->context->shop->id;
        $id_lang = $this->context->language->id;

        $datas = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT hs.`id_collection_datas` as id_data, hss.`position`, hss.`active`, hssl.`title`,
            hssl.`url`, hssl.`legend`, hssl.`description`, hssl.`image`, hssl.`fileDownload`
            FROM '._DB_PREFIX_.'collection hs
            LEFT JOIN '._DB_PREFIX_.'collection_datas hss ON (hs.id_collection_datas = hss.id_collection_datas)
            LEFT JOIN '._DB_PREFIX_.'collection_datas_lang hssl ON (hss.id_collection_datas = hssl.id_collection_datas)
            WHERE id_shop = '.(int)$id_shop.'
            AND hssl.id_lang = '.(int)$id_lang.
            ($active ? ' AND hss.`active` = 1' : ' ').'
            ORDER BY hss.position'
        );

        foreach ($datas as &$data) {
            $data['image_url'] = $this->context->link->getMediaLink(_MODULE_DIR_.'bv_collection/images/'.$data['image']);
            $data['pdf_url'] = $this->context->link->getMediaLink(_MODULE_DIR_.'bv_collection/pdf/'.$data['fileDownload']);
        } 
        return $datas;
    }

    public function getAllImagesByDataId($id_data, $active = null, $id_shop = null)
    {
        $this->context = Context::getContext();
        $id_shop = $this->context->shop->id;
        $id_lang = $this->context->language->id;
        $images = array();

        if (!isset($id_shop))
            $id_shop = $this->context->shop->id;
        $querySql = '
        SELECT hs.`id_collection_datas` as id_data, hss.`position`, hss.`active`, hssl.`title`,
        hssl.`url`, hssl.`legend`, hssl.`description`, hssl.`image`, hssl.`fileDownload`
        FROM '._DB_PREFIX_.'collection hs
        LEFT JOIN '._DB_PREFIX_.'collection_datas hss ON (hs.id_collection_datas = hss.id_collection_datas)
        LEFT JOIN '._DB_PREFIX_.'collection_datas_lang hssl ON (hss.id_collection_datas = hssl.id_collection_datas)
        WHERE id_shop = '.(int)$id_shop.'
        AND hssl.id_lang = '.(int)$id_lang.'
        AND hs.id_collection_datas = '.(int)$id_data.
        ($active ? ' AND hss.`active` = 1' : ' ').'
        ORDER BY hss.position';
        $datas = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS( $querySql  );
        
        foreach ($datas as &$data) {
            $_folder= dirname($data['fileDownload']);
            if(!empty($_folder)) {
                $listImage = scandir($_folder . '/images');
                
                foreach($listImage as $img) {
                    $extension = pathinfo($img);    
                    $allowedExts = array('jpg', 'png', 'JPG', 'PNG');
                    if(in_array($extension['extension'], $allowedExts))  {
                        $_tmpFolder = explode("bv_collection", $_folder); 
                        $data['links'][] = $this->context->link->getMediaLink('/modules/bv_collection' . $_tmpFolder[1] . '/images/'.$img);
                    }
                }
            }
            
        }
        
        if(!empty($datas)) {
            return $datas[0];
        }
        return array();
    }

    public function displayStatus($id_data, $active)
    {
        $title = ((int)$active == 0 ? $this->getTranslator()->trans('Disabled', array(), 'Admin.Global') : $this->getTranslator()->trans('Enabled', array(), 'Admin.Global'));
        $icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
        $class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
        $html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
            '&configure='.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules').
                '&changeStatus&id_data='.(int)$id_data.'" title="'.$title.'"><i class="'.$icon.'"></i> '.$title.'</a>';

        return $html;
    }

    public function slideExists($id_data)
    {
        $req = 'SELECT hs.`id_collection_datas` as id_data
                FROM `'._DB_PREFIX_.'collection` hs
                WHERE hs.`id_collection_datas` = '.(int)$id_data;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);

        return ($row);
    }

    public function renderList()
    {
        $datas = $this->getDatas();
        foreach ($datas as $key => $data) {
            $datas[$key]['status'] = $this->displayStatus($data['id_data'], $data['active']);
            $associated_shop_ids = Bv_MainCollection::getAssociatedIdsShop((int)$data['id_data']);
            if ($associated_shop_ids && count($associated_shop_ids) > 1) {
                $datas[$key]['is_shared'] = true;
            } else {
                $datas[$key]['is_shared'] = false;
            }
        }

        $this->context->smarty->assign(
            array(
                'link' => $this->context->link,
                'datas' => $datas,
                'image_baseurl' => $this->_path.'images/'
            )
        );

        return $this->display(__FILE__, 'list.tpl');
    }

    public function renderAddForm()
    {
        // $promotions = $this->getProducts();
        // $arrOptions = array(array("id"=> 0, "name" => "--Please select--"));

        // if(!empty($promotions)) {
        //     foreach ($promotions as $key => $value) {
        //         $arrOptions[] = array("id"=>$value["id_product"], "name" => $value["name"]);
        //     }
        // }
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->getTranslator()->trans('Grid information', array(), 'Modules.Collection.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'file_lang',
                        'label' => $this->getTranslator()->trans('Image', array(), 'Admin.Global'),
                        'name' => 'image',
                        'required' => true,
                        'lang' => true,
                        'desc' => $this->getTranslator()->trans('Maximum image size: %s.', array(ini_get('upload_max_filesize')), 'Admin.Global')
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->getTranslator()->trans('Title', array(), 'Admin.Global'),
                        'name' => 'title',
                        'required' => true,
                        'lang' => true,
                    ),
                    // array(
                    //     'type' => 'text',
                    //     'label' => $this->getTranslator()->trans('URL', array(), 'Modules.Collection.Admin'),
                    //     'name' => 'url',
                    //     'required' => false,
                    //     'lang' => true,
                    // ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->getTranslator()->trans('Motto', array(), 'Modules.Collection.Admin'),
                        'name' => 'legend',
                        'required' => true,
                        'lang' => true,
                    ),
                    // array(
                    //     'type' => 'select',
                    //     'label' => $this->getTranslator()->trans('SelectProduct', array(), 'Modules.Collection.Admin'),
                    //     'name' => 'legend',
                    //     'required' => false,
                    //     'default_value' => (int) $this->context->country->id,
                    //     'options' => array(
                    //         'query' => $arrOptions,
                    //         'id' => 'id',
                    //         'name' => 'name',
                    //     ),

                    // ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->getTranslator()->trans('Description', array(), 'Admin.Global'),
                        'name' => 'description',
                        'autoload_rte' => true,
                        'lang' => true,
                    ),
                    array(
                        'type' => 'file',
                        'label' => $this->getTranslator()->trans('Upload .ZIP', array(), 'Admin.Global'),
                        'name' => 'fileDownload',
                        'required' => false,
                        'lang' => false,
                        'desc' => $this->getTranslator()->trans('Maximum .ZIP size: %s. You have to zip all images (without folder)', array(ini_get('upload_max_filesize')), 'Admin.Global')
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->getTranslator()->trans('Enabled', array(), 'Admin.Global'),
                        'name' => 'active_data',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->getTranslator()->trans('Yes', array(), 'Admin.Global')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->getTranslator()->trans('No', array(), 'Admin.Global')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->getTranslator()->trans('Save', array(), 'Admin.Actions'),
                )
            ),
        );

        if (Tools::isSubmit('id_data') && $this->slideExists((int)Tools::getValue('id_data'))) {
            $data = new Bv_MainCollection((int)Tools::getValue('id_data'));
            $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_data');
            $fields_form['form']['images'] = $data->image;
            $fields_form['form']['fileDownload'] = $data->fileDownload;

            $has_picture = true;

            foreach (Language::getLanguages(false) as $lang) {
                if (!isset($data->image[$lang['id_lang']])) {
                    $has_picture &= false;
                }
            }

            if ($has_picture) {
                $fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'has_picture');
            }
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCollection';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->tpl_vars = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'language' => array(
                'id_lang' => $language->id,
                'iso_code' => $language->iso_code
            ),
            'fields_value' => $this->getAddFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'images/',
            'pdf_baseurl' => $this->_path.'pdf/'
        );

        $helper->override_folder = '/';

        $languages = Language::getLanguages(false);

        if (count($languages) > 1) {
            return $this->getMultiLanguageInfoMsg() . $helper->generateForm(array($fields_form));
        } else {
            return $helper->generateForm(array($fields_form));
        }
    } 

    public function getConfigFieldsValues()
    {
        $id_shop_group = Shop::getContextShopGroupID();
        $id_shop = Shop::getContextShopID();

        return array(
            'COLLECTION_SPEED' => Tools::getValue('COLLECTION_SPEED', Configuration::get('COLLECTION_SPEED', null, $id_shop_group, $id_shop)),
            'COLLECTION_PAUSE_ON_HOVER' => Tools::getValue('COLLECTION_PAUSE_ON_HOVER', Configuration::get('COLLECTION_PAUSE_ON_HOVER', null, $id_shop_group, $id_shop)),
            'COLLECTION_WRAP' => Tools::getValue('COLLECTION_WRAP', Configuration::get('COLLECTION_WRAP', null, $id_shop_group, $id_shop)),
        );
    }

    public function getAddFieldsValues()
    {
        $fields = array();

        if (Tools::isSubmit('id_data') && $this->slideExists((int)Tools::getValue('id_data'))) {
            $data = new Bv_MainCollection((int)Tools::getValue('id_data'));
            $fields['id_data'] = (int)Tools::getValue('id_data', $data->id);
        } else {
            $data = new Bv_MainCollection();
        }

        $fields['active_data'] = Tools::getValue('active_data', $data->active);
        $fields['has_picture'] = true;

        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $fields['image'][$lang['id_lang']] = Tools::getValue('image_'.(int)$lang['id_lang']);
            $fields['title'][$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], $data->title[$lang['id_lang']]);
            $fields['url'][$lang['id_lang']] = Tools::getValue('url_'.(int)$lang['id_lang'], $data->url[$lang['id_lang']]);
            $fields['legend'][$lang['id_lang']]= Tools::getValue('legend_'.(int)$lang['id_lang'], $data->legend[$lang['id_lang']]);
            $fields['description'][$lang['id_lang']] = Tools::getValue('description_'.(int)$lang['id_lang'], $data->description[$lang['id_lang']]);
            $fields['fileDownload'][$lang['id_lang']] = Tools::getValue('fileDownload_'.(int)$lang['id_lang']);
        } 
        return $fields;
    }

    protected function getMultiLanguageInfoMsg()
    {
        return '<p class="alert alert-warning">'.
                    $this->getTranslator()->trans('Since multiple languages are activated on your shop, please mind to upload your image for each one of them', array(), 'Modules.Collection.Admin').
                '</p>';
    }

    protected function getWarningMultishopHtml()
    {
        if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL) {
            return '<p class="alert alert-warning">' .
            $this->getTranslator()->trans('You cannot manage datas items from a "All Shops" or a "Group Shop" context, select directly the shop you want to edit', array(), 'Modules.Collection.Admin') .
            '</p>';
        } else {
            return '';
        }
    }

    protected function getShopContextError($shop_contextualized_name, $mode)
    {
        if (is_array($shop_contextualized_name)) {
            $shop_contextualized_name = implode('<br/>', $shop_contextualized_name);
        }

        if ($mode == 'edit') {
            return '<p class="alert alert-danger">' .
            $this->trans('You can only edit this slide from the shop(s) context: %s', array($shop_contextualized_name), 'Modules.Collection.Admin') .
            '</p>';
        } else {
            return '<p class="alert alert-danger">' .
            $this->trans('You cannot add datas from a "All Shops" or a "Group Shop" context', array(), 'Modules.Collection.Admin') .
            '</p>';
        }
    }

    protected function getShopAssociationError($id_data)
    {
        return '<p class="alert alert-danger">'.
                        $this->trans('Unable to get slide shop association information (id_data: %d)', array((int)$id_data), 'Modules.Collection.Admin') .
                '</p>';
    }


    protected function getCurrentShopInfoMsg()
    {
        $shop_info = null;

        if (Shop::isFeatureActive()) {
            if (Shop::getContext() == Shop::CONTEXT_SHOP) {
                $shop_info = $this->trans('The modifications will be applied to shop: %s', array($this->context->shop->name),'Modules.Collection.Admin');
            } else if (Shop::getContext() == Shop::CONTEXT_GROUP) {
                $shop_info = $this->trans('The modifications will be applied to this group: %s', array(Shop::getContextShopGroup()->name), 'Modules.Collection.Admin');
            } else {
                $shop_info = $this->trans('The modifications will be applied to all shops and shop groups', array(), 'Modules.Collection.Admin');
            }

            return '<div class="alert alert-info">'.
                        $shop_info.
                    '</div>';
        } else {
            return '';
        }
    }

    protected function getSharedSlideWarning()
    {
        return '<p class="alert alert-warning">'.
                    $this->trans('This slide is shared with other shops! All shops associated to this slide will apply modifications made here', array(), 'Modules.Collection.Admin').
                '</p>';
    }

    protected function getProducts()
    {
        $result = Product::getProducts($this->context->language->id, 0, 10, 'id_product', 'DESC', $this->promo_cat_id,
            true,$this->context);
        return $result;
    }

    // public function hookModuleRoutes()
    // {
    //     return array(
    //         'module-jxblog-categories'  => array(
    //             'controller' => 'categories',
    //             'rule'       => 'blog/categories',
    //             'keywords'   => array(),
    //             'params'     => array(
    //                 'fc'     => 'module',
    //                 'module' => 'jxblog',
    //             )
    //         )
    //     );
    // }
}
