<!DOCTYPE html>
<html lang="en-US" class="liza_content_under_header liza_transparent_header ">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">    
	<link rel="icon" href="/gallery/img/pm-logo192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="/gallery/img/pm-logo192.png" />
	<title>Sacombank - Albums Slider</title>    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Responsive HTML5 Template">

    <!--Mobile Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS-->
	<link rel="stylesheet" id="fontawesome-css" href="/gallery/css/font-awesome.min.css" type="text/css" media="all" />
	<link rel="stylesheet" id="kube-css" href="/gallery/css/kube.css" type="text/css" media="all" />
	<link rel="stylesheet" id="swipebox-css" href="/gallery/css/swipebox.css" type="text/css" media="all" />
	<link rel="stylesheet" id="owl-css" href="/gallery/css/owl.carousel.css" type="text/css" media="all" />
	
	<link rel="stylesheet" id="liza-modules-css" href="/gallery/css/modules.css" type="text/css" media="all" />
	<link rel="stylesheet" id="liza-theme-css" href="/gallery/css/theme.css" type="text/css" media="all" />
	<link rel="stylesheet" id="liza-responsive-css" href="/gallery/css/responsive.css" type="text/css" media="all" />
	<link rel="stylesheet" id="liza-settings-css" href="/gallery/css/settings.css" type="text/css" media="all" />
	 
</head>
<body>
  	<!-- Simple Header -->
	<header class="liza_header logo_center menu_simple">
		<div class="liza_header_bg liza_blur_on_aside"></div>
		<div class="liza_header_wrapper">
			<div class="liza_header_left_part">
				<div class="liza_menu_block">
					<a class="liza_menu_toggler" href="javascript:void(0)">
						<span class="liza_menu_ico">
							<span class="liza_menu_ico_line1"></span>
							<span class="liza_menu_ico_line2"></span>
							<span class="liza_menu_ico_line3"></span>
						</span>Menu
					</a><!-- .liza_menu_toggler -->
					<nav class="liza_main_nav liza_blur_on_aside">				
						<ul class="liza_menu">
                            <li class="menu-item-has-children">
								<a href="/module/bv_collection/list">Bộ sưu tập</a>
							</li>
							<li class="menu-item-has-children">
								<a href="/">Trang chủ</a>
							</li>
						</ul>
					</nav><!-- .liza_main_nav -->
				</div><!-- .liza_menu_block -->
			</div><!-- .liza_header_left_part -->

			<div class="liza_header_middle_part">
				<div class="liza_logo_wrapper liza_blur_on_aside">
					<a href="/" class="liza_image_logo liza_retina"><img src="/gallery/img/logo.png" alt="" width="80" height="43" /></a>
				</div>
			</div><!-- .liza_header_middle_part -->

			<div class="liza_header_right_part">		
				<div class="liza_social_icons liza_blur_on_aside">
					<ul class="liza_social_icons_list">
						<li><a class='facebook' target='_blank' href='https://www.facebook.com/trangsuc.sbj' title='Facebook'><i class='fa fa-facebook'></i></a></li>
						<li><a class='youtube' target='_blank' href='https://www.youtube.com/channel/UChCMoE1Uuix7Q4mnarTa52w' title='Youtube'><i class='fa fa-youtube'></i></a></li>
					</ul><!-- .liza_social_icons_list -->
				</div>
			</div><!-- .liza_header_right_part -->
			<div class="clear"></div>
		</div><!-- .liza_header_wrapper -->
	</header><!-- .menu_simple -->
	  	<!-- Mobile Header -->
	<header class="liza_mobile_header">
		<div class="liza_mobile_header_wrapper">
			<div class="liza_mobile_header_logo">
				<div class="liza_logo_wrapper liza_blur_on_aside">
					<a href="/" class="liza_image_logo liza_retina"><img src="/gallery/img/logo.png" alt="" width="145" height="59" /></a>
				</div>
			</div><!-- .liza_mobile_header_logo -->
			<div class="liza_mobile_button_wrapper">
				<a class="liza_menu_toggler liza_menu_toggler_mobile" href="javascript:void(0)">
					<span class="liza_menu_ico">
						<span class="liza_menu_ico_line1"></span>
						<span class="liza_menu_ico_line2"></span>
						<span class="liza_menu_ico_line3"></span>
					</span>Menu
				</a>
			</div><!-- .liza_mobile_button_wrapper -->
		</div><!-- .liza_mobile_header_wrapper -->
		<div class="liza_mobile_menu_wrapper">
			<div class="liza_social_icons liza_social_icons_mobile liza_container">
				<ul class="liza_social_icons_list">
					<li><a class='facebook' target='_blank' href='https://www.facebook.com/' title='Facebook'><i class='fa fa-facebook'></i></a></li>
					<li><a class='google-plus' target='_blank' href='https://plus.google.com/' title='Google+'><i class='fa fa-google-plus'></i></a></li>
				</ul><!-- .liza_social_icons_list -->
			</div><!-- .liza_social_icons_mobile -->
			
			<nav class="liza_mobile_menu liza_container"></nav>
		</div><!-- .liza_mobile_menu_wrapper -->
	</header><!-- .liza_mobile_header -->
	  	
	<div class="liza_albums_slider_wrapper liza_blur_on_aside">
		<div class="liza_albums_slider_inner no_fit">
			{foreach from=$datas.links key=key item=link}
                <div class="liza_albums_slide liza_js_bg_image liza_albums_slide1" data-src="{$link}" data-count="{$key}">
                    <div class="liza_albums_slide_content">
                        <h2 class="liza_albums_slide_title"><a href="javascript:;">{$datas.title}</a></h2>
                        <div class="liza_albums_slide_meta liza_meta">
                            <div>{$datas.description nofilter}</div>  
                        </div>
                    </div><!-- .liza_albums_slide_content -->
                </div><!-- .liza_albums_slide -->
            {/foreach}
		</div><!-- .liza_albums_slider_inner -->
	</div><!-- .liza_albums_slider_wrapper -->
	<div class="liza_scroll_to_explore"><span>Kéo xuống để khám phá</span></div>

	<div class="liza_albums_slider_thumbs">
		<div class="liza_albums_slider_thumbs_wrapper">
			<div class="liza_albums_slider_thumbs_inner">                
				{foreach from=$datas.links key=key item=link}
                    <div class="liza_albums_thumb liza_albums_thumb1" data-count="{$key}">
                        <img src="{$link}" alt=""/>
                    </div>					
				{/foreach}
			</div><!-- .liza_albums_slider_thumbs_inner -->
		</div><!-- .liza_albums_slider_thumbs_wrapper -->
		<div class="liza_albums_slider_counter">
			<span class="liza_albums_current_counter">0</span>
			<span>/</span>
			<span class="liza_albums_all_counter">{count($datas.links)}</span>
			<i class="fa fa-angle-right"></i>
		</div><!-- .liza_albums_slider_counter -->
		<div class="liza_mobile_thumbs_toggler"></div>
	</div><!-- .liza_albums_slider_thumbs -->

	<div class="liza_preloader_wrapper">
		<div class="liza_preloader_bar">
			<div class="liza_preloader_line"></div>
		</div>
	</div>
   
    <!-- Scripts -->
    <script src="/gallery/js/jquery.min.js"></script>
	<script src="/gallery/js/kube.js"></script>
    <script src="/gallery/js/jquery.mousewheel.js"></script>
    <script src="/gallery/js/jquery.event.swipe.js"></script>
    
    <script src="/gallery/js/pm_albums_slider.js"></script>
    
    <script src="/gallery/js/theme.js"></script>
</body>
</html>