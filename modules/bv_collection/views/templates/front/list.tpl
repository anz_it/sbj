{*
* 2017 Zemez
*
* JX Blog
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez (Alexander Grosul)
*  @copyright 2017 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<!DOCTYPE html>
<html lang="en-US" class="liza_content_under_header liza_transparent_header has_admin_bar liza_height_100">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">    
	<link rel="icon" href="/gallery/img/pm-logo192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="/gallery/img/pm-logo192.png" />
	<title>Sacombank SBJ - Bộ sưu tập</title> 

    <!--Mobile Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    
    <!-- CSS-->
	<link rel="stylesheet" id="fontawesome-css" href="/gallery/css/font-awesome.min.css" type="text/css" media="all" />
	<link rel="stylesheet" id="kube-css" href="/gallery/css/kube.css" type="text/css" media="all" />
	<link rel="stylesheet" id="swipebox-css" href="/gallery/css/swipebox.css" type="text/css" media="all" />
	<link rel="stylesheet" id="owl-css" href="/gallery/css/owl.carousel.css" type="text/css" media="all" />
	
	<link rel="stylesheet" id="liza-modules-css" href="/gallery/css/modules.css" type="text/css" media="all" />
	<link rel="stylesheet" id="liza-theme-css" href="/gallery/css/theme.css" type="text/css" media="all" />
	<link rel="stylesheet" id="liza-responsive-css" href="/gallery/css/responsive.css" type="text/css" media="all" />
	<link rel="stylesheet" id="liza-settings-css" href="/gallery/css/settings.css" type="text/css" media="all" />
	 
</head>
<body>
  	<!-- Simple Header -->
	<header class="liza_header logo_center menu_simple height-180">
		<div class="liza_header_bg liza_blur_on_aside"></div>
		<div class="liza_header_wrapper">
			<div class="liza_header_left_part">
				<div class="liza_menu_block">
					<a class="liza_menu_toggler" href="javascript:void(0)">
						<span class="liza_menu_ico">
							<span class="liza_menu_ico_line1"></span>
							<span class="liza_menu_ico_line2"></span>
							<span class="liza_menu_ico_line3"></span>
						</span>Menu
					</a><!-- .liza_menu_toggler -->
					<nav class="liza_main_nav liza_blur_on_aside">
						<ul class="liza_menu">
							<li class="menu-item-has-children">
								<a href="javascript:void(0)">Trang chủ</a>
							</li>
						</ul>
					</nav><!-- .liza_main_nav -->
				</div><!-- .liza_menu_block -->
			</div><!-- .liza_header_left_part -->

			<div class="liza_header_middle_part">
				<div class="liza_logo_wrapper liza_blur_on_aside">
					<a href="/" class="liza_image_logo liza_retina"><img src="/gallery/img/logo.png" alt="" width="80" height="43" /></a>
				</div>
			</div><!-- .liza_header_middle_part -->

			<div class="liza_header_right_part">		
				<div class="liza_social_icons liza_blur_on_aside">
					<ul class="liza_social_icons_list">
						<li><a class='facebook' target='_blank' href='https://www.facebook.com/trangsuc.sbj' title='Facebook'><i class='fa fa-facebook'></i></a></li>
						<li><a class='youtube' target='_blank' href='https://www.youtube.com/channel/UChCMoE1Uuix7Q4mnarTa52w' title='Youtube'><i class='fa fa-youtube'></i></a></li>
					</ul><!-- .liza_social_icons_list -->
				</div>
			</div><!-- .liza_header_right_part -->
			<div class="clear"></div>
		</div><!-- .liza_header_wrapper -->
	</header><!-- .menu_simple -->
	
	<!-- Mobile Header -->
	<header class="liza_mobile_header">
		<div class="liza_mobile_header_wrapper">
			<div class="liza_mobile_header_logo">
				<div class="liza_logo_wrapper liza_blur_on_aside">
					<a href="/" class="liza_image_logo liza_retina"><img src="/gallery/img/logo.png" alt="" width="145" height="59" /></a>
				</div>
			</div><!-- .liza_mobile_header_logo -->
			<div class="liza_mobile_button_wrapper">
				<a class="liza_menu_toggler liza_menu_toggler_mobile" href="javascript:void(0)">
					<span class="liza_menu_ico">
						<span class="liza_menu_ico_line1"></span>
						<span class="liza_menu_ico_line2"></span>
						<span class="liza_menu_ico_line3"></span>
					</span>Menu
				</a>
			</div><!-- .liza_mobile_button_wrapper -->
		</div><!-- .liza_mobile_header_wrapper -->
		<div class="liza_mobile_menu_wrapper">
			<div class="liza_social_icons liza_social_icons_mobile liza_container">
				<ul class="liza_social_icons_list">
					<li><a class='facebook' target='_blank' href='https://www.facebook.com/trangsuc.sbj' title='Facebook'><i class='fa fa-facebook'></i></a></li>
                    <li><a class='youtube' target='_blank' href='https://www.youtube.com/channel/UChCMoE1Uuix7Q4mnarTa52w' title='Youtube'><i class='fa fa-youtube'></i></a></li>
					</ul><!-- .liza_social_icons_list -->
			</div><!-- .liza_social_icons_mobile -->
			
			<nav class="liza_mobile_menu liza_container"></nav>
		</div><!-- .liza_mobile_menu_wrapper -->
	</header><!-- .liza_mobile_header -->
	  	
	<div class="liza_container liza_blur_on_aside liza_no_sidebar liza_header_padding_yes liza_footer_padding_yes">
		<div class="liza_content_no_sidebar liza_content">
			<div class="liza_tiny">
				<!-- Content -->
				<div class="row">
					<div class="col col-12 liza_js_padding" data-padBottom="30">
						<div class="liza_albums_grid liza_albums_grid3" data-uniqid="8202">
							<div class="liza_albums_grid_inner" data-pad="60">
								{foreach from=$datas key=key item=data}
                                    <div class="liza_album_grid_post_wrapper liza_album_element liza_album_loading">
                                        <div class="liza_album_grid_post_inner">
                                            <div class="liza_album_grid_image">
                                                <a href="{$link}?id_data={$datas[$key].id_data}">
                                                    <img src="{$data.image_url}" alt=""/>
                                                </a>
                                            </div>
                                            <div class="liza_grid_post_title">
                                                <a class="notextdecor" href="{$link}?id_data={$datas[$key].id_data}">
                                                <h4 class="innertitle entry-title">{$data.title}</h4>
                                                </a>
                                            </div>
                                            <div class="liza_grid_album_meta liza_meta">
                                                <div>{$data.legend}</div> 
                                            </div>
                                        </div>
                                    </div> 
                                {/foreach}
							</div><!-- .liza_albums_grid_inner -->
						</div><!-- .liza_albums_grid -->
					</div><!-- .col-12 -->
				</div><!-- .row -->
				
				 

				<!-- End of Content -->
			</div><!-- .liza_tiny -->
			<!--  Sidebar Placeholder -->
		</div><!-- .liza_content -->
	</div><!-- .liza_container -->
   
    <!-- Scripts -->
    <script src="/gallery/js/jquery.min.js"></script>
	<script src="/gallery/js/kube.js"></script>
    <script src="/gallery/js/jquery.mousewheel.js"></script>
    <script src="/gallery/js/jquery.event.swipe.js"></script>
    <script src="/gallery/js/simple_slider.js"></script>
    <script src="/gallery/js/jquery.isotope.min.js"></script>
    <script src="/gallery/js/posts_sorting.js"></script>
    <script src="/gallery/js/theme.js"></script>
</body>
</html>