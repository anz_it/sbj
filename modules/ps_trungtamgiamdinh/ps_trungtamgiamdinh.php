<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Ps_trungtamgiamdinh extends Module implements WidgetInterface
{
    private $templateFile;

	public function __construct()
	{
		$this->name = 'ps_trungtamgiamdinh';
		$this->version = '2.1.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('trungtamgiamdinh', array(), 'Modules.trungtamgiamdinh.Admin');
        $this->description = $this->trans('Displays a trungtamgiamdinh on your shop.', array(), 'Modules.trungtamgiamdinh.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:ps_trungtamgiamdinh/ps_trungtamgiamdinh.tpl';
    }

    public function install()
    {
        return (parent::install() &&
            $this->registerHook('displayHome') &&
            $this->registerHook('actionObjectLanguageAddAfter') &&
            $this->installFixtures() &&
            $this->disableDevice(Context::DEVICE_MOBILE));
    }

    public function hookActionObjectLanguageAddAfter($params)
    {
        return $this->installFixture((int)$params['object']->id, Configuration::get('trungtamgiamdinh_IMG', (int)Configuration::get('PS_LANG_DEFAULT')));
    }

    protected function installFixtures()
    {
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $this->installFixture((int)$lang['id_lang'], 'sale70.png');
        }

        return true;
    }

    protected function installFixture($id_lang, $image = null)
    {
        $values['trungtamgiamdinh_IMG'][(int)$id_lang] = $image;
        $values['trungtamgiamdinh_whisper'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_whisper_id'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_whisper_desc'][(int)$id_lang] = '';

        $values['trungtamgiamdinh_dich_vu_kim_hoan'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_dich_vu_kim_hoan_img'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_dich_vu_kim_hoan_desc'][(int)$id_lang] = '';

        $values['trungtamgiamdinh_dich_vu_giam_dinh'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_dich_vu_giam_dinh_img'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_dich_vu_giam_dinh_desc'][(int)$id_lang] = '';

        $values['trungtamgiamdinh_tieu_chuan_chat_luong'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_tieu_chuan_chat_luong_img'][(int)$id_lang] = '';
        $values['trungtamgiamdinh_tieu_chuan_chat_luong_desc'][(int)$id_lang] = '';

        $values['trungtamgiamdinh_tin_tuc_kien_thuc'][(int)$id_lang] = '';

        Configuration::updateValue('trungtamgiamdinh_IMG', $values['trungtamgiamdinh_IMG']);
        Configuration::updateValue('trungtamgiamdinh_whisper', $values['trungtamgiamdinh_whisper']);
        Configuration::updateValue('trungtamgiamdinh_whisper_id', $values['trungtamgiamdinh_whisper_id']);
        Configuration::updateValue('trungtamgiamdinh_whisper_desc', $values['trungtamgiamdinh_whisper_desc']);

        Configuration::updateValue('trungtamgiamdinh_dich_vu_kim_hoan', $values['trungtamgiamdinh_dich_vu_kim_hoan']);
        Configuration::updateValue('trungtamgiamdinh_dich_vu_kim_hoan_img', $values['trungtamgiamdinh_dich_vu_kim_hoan_img']);
        Configuration::updateValue('trungtamgiamdinh_dich_vu_kim_hoan_desc', $values['trungtamgiamdinh_dich_vu_kim_hoan_desc']);

        Configuration::updateValue('trungtamgiamdinh_dich_vu_giam_dinh', $values['trungtamgiamdinh_dich_vu_giam_dinh']);
        Configuration::updateValue('trungtamgiamdinh_dich_vu_giam_dinh_img', $values['trungtamgiamdinh_dich_vu_giam_dinh_img']);
        Configuration::updateValue('trungtamgiamdinh_dich_vu_giam_dinh_desc', $values['trungtamgiamdinh_dich_vu_giam_dinh_desc']);


        Configuration::updateValue('trungtamgiamdinh_tieu_chuan_chat_luong', $values['trungtamgiamdinh_tieu_chuan_chat_luong']);
        Configuration::updateValue('trungtamgiamdinh_tieu_chuan_chat_luong_img', $values['trungtamgiamdinh_tieu_chuan_chat_luong_img']);
        Configuration::updateValue('trungtamgiamdinh_tieu_chuan_chat_luong_desc', $values['trungtamgiamdinh_tieu_chuan_chat_luong_desc']);

        Configuration::updateValue('trungtamgiamdinh_tin_tuc_kien_thuc', $values['trungtamgiamdinh_tin_tuc_kien_thuc']);
    }

    public function uninstall()
    {
        Configuration::deleteByName('trungtamgiamdinh_IMG');
        Configuration::deleteByName('trungtamgiamdinh_whisper');
        Configuration::deleteByName('trungtamgiamdinh_whisper_id');
        Configuration::deleteByName('trungtamgiamdinh_whisper_desc');

        Configuration::deleteByName('trungtamgiamdinh_dich_vu_kim_hoan');
        Configuration::deleteByName('trungtamgiamdinh_dich_vu_kim_hoan_img');
        Configuration::deleteByName('trungtamgiamdinh_dich_vu_kim_hoan_desc');

        Configuration::deleteByName('trungtamgiamdinh_dich_vu_giam_dinh');
        Configuration::deleteByName('trungtamgiamdinh_dich_vu_giam_dinh_img');
        Configuration::deleteByName('trungtamgiamdinh_dich_vu_giam_dinh_desc');

        Configuration::deleteByName('trungtamgiamdinh_tieu_chuan_chat_luong');
        Configuration::deleteByName('trungtamgiamdinh_tieu_chuan_chat_luong_img');
        Configuration::deleteByName('trungtamgiamdinh_tieu_chuan_chat_luong_desc');

        Configuration::deleteByName('trungtamgiamdinh_tin_tuc_kien_thuc');

        return parent::uninstall();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            $languages = Language::getLanguages(false);
            $values = array();
            $update_images_values = false;
            $trungtamgiamdinh_dich_vu_kim_hoan_img = false;
            $trungtamgiamdinh_dich_vu_giam_dinh_img = false;
            $trungtamgiamdinh_tieu_chuan_chat_luong_img = false;

            foreach ($languages as $lang) {
                if (isset($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']])
                    && isset($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']]['tmp_name'])
                    && !empty($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']]['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']], 4000000)) {
                        return $error;
                    } else {
                        $ext = substr($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']]['name'], strrpos($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']]['name'], '.') + 1);
                        $file_name = md5($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']]['name']).'.'.$ext;

                        if (!move_uploaded_file($_FILES['trungtamgiamdinh_IMG_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                            return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                        } else {
                            if (Configuration::hasContext('trungtamgiamdinh_IMG', $lang['id_lang'], Shop::getContext())
                                && Configuration::get('trungtamgiamdinh_IMG', $lang['id_lang']) != $file_name) {
                                @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('trungtamgiamdinh_IMG', $lang['id_lang']));
                            }

                            $values['trungtamgiamdinh_IMG'][$lang['id_lang']] = $file_name;
                        }
                    }

                    $update_images_values = true;
                }

                if (isset($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']])
                    && isset($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']]['tmp_name'])
                    && !empty($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']]['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']], 4000000)) {
                        return $error;
                    } else {
                        $ext = substr($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']]['name'], strrpos($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']]['name'], '.') + 1);
                        $file_name = md5($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']]['name']).'.'.$ext;
                        if (!move_uploaded_file($_FILES['trungtamgiamdinh_dich_vu_kim_hoan_img_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                            return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                        } else {
                            if (Configuration::hasContext('trungtamgiamdinh_dich_vu_kim_hoan_img', $lang['id_lang'], Shop::getContext())
                                && Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_img', $lang['id_lang']) != $file_name) {
                                @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_img', $lang['id_lang']));
                            }

                            $values['trungtamgiamdinh_dich_vu_kim_hoan_img'][$lang['id_lang']] = $file_name;
                        }
                    }
                    $trungtamgiamdinh_dich_vu_kim_hoan_img = true;
                }

                if (isset($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']])
                    && isset($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']]['tmp_name'])
                    && !empty($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']]['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']], 4000000)) {
                        return $error;
                    } else {
                        $ext = substr($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']]['name'], strrpos($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']]['name'], '.') + 1);
                        $file_name = md5($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']]['name']).'.'.$ext;

                        if (!move_uploaded_file($_FILES['trungtamgiamdinh_dich_vu_giam_dinh_img_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                            return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                        } else {
                            if (Configuration::hasContext('trungtamgiamdinh_dich_vu_giam_dinh_img', $lang['id_lang'], Shop::getContext())
                                && Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_img', $lang['id_lang']) != $file_name) {
                                @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_img', $lang['id_lang']));
                            }

                            $values['trungtamgiamdinh_dich_vu_giam_dinh_img'][$lang['id_lang']] = $file_name;
                        }
                    }

                    $trungtamgiamdinh_dich_vu_giam_dinh_img = true;
                }

                if (isset($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']])
                    && isset($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']]['tmp_name'])
                    && !empty($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']]['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']], 4000000)) {
                        return $error;
                    } else {
                        $ext = substr($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']]['name'], strrpos($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']]['name'], '.') + 1);
                        $file_name = md5($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']]['name']).'.'.$ext;

                        if (!move_uploaded_file($_FILES['trungtamgiamdinh_tieu_chuan_chat_luong_img_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                            return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                        } else {
                            if (Configuration::hasContext('trungtamgiamdinh_tieu_chuan_chat_luong_img', $lang['id_lang'], Shop::getContext())
                                && Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_img', $lang['id_lang']) != $file_name) {
                                @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_img', $lang['id_lang']));
                            }
                            $values['trungtamgiamdinh_tieu_chuan_chat_luong_img'][$lang['id_lang']] = $file_name;
                        }
                    }

                    $trungtamgiamdinh_tieu_chuan_chat_luong_img = true;
                }
                $values['trungtamgiamdinh_whisper'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_whisper_'.$lang['id_lang']);
                $values['trungtamgiamdinh_whisper_id'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_whisper_id_'.$lang['id_lang']);
                $values['trungtamgiamdinh_whisper_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_whisper_desc_'.$lang['id_lang']);

                $values['trungtamgiamdinh_dich_vu_giam_dinh'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_giam_dinh_'.$lang['id_lang']);
                $values['trungtamgiamdinh_dich_vu_giam_dinh_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_giam_dinh_desc_'.$lang['id_lang']);

                $values['trungtamgiamdinh_dich_vu_kim_hoan'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_kim_hoan_'.$lang['id_lang']);
                $values['trungtamgiamdinh_dich_vu_kim_hoan_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_kim_hoan_desc_'.$lang['id_lang']);

                $values['trungtamgiamdinh_tieu_chuan_chat_luong'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tieu_chuan_chat_luong_'.$lang['id_lang']);
                $values['trungtamgiamdinh_tieu_chuan_chat_luong_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tieu_chuan_chat_luong_desc_'.$lang['id_lang']);

                $values['trungtamgiamdinh_tin_tuc_kien_thuc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tin_tuc_kien_thuc_'.$lang['id_lang']);

            }

            if ($update_images_values) {
                Configuration::updateValue('trungtamgiamdinh_IMG', $values['trungtamgiamdinh_IMG']);
            }

            if ($trungtamgiamdinh_dich_vu_kim_hoan_img) {
                Configuration::updateValue('trungtamgiamdinh_dich_vu_kim_hoan_img', $values['trungtamgiamdinh_dich_vu_kim_hoan_img']);
            }
            if ($trungtamgiamdinh_dich_vu_giam_dinh_img) {
                Configuration::updateValue('trungtamgiamdinh_dich_vu_giam_dinh_img', $values['trungtamgiamdinh_dich_vu_giam_dinh_img']);
            }
            if ($trungtamgiamdinh_tieu_chuan_chat_luong_img) {
                Configuration::updateValue('trungtamgiamdinh_tieu_chuan_chat_luong_img', $values['trungtamgiamdinh_tieu_chuan_chat_luong_img']);
            }
            Configuration::updateValue('trungtamgiamdinh_whisper', $values['trungtamgiamdinh_whisper']);
            Configuration::updateValue('trungtamgiamdinh_whisper_id', $values['trungtamgiamdinh_whisper_id']);
            Configuration::updateValue('trungtamgiamdinh_whisper_desc', $values['trungtamgiamdinh_whisper_desc']);

            Configuration::updateValue('trungtamgiamdinh_dich_vu_giam_dinh', $values['trungtamgiamdinh_dich_vu_giam_dinh']);
            Configuration::updateValue('trungtamgiamdinh_dich_vu_giam_dinh_desc', $values['trungtamgiamdinh_dich_vu_giam_dinh_desc']);

            Configuration::updateValue('trungtamgiamdinh_dich_vu_kim_hoan', $values['trungtamgiamdinh_dich_vu_kim_hoan']);
            Configuration::updateValue('trungtamgiamdinh_dich_vu_kim_hoan_desc', $values['trungtamgiamdinh_dich_vu_kim_hoan_desc']);

            Configuration::updateValue('trungtamgiamdinh_tieu_chuan_chat_luong', $values['trungtamgiamdinh_tieu_chuan_chat_luong']);
            Configuration::updateValue('trungtamgiamdinh_tieu_chuan_chat_luong_desc', $values['trungtamgiamdinh_tieu_chuan_chat_luong_desc']);

            Configuration::updateValue('trungtamgiamdinh_tin_tuc_kien_thuc', $values['trungtamgiamdinh_tin_tuc_kien_thuc']);

            $this->_clearCache($this->templateFile);
            return $this->displayConfirmation($this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success'));
        }

        return '';
    }

    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();

        foreach ($languages as $lang) {
            $fields['trungtamgiamdinh_IMG'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_IMG_'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_IMG', $lang['id_lang']));
            $fields['trungtamgiamdinh_whisper'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_whisper'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_whisper', $lang['id_lang']));
            $fields['trungtamgiamdinh_whisper_id'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_whisper_id'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_whisper_id', $lang['id_lang']));
            $fields['trungtamgiamdinh_whisper_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_whisper_desc'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_whisper_desc', $lang['id_lang']));

            $fields['trungtamgiamdinh_dich_vu_kim_hoan'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_kim_hoan'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan', $lang['id_lang']));
            $fields['trungtamgiamdinh_dich_vu_kim_hoan_img'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_kim_hoan_img'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_img', $lang['id_lang']));
            $fields['trungtamgiamdinh_dich_vu_kim_hoan_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_kim_hoan_desc'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_desc', $lang['id_lang']));

            $fields['trungtamgiamdinh_dich_vu_giam_dinh'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_giam_dinh'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh', $lang['id_lang']));
            $fields['trungtamgiamdinh_dich_vu_giam_dinh_img'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_giam_dinh_img'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_img', $lang['id_lang']));
            $fields['trungtamgiamdinh_dich_vu_giam_dinh_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_dich_vu_giam_dinh_desc'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh', $lang['id_lang']));

            $fields['trungtamgiamdinh_tieu_chuan_chat_luong'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tieu_chuan_chat_luong'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong', $lang['id_lang']));
            $fields['trungtamgiamdinh_tieu_chuan_chat_luong_img'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tieu_chuan_chat_luong_img'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_img', $lang['id_lang']));
            $fields['trungtamgiamdinh_tieu_chuan_chat_luong_desc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tieu_chuan_chat_luong_desc'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_desc', $lang['id_lang']));

            $fields['trungtamgiamdinh_tin_tuc_kien_thuc'][$lang['id_lang']] = Tools::getValue('trungtamgiamdinh_tin_tuc_kien_thuc'.$lang['id_lang'], Configuration::get('trungtamgiamdinh_tin_tuc_kien_thuc', $lang['id_lang']));
        }
        return $fields;
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    /////whisper
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Whisper from the valley banner', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_IMG',
                        'desc' => $this->trans('Chọn banner cho block Whisper', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Id page whisper', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_whisper_id',
                        'desc' => $this->trans('Nhập Id cho page whisper', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Sub description whisper', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_whisper',
                        'desc' => $this->trans('Nhập sub cho page whisper', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Whisper description', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_whisper_desc',
                        'desc' => $this->trans('Nhập mô tả cho block Whispe ', array(), 'Modules.Banner.Admin')
                    ),
                    ////// Dich vu giam dinh
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Dịch vụ giám định banner', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_dich_vu_giam_dinh_img',
                        'desc' => $this->trans('Chọn banner cho block dịch vụ kim hoàn', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Dịch vụ giám định page ID', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_dich_vu_giam_dinh',
                        'desc' => $this->trans('Nhập Id cua page dịch vụ giám định', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Dịch vụ giám định description', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_dich_vu_giam_dinh_desc',
                        'desc' => $this->trans('Nhập mô tả cho block dịch vụ giám định', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),
                    // Dich vu kim hoan
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Dich vu kim hoàn banner', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_dich_vu_kim_hoan_img',
                        'desc' => $this->trans('Chọn banner cho block dịch vụ kim hoàn', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'lang' => true,
                    ),

                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Dich vu kim hoàn page ID', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_dich_vu_kim_hoan',
                        'desc' => $this->trans('Nhập Id cua page dịch vụ kim hoàn', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Dịch vụ kim hoàn description', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_dich_vu_kim_hoan_desc',
                        'desc' => $this->trans('Nhập nội dung mô tả dịch vụ kim hoàn ', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),
                    // Tieu chuan chat luong
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Tiêu chuẩn chất lượng banner', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_tieu_chuan_chat_luong_img',
                        'desc' => $this->trans('Chọn banner cho block tiêu chuẩn chất lượng', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Tiêu chuẩn chất lượng page Id', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_tieu_chuan_chat_luong',
                        'desc' => $this->trans('Nhập tiêu chuẩn chất lượng page Id', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),

                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Tiêu chuẩn chất lượng description', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_tieu_chuan_chat_luong_desc',
                        'desc' => $this->trans('nhập mô tả tiêu chuẩn chất lượng', array(), 'Modules.trungtamgiamdinh.Admin')
                    ),

                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Tin tức kiến thức ID', array(), 'Modules.trungtamgiamdinh.Admin'),
                        'name' => 'trungtamgiamdinh_tin_tuc_kien_thuc',
                        'desc' => $this->trans('Nhập Id categtory tin tức kiến thức', array(), 'Modules.trungtamgiamdinh.Admin')
                    )
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions')
                )
            ),
        );

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStoreConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }


    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('ps_trungtamgiamdinh'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('ps_trungtamgiamdinh'));
    }


    public function getWidgetVariables($hookName, array $params)
    {

        $imgname = Configuration::get('trungtamgiamdinh_IMG', $this->context->language->id);
        $imgname_dichvukimhoan = Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_img', $this->context->language->id);
        $imgname_dichvugiamdinh = Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_img', $this->context->language->id);

        if ($imgname && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)) {
            $this->smarty->assign('trungtamgiamdinh_img', $this->context->link->protocol_content . Tools::getMediaServer($imgname) . $this->_path . 'img/' . $imgname);
        }

        if ($imgname_dichvukimhoan && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)) {
            $this->smarty->assign('trungtamgiamdinh_dich_vu_kim_hoan_img', $this->context->link->protocol_content . Tools::getMediaServer($imgname) . $this->_path . 'img/' . $imgname);
        }
        if ($imgname_dichvugiamdinh && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)) {
            $this->smarty->assign('trungtamgiamdinh_dich_vu_giam_dinh_img', $this->context->link->protocol_content . Tools::getMediaServer($imgname) . $this->_path . 'img/' . $imgname);
        }

        $trungtamgiamdinh_whisper_link = Configuration::get('trungtamgiamdinh_whisper_id', $this->context->language->id);
        $trungtamgiamdinh_link = Configuration::get('trungtamgiamdinh_LINK', $this->context->language->id);
        $dichvukimhoan_link = Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_img', $this->context->language->id);
        $dichvugiamdinh_link = Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_img', $this->context->language->id);

        $trungtamgiamdinh_whisper_link = $this->context->link->getCMSLink($trungtamgiamdinh_whisper_link);
        if (!$trungtamgiamdinh_link) {
            $trungtamgiamdinh_link = $this->context->link->getPageLink('index');
        }
        if (!$dichvukimhoan_link) {
            $dichvukimhoan_link = $this->context->link->getPageLink('index');
        }
        if (!$dichvugiamdinh_link) {
            $dichvugiamdinh_link = $this->context->link->getPageLink('index');
        }
        return array(
            'trungtamgiamdinh_link' => $this->updateUrl($trungtamgiamdinh_link),
            'trungtamgiamdinh_dich_vu_kim_hoan_img' => $this->updateUrl($dichvukimhoan_link),
            'trungtamgiamdinh_dich_vu_giam_dinh_img' => $this->updateUrl($dichvugiamdinh_link),
            'trungtamgiamdinh_whisper' => Configuration::get('trungtamgiamdinh_whisper', $this->context->language->id),
            '$trungtamgiamdinh_whisper_link' => $trungtamgiamdinh_whisper_link,
            'trungtamgiamdinh_whisper_id' => Configuration::get('trungtamgiamdinh_whisper_id', $this->context->language->id),
            'trungtamgiamdinh_whisper_desc' => Configuration::get('trungtamgiamdinh_whisper', $this->context->language->id),
            'trungtamgiamdinh_dich_vu_kim_hoan' => Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan', $this->context->language->id),
            'trungtamgiamdinh_dich_vu_kim_hoan_desc' => Configuration::get('trungtamgiamdinh_dich_vu_kim_hoan_desc', $this->context->language->id),
            'trungtamgiamdinh_dich_vu_giam_dinh' => Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh', $this->context->language->id),
            'trungtamgiamdinh_dich_vu_giam_dinh_desc' => Configuration::get('trungtamgiamdinh_dich_vu_giam_dinh_desc', $this->context->language->id),
            'trungtamgiamdinh_tieu_chuan_chat_luong' => Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong', $this->context->language->id),
            'trungtamgiamdinh_tieu_chuan_chat_luong_desc' => Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_desc', $this->context->language->id),
            'trungtamgiamdinh_tieu_chuan_chat_luong_img' => Configuration::get('trungtamgiamdinh_tieu_chuan_chat_luong_img', $this->context->language->id),
            'trungtamgiamdinh_tin_tuc_kien_thuc' => Configuration::get('trungtamgiamdinh_tin_tuc_kien_thuc', $this->context->language->id),
        );
    }

    private function updateUrl($link)
    {
        if (substr($link, 0, 7) !== "http://" && substr($link, 0, 8) !== "https://") {
            $link = "http://" . $link;
        }

        return $link;
    }
}
