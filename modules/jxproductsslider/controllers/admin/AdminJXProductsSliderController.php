<?php
/**
* 2002-2018 Zemez
*
* JX Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez
*  @copyright 2002-2018 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

class AdminJXProductsSliderController extends ModuleAdminController
{
    public function ajaxProcessUpdatePosition()
    {
        $items = Tools::getValue('item');
        $total = count($items);
        $success = true;
        for ($i = 1; $i <= $total; $i++) {
            $success &= Db::getInstance()->update(
                'jxproductsslider_item',
                array('slide_order' => $i),
                '`id_slide` = '.str_replace('_0', '', preg_replace('/(tr__)([0-9]+)/', '${2}', $items[$i - 1]))
            );
        }
        if (!$success) {
            die(json_encode(array('error' => 'Update Fail')));
        }
        die(json_encode(array('success' => 'Update Success !', 'error' => false)));
    }

    /**
     * Add item tab
     * @return array
     */
    public function ajaxProcessSaveItemTab()
    {
        $id_product = (int)Tools::getValue('id_product');
        $hook_name = Tools::getValue('hook_name');
        $is_slide = (int)Tools::getValue('is_slide');

        if (!$is_slide) {
            $this->removeSlide($id_product, $hook_name);
        } else {
            $this->addSlide($id_product, $hook_name);
        }

        die(json_encode(array('success_status' => 'Information Add Success!', 'error' => false)));
    }


    /**
     * Use product us a slide
     * @param $id_product
     *
     * @return bool|string
     */
    protected function addSlide($id_product, $hook_name)
    {
        if (!JXProductSlide::checkSlideExist($id_product, $hook_name, $this->context->shop->id)) {
            $product_slide = new JXProductSlide();
            $product_slide->id_product = $id_product;
            $product_slide->id_shop = $this->context->shop->id;
            $product_slide->hook_name = $hook_name;
            $product_slide->slide_order = $product_slide->setSortOrder(
                $product_slide->id_shop,
                $product_slide->id_product,
                true
            );
            $product_slide->slide_status = true;
            if (!$product_slide->add()) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ').mysqli_error();
            }
        }
    }

    /**
     * Remove product from slides
     * @param $id_product
     *
     * @return bool|string
     */
    protected function removeSlide($id_product, $hook_name)
    {
        if ($slide_id = JXProductSlide::checkSlideExist($id_product, $hook_name, $this->context->shop->id)) {
            $product_slide = new JXProductSlide($slide_id['id_slide']);

            if (!$product_slide->delete()) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ').mysqli_error();
                return false;
            }

            return true;
        } else {
            return false;
        }
    }
}
