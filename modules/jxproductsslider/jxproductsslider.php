<?php
/**
* 2002-2018 Zemez
*
* JX Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Zemez
* @copyright 2002-2018 Zemez
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;

require_once(dirname(__FILE__).'/classes/JXProductSlider.php');
require_once(dirname(__FILE__).'/classes/JXProductSlide.php');

class JXProductsSlider extends Module implements WidgetInterface
{

    public  $repository;
    private $defaultHook;
    private $currentHook;
    private $hooks = false;

    public function __construct()
    {
        $this->name = 'jxproductsslider';
        $this->tab = 'front_office_features';
        $this->version = '2.0.0';
        $this->bootstrap = true;
        $this->author = 'Zemez';
        $this->default_language = Language::getLanguage(Configuration::get('PS_LANG_DEFAULT'));
        $this->id_shop = Context::getContext()->shop->id;
        $this->languages = Language::getLanguages();
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = '4d13770dd3ec44a69f4ab2ca34e14fdc';
        parent::__construct();
        $this->displayName = $this->l('JX Products Slider');
        $this->description = $this->l('Module for displaying products in slider.');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->defaultHook = 'displayHome';
        $this->currentHook = $this->defaultHook;
        $this->repository = new JXProductSlide();
        $h = $this->repository->getAllModuleHooks($this->name, $this->defaultHook);
        if (is_array($h)) {
            $this->hooks = $h;
        } elseif ($h) {
            $this->currentHook = $h;
        }
    }

    public function createAjaxController()
    {
        $tab = new Tab();
        $tab->active = 1;
        $languages = Language::getLanguages(false);
        if (is_array($languages)) {
            foreach ($languages as $language) {
                $tab->name[$language['id_lang']] = 'jxproductsslider';
            }
        }
        $tab->class_name = 'AdminJXProductsSlider';
        $tab->module = $this->name;
        $tab->id_parent = - 1;
        return (bool)$tab->add();
    }

    private function removeAjaxContoller()
    {
        if ($tab_id = (int)Tab::getIdFromClassName('AdminJXProductsSlider')) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }
        return true;
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        if (!parent::install()
            || !$this->registerHook('displayBackOfficeHeader')
            || !$this->registerHook('actionAdminControllerSetMedia')
            || !$this->registerHook('displayAdminProductsExtra')
            || !$this->registerHook('actionProductUpdate')
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('displayHome')
            || !$this->registerHook('displayBeforeBodyClosingTag')
            || !$this->createAjaxController()) {
            return false;
        } else {
            //set default setting each shop
            $shops = Shop::getContextListShopID();

            foreach ($shops as $shop_id) {
                $this->setDefaultSettings($shop_id);
            }
        }

        return true;
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        if (!$this->removeAjaxContoller()
            || !parent::uninstall()) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $output = '';

        if ($loadedHook = Tools::getValue('hookName')) {
            $this->currentHook = $loadedHook;
        }

        if ($this->id_shop != Tools::getValue('id_shop')) {
            $token = Tools::getAdminTokenLite('AdminModules');
            $current_index = AdminController::$currentIndex;
            Tools::redirectAdmin(
                $current_index . '&configure=' . $this->name . '&token=' . $token . '&shopselected&id_shop=' . $this->id_shop . ($this->hooks ? '&hookName=' . $this->currentHook : '')
            );
        }
        if (!$multiwarning = $this->getWarningMultishopHtml()) {
            if (Tools::isSubmit('submitJxproductSliderModule')) {
                $this->postProcess();
                $output .= $this->displayConfirmation($this->l('Settings successfully saved.'));
            }
        } else {
            $output .= $multiwarning;
            return $output;
        }

        if (Tools::getIsset('deletejxproductsslider_item')) {
            if ($id_slide = Tools::getValue('id_slide')) {
                $slide = new JXProductSlide($id_slide);
                if ($slide->id) {
                    if ($this->removeSlide($slide->id_product)) {
                        $output .= $this->displayConfirmation($this->l('Slide is removed.'));
                    } else {
                        $output .= $this->displayError($this->l('Some problem occurred during slide removing.'));
                    }
                }
            } else {
                $output .= $this->displayError($this->l('Ooops! It\'s look like no slider id defined.'));
            }
        }

        if (Tools::getIsset('slide_status')) {
            if ($id_slide = Tools::getValue('id_slide')) {
                if ($this->changeStatus($id_slide)) {
                    $output .= $this->displayConfirmation($this->l('Slide status successfully updated'));
                } else {
                    $output .= $this->displayError($this->l('Some problem occurred during changing the slide status.'));
                }
            } else {
                $output .= $this->displayError($this->l('No slide id found.'));
            }
        }
        if ($this->hooks) {
            $output .= $this->renderHooksForm($this->hooks);
        }
        $output .= $this->renderList();
        $output .= $this->renderForm();

        return $output;
    }

    /**
     * Build the module form
     * @return mixed
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitJxproductSliderModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_shop='.$this->id_shop.'&hookName='.$this->currentHook;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    protected function getConfigForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'form_group_class' => 'slider-type',
                        'type' => 'select',
                        'label' => $this->l('Gallery Type'),
                        'name' => 'slider_type',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 'standard',
                                    'name' => $this->l('standard')),
                                array(
                                    'id' => 'list',
                                    'name' => $this->l('list')),
                                array(
                                    'id' => 'grid',
                                    'name' => $this->l('grid')),
                                array(
                                    'id' => 'fullwidth',
                                    'name' => $this->l('full width'))
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $extended_fields = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'extended_settings', /*'lable'*/ 'Extended Settings', /*'desc'*/ 'Extended Settings', /*'class'*/ '')
        );

        foreach ($extended_fields as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $slider_duration = $this->addPropertyField(
            'text',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_duration', /*'lable'*/ 'Slider Duration', /*'desc'*/ 'Interval in milliseconds.', /*'class'*/ 'extended')
        );

        foreach ($slider_duration as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $nav_fields = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_navigation', /*'lable'*/ 'Use navigation', /*'desc'*/ 'Use navigation', /*'class'*/ 'extended')
        );

        foreach ($nav_fields as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $nav_fields = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_thumbnails', /*'lable'*/ 'Use Thumbnails', /*'desc'*/ 'Use Thumbnails', /*'class'*/ 'extended')
        );

        foreach ($nav_fields as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $pag_fields = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_pagination', /*'lable'*/ 'Use Pagination ', /*'desc'*/ 'Use Pagination', /*'class'*/ 'extended')
        );

        foreach ($pag_fields as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $imggallery_fields = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'images_gallery', /*'lable'*/ 'Use Image Gallery', /*'desc'*/ 'Use gallery to display sub images', /*'class'*/ 'extended')
        );

        foreach ($imggallery_fields as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $slider_autoplay = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_autoplay', /*'lable'*/ 'Allow autoplay', /*'desc'*/ 'Allow slider autoplay', /*'class'*/ 'extended')
        );

        foreach ($slider_autoplay as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $slider_interval = $this->addPropertyField(
            'text',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_interval', /*'lable'*/ 'Gallery Interval', /*'desc'*/ 'Interval between slides showing in milliseconds.', /*'class'*/ 'extended autoplay')
        );

        foreach ($slider_interval as $field) {
            $fields_form['form']['input'][] = $field;
        }

        $slider_loop = $this->addPropertyField(
            'switch',
            array('standard', 'list', 'grid', 'fullwidth'),
            array(/*name*/ 'slider_loop', /*'lable'*/ 'Allow Loop', /*'desc'*/ 'Allow slider loop. Slideshow starts from the first slide after last was showed.', /*'class'*/ 'extended')
        );

        foreach ($slider_loop as $field) {
            $fields_form['form']['input'][] = $field;
        }

        return $fields_form;
    }

    public function getConfigFormValues()
    {
        $fields_values = array();
        if ($item = JXProductSlider::getShopSliderSettings($this->context->shop->id, Tools::getValue("hookName"))) {
            $slider = new JXProductSlider($item['id_slider']);
        } else {
            $slider = new JXProductSlider($this->setDefaultSettings($this->context->shop->id, Tools::getValue("hookName")));
            $item = JXProductSlider::getShopSliderSettings($this->context->shop->id, Tools::getValue("hookName"));
        }

        foreach (array_keys($item) as $name) {
            if ($name != 'id_slider') {
                $fields_values[$name] = Tools::getValue($name, $slider->$name);
            }
        }

        return $fields_values;
    }

    protected function renderHooksForm($availableHooks, array $hooks = [])
    {
        foreach ($availableHooks as $hook) {
            $hooks[] = ['id' => $hook['name'], 'name' => $hook['name']];
        }
        $fields_form = array(
            'form' => array(
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select the hook to set up'),
                        'name'    => 'hookName',
                        'options' => array(
                            'query' => $hooks,
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                )
            )
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getHooksFormValues(), /* Add values for your inputs */
            'languages'    => $this->context->controller->getLanguages(),
            'id_language'  => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    protected function getHooksFormValues()
    {
        return array('hookName' => $this->currentHook);
    }

    protected function removeSlide($id_product)
    {
        if ($slide_id = JXProductSlide::checkSlideExist($id_product, Tools::getValue("hookName"), $this->context->shop->id)) {
            $product_slide = new JXProductSlide($slide_id['id_slide']);
            if (!$product_slide->delete()) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ').mysqli_error();
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update Settings values
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();
        $hook = Tools::getValue('hookName');
        if ($item = JXProductSlider::getShopSliderSettings($this->context->shop->id, $hook)) {
            $slider = new JXProductSlider($item['id_slider']);
        } else {
            $slider = new JXProductSlider();
        }

        foreach (array_keys($form_values) as $name) {
            $slider->$name = Tools::getValue($name);
        }

        $slider->hook_name = $hook;
        $slider->id_shop = $this->context->shop->id;

        if ($item) {
            $slider->update();
        } else {
            $slider->add();
        }
    }

    protected function addPropertyField($type, $sliders, $info)
    {
        $field = array();

        foreach ($sliders as $name) {
            switch ($type) {
                case 'switch':
                    $field[] =  $this->addPropertySwitch($name, $info[0], $info[1], $info[3]);
                    break;
                case 'text':
                    $field[] = $this->addPropertyText($name, $info[0], $info[1], $info[2], $info[3]);
                    break;
                case 'select':
                    $field[] = $this->addPropertySelect($name, $info[0], $info[1], $info[2], $info[3], $info[4]);
                    break;
                default:
                    $field[] = $this->addPropertyText($name, $info[0], $info[1], $info[2], $info[3]);
            }
        }

        return $field;
    }

    protected function addPropertySwitch($slider_type, $field_name, $label, $class)
    {
        return array(
            'form_group_class' => 'property slider-'.$slider_type.' '.$class,
            'type' => 'switch',
            'label' => $this->l($label),
            'name' => $slider_type.'_'.$field_name,
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => true,
                    'label' => $this->l('Enabled')
                ),
                array(
                    'id' => 'active_off',
                    'value' => false,
                    'label' => $this->l('Disabled')
                )
            ),
        );
    }

    protected function addPropertyText($slider_type, $field_name, $label, $description, $class)
    {
        return array(
            'form_group_class' => 'property slider-'.$slider_type.' '.$class,
            'col' => 2,
            'type' => 'text',
            'name' => $slider_type.'_'.$field_name,
            'label' => $this->l($label),
            'desc' => $this->l($description),
        );
    }

    protected function addPropertySelect($slider_type, $field_name, $label, $description, $class, $fields)
    {
        $options = array();
        for ($i = 1; $fields[1] >= $i; $i++) {
            $options[] = array('id' => $fields[0].'_'.$i, 'name' => $fields[0].'-'.$i);
        }

        return array(
            'form_group_class' => 'property slider-'.$slider_type.' '.$class,
            'type' => 'select',
            'label' => $this->l($label),
            'name' => $slider_type.'_'.$field_name,
            'desc' => $this->l($description),
            'options' => array(
                'query' =>
                    $options
                ,
                'id' => 'id',
                'name' => 'name'
            )
        );
    }

    /**
     * @return string Html of html content form
     */
    public function renderList()
    {
        if (!$slides = JXProductSlide::getShopSlides($this->currentHook, $this->context->shop->id, $this->context->language->id)) {
            $slides = array();
        }

        $fields_list = array(
            'id_slide' => array(
                'title' => $this->l('Slide ID'),
                'type' => 'text',
                'class' => 'id_slide'
            ),
            'id_product' => array(
                'title' => $this->l('Product ID'),
                'type' => 'text',
                'class' => 'id_product'
            ),
            'name' => array(
                'title' => $this->l('Product name'),
                'type' => 'text',
            ),
            'slide_order' => array(
                'title' => $this->l('Slide order'),
                'class' => 'sort_order'
            ),
            'slide_status' => array(
                'type' => 'bool',
                'title' => $this->l('Status'),
                'align' => 'center',
                'active' => 'slide_status&',
                'search' => false,
                'orderby' => false
            )
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_slide';
        $helper->position_identifier = true;
        $helper->table = 'jxproductsslider_item';
        $helper->actions = array('delete');
        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->no_link = true;
        $helper->title = $this->l('Slides list');
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&id_shop='.$this->id_shop.($this->hooks ? '&hookName='.$this->currentHook : '');

        return $helper->generateList($slides, $fields_list);
    }

    /**
     * Set default settings values for shop when install or activate the module
     * @param $id_shop
     *
     * @return int
     * @throws PrestaShopException
     */
    public function setDefaultSettings($id_shop, $id_hook = null)
    {
        $errors = array();
        if ($result = JXProductSlider::getShopSliderSettings($id_shop, $id_hook)) {
            $slider = new JXProductSlider($result['id_slider']);
        } else {
            $slider = new JXProductSlider();
        }

        $slider->id_shop = $id_shop;
        $slider->slider_type = 'standard';
        $slider->hook_name = $this->currentHook;

        $slider->standard_extended_settings = false;
        $slider->list_extended_settings = false;
        $slider->grid_extended_settings = false;
        $slider->fullwidth_extended_settings = false;

        $slider->standard_images_gallery = true;
        $slider->list_images_gallery = true;
        $slider->grid_images_gallery = true;
        $slider->fullwidth_images_gallery = true;

        $slider->standard_slider_navigation = true;
        $slider->list_slider_navigation = true;
        $slider->grid_slider_navigation = true;
        $slider->fullwidth_slider_navigation = true;

        $slider->standard_slider_thumbnails = false;
        $slider->list_slider_thumbnails = false;
        $slider->grid_slider_thumbnails = false;
        $slider->fullwidth_slider_thumbnails = false;

        $slider->standard_slider_pagination = false;
        $slider->list_slider_pagination = false;
        $slider->grid_slider_pagination = false;
        $slider->fullwidth_slider_pagination = false;

        $slider->standard_slider_autoplay = false;
        $slider->list_slider_autoplay = false;
        $slider->grid_slider_autoplay = false;
        $slider->fullwidth_slider_autoplay = false;

        $slider->standard_slider_loop = true;
        $slider->list_slider_loop = true;
        $slider->grid_slider_loop = true;
        $slider->fullwidth_slider_loop = true;

        $slider->standard_slider_interval = 5000;
        $slider->list_slider_interval = 5000;
        $slider->grid_slider_interval = 5000;
        $slider->fullwidth_slider_interval = 5000;

        $slider->standard_slider_duration = 500;
        $slider->list_slider_duration = 500;
        $slider->grid_slider_duration = 500;
        $slider->fullwidth_slider_duration = 500;

        if (!$result) {
            if (!$slider->save()) {
                $errors[] = sprintf($this->l('Can\'t save settings for shop = '), $id_shop);
            }
        } else {
            if (!$slider->update()) {
                $errors[] = sprintf($this->l('Can\'t update settings for shop = '), $id_shop);
            }
        }

        if (count($errors)) {
            return $this->displayError(implode('<br />', $errors));
        }

        return $slider->id;
    }

    /**
     * Add content for new product tab
     * @return mixed
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        $all_hooks = $this->repository->getAllModuleHooks($this->name, $this->defaultHook);
        $hooks = array();

        if ($all_hooks) {
            if (count($all_hooks) == 1) {
                    $hooks = $this->repository->getAllModuleHooks($this->name, $this->defaultHook);
            } else {
                foreach ($all_hooks as $key => $hook) {
                    $hooks[$key]['name'] = $hook['name'];
                    $hooks[$key]['is_slide'] = JXProductSlide::checkSlideExist($params['id_product'], $hook['name'], $this->context->shop->id);
                }
            }
        }

        $this->context->smarty->assign(array(
            'theme_url' => $this->context->link->getAdminLink('AdminJXProductsSlider'),
            'hooks' => $hooks,
            'test' => $this->repository->getAllModuleHooks($this->name, $this->defaultHook),
            'is_slide_one_hook' => JXProductSlide::checkSlide($params['id_product'], $this->context->shop->id)
        ));

        return $this->display(__FILE__, 'views/templates/admin/jxproductsslider_tab.tpl');
    }

    protected function changeStatus($id_slide)
    {
        $slide = new JXProductSlide($id_slide);
        if ($slide->id) {
            if ($slide->slide_status == 1) {
                $slide->slide_status = 0;
            } else {
                $slide->slide_status = 1;
            }

            if (!$slide->update()) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Display Warning if try to change settings for few stores simultaneously
     * return alert with warning multishop
     */
    private function getWarningMultishopHtml()
    {
        if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL) {
            return $this->displayWarning(
                $this->l('You cannot manage this module settings from "All Shops" or "Group Shop" context,
                 select the store you want to edit')
            );
        } else {
            return '';
        }
    }

    protected function getSlides($hookName)
    {
        $slides = array();
        $shopslides = JXProductSlide::getShopSlides($hookName, $this->context->shop->id, $this->context->language->id);


        foreach ($shopslides as $key => $slide) {
            $image = new Image();
            $product = (new ProductAssembler($this->context))->assembleProduct(array('id_product' => $slide['id_product']));
            $presenterFactory = new ProductPresenterFactory($this->context);
            $presentationSettings = $presenterFactory->getPresentationSettings();
            $presenter = new ProductListingPresenter(new ImageRetriever($this->context->link), $this->context->link, new PriceFormatter(), new ProductColorsRetriever(), $this->context->getTranslator());
            $slides[$key]['info'] = $products = new Product($slide['id_product'], true, $this->context->language->id);
            $slides[$key]['info_array'] = $presenter->present($presentationSettings, $product, $this->context->language);
            $slides[$key]['image'] = $image->getCover($slide['id_product']);
            $slides[$key]['images'] = $products->getImages($this->context->language->id);
        }

        return $slides;
    }

    /**
     * Get product price reduction type
     *
     * @param $id_product
     * @param $id_shop
     *
     * @return false|null|string
     */
    public static function getProductReductionType($id_product, $id_shop)
    {
        $reduction_type = Db::getInstance()->getValue(
            'SELECT `reduction_type`
                FROM '._DB_PREFIX_.'specific_price
                WHERE `id_product` = '.(int)$id_product.'
                AND `id_shop` = '.(int)$id_shop
        );

        return $reduction_type;
    }

    /**
     * Get product price reduction amount
     *
     * @param $id_product
     * @param $id_shop
     *
     * @return false|null|string
     */
    public static function getProductReductionAmount($id_product, $id_shop)
    {
        $reduction_amount = Db::getInstance()->getValue(
            'SELECT `reduction`
                FROM '._DB_PREFIX_.'specific_price
                WHERE `id_product` = '.(int)$id_product.'
                AND `id_shop` = '.(int)$id_shop
        );

        return $reduction_amount;
    }

    public function hookDisplayBackOfficeHeader()
    {
        Media::addJsDef(array('theme_url' => $this->context->link->getAdminLink('AdminJXProductsSlider')));
        $this->context->controller->addJquery();
        $this->context->controller->addJqueryUI('ui.sortable');
        $this->context->controller->addJs($this->_path.'views/js/jxproductsslider_admin.js');
    }

    public function hookActionAdminControllerSetMedia()
    {
        if ($this->context->controller->controller_name == 'AdminProducts') {
            $this->context->controller->addJS($this->_path.'/views/js/jxproductsslider_admin.js');
        }
    }

    public function hookHeader()
    {
        $this->context->controller->registerJavascript('swiper', 'modules/' .$this->name. '/views/js/swiper.min.js', array('media' => 'all', 'priority' => 10));
        $this->context->controller->registerStylesheet('swiper', 'modules/' .$this->name. '/views/css/swiper.min.css', array('media' => 'all', 'priority' => 10));
        $this->context->controller->registerStylesheet('module-jxproductsslider', 'modules/'.$this->name.'/views/css/jxproductsslider.css');
    }

    public function hookDisplayBeforeBodyClosingTag()
    {
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index') {
            if (JXProductSlide::getSlide($this->context->shop->id, $this->context->language->id)) {
                $slidersettings = JXProductSlider::getShopSliderSettings($this->context->shop->id);
                $this->context->smarty->assign('hook_name', $this->currentHook);
                $this->context->smarty->assign('id_lang', $this->context->language->id);
                $this->context->smarty->assign('id_shop', $this->context->shop->id);
                $this->context->smarty->assign('slides', $this->getSlides($this->currentHook));
                $this->context->smarty->assign('settings', $slidersettings);

                return $this->display($this->_path, '/views/templates/hook/jxproductsslider-script.tpl');
            }
        }
    }

    public function getWidgetVariables($hookName = null, array $configuration = [])
    {
        $slidersettings = JXProductSlider::getShopSliderSettings($this->context->shop->id);
        $this->context->smarty->assign('hook_name', $hookName);
        $this->context->smarty->assign('id_lang', $this->context->language->id);
        $this->context->smarty->assign('id_shop', $this->context->shop->id);
        $this->context->smarty->assign('slides', $this->getSlides($hookName));
        $this->context->smarty->assign('settings', $slidersettings);
    }

    public function renderWidget($hookName = null, array $configuration = [])
    {
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index') {
            $slidersettings = JXProductSlider::getShopSliderSettings($this->context->shop->id, $hookName);
            $templatePath = 'views/templates/hook/' . $this->name . '_' . $slidersettings['slider_type'] . '.tpl';
            if ($this->getTemplatePath('views/templates/hook/' . Tools::strtolower($hookName) . '/' . $this->name . '_' . $slidersettings['slider_type'] . '.tpl')) {
                $templatePath = 'views/templates/hook/' . Tools::strtolower($hookName) . '/' . $this->name . '_' . $slidersettings['slider_type'] . '.tpl';
            }
            $cacheName = $this->name . '_' . Tools::strtolower($hookName);
            if (!$this->isCached($this->name . '.tpl', $this->getCacheId($cacheName))) {
                $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
                $this->smarty->assign('hook_class', Tools::strtolower($hookName));
            }
            return $this->display(__FILE__, $templatePath, $this->getCacheId($cacheName));
        }
    }
}
