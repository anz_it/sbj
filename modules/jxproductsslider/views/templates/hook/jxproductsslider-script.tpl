{*
* 2002-2018 Zemez
*
* JX Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     Zemez
* @copyright  2002-2018 Zemez
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $settings.slider_type == standard}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          {if ($settings.standard_extended_settings && $settings.standard_slider_autoplay) || !$settings.standard_extended_settings}
            autoplay: {
              delay: {$settings.standard_slider_interval|escape:'htmlall':'UTF-8'}
            },
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_loop) || !$settings.standard_extended_settings}
            loop: true,
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_loop && $settings.standard_slider_thumbnails) || !$settings.standard_extended_settings}
            loopedSlides: 5,
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_navigation) || !$settings.standard_extended_settings}
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
          {/if}
          {if ($settings.standard_extended_settings && $settings.standard_slider_pagination) || !$settings.standard_extended_settings}
            pagination: {
              el: '.swiper-pagination',
              clickable: true
            },
          {/if}
          speed: {$settings.standard_slider_duration|escape:'htmlall':'UTF-8'}
        });

        {if ($settings.standard_extended_settings && $settings.standard_slider_autoplay) || !$settings.standard_extended_settings}
          $(this).parent().hover(function () {
            jx_products_slider.autoplay.stop();
          }, function () {
            jx_products_slider.autoplay.start();
          });
        {/if}
        {if ($settings.standard_extended_settings && $settings.standard_images_gallery) || !$settings.standard_extended_settings}
          var innerSliders = [];
          $(this).find('.jxpr-inner-slider').each(function () {
            var jx_products_slider_inner = new Swiper($(this), {
              loop: true,
              loopedSlides: 8,
              nested: true,
              on: {
                slideChangeTransitionEnd: function () {
                  for (var i = 0; i < innerSliders.length; i++) {
                    if (this.parentIndex === innerSliders[i].parentIndex && innerSliders[i] !== this) {
                      innerSliders[i].slideTo(this.activeIndex, 0, false);
                    }
                  }
                }
              }
            });
            jx_products_slider_inner.parentIndex = jx_products_slider_inner.$el.closest('.swiper-slide').attr('data-swiper-slide-index');
            innerSliders.push(jx_products_slider_inner);
            var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
              slidesPerView: 4,
              spaceBetween: 10,
              touchRatio: 0.2,
              loop: true,
              loopedSlides: 8,
              slideToClickedSlide: true,
              nested: true
            });
            jx_products_slider_inner.controller.control = galleryThumbsInner;
            galleryThumbsInner.controller.control = jx_products_slider_inner;
          });
        {/if}

        {if ($settings.standard_extended_settings && $settings.standard_slider_thumbnails) || !$settings.standard_extended_settings}
          var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails'), {
            slideToClickedSlide: true,
            grabCursor: true,
            touchRatio: 0.2,
            slidesPerView: 3,
            spaceBetween: 30,
            {if ($settings.standard_extended_settings && $settings.standard_slider_loop && $settings.standard_slider_thumbnails) || !$settings.standard_extended_settings}
              loop: true,
              loopedSlides: 5
            {elseif $settings.standard_extended_settings && !$settings.standard_slider_loop && $settings.standard_slider_thumbnails}
              centeredSlides: true
            {/if}
          });
          jx_products_slider.controller.control = galleryThumbs;
          galleryThumbs.controller.control = jx_products_slider;
        {/if}
      });
    });
  </script>
{elseif $settings.slider_type == list}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          {if ($settings.list_extended_settings && $settings.list_slider_autoplay) || !$settings.list_extended_settings}
            autoplay: {
              delay: {$settings.list_slider_interval|escape:'htmlall':'UTF-8'}
            },
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_loop) || !$settings.list_extended_settings}
            loop: true,
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_loop && $settings.list_slider_thumbnails) || !$settings.list_extended_settings}
            loopedSlides: 5,
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_navigation) || !$settings.list_extended_settings}
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
          {/if}
          {if ($settings.list_extended_settings && $settings.list_slider_pagination) || !$settings.list_extended_settings}
            pagination: {
              el: '.swiper-pagination',
              clickable: true
            },
          {/if}
          speed: {$settings.list_slider_duration|escape:'htmlall':'UTF-8'}
        });

        {if ($settings.list_extended_settings && $settings.list_slider_autoplay) || !$settings.list_extended_settings}
          $(this).parent().hover(function () {
            jx_products_slider.autoplay.stop();
          }, function () {
            jx_products_slider.autoplay.start();
          });
        {/if}

        {if $settings.list_extended_settings && $settings.list_images_gallery}
          var innerSliders = [];
          $(this).find('.jxpr-inner-slider').each(function () {
            var jx_products_slider_inner = new Swiper($(this), {
              loop: true,
              loopedSlides: 8,
              nested: true,
              on: {
                slideChangeTransitionEnd: function () {
                  for (var i = 0; i < innerSliders.length; i++) {
                    if (this.parentIndex === innerSliders[i].parentIndex && innerSliders[i] !== this) {
                      innerSliders[i].slideTo(this.activeIndex, 0, false);
                    }
                  }
                }
              }
            });
            jx_products_slider_inner.parentIndex = jx_products_slider_inner.$el.closest('.swiper-slide').attr('data-swiper-slide-index');
            innerSliders.push(jx_products_slider_inner);
            var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
              slidesPerView: 4,
              spaceBetween: 10,
              touchRatio: 0.2,
              loop: true,
              loopedSlides: 8,
              slideToClickedSlide: true,
              nested: true
            });
            jx_products_slider_inner.controller.control = galleryThumbsInner;
            galleryThumbsInner.controller.control = jx_products_slider_inner;
          });
        {/if}

        {if ($settings.list_extended_settings && $settings.list_slider_thumbnails) || !$settings.list_extended_settings}
          var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails').find('.swiper-container'), {
            slideToClickedSlide: true,
            grabCursor: true,
            navigation: {
              nextEl: '.swiper-button-thumb-next',
              prevEl: '.swiper-button-thumb-prev'
            },
            direction: 'vertical',
            spaceBetween: 10,
            touchRatio: 0.2,
            slidesPerView: 3,
            {if ($settings.list_extended_settings && $settings.list_slider_loop && $settings.list_slider_thumbnails) || !$settings.list_extended_settings}
              loop: true,
              loopedSlides: 5
            {elseif $settings.list_extended_settings && !$settings.list_slider_loop && $settings.list_slider_thumbnails}
              centeredSlides: true
            {/if}
          });
          jx_products_slider.controller.control = galleryThumbs;
          galleryThumbs.controller.control = jx_products_slider;
        {/if}
      });
    });
  </script>
{elseif $settings.slider_type == grid}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          {if ($settings.grid_extended_settings && $settings.grid_slider_autoplay) || !$settings.grid_extended_settings}
            autoplay: {
              delay: {$settings.grid_slider_interval|escape:'htmlall':'UTF-8'}
            },
          {/if}
          {if ($settings.grid_extended_settings && $settings.grid_slider_loop && !$settings.grid_slider_thumbnails)}
            loop: true,
          {/if}
          {if ($settings.grid_extended_settings && $settings.grid_slider_navigation) || !$settings.grid_extended_settings}
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
          {/if}
          {if ($settings.grid_extended_settings && $settings.grid_slider_pagination) || !$settings.grid_extended_settings}
            pagination: {
              el: '.swiper-pagination',
              clickable: true
            },
          {/if}
          speed: {$settings.grid_slider_duration|escape:'htmlall':'UTF-8'},
          {if ($settings.grid_extended_settings && $settings.grid_slider_thumbnails) || !$settings.grid_extended_settings}
            on: {
              slideChangeTransitionEnd: function () {
                if (galleryThumbs) {
                  galleryThumbs.slideTo(jx_products_slider.activeIndex);
                }
              }
            }
          {/if}
        });


        {if ($settings.grid_extended_settings && $settings.grid_slider_autoplay) || !$settings.grid_extended_settings}
          $(this).parent().hover(function () {
            jx_products_slider.autoplay.stop();
          }, function () {
            jx_products_slider.autoplay.start();
          });
        {/if}


        {if $settings.grid_extended_settings && $settings.grid_images_gallery}
          var innerSliders = [];
          $(this).find('.jxpr-inner-slider').each(function () {
            var jx_products_slider_inner = new Swiper($(this), {
              loop: true,
              loopedSlides: 8,
              nested: true,
              on: {
                slideChangeTransitionEnd: function () {
                  for (var i = 0; i < innerSliders.length; i++) {
                    if (this.parentIndex === innerSliders[i].parentIndex && innerSliders[i] !== this) {
                      innerSliders[i].slideTo(this.activeIndex, 0, false);
                    }
                  }
                }
              }
            });
            jx_products_slider_inner.parentIndex = jx_products_slider_inner.$el.closest('.swiper-slide').attr('data-swiper-slide-index');
            innerSliders.push(jx_products_slider_inner);
            var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
              slidesPerView: 4,
              spaceBetween: 10,
              touchRatio: 0.2,
              loop: true,
              loopedSlides: 8,
              slideToClickedSlide: true,
              nested: true
            });
            jx_products_slider_inner.controller.control = galleryThumbsInner;
            galleryThumbsInner.controller.control = jx_products_slider_inner;
          });
        {/if}

        {if ($settings.grid_extended_settings && $settings.grid_slider_thumbnails) || !$settings.grid_extended_settings}
          var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails'), {
            slidesPerView: 3,
            direction: 'vertical',
            spaceBetween: 0,
            slidesPerColumn: 3,
            slidesPerColumnFill: 'column',
            slidesPerGroup: 3,
            slideToClickedSlide: true,
            normalizeSlideIndex: false,
            on: {
              click: function () {
                jx_products_slider.slideTo(galleryThumbs.clickedIndex);
              }
            }
          });
        {/if}
      });
    });
  </script>
{elseif $settings.slider_type == fullwidth}
  <script type="text/javascript">
    $(document).ready(function () {
      $('.jx-products-slider').each(function () {
        var jx_products_slider = new Swiper($(this), {
          grabCursor: true,
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_autoplay) || !$settings.fullwidth_extended_settings}
            autoplay: {
              delay: {$settings.standard_slider_interval|escape:'htmlall':'UTF-8'}
            },
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop) || !$settings.fullwidth_extended_settings}
            loop: true,
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails) || !$settings.fullwidth_extended_settings}
            loopedSlides: 5,
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_navigation) || !$settings.fullwidth_extended_settings}
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev'
            },
          {/if}
          {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_pagination) || !$settings.fullwidth_extended_settings}
            pagination: {
              el: '.swiper-pagination',
              clickable: true
            },
          {/if}
          speed: {$settings.fullwidth_slider_duration|escape:'htmlall':'UTF-8'}
        });

        {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_autoplay) || !$settings.fullwidth_extended_settings}
          $(this).parent().hover(function () {
            jx_products_slider.autoplay.stop();
          }, function () {
            jx_products_slider.autoplay.start();
          });
        {/if}

        {if ($settings.fullwidth_extended_settings && $settings.fullwidth_images_gallery) || !$settings.fullwidth_extended_settings}
          var innerSliders = [];
          $(this).find('.jxpr-inner-slider').each(function () {
            var jx_products_slider_inner = new Swiper($(this), {
              loop: true,
              loopedSlides: 8,
              nested: true,
              on: {
                slideChangeTransitionEnd: function () {
                  for (var i = 0; i < innerSliders.length; i++) {
                    if (this.parentIndex === innerSliders[i].parentIndex && innerSliders[i] !== this) {
                      innerSliders[i].slideTo(this.activeIndex, 0, false);
                    }
                  }
                }
              }
            });
            jx_products_slider_inner.parentIndex = jx_products_slider_inner.$el.closest('.swiper-slide').attr('data-swiper-slide-index');
            innerSliders.push(jx_products_slider_inner);
            var galleryThumbsInner = new Swiper($(this).next('.swiper-thumbnails'), {
              slidesPerView: 4,
              spaceBetween: 10,
              touchRatio: 0.2,
              loop: true,
              loopedSlides: 8,
              slideToClickedSlide: true,
              nested: true
            });
            jx_products_slider_inner.controller.control = galleryThumbsInner;
            galleryThumbsInner.controller.control = jx_products_slider_inner;
          });
        {/if}

        {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_thumbnails) || !$settings.fullwidth_extended_settings}
          var galleryThumbs = new Swiper($(this).next('.swiper-thumbnails'), {
            slideToClickedSlide: true,
            grabCursor: true,
            touchRatio: 0.2,
            slidesPerView: 4,
            spaceBetween: 30,
            {if ($settings.fullwidth_extended_settings && $settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails) || !$settings.fullwidth_extended_settings}
              loop: true,
              loopedSlides: 5
            {elseif $settings.fullwidth_extended_settings && !$settings.fullwidth_slider_loop && $settings.fullwidth_slider_thumbnails}
              centeredSlides: true
            {/if}
          });
          jx_products_slider.controller.control = galleryThumbs;
          galleryThumbs.controller.control = jx_products_slider;
        {/if}
      });
    });
  </script>
{/if}