{*
* 2002-2018 Zemez
*
* JX Products Slider
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     Zemez
* @copyright  2002-2018 Zemez
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="product-tab">
  <p>{l s='Use this product for JX Product Slider in such hooks:' mod='jxproductsslider'}</p>
  {if count($hooks) > 1}
  {foreach from=$hooks item='hook' name='hook'}
    <div class="checkbox">
      <label for="{$hook.name|escape:'htmlall':'UTF-8'}">
        <input type="checkbox" name="is_slide" class="is_slide" id="{$hook.name|escape:'htmlall':'UTF-8'}" value="" {if $hook.is_slide}checked="checked"{/if} >
        <input id="hook_name" name="hook_name" class="form-control" value="{$hook.name|escape:'htmlall':'UTF-8'}" type="hidden">
        {$hook.name}
      </label>
    </div>
  {/foreach}
  {elseif count($hooks) == 1}
    <input type="checkbox" name="is_slide" class="is_slide" id="{$hooks|escape:'htmlall':'UTF-8'}" value="" {if $is_slide_one_hook}checked="checked"{/if} >
    <input id="hook_name" name="hook_name" class="form-control" value="{$hooks|escape:'htmlall':'UTF-8'}" type="hidden">
    {$hooks}
  {/if}
</div>

<script type="text/javascript">
  theme_url_tab = '{$theme_url|escape:"javascript":"UTF-8"}';
</script>