<script type="text/javascript">
  {foreach from=$params item=hook}
    {if $hook.display_caroucel}
      {literal}
        $(document).ready(function() {
          if ($('#jx_manufacturers_block_{/literal}{$hook.hookName}{literal}').length) {
            initJXManufacturerCarousel('#jx_manufacturers_block_{/literal}{$hook.hookName}{literal}'{/literal}, {$hook.caroucel_nb}, {$hook.slide_margin}, {$hook.caroucel_item_scroll}, {$hook.caroucel_auto}, {$hook.caroucel_speed}, {$hook.caroucel_auto_pause}, {$hook.caroucel_loop}{literal});
          }
        });
      {/literal}
    {/if}
  {/foreach}
  function initJXManufacturerCarousel(m_id_carousel, m_caroucel_nb, m_caroucel_slide_margin, m_caroucel_item_scroll, m_caroucel_auto, m_caroucel_speed, m_caroucel_auto_pause, m_caroucel_loop) {
    var m_jxslider = new Swiper(m_id_carousel,{
      {if m_caroucel_auto}
        autoplay: {
          delay: m_caroucel_auto_pause
        },
      {/if}
      slidesPerView: m_caroucel_nb,
      spaceBetween: m_caroucel_slide_margin,
      slidesPerGroup: m_caroucel_item_scroll,
      speed: m_caroucel_speed,
      loop: m_caroucel_loop,
      pagination: {
        el: m_id_carousel + '-swiper-pagination',
        clickable: true,
        type: 'bullets'
      },
      navigation: {
        nextEl: m_id_carousel + '-swiper-next',
        prevEl: m_id_carousel + '-swiper-prev'
      },
      breakpoints: {
        450: {
          slidesPerView: 2
        },
        767: {
          slidesPerView: 3
        },
        991: {
          slidesPerView: 4,
          spaceBetween: 5
        },
        1199: {
          slidesPerView: 5,
          spaceBetween: 10
        }
      }
    });
  }
</script>
