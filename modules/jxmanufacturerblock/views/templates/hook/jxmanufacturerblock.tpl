{*
* 2002-2017 Jetimpex
*
* JX Manufacturers block
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     Jetimpex (Alexander Grosul)
* @copyright  2002-2017 Jetimpex
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $manufacturers}
  <div id="jx_manufacturers_block_{$hookName}" class="{$hookName}{if $display_caroucel} swiper-container{/if} jx_manufacturers_block">
    <ul class="manufacturers_items{if !$display_caroucel} row{else} swiper-wrapper{/if}">
      {foreach from=$manufacturers item=manufacturer name=manufacturers}
        {if $smarty.foreach.manufacturers.iteration <= $nb_display}
          <li class="manufacturer_item{if !$display_caroucel} col-sm-6 col-md-3{else} swiper-slide{/if}">
            {if isset($display_image) && $display_image}
              <a href="{$link->getManufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)}" title="{l s='More about %s' sprintf=[$manufacturer.name] mod='jxmanufacturerblock'}">
                <img class="img-fluid" src="{$urls.img_manu_url}{$manufacturer.id_manufacturer}-{$image_type}.jpg" alt="{$manufacturer.image}"/>
              </a>
            {/if}
            {if isset($display_name) && $display_name}
              <h1 class="h6">
                <a href="{$link->getManufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)}" title="{l s='More about %s' sprintf=[$manufacturer.name] mod='jxmanufacturerblock'}">{$manufacturer.name}</a>
              </h1>
            {/if}
          </li>
        {/if}
      {/foreach}
    </ul>
    {if $display_caroucel}
      {if isset($caroucel_pager) && $caroucel_pager}
        <div id="jx_manufacturers_block_{$hookName}-swiper-pagination" class="swiper-pagination"></div>
      {/if}
      {if isset($caroucel_control) && $caroucel_control}
        <div id="jx_manufacturers_block_{$hookName}-swiper-next" class="swiper-button-next{if isset($carousel_hide_control) && $carousel_hide_control} hideControlOnEnd{/if}"></div>
        <div id="jx_manufacturers_block_{$hookName}-swiper-prev" class="swiper-button-prev{if isset($carousel_hide_control) && $carousel_hide_control} hideControlOnEnd{/if}"></div>
      {/if}
    {/if}
  </div>
{/if}