/*
* 2002-2017 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2017 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

$(document).ready(function(e) {
  jxmp_row_2_1_2 = '';
  jxmp_row_2_1_2 += '<div class="jxmp_popup_item">';
    jxmp_row_2_1_2 += '<div class="text-right btn-remove">';
      jxmp_row_2_1_2 += '<button type="button" class="btn btn-sm btn-danger button-remove-row">'+remove_row_btn_text+'</button>';
    jxmp_row_2_1_2 += '</div>';
    jxmp_row_2_1_2 += '<ul id="jxmp_row_2_1_2" class="clearfix jxmp_row_2_1_2 items">';
      jxmp_row_2_1_2 += '<li class="col-xs-12 col-sm-3 jxmp_row_li_3">';
        jxmp_row_2_1_2 += '<ul class="row">';
          jxmp_row_2_1_2 += '<li class="col-xs-12 item"><div class="content"><input type="hidden" name="element_num" value="1" /><input type="hidden" name="element_data" value="" /></div></li>';
          jxmp_row_2_1_2 += '<li class="col-xs-12 item"><div class="content"><input type="hidden" name="element_num" value="2" /><input type="hidden" name="element_data" value="" /></div></li>';
        jxmp_row_2_1_2 += '</ul>';
      jxmp_row_2_1_2 += '</li>';
      jxmp_row_2_1_2 += '<li class="col-xs-12 col-sm-6 item jxmp_row_li_2"><div class="content"><input type="hidden" name="element_num" value="3" /><input type="hidden" name="element_data" value="" /></div></li>';
      jxmp_row_2_1_2 += '<li class="col-xs-12 col-sm-3 jxmp_row_li_3">';
        jxmp_row_2_1_2 += '<ul class="row">';
          jxmp_row_2_1_2 += '<li class="col-xs-12 item"><div class="content"><input type="hidden" name="element_num" value="4" /><input type="hidden" name="element_data" value="" /></div></li>';
          jxmp_row_2_1_2 += '<li class="col-xs-12 item"><div class="content"><input type="hidden" name="element_num" value="5" /><input type="hidden" name="element_data" value="" /></div></li>';
        jxmp_row_2_1_2 += '</ul>';
      jxmp_row_2_1_2 += '</li>';
    jxmp_row_2_1_2 += '</ul>';
  jxmp_row_2_1_2 += '</div>';
  jxmp_layouts.push({name : 'jxmp_row_2_1_2', value : jxmp_row_2_1_2});
});