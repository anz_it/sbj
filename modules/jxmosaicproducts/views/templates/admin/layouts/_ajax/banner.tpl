{*
* 2002-2017 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2017 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div id="b-{$data.id_item|escape:'html':'UTF-8'}" class="jxmp-window-box jxmp-banner banner-{$data.id_item|escape:'html':'UTF-8'}">
  <div class="titler-row">
    <div class="jxmp-id">{$data.id_item|escape:'html':'UTF-8'}</div>
    <div class="jxmp-title">{l s='Title' mod='jxmosaicproducts'}</div>
    <div class="banner-title">{$data.title|escape:'html':'UTF-8'}</div>
  </div>
</div>
