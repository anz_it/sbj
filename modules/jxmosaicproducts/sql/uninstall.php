<?php
/**
* 2002-2017 Jetimpex
*
* JX Mosaic Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author    Jetimpex
* @copyright 2002-2017 Jetimpex
* @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

$sql = array();

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_lang`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_shop`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_banner`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_banner_lang`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_video`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_video_lang`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_html`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_html_lang`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_slider`';

$sql[] = 'DROP TABLE `'._DB_PREFIX_.'jxmosaicproducts_slide`';


foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
