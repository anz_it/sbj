<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class PaymentSbj extends PaymentModule
{
    protected $_html = '';
    protected $_postErrors = array();

    public $details;
    public $owner;
    public $address;
    public $extra_mail_vars;

    public function __construct()
    {
        $this->name = 'paymentsbj';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->author = 'PrestaShop';
        $this->controllers = array('validation');
        $this->is_eu_compatible = 1;

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Payment Sbj');
        $this->description = $this->l('Payment Sbj');

        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->l('No currency has been set for this module.');
        }
    }

    public function install()
    {
        if (!parent::install() || !$this->registerHook('paymentOptions') || !$this->registerHook('paymentReturn')) {
            return false;
        }
        return true;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        if (!$this->checkCurrency($params['cart'])) {
            return;
        }

        $payment_options = [ 
            $this->getExternalPaymentOption(), 
        ];

        return $payment_options;
    }

    public function sign ($params) {
        return $this->signData($this->buildDataToSign($params), 'e4203fbac03c4ce3b8b3488772420d009643f580068f4399907633a7213304326372ea9782fd407a913228c649cfc68216524ba6ac87480e9d343b1b6a998620dcdd6a62b85f498b8de91a71fffbc9c28b964efb08054238af778fd9b1561fbd79d9c26306904fef8fc1f7e6fc93aaad4ce5cde7ca1244f4a8141d94fb672108');
    }

    public function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    public function buildDataToSign($params) {
            $signedFieldNames = explode(",",$params["signed_field_names"]);
            foreach ($signedFieldNames as $field) {
                $dataToSign[] = $field . "=" . $params[$field];
            }
            return $this->commaSeparate($dataToSign);
    }

    public function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    }
    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }
 

    public function getExternalPaymentOption()
    {
        $totalAmount = $this->context->cart->getOrderTotal(true, Cart::BOTH);
        $mTransactionID = (int)$this->context->cart->id;
        //
        // global $cookie;
        // $currency = new CurrencyCore($cookie->id_currency);
        // $my_currency_iso_code = $currency->iso_code;
        $aData = array(
            'access_key' => '03c11ed07a9a3faaa728cb97831ac4ea', 
            'profile_id' => '1E9C4708-4EF6-48AA-9B70-DFD31E3BF745',
            'transaction_uuid' => uniqid(),
            'signed_field_names' => 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency',
            'unsigned_field_names' => '',
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'locale' => 'vi-vn',
            'transaction_type' => 'authorization',
            'reference_number' =>   $mTransactionID,
            'amount' =>  $totalAmount,
            'currency' => 'vnd'
        );
        
        $aConfig = array(
            'url' => 'https://testsecureacceptance.cybersource.com/pay',
            //'url' => '/module/paymentsbj/confirmation',
            'SECRET_KEY' => 'e4203fbac03c4ce3b8b3488772420d009643f580068f4399907633a7213304326372ea9782fd407a913228c649cfc68216524ba6ac87480e9d343b1b6a998620dcdd6a62b85f498b8de91a71fffbc9c28b964efb08054238af778fd9b1561fbd79d9c26306904fef8fc1f7e6fc93aaad4ce5cde7ca1244f4a8141d94fb672108'
        );
        $externalOption = new PaymentOption();
        $externalOption->setCallToActionText($this->l('Thanh toán trực tuyến'))
                       ->setAction( $aConfig['url'])
                       ->setInputs([
                            'access_key' => [
                                'name' =>'access_key',
                                'type' =>'hidden',
                                'value' =>'03c11ed07a9a3faaa728cb97831ac4ea',
                            ],
                            'profile_id' => [
                                'name' =>'profile_id',
                                'type' =>'hidden',
                                'value' => '1E9C4708-4EF6-48AA-9B70-DFD31E3BF745',
                            ],
                            'transaction_uuid' => [
                                'name' =>'transaction_uuid',
                                'type' =>'hidden',
                                'value' => $aData['transaction_uuid'],
                            ],
                            'signed_field_names' => [
                                'name' =>'signed_field_names',
                                'type' =>'hidden',
                                'value' => $aData['signed_field_names'],
                            ],
                            'unsigned_field_names' => [
                                'name' =>'unsigned_field_names',
                                'type' =>'hidden',
                                'value' => $aData['unsigned_field_names'],
                            ],
                            'signed_date_time' => [
                                'name' =>'signed_date_time',
                                'type' =>'hidden',
                                'value' => $aData['signed_date_time'],
                            ],
                            'locale' => [
                                'name' =>'locale',
                                'type' =>'hidden',
                                'value' => $aData['locale'],
                            ],
                            'transaction_type' => [
                                'name' =>'transaction_type',
                                'type' =>'hidden',
                                'value' => $aData['transaction_type'],
                            ],
                            'reference_number' => [
                                'name' =>'reference_number',
                                'type' =>'hidden',
                                'value' => $aData['reference_number'],
                            ],
                            'amount' => [
                                'name' =>'amount',
                                'type' =>'hidden',
                                'value' => $aData['amount'],
                            ],
                            'currency' => [
                                'name' =>'currency',
                                'type' =>'hidden',
                                'value' => $aData['currency'],
                            ],
                            'signature' => [
                                'name' =>'signature',
                                'type' =>'hidden',
                                'value' => $this->sign($aData)
                            ],
                            
                        ])
                       ->setAdditionalInformation($this->context->smarty->fetch('module:paymentsbj/views/templates/front/payment_infos.tpl'));

        return $externalOption;
    } 

    public function hookPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }

        $state = $params['order']->getCurrentState();

        if ($state) {
            $this->smarty->assign(array(
                'shop_name' => $this->context->shop->name,
                'total' => Tools::displayPrice(
                    $params['order']->getOrdersTotalPaid(),
                    new Currency($params['order']->id_currency),
                    false
                ),
                'status' => 'ok',
                'reference' => $params['order']->reference,
                'contact_url' => $this->context->link->getPageLink('contact', true)
            ));
        } else {
            $this->smarty->assign(array(
                'status' => 'failed',
                'contact_url' => $this->context->link->getPageLink('contact', true)
            ));
        }
        return $this->fetch('module:paymentsbj/views/templates/hook/payment_return.tpl');
    }
 
}
