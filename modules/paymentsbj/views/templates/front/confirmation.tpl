{extends "$layout"}

{block name="content"}
	<form id="payment_confirmation" action="https://testsecureacceptance.cybersource.com/pay" method="post"/>
	{foreach from=$allParams key=key item=value}
	  <input type="hidden" id="{$key}" name="{$key}" value="{$value}"/>
    {/foreach}
	
	<input type="submit" id="submit" value="Den trang thanh toan"/>
	</form>
	
{/block}
