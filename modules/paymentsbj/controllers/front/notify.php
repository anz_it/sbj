<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class PaymentSbjNotifyModuleFrontController extends ModuleFrontController
{
    /**
	 * @see FrontController::initContent()
	 */
    public function __construct()
    {
        parent::__construct();

    }
	public function initContent()
	{
		parent::initContent();
        $this->notify();
	}
    
    public function notify() {
        $mTransactionID = Tools::getValue('mTransactionID');
        $bankCode = Tools::getValue('bankCode');
        $transactionStatus = Tools::getValue('transactionStatus');
        $description = Tools::getValue('description');
        $ts = Tools::getValue('ts');
        $checksum = Tools::getValue('checksum');

        $sMySecretkey = 'MIKEY';//key use to hash checksum that will be provided by Sbj
        $sRawMyCheckSum = $mTransactionID . $bankCode . $transactionStatus . $ts . $sMySecretkey;
        $sMyCheckSum = sha1($sRawMyCheckSum);
        $allValues = Tools::getAllValues();

        $log = "============================\r\n";
        foreach ($allValues as $k => $value) {
            $log .= "Key: " . $k . " Value: " . $value. "\r\n";
        }
        $log .= "============================\r\n";
        $log .= "RawMyCheckSum: ". $sRawMyCheckSum . "\r\n";
        $log .= "MyCheckSum: ". $sMyCheckSum . "\r\n";
        $log .= "SandboxCheckSum: " . $checksum . "\r\n";

        file_put_contents(_PS_MODULE_DIR_. 'paymentsbj/logs/logsanboxsbj.txt', $log, FILE_APPEND);

//        if ($sMyCheckSum != $checksum) {
//            $this->response($mTransactionID, '-1', $sMySecretkey);
//        }
        $iCurrentTS = time();
        $iTotalSecond = $iCurrentTS - $ts;

        $iLimitSecond = 300; //5 min = 5*60 = 300
        //Hardcode $transactionStatus = 1
        $transactionStatus = 1;
        $processResult = $this->processResponseData($mTransactionID, $bankCode, $transactionStatus);
        //$this->response($mTransactionID, $processResult, $sMySecretkey);


        /* ===============================Function region======================================= */
    }

    public function processResponseData($mTransactionID, $bankCode, $transactionStatus) {
        try {

            // Payment was successful, so update the order's state, send order email and move to the success page
            if ($transactionStatus == 1) {
                 $cart = $this->context->cart;

                 $customer = new Customer($cart->id_customer);
                 if (!Validate::isLoadedObject($customer))
                     Tools::redirect('index.php?controller=order&step=1');

                 $currency = $this->context->currency;
                 $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
                 $orderId = $mTransactionID;
                 $order = new Order((int) ($orderId));
                 if (Validate::isLoadedObject($order)) {
                    $order->setCurrentState(Configuration::get('PS_OS_PAYMENT'));
                    $order->save();
                 }

            } else {
                $orderId = $mTransactionID;
                $order = new Order((int) ($orderId));
                if (Validate::isLoadedObject($order)) {
                    $order->setCurrentState(Configuration::get('PS_OS_ERROR'));
                    $order->save();
                }
            }


            return 1; //if process successfully
        } catch (Exception $_e) {
            return -3;
        }
    }

    public function response($mTransactionID, $returnCode, $key) {
        $ts = time();
        $sRawMyCheckSum = $mTransactionID . $returnCode . $ts . $key;
        $checksum = sha1($sRawMyCheckSum);
        $aData = array(
            'mTransactionID' => $mTransactionID,
            'returnCode' => $returnCode,
            'ts' => time(),
            'checksum' => $checksum
        );
        echo json_encode($aData);
        exit;
    }
}
