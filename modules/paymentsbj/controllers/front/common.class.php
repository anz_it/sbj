<?php
/**
 * Sbj Merchant Service
 * @package		miservice
 * @subpackage 	common.class.php
 * @copyright	Copyright (c) 2012 VNG
 * @version 	1.0
 * @author 		quannd3@vng.com.vn (live support; zingchat:kibac2001, yahoo:kibac2001, Tel:0904904402)
 * @created 	01/10/2012
 * @modified 	05/10/2012
 */
 ?>
<?php
include_once 'rest.client.class.php';

function sign ($params, $config) {
	return signData(buildDataToSign($params),  $config['SECRET_KEY']);
}

function signData($data, $secretKey) {
	return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
		$signedFieldNames = explode(",",$params["signed_field_names"]);
		foreach ($signedFieldNames as $field) {
			$dataToSign[] = $field . "=" . $params[$field];
		}
		return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
	return implode(",",$dataToSign);
}
class Common{
	public static function toString($o)
	{
		echo '<pre>';
		print_r($o);
		exit;	
	}
	public static function callRest($aConfig, $aData)
	{
		try 
		{	
			$sRawDataSign = '';
			foreach($aData as $k =>$v) {
				if($k != 'signature' && $k != 'addInfo' && $k != 'description')$sRawDataSign .= $v;
			} 
			
			$sign = sign($aData, $aConfig);
			$aData['signature'] = $sign;
			$request = new RestRequest($aConfig['url'], 'POST');
			$request->buildPostBody($aData);
			$request->execute();

			$http_code = $request->getHTTPCode();

			$return = array();

			$return['httpcode'] = $http_code;
			
			if($http_code == '200'){
				$result = json_decode($request->getResponseBody(),true);
				$return = $result;
				$return['httpcode'] = $http_code;
			}else{
				$return['message'] = $request->getResponseBody();
			}
			
			return $return;
		} catch (Exception $fault) {
			throw $fault;
		}
	}

	
}
?>