# JX Mega Menu

1.7.3
UPD: added an opportunity to add Google Map API key and fixed the issue when no map is added but the script is included anyway
1.7.4
FIX: removed redundant pSql statements in objects filling to prevent an adding of slashes during recording to a database
1.7.5
FIX: fixed some issues with non-existent variables
1.7.6
UPD: new functionality added: opportunity to use jx blog categories and posts in the menu
1.7.7
FIX: fixed an issue with custom url validation