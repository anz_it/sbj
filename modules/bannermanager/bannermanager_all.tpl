<!-- MODULE Banner Mannager {$banner_class} -->
<!-- modal ads-->
{if isset($banners) and $banners|count}
<div id="modal-backdrop" class="modal-backdrop fade"></div>
<div id="banner-ads-home" class="modal fade show modal-ads" id="adsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="padding-right: 17px; display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="ico-times-circle"></i></button>
            {foreach from=$banners item=banner} {if $banner.active}
            <div class="modal-body">
                <a href="{$banner.image_link}" target="{if $banner.open_blank==0}_self{else}_blank{/if}" title="{$banner.description}">
                    <img src="{$this_path}{$banner.image_name}" alt="{$banner.description}" />
                </a>
            </div>
            {/if} {/foreach}
        </div>
    </div>
</div>
{/if}
<!-- e: modal ads-->
<!-- /MODULE Banner Mannager {$banner_class} -->