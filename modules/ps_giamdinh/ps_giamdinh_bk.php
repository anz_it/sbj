<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Ps_Banner extends Module implements WidgetInterface
{
    private $templateFile;

	public function __construct()
	{
		$this->name = 'ps_giamdinh';
		$this->version = '2.1.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Banner', array(), 'Modules.Banner.Admin');
        $this->description = $this->trans('Displays a giamdinh on your shop.', array(), 'Modules.Banner.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:ps_giamdinh/ps_giamdinh.tpl';
    }

    public function install()
    {
        return (parent::install() &&
            $this->registerHook('displayHome') &&
            $this->registerHook('actionObjectLanguageAddAfter') &&
            $this->installFixtures() &&
            $this->disableDevice(Context::DEVICE_MOBILE));
    }

    public function hookActionObjectLanguageAddAfter($params)
    {
        return $this->installFixture((int)$params['object']->id, Configuration::get('TONG_QUAN_VE_KIM_CUONG_IMG', (int)Configuration::get('PS_LANG_DEFAULT')));
    }

    protected function installFixtures()
    {
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $this->installFixture((int)$lang['id_lang'], 'sale70.png');
        }

        return true;
    }

    protected function installFixture($id_lang, $image = null)
    {
        /*TONG_QUAN_VE_KIM_CUONG*/
        $values['TONG_QUAN_VE_KIM_CUONG_IMG'][(int)$id_lang] = $image;
        $values['TONG_QUAN_VE_KIM_CUONG_LINK'][(int)$id_lang] = $image;
        $values['TONG_QUAN_VE_KIM_CUONG_TITLE'][(int)$id_lang] = '';
        Configuration::updateValue('TONG_QUAN_VE_KIM_CUONG_IMG', $values['TONG_QUAN_VE_KIM_CUONG_IMG']);
        Configuration::updateValue('TONG_QUAN_VE_KIM_CUONG_LINK', $values['TONG_QUAN_VE_KIM_CUONG_LINK']);
        Configuration::updateValue('TONG_QUAN_VE_KIM_CUONG_TITLE', $values['TONG_QUAN_VE_KIM_CUONG_TITLE']);


        /*TIEU_CHUAN_PHAN_CAP_KIM_CUONG*/
        $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG'][(int)$id_lang] = $image;
        $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK'][(int)$id_lang] = $image;
        $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE'][(int)$id_lang] = '';
        Configuration::updateValue('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG', $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG']);
        Configuration::updateValue('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK', $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK']);
        Configuration::updateValue('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE', $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE']);


        /*VI_NIM_PHONG_GIAY_CHUNG_NHAN*/
        $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG'][(int)$id_lang] = $image;
        $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK'][(int)$id_lang] = $image;
        $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE'][(int)$id_lang] = '';
        Configuration::updateValue('VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG', $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG']);
        Configuration::updateValue('VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK', $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK']);
        Configuration::updateValue('VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE', $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE']);

    }

    public function uninstall()
    {
        /*TONG_QUAN_VE_KIM_CUONG*/
        Configuration::deleteByName('TONG_QUAN_VE_KIM_CUONG_IMG');
        Configuration::deleteByName('TONG_QUAN_VE_KIM_CUONG_LINK');
        Configuration::deleteByName('TONG_QUAN_VE_KIM_CUONG_TITLE');

        /*TIEU_CHUAN_PHAN_CAP_KIM_CUONG*/
        Configuration::deleteByName('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG');
        Configuration::deleteByName('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK');
        Configuration::deleteByName('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE');

        /*VI_NIM_PHONG_GIAY_CHUNG_NHAN*/
        Configuration::deleteByName('VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG');
        Configuration::deleteByName('VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK');
        Configuration::deleteByName('VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE');


        return parent::uninstall();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            $languages = Language::getLanguages(false);
            $values = array();
            $tongquan_vekimcuong_update_images_values = false;
            $tieu_chuan_phancap_kim_cuong_update_images_values = false;

            foreach ($languages as $lang) {
                if (isset($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']])
                    && isset($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']]['tmp_name'])
                    && !empty($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']]['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']], 4000000)) {
                        return $error;
                    } else {
                        $ext = substr($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']]['name'], strrpos($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']]['name'], '.') + 1);
                        $file_name = md5($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']]['name']).'.'.$ext;

                        if (!move_uploaded_file($_FILES['TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                            return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                        } else {
                            if (Configuration::hasContext('TONG_QUAN_VE_KIM_CUONG_IMG', $lang['id_lang'], Shop::getContext())
                                && Configuration::get('TONG_QUAN_VE_KIM_CUONG_IMG', $lang['id_lang']) != $file_name) {
                                @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('TONG_QUAN_VE_KIM_CUONG_IMG', $lang['id_lang']));
                            }

                            $values['TONG_QUAN_VE_KIM_CUONG_IMG'][$lang['id_lang']] = $file_name;
                        }
                    }

                    $tongquan_vekimcuong_update_images_values = true;
                }

                $values['TONG_QUAN_VE_KIM_CUONG_LINK'][$lang['id_lang']] = Tools::getValue('TONG_QUAN_VE_KIM_CUONG_LINK_'.$lang['id_lang']);
                $values['TONG_QUAN_VE_KIM_CUONG_TITLE'][$lang['id_lang']] = Tools::getValue('TONG_QUAN_VE_KIM_CUONG_TITLE_'.$lang['id_lang']);
            }

            if ($tongquan_vekimcuong_update_images_values) {
                Configuration::updateValue('TONG_QUAN_VE_KIM_CUONG_IMG', $values['TONG_QUAN_VE_KIM_CUONG_IMG']);
            }
            Configuration::updateValue('TONG_QUAN_VE_KIM_CUONG_LINK', $values['TONG_QUAN_VE_KIM_CUONG_LINK']);
            Configuration::updateValue('TONG_QUAN_VE_KIM_CUONG_TITLE', $values['TONG_QUAN_VE_KIM_CUONG_TITLE']);



            /*---------------------------------TIEU_CHUAN_PHAN_CAP_KIM_CUONG-------------------------------------------*/
            if (isset($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']])
                && isset($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']]['tmp_name'])
                && !empty($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']]['tmp_name'])) {
                if ($error = ImageManager::validateUpload($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']], 4000000)) {
                    return $error;
                } else {
                    $ext = substr($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']]['name'], strrpos($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']]['name'], '.') + 1);
                    $file_name = md5($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']]['name']).'.'.$ext;

                    if (!move_uploaded_file($_FILES['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                        return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                    } else {
                        if (Configuration::hasContext('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG', $lang['id_lang'], Shop::getContext())
                            && Configuration::get('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG', $lang['id_lang']) != $file_name) {
                            @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG', $lang['id_lang']));
                        }

                        $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG'][$lang['id_lang']] = $file_name;
                    }
                }

                $tieu_chuan_phancap_kim_cuong_update_images_values = true;
            }
            if ($tieu_chuan_phancap_kim_cuong_update_images_values) {
                Configuration::updateValue('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG', $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG']);
            }
            $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK'][$lang['id_lang']] = Tools::getValue('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK_'.$lang['id_lang']);
            $values['TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE'][$lang['id_lang']] = Tools::getValue('TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE_'.$lang['id_lang']);


            /*---------------------------------VI_NIM_PHONG_GIAY_CHUNG_NHAN-------------------------------------------*/
            if (isset($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']])
                && isset($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']]['tmp_name'])
                && !empty($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']]['tmp_name'])) {
                if ($error = ImageManager::validateUpload($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']], 4000000)) {
                    return $error;
                } else {
                    $ext = substr($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']]['name'], strrpos($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']]['name'], '.') + 1);
                    $file_name = md5($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']]['name']).'.'.$ext;

                    if (!move_uploaded_file($_FILES['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                        return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                    } else {
                        if (Configuration::hasContext('VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG', $lang['id_lang'], Shop::getContext())
                            && Configuration::get('VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG', $lang['id_lang']) != $file_name) {
                            @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG', $lang['id_lang']));
                        }

                        $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG'][$lang['id_lang']] = $file_name;
                    }
                }

                $tieu_chuan_phancap_kim_cuong_update_images_values = true;
            }
            if ($tieu_chuan_phancap_kim_cuong_update_images_values) {
                Configuration::updateValue('VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG', $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG']);
            }
            $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK'][$lang['id_lang']] = Tools::getValue('VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK_'.$lang['id_lang']);
            $values['VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE'][$lang['id_lang']] = Tools::getValue('VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE_'.$lang['id_lang']);



            $this->_clearCache($this->templateFile);





            return $this->displayConfirmation($this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success'));
        }

        return '';
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Banner image', array(), 'Modules.Banner.Admin'),
                        'name' => 'TONG_QUAN_VE_KIM_CUONG_IMG',
                        'desc' => $this->trans('Upload an image for your top giamdinh. The recommended dimensions are 1110 x 214px if you are using the default theme.', array(), 'Modules.Banner.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Banner Link', array(), 'Modules.Banner.Admin'),
                        'name' => 'TONG_QUAN_VE_KIM_CUONG_LINK',
                        'desc' => $this->trans('Enter the link associated to your giamdinh. When clicking on the giamdinh, the link opens in the same window. If no link is entered, it redirects to the homepage.', array(), 'Modules.Banner.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Banner description', array(), 'Modules.Banner.Admin'),
                        'name' => 'TONG_QUAN_VE_KIM_CUONG_TITLE',
                        'desc' => $this->trans('Please enter a short but meaningful description for the giamdinh.', array(), 'Modules.Banner.Admin')
                    ),
                    //TIEU_CHUAN_PHAN_CAP_KIM_CUONG
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Banner image', array(), 'Modules.Banner.Admin'),
                        'name' => 'TIEU_CHUAN_PHAN_CAP_KIM_CUONG_IMG',
                        'desc' => $this->trans('Upload an image for your top giamdinh. The recommended dimensions are 1110 x 214px if you are using the default theme.', array(), 'Modules.Banner.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Banner Link', array(), 'Modules.Banner.Admin'),
                        'name' => 'TIEU_CHUAN_PHAN_CAP_KIM_CUONG_LINK',
                        'desc' => $this->trans('Enter the link associated to your giamdinh. When clicking on the giamdinh, the link opens in the same window. If no link is entered, it redirects to the homepage.', array(), 'Modules.Banner.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Banner description', array(), 'Modules.Banner.Admin'),
                        'name' => 'TIEU_CHUAN_PHAN_CAP_KIM_CUONG_TITLE',
                        'desc' => $this->trans('Please enter a short but meaningful description for the giamdinh.', array(), 'Modules.Banner.Admin')
                    ),
                    //VI_NIM_PHONG_GIAY_CHUNG_NHAN
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Banner image', array(), 'Modules.Banner.Admin'),
                        'name' => 'VI_NIM_PHONG_GIAY_CHUNG_NHAN_IMG',
                        'desc' => $this->trans('Upload an image for your top giamdinh. The recommended dimensions are 1110 x 214px if you are using the default theme.', array(), 'Modules.Banner.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Banner Link', array(), 'Modules.Banner.Admin'),
                        'name' => 'VI_NIM_PHONG_GIAY_CHUNG_NHAN_LINK',
                        'desc' => $this->trans('Enter the link associated to your giamdinh. When clicking on the giamdinh, the link opens in the same window. If no link is entered, it redirects to the homepage.', array(), 'Modules.Banner.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Banner description', array(), 'Modules.Banner.Admin'),
                        'name' => 'VI_NIM_PHONG_GIAY_CHUNG_NHAN_TITLE',
                        'desc' => $this->trans('Please enter a short but meaningful description for the giamdinh.', array(), 'Modules.Banner.Admin')
                    )
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions')
                )
            ),
        );

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStoreConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();

        foreach ($languages as $lang) {
            $fields['TONG_QUAN_VE_KIM_CUONG_IMG'][$lang['id_lang']] = Tools::getValue('TONG_QUAN_VE_KIM_CUONG_IMG_'.$lang['id_lang'], Configuration::get('TONG_QUAN_VE_KIM_CUONG_IMG', $lang['id_lang']));
            $fields['TONG_QUAN_VE_KIM_CUONG_LINK'][$lang['id_lang']] = Tools::getValue('TONG_QUAN_VE_KIM_CUONG_LINK_'.$lang['id_lang'], Configuration::get('TONG_QUAN_VE_KIM_CUONG_LINK', $lang['id_lang']));
            $fields['TONG_QUAN_VE_KIM_CUONG_TITLE'][$lang['id_lang']] = Tools::getValue('TONG_QUAN_VE_KIM_CUONG_TITLE_'.$lang['id_lang'], Configuration::get('TONG_QUAN_VE_KIM_CUONG_TITLE', $lang['id_lang']));
        }

        return $fields;
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('ps_giamdinh'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('ps_giamdinh'));
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $imgname = Configuration::get('TONG_QUAN_VE_KIM_CUONG_IMG', $this->context->language->id);

        if ($imgname && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)) {
            $this->smarty->assign('giamdinh_img', $this->context->link->protocol_content . Tools::getMediaServer($imgname) . $this->_path . 'img/' . $imgname);
        }

        $giamdinh_link = Configuration::get('TONG_QUAN_VE_KIM_CUONG_LINK', $this->context->language->id);
        if (!$giamdinh_link) {
            $giamdinh_link = $this->context->link->getPageLink('index');
        }

        return array(
            'giamdinh_link' => $this->updateUrl($giamdinh_link),
            'giamdinh_desc' => Configuration::get('TONG_QUAN_VE_KIM_CUONG_TITLE', $this->context->language->id)
        );
    }

    private function updateUrl($link)
    {
        if (substr($link, 0, 7) !== "http://" && substr($link, 0, 8) !== "https://") {
            $link = "http://" . $link;
        }

        return $link;
    }
}
