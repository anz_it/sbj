
<!-- catagory gifts-->
<div class="catagory-gifts space-global" style="padding-top: 30px;">
  <div class="container">
    <h2 class="title text-center">{l s='GIÁM ĐỊNH PHÂN CẤP KIM CƯƠNG' d='Shop.Theme.Actions'}</h2>
    <div class="row">
        {foreach $configs as $key=>$config}
            {if ($key+1) > 6}
                {break}
            {/if}
          <div class="col-lg-4 col-sm-6">
            <div class="tile-gift-v2">
              <div class="img effect-2">
                <a href="{$config.giamdinh_link}">
                  <img src="{$config.giamdinh_img}" alt="Image">
                </a>
              </div>
              <h4 class="text-uppsercase"><a href="{$config.giamdinh_link}">{$config.giamdinh_title}</a></h4>
            </div>
          </div>
        {/foreach}
    </div>
  </div>
</div>



<!-- catagory gifts-->
<div class="catagory-gifts space-global">
  <div class="container">
    <h2 class="title text-center">{l s='GIÁM ĐỊNH PHÂN CẤP KIM CƯƠNG' d='Shop.Theme.Actions'}</h2>
    <div class="row">
        {foreach $configs as $key=>$config}
            {if ($key+1) >= 7}
              <div class="col-lg-4 col-sm-6">
                <div class="tile-gift-v2">
                  <div class="img effect-2">
                    <a href="{$config.giamdinh_link}">
                      <img src="{$config.giamdinh_img}" alt="Image">
                    </a>
                  </div>
                  <h4 class="text-uppsercase"><a href="{$config.giamdinh_link}">{$config.giamdinh_title}</a></h4>
                </div>
              </div>
            {/if}
        {/foreach}
    </div>
  </div>
</div>