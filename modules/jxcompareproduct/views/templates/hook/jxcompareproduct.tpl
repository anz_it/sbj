{**
* 2002-2017 Jetimpex
*
* JX Compare Product
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Jetimpex
*  @copyright 2002-2017 Jetimpex
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<a class="js-compare-button compare-button btn-link" href="#" data-id-product="{$id_product}" data-action="add-product" title="{l s='Add to Compare' mod='jxcompareproduct'}">
  <i class="fa fa-clone" aria-hidden="true"></i>
  <span>{l s="Compare" mod="jxcompareproduct"}</span>
</a>

