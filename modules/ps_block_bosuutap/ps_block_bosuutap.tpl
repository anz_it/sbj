{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- collections-->
<div class="collections">
  <div class="container">
    <div class="collections-inner" style="background-image:url({if isset($BANNER_BST_IMG)} {$BANNER_BST_IMG} {/if})">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="text">
            <h2 class="text-uppercase">{if isset($BANNER_BST_NAME)} {$BANNER_BST_NAME} {/if} </h2>
            <div class="desc">
                {if isset($BANNER_BST_DESC)} {$BANNER_BST_DESC} {/if}
            </div>
            <a class="btn btn-red" href="/bo-suu-tap">
                {if isset($BANNER_BST_NAME)} {$BANNER_BST_NAME} {/if}
              <i class="fa fa-angle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- e: collections-->
