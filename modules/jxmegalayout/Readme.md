# JX Mega Layout

v 1.3.3
FIX: fixed an issue with extra-content sorting

v 1.3.2
UPD: added hook name to module front blocks

v 1.3.1
FIX: small fixes with front-end columns' classes