<div id="main-content">
  <!-- banner inner-->
  <div class="banner-inner">
    <div class="item"><img src="{$PAGE_BST_BANNER}" alt=""></div>
  </div>
  <!-- e: banner inner-->
  <div class="container">
    <!-- slide collection-->
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <div class="slide-collection">
            {foreach from=$post_boSuuTap item='post'}
              <div class="item tile-collection text-center">
                <div class="row align-items-center">
                  <div class="col-md-6 mb-3 mb-md-0">
                     <a href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}">
                      <img src="{JXBlogImageManager::getImage('post_thumb', $post.id_jxblog_post, 'post_listing')}" alt="{$post.name}"></a>
                  </div>
                  <div class="col-md-6">
                    <div class="text">
                      <h3 class="title text-center">
                        <a href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}">{$post.name}</a>
                      </h3>
                      <div class="desc">
                          {$post.short_description nofilter}
                      </div><a class="btn btn-red btn-sm text-uppercase"
                               href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $post.id_jxblog_post, 'rewrite' => $post.link_rewrite]}">XEM THÊM</a>
                    </div>
                  </div>
                </div>
              </div>
            {/foreach}
        </div>
      </div>
    </div>
    <!-- e: slide collection-->
    <!-- list collection-->
    <div class="list-collection">
      <div class="row">
        <div class="col-lg-4 col-sm-6">
          <div class="list-collection-item">
            <a href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_catalogue->id_jxblog_category, 'rewrite' => $cate_catalogue->link_rewrite]}">
              <img src="{JXBlogImageManager::getImage('category_thumb', $cate_catalogue->id_jxblog_category, 'category_info')}" alit="Image">
              <div class="text">{$cate_catalogue->name}</div>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6">
          <div class="list-collection-item">
            <a href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_hinhAnh->id_jxblog_category, 'rewrite' => $cate_hinhAnh->link_rewrite]}">
              <img src="{JXBlogImageManager::getImage('category_thumb', $cate_hinhAnh->id_jxblog_category, 'category_info')}" alit="Image">
              <div class="text">{$cate_hinhAnh->name}</div></a>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6">
          <div class="list-collection-item">
            <a href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $cate_video->id_jxblog_category, 'rewrite' => $cate_video->link_rewrite]}">
              <img src="{JXBlogImageManager::getImage('category_thumb', $cate_video->id_jxblog_category, 'category_info')}" alit="Image">
              <div class="text">{$cate_video->name}</div></a>
          </div>
        </div>
      </div>
    </div>
    <!-- list collection-->
  </div>
</div>