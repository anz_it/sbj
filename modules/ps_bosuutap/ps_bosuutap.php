<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class ps_bosuutap extends Module implements WidgetInterface
{
    private $templateFile;

	public function __construct()
	{
		$this->name = 'ps_bosuutap';
		$this->version = '2.1.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Page Bộ sưu tập', array(), 'Modules.Banner.Admin');
        $this->description = $this->trans('Page Bộ sưu tập cấu hình.', array(), 'Modules.Banner.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:ps_bosuutap/ps_bosuutap.tpl';
    }

    public function install()
    {
        return (parent::install() &&
            $this->registerHook('displayHome') &&
            $this->registerHook('actionObjectLanguageAddAfter') &&
            $this->installFixtures() &&
            $this->disableDevice(Context::DEVICE_MOBILE));
    }

    public function hookActionObjectLanguageAddAfter($params)
    {
        return $this->installFixture((int)$params['object']->id, Configuration::get('PAGE_BST_BANNER', (int)Configuration::get('PS_LANG_DEFAULT')));
    }

    protected function installFixtures()
    {
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $this->installFixture((int)$lang['id_lang'], 'sale70.png');
        }

        return true;
    }

    protected function installFixture($id_lang, $image = null)
    {
        $values['PAGE_BST_BANNER'][(int)$id_lang] = $image;
        $values['PAGE_BST_BOSUUTAP'][(int)$id_lang] = '';
        $values['PAGE_BST_CATALOGUE'][(int)$id_lang] = '';
        $values['PAGE_BST_HINHANH'][(int)$id_lang] = '';
        $values['PAGE_BST_VIDEO'][(int)$id_lang] = '';

        Configuration::updateValue('PAGE_BST_BANNER', $values['PAGE_BST_BANNER']);
        Configuration::updateValue('PAGE_BST_BOSUUTAP', $values['PAGE_BST_BOSUUTAP']);
        Configuration::updateValue('PAGE_BST_CATALOGUE', $values['PAGE_BST_CATALOGUE']);
        Configuration::updateValue('PAGE_BST_HINHANH', $values['PAGE_BST_HINHANH']);
        Configuration::updateValue('PAGE_BST_VIDEO', $values['PAGE_BST_VIDEO']);
    }

    public function uninstall()
    {
        Configuration::deleteByName('PAGE_BST_BANNER');
        Configuration::deleteByName('PAGE_BST_CATALOGUE');
        Configuration::deleteByName('PAGE_BST_HINHANH');
        Configuration::deleteByName('PAGE_BST_VIDEO');

        return parent::uninstall();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            $languages = Language::getLanguages(false);
            $values = array();
            $update_images_values = false;

            foreach ($languages as $lang) {
                if (isset($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']])
                    && isset($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']]['tmp_name'])
                    && !empty($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']]['tmp_name'])) {
                    if ($error = ImageManager::validateUpload($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']], 4000000)) {
                        return $error;
                    } else {
                        $ext = substr($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']]['name'], strrpos($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']]['name'], '.') + 1);
                        $file_name = md5($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']]['name']).'.'.$ext;

                        if (!move_uploaded_file($_FILES['PAGE_BST_BANNER_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                            return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                        } else {
                            if (Configuration::hasContext('PAGE_BST_BANNER', $lang['id_lang'], Shop::getContext())
                                && Configuration::get('PAGE_BST_BANNER', $lang['id_lang']) != $file_name) {
                                @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('PAGE_BST_BANNER', $lang['id_lang']));
                            }

                            $values['PAGE_BST_BANNER'][$lang['id_lang']] = $file_name;
                        }
                    }

                    $update_images_values = true;
                }

                $values['PAGE_BST_BOSUUTAP'][$lang['id_lang']] = Tools::getValue('PAGE_BST_BOSUUTAP_'.$lang['id_lang']);
                $values['PAGE_BST_CATALOGUE'][$lang['id_lang']] = Tools::getValue('PAGE_BST_CATALOGUE_'.$lang['id_lang']);
                $values['PAGE_BST_HINHANH'][$lang['id_lang']] = Tools::getValue('PAGE_BST_HINHANH_'.$lang['id_lang']);
                $values['PAGE_BST_VIDEO'][$lang['id_lang']] = Tools::getValue('PAGE_BST_VIDEO_'.$lang['id_lang']);
            }

            if ($update_images_values) {
                Configuration::updateValue('PAGE_BST_BANNER', $values['PAGE_BST_BANNER']);
            }

            Configuration::updateValue('PAGE_BST_BOSUUTAP', $values['PAGE_BST_BOSUUTAP']);
            Configuration::updateValue('PAGE_BST_CATALOGUE', $values['PAGE_BST_CATALOGUE']);
            Configuration::updateValue('PAGE_BST_HINHANH', $values['PAGE_BST_HINHANH']);
            Configuration::updateValue('PAGE_BST_VIDEO', $values['PAGE_BST_VIDEO']);

            $this->_clearCache($this->templateFile);

            return $this->displayConfirmation($this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success'));
        }

        return '';
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'file_lang',
                        'label' => $this->trans('Cấu hình banner page Bộ sưu tập', array(), 'Modules.Banner.Admin'),
                        'name' => 'PAGE_BST_BANNER',
                        'desc' => $this->trans('Chọn hình banner cho page Bộ sưu tập', array(), 'Modules.Banner.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Category Bộ sưu tập', array(), 'Modules.Banner.Admin'),
                        'name' => 'PAGE_BST_BOSUUTAP',
                        'desc' => $this->trans('Nhập Id category bộ sưu tập tháng', array(), 'Modules.Banner.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Category E-catalogue', array(), 'Modules.Banner.Admin'),
                        'name' => 'PAGE_BST_CATALOGUE',
                        'desc' => $this->trans('Nhập Id category E-categoue', array(), 'Modules.Banner.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Category Hình ảnh', array(), 'Modules.Banner.Admin'),
                        'name' => 'PAGE_BST_HINHANH',
                        'desc' => $this->trans('Nhập Id category Hình ảnh', array(), 'Modules.Banner.Admin')
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->trans('Category Videos', array(), 'Modules.Banner.Admin'),
                        'name' => 'PAGE_BST_VIDEO',
                        'desc' => $this->trans('Nhập Id category Videos', array(), 'Modules.Banner.Admin')
                    )
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions')
                )
            ),
        );

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStoreConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();

        foreach ($languages as $lang) {
            $fields['PAGE_BST_BANNER'][$lang['id_lang']] = Tools::getValue('PAGE_BST_BANNER_'.$lang['id_lang'], Configuration::get('PAGE_BST_BANNER', $lang['id_lang']));
            $fields['PAGE_BST_BOSUUTAP'][$lang['id_lang']] = Tools::getValue('PAGE_BST_BOSUUTAP_'.$lang['id_lang'], Configuration::get('PAGE_BST_BOSUUTAP', $lang['id_lang']));
            $fields['PAGE_BST_CATALOGUE'][$lang['id_lang']] = Tools::getValue('PAGE_BST_CATALOGUE_'.$lang['id_lang'], Configuration::get('PAGE_BST_CATALOGUE', $lang['id_lang']));
            $fields['PAGE_BST_HINHANH'][$lang['id_lang']] = Tools::getValue('PAGE_BST_HINHANH_'.$lang['id_lang'], Configuration::get('PAGE_BST_HINHANH', $lang['id_lang']));
            $fields['PAGE_BST_VIDEO'][$lang['id_lang']] = Tools::getValue('PAGE_BST_VIDEO_'.$lang['id_lang'], Configuration::get('PAGE_BST_VIDEO', $lang['id_lang']));
        }

        return $fields;
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('ps_bosuutap'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('ps_bosuutap'));
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $imgname = Configuration::get('PAGE_BST_BANNER', $this->context->language->id);

        if ($imgname && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)) {
            $this->smarty->assign('PAGE_BST_BANNER', $this->context->link->protocol_content . Tools::getMediaServer($imgname) . $this->_path . 'img/' . $imgname);
        }
        $post_boSuuTap = JXBlogPost::getPostsByCategory(Configuration::get('PAGE_BST_BOSUUTAP', $this->context->language->id),$this->context->language->id,1,3);
        $cate_catalogue = new JXBlogCategory(Configuration::get('PAGE_BST_CATALOGUE', $this->context->language->id),$this->context->language->id);
        $cate_hinhAnh = new JXBlogCategory(Configuration::get('PAGE_BST_HINHANH', $this->context->language->id),$this->context->language->id);
        $cate_video = new JXBlogCategory(Configuration::get('PAGE_BST_VIDEO', $this->context->language->id),$this->context->language->id);
        return array(
            'post_boSuuTap' => $post_boSuuTap,
            'cate_catalogue' => $cate_catalogue,
            'cate_hinhAnh' => $cate_hinhAnh,
            'cate_video' => $cate_video
        );
    }

    private function updateUrl($link)
    {
        if (substr($link, 0, 7) !== "http://" && substr($link, 0, 8) !== "https://") {
            $link = "http://" . $link;
        }

        return $link;
    }
}
