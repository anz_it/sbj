<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Bv_Cybersource extends PaymentModule
{
    const CYBER_SOURCE_MODE = 'CYBER_SOURCE_SANDBOX';

    protected $_html = '';
    protected $_postErrors = array();

    public $accessKey;
    public $profileId;
    public $secretKey;
    public $extra_mail_vars;

    public function __construct()
    {
        $this->name = 'bv_cybersource';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->author = 'Brian Vo';
        $this->controllers = array('payment', 'validation');
        $this->is_eu_compatible = 1;

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $config = Configuration::getMultiple(array('CYBER_SOURCE_ACCESS_KEY', 'CYBER_SOURCE_PROFILE_ID', 'CYBER_SOURCE_SECRET_KEY', 'CYBER_SOURCE_RESERVATION_DAYS'));
        if (!empty($config['CYBER_SOURCE_PROFILE_ID'])) {
            $this->profileId = $config['CYBER_SOURCE_PROFILE_ID'];
        }
        if (!empty($config['CYBER_SOURCE_ACCESS_KEY'])) {
            $this->accessKey = $config['CYBER_SOURCE_ACCESS_KEY'];
        }
        if (!empty($config['CYBER_SOURCE_SECRET_KEY'])) {
            $this->secretKey = $config['CYBER_SOURCE_SECRET_KEY'];
        }
        if (!empty($config['CYBER_SOURCE_RESERVATION_DAYS'])) {
            $this->reservation_days = $config['CYBER_SOURCE_RESERVATION_DAYS'];
        }

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Cybersource payment', array(), 'Modules.Cybersource.Admin');
        $this->description = $this->trans('Accept payments visa master by cyber source.', array(), 'Modules.Cybersource.Admin');
        $this->confirmUninstall = $this->trans('Are you sure about removing these accessKey?', array(), 'Modules.Cybersource.Admin');
        if (!isset($this->profileId) || !isset($this->accessKey) || !isset($this->secretKey)) {
            $this->warning = $this->trans('Profile Id and account accessKey must be configured before using this module.', array(), 'Modules.Cybersource.Admin');
        }
        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->trans('No currency has been set for this module.', array(), 'Modules.Cybersource.Admin');
        }

        $this->extra_mail_vars = array(
                                        '{cybersource_profileId}' => Configuration::get('CYBER_SOURCE_PROFILE_ID'),
                                        '{cybersource_accessKey}' => nl2br(Configuration::get('CYBER_SOURCE_ACCESS_KEY')),
                                        '{cybersource_address}' => nl2br(Configuration::get('CYBER_SOURCE_SECRET_KEY')),
                                        );
    }

    public function install()
    {
        Configuration::updateValue(self::CYBER_SOURCE_MODE, true);
        if (!parent::install() || !$this->registerHook('paymentReturn') || !$this->registerHook('paymentOptions')) {
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            if (!Configuration::deleteByName('CYBER_SOURCE_CUSTOM_TEXT', $lang['id_lang'])) {
                return false;
            }
        }

        if (!Configuration::deleteByName('CYBER_SOURCE_ACCESS_KEY')
                || !Configuration::deleteByName('CYBER_SOURCE_PROFILE_ID')
                || !Configuration::deleteByName('CYBER_SOURCE_SECRET_KEY')
                || !Configuration::deleteByName('CYBER_SOURCE_RESERVATION_DAYS')
                || !Configuration::deleteByName(self::CYBER_SOURCE_MODE)
                || !parent::uninstall()) {
            return false;
        }
        return true;
    }

    protected function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue(self::CYBER_SOURCE_MODE,
                Tools::getValue(self::CYBER_SOURCE_MODE));

            if (!Tools::getValue('CYBER_SOURCE_ACCESS_KEY')) {
                $this->_postErrors[] = $this->trans('Access key are required.', array(), 'Modules.Cybersource.Admin');
            } elseif (!Tools::getValue('CYBER_SOURCE_PROFILE_ID')) {
                $this->_postErrors[] = $this->trans('Profile Id is required.', array(), "Modules.Cybersource.Admin");
            }
        }
    }

    protected function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('CYBER_SOURCE_ACCESS_KEY', Tools::getValue('CYBER_SOURCE_ACCESS_KEY'));
            Configuration::updateValue('CYBER_SOURCE_PROFILE_ID', Tools::getValue('CYBER_SOURCE_PROFILE_ID'));
            Configuration::updateValue('CYBER_SOURCE_SECRET_KEY', Tools::getValue('CYBER_SOURCE_SECRET_KEY'));

            $custom_text = array();
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                if (Tools::getIsset('CYBER_SOURCE_CUSTOM_TEXT_'.$lang['id_lang'])) {
                    $custom_text[$lang['id_lang']] = Tools::getValue('CYBER_SOURCE_CUSTOM_TEXT_'.$lang['id_lang']);
                }
            }
            Configuration::updateValue('CYBER_SOURCE_RESERVATION_DAYS', Tools::getValue('CYBER_SOURCE_RESERVATION_DAYS'));
            Configuration::updateValue('CYBER_SOURCE_CUSTOM_TEXT', $custom_text);
        }
        $this->_html .= $this->displayConfirmation($this->trans('Settings updated', array(), 'Admin.Global'));
    }

    protected function _displayBankWire()
    {
        return $this->display(__FILE__, 'infos.tpl');
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors)) {
                $this->_postProcess();
            } else {
                foreach ($this->_postErrors as $err) {
                    $this->_html .= $this->displayError($err);
                }
            }
        } else {
            $this->_html .= '<br />';
        }

        $this->_html .= $this->_displayBankWire();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        if (!$this->checkCurrency($params['cart'])) {
            return;
        }

        $payment_options = [ 
            $this->getExternalPaymentOption(), 
        ];

        return $payment_options;
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active || !Configuration::get(self::CYBER_SOURCE_MODE)) {
            return;
        }

        $state = $params['order']->getCurrentState();
        if (
            in_array(
                $state,
                array(
                    Configuration::get('PS_OS_WS_PAYMENT'),
                    Configuration::get('PS_OS_OUTOFSTOCK'),
                    Configuration::get('PS_OS_OUTOFSTOCK_UNPAID'),
                )
        )) {
            $cybersourceOwner = $this->profileId;
            if (!$cybersourceOwner) {
                $cybersourceOwner = '___________';
            }

            $cybersourceDetails = Tools::nl2br($this->accessKey);
            if (!$cybersourceDetails) {
                $cybersourceDetails = '___________';
            }

            $cybersourceAddress = Tools::nl2br($this->secretKey);
            if (!$cybersourceAddress) {
                $cybersourceAddress = '___________';
            }

            $this->smarty->assign(array(
                'shop_name' => $this->context->shop->name,
                'total' => Tools::displayPrice(
                    $params['order']->getOrdersTotalPaid(),
                    new Currency($params['order']->id_currency),
                    false
                ),
                'cybersourceDetails' => $cybersourceDetails,
                'cybersourceAddress' => $cybersourceAddress,
                'cybersourceOwner' => $cybersourceOwner,
                'status' => 'ok',
                'reference' => $params['order']->reference,
                'contact_url' => $this->context->link->getPageLink('contact', true)
            ));
        } else {
            $this->smarty->assign(
                array(
                    'status' => 'failed',
                    'contact_url' => $this->context->link->getPageLink('contact', true),
                )
            );
        }

        return $this->fetch('module:bv_cybersource/views/templates/hook/payment_return.tpl');
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Access key', array(), 'Modules.Cybersource.Admin'),
                    'icon' => 'icon-envelope'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Profile Id', array(), 'Modules.Cybersource.Admin'),
                        'placeholder' => 'Ex: 1E9C4708-4EF6-48AA-9B70-DFD31E3BF745',
                        'name' => 'CYBER_SOURCE_PROFILE_ID',
                        'desc' => $this->trans('String, provided by CyberSource', array(), 'Modules.Cybersource.Admin'),
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Access key', array(), 'Modules.Cybersource.Admin'),
                        'placeholder' => 'Ex: 03c11ed07a9a3faaa728cb97831ac4ea',
                        'name' => 'CYBER_SOURCE_ACCESS_KEY',
                        'desc' => $this->trans('String, provided by CyberSource.', array(), 'Modules.Cybersource.Admin'),
                        'required' => true
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->trans('Secret key', array(), 'Modules.Cybersource.Admin'),
                        'placeholder' => 'Ex: 03c11ed07a9a3faaa728cb97831ac4ea',
                        'name' => 'CYBER_SOURCE_SECRET_KEY',
                        'desc' => $this->trans('String, provided by CyberSource. Ex: e4203fbac03c4ce3b8b3488772420d009643f580068f4399907633a7213304326372ea9782fd407a913228c649c fc68216524ba6ac87480e9d343b1b6a998620dcdd6a62b85f498b8de91a71fffbc9c28b964efb08054238af778fd9b1561fbd79d9c26306904fef8fc1f7 e6fc93aaad4ce5cde7ca1244f4a8141d94fb672108', array(), 'Modules.Cybersource.Admin'),
                        'required' => true
                    ),
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                )
            ),
        );
        $fields_form_customization = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Customization', array(), 'Modules.Cybersource.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Reservation period', array(), 'Modules.Cybersource.Admin'),
                        'desc' => $this->trans('Number of days the items remain reserved', array(), 'Modules.Cybersource.Admin'),
                        'name' => 'CYBER_SOURCE_RESERVATION_DAYS',
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->trans('Information to the customer', array(), 'Modules.Cybersource.Admin'),
                        'name' => 'CYBER_SOURCE_CUSTOM_TEXT',
                        'desc' => $this->trans('Information on the bank transfer (processing time, starting of the shipping...)', array(), 'Modules.Cybersource.Admin'),
                        'lang' => true
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->trans('Mode', array(), 'Modules.Cybersource.Admin'),
                        'name' => self::CYBER_SOURCE_MODE,
                        'is_bool' => false,
                        'hint' => $this->trans('Test: https://ebctest.cybersource.com OR production: https://ebc.cybersource.com.', array(), 'Modules.Cybersource.Admin'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->trans('Sandbox', array(), 'Admin.Global'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->trans('Live', array(), 'Admin.Global'),
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='
            .$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form, $fields_form_customization));
    }

    public function getConfigFieldsValues()
    {
        $custom_text = array();
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $custom_text[$lang['id_lang']] = Tools::getValue(
                'CYBER_SOURCE_CUSTOM_TEXT_'.$lang['id_lang'],
                Configuration::get('CYBER_SOURCE_CUSTOM_TEXT', $lang['id_lang'])
            );
        }

        return array(
            'CYBER_SOURCE_ACCESS_KEY' => Tools::getValue('CYBER_SOURCE_ACCESS_KEY', Configuration::get('CYBER_SOURCE_ACCESS_KEY')),
            'CYBER_SOURCE_PROFILE_ID' => Tools::getValue('CYBER_SOURCE_PROFILE_ID', Configuration::get('CYBER_SOURCE_PROFILE_ID')),
            'CYBER_SOURCE_SECRET_KEY' => Tools::getValue('CYBER_SOURCE_SECRET_KEY', Configuration::get('CYBER_SOURCE_SECRET_KEY')),
            'CYBER_SOURCE_RESERVATION_DAYS' => Tools::getValue('CYBER_SOURCE_RESERVATION_DAYS', Configuration::get('CYBER_SOURCE_RESERVATION_DAYS')),
            'CYBER_SOURCE_CUSTOM_TEXT' => $custom_text,
            self::CYBER_SOURCE_MODE => Tools::getValue(self::CYBER_SOURCE_MODE,
                Configuration::get(self::CYBER_SOURCE_MODE))
        );
    }

    public function sign ($params) {
        return $this->signData($this->buildDataToSign($params), Configuration::get('CYBER_SOURCE_SECRET_KEY'));
    }

    public function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    public function buildDataToSign($params) {
            $signedFieldNames = explode(",",$params["signed_field_names"]);
            foreach ($signedFieldNames as $field) {
                $dataToSign[] = $field . "=" . $params[$field];
            }
            return $this->commaSeparate($dataToSign);
    }

    public function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    } 

    public function getExternalPaymentOption()
    {
        $totalAmount = $this->context->cart->getOrderTotal(true, Cart::BOTH);
        $mTransactionID = (int)$this->context->cart->id;
        //
        // global $cookie;
        // $currency = new CurrencyCore($cookie->id_currency);
        // $my_currency_iso_code = $currency->iso_code;
        $aData = array(
            'access_key' => Configuration::get('CYBER_SOURCE_ACCESS_KEY'), 
            'profile_id' => Configuration::get('CYBER_SOURCE_PROFILE_ID'),
            'transaction_uuid' => uniqid(),
            'signed_field_names' => 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency',
            'unsigned_field_names' => '',
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'locale' => 'vi-vn',
            'transaction_type' => 'authorization',
            'reference_number' =>   $mTransactionID,
            'amount' =>  $totalAmount,
            'currency' => 'vnd'
        );
        
         
        if(Configuration::get(self::CYBER_SOURCE_MODE) == true ) {
            $actionUrl = "https://testsecureacceptance.cybersource.com/pay";
        } else {
            $actionUrl = "https://secureacceptance.cybersource.com/pay";
        }
        $externalOption = new PaymentOption();
        $externalOption->setCallToActionText($this->l('Thanh toán trực tuyến (Visa, Master)'))
                       ->setAction( $actionUrl )
                       ->setInputs([
                            'access_key' => [
                                'name' =>'access_key',
                                'type' =>'hidden',
                                'value' => Configuration::get('CYBER_SOURCE_ACCESS_KEY'),
                            ],
                            'profile_id' => [
                                'name' =>'profile_id',
                                'type' =>'hidden',
                                'value' => Configuration::get('CYBER_SOURCE_PROFILE_ID'),
                            ],
                            'transaction_uuid' => [
                                'name' =>'transaction_uuid',
                                'type' =>'hidden',
                                'value' => $aData['transaction_uuid'],
                            ],
                            'signed_field_names' => [
                                'name' =>'signed_field_names',
                                'type' =>'hidden',
                                'value' => $aData['signed_field_names'],
                            ],
                            'unsigned_field_names' => [
                                'name' =>'unsigned_field_names',
                                'type' =>'hidden',
                                'value' => $aData['unsigned_field_names'],
                            ],
                            'signed_date_time' => [
                                'name' =>'signed_date_time',
                                'type' =>'hidden',
                                'value' => $aData['signed_date_time'],
                            ],
                            'locale' => [
                                'name' =>'locale',
                                'type' =>'hidden',
                                'value' => $aData['locale'],
                            ],
                            'transaction_type' => [
                                'name' =>'transaction_type',
                                'type' =>'hidden',
                                'value' => $aData['transaction_type'],
                            ],
                            'reference_number' => [
                                'name' =>'reference_number',
                                'type' =>'hidden',
                                'value' => $aData['reference_number'],
                            ],
                            'amount' => [
                                'name' =>'amount',
                                'type' =>'hidden',
                                'value' => $aData['amount'],
                            ],
                            'currency' => [
                                'name' =>'currency',
                                'type' =>'hidden',
                                'value' => $aData['currency'],
                            ],
                            'signature' => [
                                'name' =>'signature',
                                'type' =>'hidden',
                                'value' => $this->sign($aData)
                            ],
                            
                        ])
                       ->setAdditionalInformation($this->context->smarty->fetch('module:paymentsbj/views/templates/front/payment_infos.tpl'));

        return $externalOption;
    } 
}
