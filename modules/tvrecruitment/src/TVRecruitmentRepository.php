<?php
/**
 * 2017 Zemz
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    BrianVo (Brian Vo)
 *  @copyright 2017 BrianVo
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

use \PrestaShopBundle\Translation\TranslatorComponent as Translator;

class TVRecruitmentRepository
{
    private $db;
    private $shop;
    private $db_prefix;
    private $translator;

    public function __construct(Db $db, Shop $shop, Translator $translator)
    {
        $this->db = $db;
        $this->shop = $shop;
        $this->db_prefix = $db->getPrefix();
        $this->translator = $translator;
    }

    public function createTables()
    {
        $engine = _MYSQL_ENGINE_;
        $success = true;
        $this->dropTables();

        $queries = array(
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_category`(
    			`id_tvrecruitment_category` int(10) NOT NULL auto_increment,
    			`active` int(1) NOT NULL,
    			`position` int(10) NOT NULL,
    			`date_add` datetime NOT NULL,
    			`date_upd` datetime NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_category`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_category_lang` (
    			`id_tvrecruitment_category` int(10) NOT NULL,
    			`id_lang` int(10) NOT NULL,
    			`name` varchar(40) NOT NULL default '',
    			`description` text NOT NULL,
    			`short_description` text NOT NULL,
    			`link_rewrite` varchar(40) NOT NULL default '',
    			`meta_keyword` text NOT NULL,
    			`meta_description` text NOT NULL,
    			`badge` varchar(100) NOT NULL default '',
    			PRIMARY KEY (`id_tvrecruitment_category`, `id_lang`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_category_group` (
    			`id_tvrecruitment_category` int(10) NOT NULL,
    			`id_group` int(10) NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_category`, `id_group`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_category_shop` (
    			`id_tvrecruitment_category` int(11) NOT NULL,
    			`id_shop` int(11) NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_category`, `id_shop`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_recruitment`(
    			`id_tvrecruitment_recruitment` int(10) NOT NULL auto_increment,
    			`id_tvrecruitment_category_default` int(1) NOT NULL,
    			`author` int(1) NOT NULL,
    			`views` int(1) NOT NULL DEFAULT '0',
    			`active` int(1) NOT NULL,
    			`date_add` datetime NOT NULL,
    			`date_upd` datetime NOT NULL,
    			`date_start` datetime NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_recruitment`, `id_tvrecruitment_category_default`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_recruitment_lang`(
    			`id_tvrecruitment_recruitment` int(10) NOT NULL,
    			`id_lang` int(1) NOT NULL,
    			`name` varchar(100) NOT NULL default '',
    			`description` text NOT NULL,
    			`short_description` text NOT NULL,
    			`meta_keyword` text NOT NULL,
    			`meta_description` text NOT NULL,
    			`link_rewrite` varchar(100) NOT NULL default '',
    			PRIMARY KEY (`id_tvrecruitment_recruitment`, `id_lang`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_recruitment_category`(
    			`id_tvrecruitment_recruitment` int(10) NOT NULL,
    			`id_tvrecruitment_category` int(1) NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_recruitment`, `id_tvrecruitment_category`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_tag` (
    			`id_tvrecruitment_tag` int(10) NOT NULL auto_increment,
    			`id_lang` int(10) NOT NULL,
    			`tag` varchar(100) NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_tag`, `id_lang`, `tag`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_tag_recruitment` (
    			`id_tvrecruitment_tag` int(10) NOT NULL,
    			`id_tvrecruitment_recruitment` int(10) NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_tag`, `id_tvrecruitment_recruitment`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8",
            "CREATE TABLE IF NOT EXISTS `{$this->db_prefix}tvrecruitment_image` (
    			`id_tvrecruitment_image` int(10) NOT NULL auto_increment,
    			`name` varchar(40) NOT NULL default '',
    			`width` int(10) NOT NULL,
    			`height` int(10) NOT NULL,
    			`category` int(10) NOT NULL,
    			`category_thumb` int(10) NOT NULL,
    			`recruitment` int(10) NOT NULL,
    			`recruitment_thumb` int(10) NOT NULL,
    			`user` int(10) NOT NULL,
    			PRIMARY KEY (`id_tvrecruitment_image`, `name`)
            ) ENGINE=$engine DEFAULT CHARSET=utf8"
        );

        foreach ($queries as $query) {
            $success &= $this->db->execute($query);
        }

        return $success;
    }

    public function dropTables()
    {
        $sql = "DROP TABLE IF EXISTS
			`{$this->db_prefix}tvrecruitment_category`,
			`{$this->db_prefix}tvrecruitment_category_lang`,
			`{$this->db_prefix}tvrecruitment_category_group`,
			`{$this->db_prefix}tvrecruitment_category_shop`,
			`{$this->db_prefix}tvrecruitment_recruitment`,
			`{$this->db_prefix}tvrecruitment_recruitment_lang`,
			`{$this->db_prefix}tvrecruitment_recruitment_category`,
			`{$this->db_prefix}tvrecruitment_tag`,
			`{$this->db_prefix}tvrecruitment_tag_recruitment`,
			`{$this->db_prefix}tvrecruitment_image`";

        return $this->db->execute($sql);
    }
}
