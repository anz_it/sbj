<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    BrianVo (Brian Vo)
 * @copyright 2017 BrianVo
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class AdminTVRecruitmentPostsController extends ModuleAdminController
{
    public $translator;
    public $categories = false;

    public function __construct()
    {
        $this->table = 'tvrecruitment_recruitment';
        $this->list_id = $this->table;
        $this->identifier = 'id_tvrecruitment_recruitment';
        $this->className = 'TVRecruitmentPost';
        $this->module = $this;
        $this->lang = true;
        $this->bootstrap = true;
        $this->languages = Language::getLanguages(false);
        $this->default_language = Configuration::get('PS_LANG_DEFAULT');
        $this->context = Context::getContext();
        $this->translator = $this->context->getTranslator();
        $this->_defaultOrderBy = 'a.id_tvrecruitment_recruitment';
        $this->_defaultOrderWay = 'ASC';
        $this->_default_pagination = 10;
        $this->_pagination = array(10, 20, 50, 100);
        $this->_orderBy = Tools::getValue($this->table.'Orderby');
        $this->_orderWay = Tools::getValue($this->table.'Orderway');
        $this->imageDir = '../modules/tvrecruitment/img/p/';
        $this->bulk_actions = array(
            'delete' => array(
                'text'    => $this->trans('Delete selected', array(), 'Modules.TVRecruitment.Admin'),
                'icon'    => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Modules.TVRecruitment.Admin')
            )
        );
        $this->fields_list = array(
            'id_tvrecruitment_recruitment' => array(
                'title'   => $this->trans('ID Post', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 50,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true
            ),
            // 'image' => array(
            //     'title' => $this->trans('Image', array(), 'Modules.TVRecruitment.Admin'),
            //     'image' => $this->imageDir,
            //     'width' => 150,
            //     'align' => 'center',
            //     'orderby' => false,
            //     'filter' => false,
            //     'search' => false
            // ),
            'name'           => array(
                'title'   => $this->trans('Name', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 300,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true,
                'filter_key' => 'b!name'
            ),
            'category_name' => array(
                'title'   => $this->trans('Location', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 300,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true,
                'filter_key' => 'cl!name'
            ),
            'employee_last_name' => array(
                'title'   => $this->trans('Author', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 300,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true,
                'filter_key' => 'e!firstname'
            ),
            'views'           => array(
                'title'   => $this->trans('Views', array(), 'Modules.TVRecruitment.Admin'),
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true
            ),
            'date_add' => array(
                'title'   => $this->trans('Date added', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 100,
                'type'    => 'datetime',
                'search'  => true,
                'orderby' => true
            ),
            'date_start' => array(
                'title'   => $this->trans('Posted date', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 100,
                'type'    => 'datetime',
                'search'  => true,
                'orderby' => true
            ),
            'active'         => array(
                'title'   => $this->trans('Active', array(), 'Modules.TVRecruitment.Admin'),
                'active'  => 'status',
                'type'    => 'bool',
                'class'   => 'fixed-width-xs',
                'align'   => 'center',
                'ajax'    => true,
                'orderby' => false
            )
        );
        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'tvrecruitment_category_lang` cl ON(a.`id_tvrecruitment_category_default` = cl.`id_tvrecruitment_category` AND cl.`id_lang` = '.$this->context->language->id.')';
        $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'employee` e ON(a.`author` = e.`id_employee`)';
        $this->_select = 'cl.`name` as `category_name`, CONCAT(e.`lastname`," ",e.`firstname`)  as `employee_last_name`';
        parent::__construct();
        if (!$this->module->active) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
        }
    }

    public function setMedia()
    {
        $this->context->controller->addJquery();
        $this->context->controller->addJqueryUI('ui.widget');
        $this->context->controller->addJqueryPlugin(array('tagify'));

        parent::setMedia();
    }

    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function initContent()
    {
        if (!$this->categories = TVRecruitmentCategory::getAllCategoriesWithInfo()) {
            return $this->errors[] = $this->trans(
                'There are no categories in the recruitment. To create a recruitment you need to create at least one category before.',
                array(),
                'Modules.TVRecruitment.Admin'
            );
        }
		return parent::initContent();
    }

    public function renderForm()
    {
        $image = false;
        $thumb = false;
        if (Tools::getIsset('id_tvrecruitment_recruitment') && $id_tvrecruitment_recruitment = Tools::getValue('id_tvrecruitment_recruitment')) {
            if (file_exists($this->module->modulePath.'img/p/'.$id_tvrecruitment_recruitment.'.jpg')) {
                $image = '<img class="imgm img-thumbnail" src="'.$this->module->_link.'img/p/'.$id_tvrecruitment_recruitment.'.jpg" width="300" />';
            }elseif (file_exists($this->module->modulePath.'img/p/'.$id_tvrecruitment_recruitment.'.png')) {
                $image = '<img class="imgm img-thumbnail" src="'.$this->module->_link.'img/p/'.$id_tvrecruitment_recruitment.'.png" width="300" />';
            }
        }
        if (isset($id_tvrecruitment_recruitment)) {
            if (file_exists($this->module->modulePath.'img/pt/'.$id_tvrecruitment_recruitment.'.jpg')) {
                $thumb = '<img class="imgm img-thumbnail" src="'.$this->module->_link.'img/pt/'.$id_tvrecruitment_recruitment.'.jpg" width="300" />';
            }elseif (file_exists($this->module->modulePath.'img/pt/'.$id_tvrecruitment_recruitment.'.png')) {
                $thumb = '<img class="imgm img-thumbnail" src="'.$this->module->_link.'img/pt/'.$id_tvrecruitment_recruitment.'.png" width="300" />';
            }
        }
        if (Tools::getIsset('id_tvrecruitment_recruitment') && $id_tvrecruitment_recruitment = Tools::getValue('id_tvrecruitment_recruitment')) {
            $recruitment = new TVRecruitmentPost($id_tvrecruitment_recruitment);
        }

        $this->fields_form = array(
            'input'  => array(
                array(
                    'type'     => 'text',
                    'required' => true,
                    'class'    => 'copy2friendlyUrl',
                    'label'    => $this->trans('Post name', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'     => $this->trans('Invalid characters: &lt;&gt;;=#{}', array(), 'Modules.TVRecruitment.Admin'),
                    'desc'     => $this->trans('Enter the recruitment recruitment name', array(), 'Modules.TVRecruitment.Admin'),
                    'name'     => 'name',
                    'lang'     => true,
                    'col'      => 4
                ),
                array(
                    'type'     => 'text',
                    'hint'     => $this->trans(
                        'Only letters, numbers, underscore (_) and the minus (-) character are allowed.',
                        array(),
                        'Modules.TVRecruitment.Admin'
                    ),
                    'label'    => $this->trans('Friendly URL', array(), 'Modules.TVRecruitment.Admin'),
                    'name'     => 'link_rewrite',
                    'required' => true,
                    'desc'     => $this->trans(
                        'Enter the recruitment recruitment friendly URL. Will be used as a link to the recruitment in the "Friendly URL" mode',
                        array(),
                        'Modules.TVRecruitment.Admin'
                    ),
                    'lang'     => true,
                    'col'      => 3
                ),
                array(
                    'type'         => 'textarea',
                    'label'        => $this->trans('Short description', array(), 'Modules.TVRecruitment.Admin'),
                    'name'         => 'short_description',
                    'desc'         => $this->trans('Enter the recruitment short description', array(), 'Modules.TVRecruitment.Admin'),
                    'lang'         => true,
                    'col'          => 6,
                    'autoload_rte' => true
                ),
                array(
                    'type'         => 'textarea',
                    'label'        => $this->trans('Full description', array(), 'Modules.TVRecruitment.Admin'),
                    'name'         => 'description',
                    'desc'         => $this->trans('Enter the recruitment full description', array(), 'Modules.TVRecruitment.Admin'),
                    'lang'         => true,
                    'col'          => 6,
                    'autoload_rte' => true
                ),
                array(
                    'type'  => 'text',
                    'hint'  => $this->trans(
                        'Only letters, numbers, underscore (_) and the minus (-) character are allowed.',
                        array(),
                        'Modules.TVRecruitment.Admin'
                    ),
                    'label' => $this->trans('Meta Keywords', array(), 'Modules.TVRecruitment.Admin'),
                    'name'  => 'meta_keyword',
                    'desc'  => $this->trans(
                        'Enter Your Post Meta Keywords. Separated by comma(,) ',
                        array(),
                        'Modules.TVRecruitment.Admin'
                    ),
                    'lang'  => true,
                    'col'   => 6
                ),
                array(
                    'type'         => 'textarea',
                    'label'        => $this->trans('Meta Description', array(), 'Modules.TVRecruitment.Admin'),
                    'name'         => 'meta_description',
                    'desc'         => $this->trans('Enter the recruitment meta description', array(), 'Modules.TVRecruitment.Admin'),
                    'lang'         => true,
                    'col'          => 6,
                    'autoload_rte' => false
                ),
                // array(
                //     'type'          => 'file',
                //     'label'         => $this->trans('Post thumb', array(), 'Modules.TVRecruitment.Admin'),
                //     'name'          => 'thumbnail',
                //     'display_image' => true,
                //     'image'         => $thumb ? $thumb : false,
                //     'desc'          => $this->trans('Only .jpg, .png images are allowed', array(), 'Modules.TVRecruitment.Admin')
                // ),
                array(
                    'type'   => 'switch',
                    'label'  => $this->trans('Status', array(), 'Modules.TVRecruitment.Admin'),
                    'name'   => 'active',
                    'values' => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Modules.TVRecruitment.Admin')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Modules.TVRecruitment.Admin')
                        )
                    )
                ),
                array(
                    'type' => 'cat_list',
                    'label' => $this->trans('Associated categories', array(), 'Modules.TVRecruitment.Admin'),
                    'categories' => $this->categories,
                    'name' => 'cat_list',
                    'required' => false,
                    'hint' => $this->trans('Select default category(radio)', array(), 'Modules.TVRecruitment.Admin'),
                    'id_tvrecruitment_category_default' => isset($recruitment) ? $recruitment->id_tvrecruitment_category_default : false
                ),
                array(
                    'type' => 'tags',
                    'name' => 'tags',
                    'label' => $this->trans('Post tags', array(), 'Modules.TVRecruitment.Admin'),
                    'lang' => true,
                    'hint' => $this->trans('To add "tags," click in the field, write something, and then press "Enter."', array(), 'Modules.TVRecruitment.Admin').'&nbsp;'.$this->trans('Forbidden characters:', array(), 'Modules.TVRecruitment.Admin').' <>;=#{}'
                ),
                array(
                    'type' => 'datetime',
                    'name' => 'date_start',
                    'label' => $this->trans('Publishing date', array(), 'Modules.TVRecruitment.Admin'),
                    'desc' => $this->trans('Set the date if you want to delay the article publishing.', array(), 'Modules.TVRecruitment.Admin')
                )
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Modules.TVRecruitment.Admin'),
                'class' => 'button pull-right btn btn-default'
            )
        );

        $extraData = array_values(Hook::exec('displayTvrecruitmentPostExtra', array('recruitment' => isset($recruitment) ? $recruitment : false), null, true));
        // add all necessary data from related modules
        if ($extraData) {
            foreach ($extraData as $extra) {
                $extraFields = $extra['fields'];
                $extraValues = $extra['values'];
                foreach ($extraFields as $filed) {
                    $this->fields_form['input'][] = $filed;
                }
                foreach ($extraValues as $key => $filed) {
                    $this->fields_value[$key] = $filed;
                }
            }
        }

        $this->fields_value['tags'] = isset($recruitment) ? $recruitment->getAdminPostTags() : false;

        if (!($TVRecruitmentPost = $this->loadObject(true))) {
            return;
        }
        return parent::renderForm();
    }

    public function ajaxProcessSearchRecruitments()
    {
        $excludeIds = array();
        $exclude = explode(',', Tools::getValue('excludeIds'));
        foreach ($exclude as $item) {
            if ($item) {
                $excludeIds[] = $item;
            }
        }

        $recruitments = TVRecruitmentPost::searchRecruitmentsLive(Tools::getValue('q'), $this->context->language->id, Tools::getValue('limit'), $excludeIds);
        if ($recruitments) {
            die(implode("\n", $recruitments));
        }
    }

    public function ajaxProcessStatustvrecruitmentPost()
    {
        if (!$id_recruitment = (int)Tools::getValue('id_tvrecruitment_recruitment')) {
            die(json_encode(array('success' => false, 'error' => true, 'text' => $this->trans('Failed to update the status', array(), 'Modules.TVRecruitment.Admin'))));
        } else {
            $recruitment = new TVRecruitmentPost((int)$id_recruitment);
            if (Validate::isLoadedObject($recruitment)) {
                $recruitment->active = $recruitment->active == 1 ? 0 : 1;
                $recruitment->save() ?
                    die(json_encode(array('success' => true, 'text' => $this->trans('The status has been updated successfully', array(), 'Modules.TVRecruitment.Admin')))) :
                    die(json_encode(array('success' => false, 'error' => true, 'text' => $this->trans('Failed to update the status', array(), 'Modules.TVRecruitment.Admin'))));
            }
        }
    }
}
