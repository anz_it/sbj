<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    BrianVo (Brian Vo)
 * @copyright 2017 BrianVo
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class AdminTVRecruitmentImagesController extends ModuleAdminController
{
    public $translator;
    public $imageTypes;
    public $imageManager;

    public function __construct()
    {
        $this->translator = Context::getContext()->getTranslator();
        $this->table = 'tvrecruitment_image';
        $this->list_id = $this->table;
        $this->identifier = 'id_tvrecruitment_image';
        $this->className = 'TVRecruitmentImage';
        $this->module = $this;
        $this->lang = false;
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->_defaultOrderBy = 'id_tvrecruitment_image';
        $this->_defaultOrderWay = 'DESC';
        $this->_default_pagination = 10;
        $this->_pagination = array(10, 20, 50, 100);
        $this->_orderBy = Tools::getValue($this->table.'Orderby');
        $this->_orderWay = Tools::getValue($this->table.'Orderway');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning')
            )
        );
        $this->fields_list = array(
            'id_tvrecruitment_image' => array(
                'title'   => $this->trans('ID Image', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 100,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true
            ),
            'name'        => array(
                'title'   => $this->trans('Name', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 440,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true
            ),
            'width'        => array(
                'title'   => $this->trans('Width', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 150,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true
            ),
            'height'        => array(
                'title'   => $this->trans('Height', array(), 'Modules.TVRecruitment.Admin'),
                'width'   => 150,
                'type'    => 'text',
                'search'  => true,
                'orderby' => true,
                'lang'    => true
            ),
            'category' => array(
                'title' => $this->trans('Categories', array(), 'Modules.TVRecruitment.Admin'),
                'active' => 'category',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'category_thumb' => array(
                'title' => $this->trans('Categories Thumbnails', array(), 'Modules.TVRecruitment.Admin'),
                'active' => 'category_thumb',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'recruitment' => array(
                'title' => $this->trans('Recruitments', array(), 'Modules.TVRecruitment.Admin'),
                'active' => 'recruitment',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'recruitment_thumb' => array(
                'title' => $this->trans('Recruitments Thumbnails', array(), 'Modules.TVRecruitment.Admin'),
                'active' => 'recruitment_thumb',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            ),
            'user' => array(
                'title' => $this->trans('Users', array(), 'Modules.TVRecruitment.Admin'),
                'active' => 'user',
                'type' => 'bool',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            )
        );
        $this->languages = Language::getLanguages(false);
        $this->default_language = Configuration::get('PS_LANG_DEFAULT');
        parent::__construct();
        $this->imageManager = new TVRecruitmentImageManager($this->module);
        if (!$this->module->active) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
        }
    }

    public function renderList()
    {
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        return parent::renderList();
    }

    public function initContent()
    {
        $this->content = '';
        if (Tools::isSubmit('updateDefaultImages')) {
            $this->updateDefaultImages();
            $this->regenerateImages('default');
        }
        if (Tools::isSubmit('regenerateImages')) {
            $this->regenerateImages(Tools::getValue('regenerate_type'));
        }
        if (Tools::getIsset('ajax') && Tools::getValue('ajax')) {
            $action = str_replace($this->table, '', Tools::getValue('action'));
            $id_image_type = Tools::getValue('id_tvrecruitment_image');
            if (!$id_image_type || !$action) {
                die(json_encode(array('success' => false, 'error' => true, 'text' => $this->trans('Failed to update the status', array(), 'Modules.TVRecruitment.Admin'))));
            }
            $this->ajaxUpdate($action, $id_image_type);
        } else {
            if ($this->display == 'edit' || $this->display == 'add') {
                $this->content .= $this->renderForm();
            } else {
                $this->content .= $this->renderList();
                $this->content .= $this->renderRegenerateForm();
                $this->content .= $this->renderDefaultImagesForm();
            }
            $this->context->smarty->assign(
                array(
                    'content'                   => $this->content,
                    'url_recruitment'                  => self::$currentIndex.'&token='.$this->token,
                    'show_page_header_toolbar'  => $this->show_page_header_toolbar,
                    'page_header_toolbar_title' => $this->page_header_toolbar_title,
                    'page_header_toolbar_btn'   => $this->page_header_toolbar_btn
                )
            );
        }
    }

    public function renderForm()
    {
        $this->fields_form = array(
            'input'  => array(
                array(
                    'type'     => 'text',
                    'hint'     => $this->trans('Letters, underscores and hyphens only (e.g. "small_custom", "recruitment_medium", "large", "thickbox_extra-large").', array(), 'Modules.TVRecruitment.Admin'),
                    'label'    => $this->trans('Name', array(), 'Modules.TVRecruitment.Admin'),
                    'name'     => 'name',
                    'required' => true,
                    'desc'     => $this->trans('Enter the image type name', array(), 'Modules.TVRecruitment.Admin'),
                    'col'      => 3
                ),
                array(
                    'type'     => 'text',
                    'label'    => $this->trans('Width', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'    => $this->trans('Maximum image width in pixels.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'     => 'width',
                    'required' => true,
                    'desc'     => $this->trans('Enter the image type width', array(), 'Modules.TVRecruitment.Admin'),
                    'col'      => 3,
                    'suffix' => $this->trans('px', array(), 'Modules.TVRecruitment.Admin')
                ),
                array(
                    'type'     => 'text',
                    'label'    => $this->trans('Height', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'    => $this->trans('Maximum image height in pixels.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'     => 'height',
                    'required' => true,
                    'desc'     => $this->trans('Enter the image type height', array(), 'Modules.TVRecruitment.Admin'),
                    'col'      => 3,
                    'suffix' => $this->trans('px', array(), 'Modules.TVRecruitment.Admin')
                ),
                array(
                    'type'             => 'switch',
                    'label'            => $this->trans('Categories', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'            => $this->trans('This type will be used for Blog categories images.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'             => 'category',
                    'values'           => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Modules.TVRecruitment.Admin')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Modules.TVRecruitment.Admin')
                        )
                    )
                ),
                array(
                    'type'             => 'switch',
                    'label'            => $this->trans('Categories Thumbnails', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'             => $this->trans('This type will be used for Blog categories thumbnails images.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'             => 'category_thumb',
                    'values'           => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Modules.TVRecruitment.Admin')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Modules.TVRecruitment.Admin')
                        )
                    )
                ),
                array(
                    'type'             => 'switch',
                    'label'            => $this->trans('Recruitments', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'            => $this->trans('This type will be used for Blog recruitments images.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'             => 'recruitment',
                    'values'           => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Modules.TVRecruitment.Admin')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Modules.TVRecruitment.Admin')
                        )
                    )
                ),
                array(
                    'type'             => 'switch',
                    'label'            => $this->trans('Recruitments Thumbnails', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'             => $this->trans('This type will be used for Blog recruitments thumbnails.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'             => 'recruitment_thumb',
                    'values'           => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Modules.TVRecruitment.Admin')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Modules.TVRecruitment.Admin')
                        )
                    )
                ),
                array(
                    'type'             => 'switch',
                    'label'            => $this->trans('Blog users', array(), 'Modules.TVRecruitment.Admin'),
                    'hint'             => $this->trans('This type will be used for not registered Blog users.', array(), 'Modules.TVRecruitment.Admin'),
                    'name'             => 'user',
                    'values'           => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->trans('Enabled', array(), 'Modules.TVRecruitment.Admin')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->trans('Disabled', array(), 'Modules.TVRecruitment.Admin')
                        )
                    )
                )
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Modules.TVRecruitment.Admin'),
                'class' => 'button pull-right btn btn-default'
            )
        );

        return parent::renderForm();
    }

    public function renderDefaultImagesForm()
    {
        $defaultImages = $this->getDefaultImages();
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Default images', array(), 'Modules.TVRecruitment.Admin')
            ),
            'input' => array(
                array(
                    'type' => 'multilingual_image',
                    'name' => 'image_default',
                    'label' => $this->trans('Default image', array(), 'Modules.TVRecruitment.Admin'),
                    'hint' => $this->trans('Upload default image.', array(), 'Modules.TVRecruitment.Admin'),
                    'folder' => false,
                    'images' => $defaultImages['image_default']
                ),
                array(
                    'type' => 'multilingual_image',
                    'name' => 'image_category',
                    'label' => $this->trans('Categories default image', array(), 'Modules.TVRecruitment.Admin'),
                    'hint' => $this->trans('Upload default image for categories.', array(), 'Modules.TVRecruitment.Admin'),
                    'folder' => 'c',
                    'images' => $defaultImages['image_category']
                ),
                array(
                    'type' => 'multilingual_image',
                    'name' => 'image_category_thumb',
                    'label' => $this->trans('Categories thumbnails default image', array(), 'Modules.TVRecruitment.Admin'),
                    'hint' => $this->trans('Upload default image for categories thumbnails.', array(), 'Modules.TVRecruitment.Admin'),
                    'folder' => 'ct',
                    'images' => $defaultImages['image_category_thumb']
                ),
                array(
                    'type' => 'multilingual_image',
                    'name' => 'image_recruitment',
                    'label' => $this->trans('Recruitments default image', array(), 'Modules.TVRecruitment.Admin'),
                    'hint' => $this->trans('Upload default image for recruitments.', array(), 'Modules.TVRecruitment.Admin'),
                    'folder' => 'p',
                    'images' => $defaultImages['image_recruitment']
                ),
                array(
                    'type' => 'multilingual_image',
                    'name' => 'image_recruitment_thumb',
                    'label' => $this->trans('Recruitments thumbnail default image', array(), 'Modules.TVRecruitment.Admin'),
                    'hint' => $this->trans('Upload default image for recruitments thumbnails.', array(), 'Modules.TVRecruitment.Admin'),
                    'folder' => 'pt',
                    'images' => $defaultImages['image_recruitment_thumb']
                ),
                array(
                    'type' => 'multilingual_image',
                    'name' => 'image_user',
                    'label' => $this->trans('Users default image', array(), 'Modules.TVRecruitment.Admin'),
                    'hint' => $this->trans('Upload default image for users.', array(), 'Modules.TVRecruitment.Admin'),
                    'folder' => 'u',
                    'images' => $defaultImages['image_user']
                )
            ),
            'submit' => array(
                'title' => $this->trans('Save', array(), 'Modules.TVRecruitment.Admin'),
                'class' => 'button pull-right btn btn-default',
                'name' => 'updateDefaultImages',
                'id' => ''
            )
        );

        // add extra fields from related modules
        $extraFields = array_values(Hook::exec('displayTvrecruitmentImageManagerExtra', array(), null, true));
        if ($extraFields) {
            foreach ($extraFields[0] as $filed) {
                $this->fields_form['input'][] = $filed;
            }
        }

        return parent::renderForm();
    }

    public function renderRegenerateForm()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->trans('Regenerate images', array(), 'Modules.TVRecruitment.Admin')
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'name' => 'regenerate_type',
                    'label' => $this->trans('Select images type to regenerate', array(), 'Modules.TVRecruitment.Admin'),
                    'desc' => $this->trans('All images will regenerated. All old images will be lost.', array(), 'Modules.TVRecruitment.Admin'),
                    'options' => array(
                        'query' => array(
                            array(
                                'id'   => 'all',
                                'type' => $this->trans('All', array(), 'Modules.TVRecruitment.Admin')
                            ),
                            array(
                                'id'   => 'category',
                                'type' => $this->trans('Categories', array(), 'Modules.TVRecruitment.Admin')
                            ),
                            array(
                                'id'   => 'category_thumb',
                                'type' => $this->trans('Categories thumbnails', array(), 'Modules.TVRecruitment.Admin')
                            ),
                            array(
                                'id'   => 'recruitment',
                                'type' => $this->trans('Recruitments', array(), 'Modules.TVRecruitment.Admin')
                            ),
                            array(
                                'id'   => 'recruitment_thumb',
                                'type' => $this->trans('Recruitments thumbnails', array(), 'Modules.TVRecruitment.Admin')
                            ),
                            array(
                                'id'   => 'user',
                                'type' => $this->trans('Users', array(), 'Modules.TVRecruitment.Admin')
                            )
                        ),
                        'id'    => 'id',
                        'name'  => 'type'
                    )
                )
            ),
            'submit' => array(
                'title' => $this->trans('Regenerate', array(), 'Modules.TVRecruitment.Admin'),
                'class' => 'button pull-right btn btn-default',
                'id' => 'regenerate-images',
                'name' => 'regenerateImages'
            )
        );

        return parent::renderForm();
    }

    public function validateRules($class_name = false)
    {
        // check if this image type name is not already used
        if (!Tools::isEmpty(Tools::getValue('name')) && TVRecruitmentImage::checkIfNameExists(Tools::getValue('name'), Tools::getValue('id_tvrecruitment_image'))) {
            $this->errors[] = $this->trans('The image type with this name already exists, you can\'t use the same name more than once.', array(), 'Modules.TVRecruitment.Admin');
        }
        parent::validateRules();
    }

    public function recruitmentProcess()
    {
        $id_tvrecruitment_image = Tools::getValue('id_tvrecruitment_image');
        if (!Tools::isSubmit('updateDefaultImages') && !Tools::isSubmit('regenerateImages') && Tools::isSubmit('submitAddtvrecruitment_image')) {
            // remove all old images related to current image type in order to delete images if they are no more related to this type after saving
            if ($this->module->imagesAutoRegenerate && $id_tvrecruitment_image && !$this->imageManager->removeImageTypeImages($id_tvrecruitment_image)) {
                $this->errors[] = $this->lang('Fail to remove old images', array(), 'Modules.TVRecruitment.Admin');
            }
            $this->validateRules(false);
            if (count($this->errors)) {
                parent::recruitmentProcess();
                return false;
            }
            if ($id_tvrecruitment_image = Tools::getValue('id_tvrecruitment_image')) {
                $recruitmentimage = new TVRecruitmentImage($id_tvrecruitment_image);
            } else {
                $recruitmentimage = new TVRecruitmentImage();
            }
            $recruitmentimage->name = Tools::getValue('name');
            $recruitmentimage->width = Tools::getValue('width');
            $recruitmentimage->height = Tools::getValue('height');
            $recruitmentimage->category = Tools::getValue('category');
            $recruitmentimage->category_thumb = Tools::getValue('category_thumb');
            $recruitmentimage->recruitment = Tools::getValue('recruitment');
            $recruitmentimage->recruitment_thumb = Tools::getValue('recruitment_thumb');
            $recruitmentimage->user = Tools::getValue('user');
            if (!$recruitmentimage->save()) {
                $this->errors[] = $this->lang('An error occurred during object regeneration', array(), 'Modules.TVRecruitment.Admin');
            } else {
                $result = true;
                // check if images auto regeneration option is enabled
                if ($this->module->imagesAutoRegenerate) {
                    foreach (array_keys($this->module->imageTypes) as $imageType) {
                        if ($imageType == 'default' || ($imageType != 'default' && $recruitmentimage->$imageType)) {
                            $result &= $this->imageManager->regenerateTypeImages($imageType, false, $recruitmentimage->name);
                        }
                    }
                }
                if (!$result) {
                    $this->errors[] = $this->lang('An error occurred during images regeneration', array(), 'Modules.TVRecruitment.Admin');
                } else {
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminTVRecruitmentImages').'&conf=4');
                }
            }
        } elseif (!Tools::isSubmit('updateDefaultImages') && !Tools::isSubmit('regenerateImages')) {
            parent::recruitmentProcess();
        }
    }

    /**
     * Update status for image content type
     *
     * @param $action
     * @param $id_image_type
     */
    public function ajaxUpdate($action, $id_image_type)
    {
        $image = new TVRecruitmentImage((int)$id_image_type);
        if (Validate::isLoadedObject($image)) {
            $image->$action = $image->$action == 1 ? 0 : 1;
            if ($image->save()) {
                // check if images auto regeneration option is enabled
                if ($this->module->imagesAutoRegenerate) {
                    if ($image->$action) {
                        if (!$this->imageManager->regenerateTypeImages($action, false, $image->name)) {
                            die(json_encode(
                                array('success' => false, 'text' => $this->trans(
                                    'The status has been updated successfully but images were not generated properly. Please, regenerate it manually.',
                                    array(),
                                    'Modules.TVRecruitment.Admin'
                                ))
                            ));
                        }
                    } else {
                        if (!$this->imageManager->removeImageTypeImages($image->id, $action)) {
                            die(json_encode(
                                array('success' => false, 'text' => $this->trans(
                                    'The status has been updated successfully but images were not removed properly. Please, regenerate it manually.',
                                    array(),
                                    'Modules.TVRecruitment.Admin'
                                ))
                            ));
                        }
                    }
                }
                die(json_encode(array('success' => true, 'text' => $this->trans('The status has been updated successfully', array(), 'Modules.TVRecruitment.Admin'))));
            } else {
                die(json_encode(array('success' => false, 'error' => true, 'text' => $this->trans('Failed to update the status', array(), 'Modules.TVRecruitment.Admin'))));
            }
        }
    }

    public function updateDefaultImages()
    {
        $errors = array();
        foreach ($this->module->imageTypes as $key => $type) {
            foreach ($this->languages as $language) {
                if (Tools::getValue('image_'.$key.'_'.$language['id_lang'])) {
                    if ($error = $this->imageManager->uploadImage($language['iso_code'], $_FILES['image_'.$key.'_'.$language['id_lang']], $key)) {
                        $errors[] = $error;
                    }
                }
            }
        }

        Hook::exec('actionUpdateTvrecruitmentImages', array('values' => Tools::getAllValues(), 'images' => $_FILES));

        if (count($errors)) {
            $this->errors[] = $errors;
        }
        $this->confirmations[] = $this->trans('Everything is cool!', array(), 'Module.TVRecruitment.Admin');
    }

    public function getDefaultImages()
    {
        $defaultImages = array();
        foreach ($this->module->imageTypes as $key => $type) {
            foreach ($this->languages as $language) {
                $defaultImages['image_'.$key][$language['id_lang']] = false;
                if ($key == 'default') {
                    if (file_exists($this->module->modulePath.'img/'.$language['iso_code'].'.jpg')) {
                        $defaultImages['image_'.$key][$language['id_lang']] = $this->module->_link.'img/'.$language['iso_code'].'.jpg';
                    }
                } else {
                    if (file_exists($this->module->modulePath.'img/'.$type[0].$language['iso_code'].'.jpg')) {
                        $defaultImages['image_'.$key][$language['id_lang']] = $this->module->_link.'img/'.$type[0].$language['iso_code'].'.jpg';
                    }
                }
            }
        }

        return $defaultImages;
    }

    /**
     * Launch images regeneration
     *
     * @param $type
     */
    public function regenerateImages($type)
    {
        $result = true;
        if ($type == 'all') {
            $result &= $this->imageManager->regenerateTypeImages('default');
            $result &= $this->imageManager->regenerateTypeImages('category');
            $result &= $this->imageManager->regenerateTypeImages('category_thumb');
            $result &= $this->imageManager->regenerateTypeImages('recruitment');
            $result &= $this->imageManager->regenerateTypeImages('recruitment_thumb');
            $result &= $this->imageManager->regenerateTypeImages('user');
        } else {
            $result &= $this->imageManager->regenerateTypeImages($type);
        }
        if (!$result) {
            $this->errors[] = $this->trans('An error occurred during the images regeneration', array(), 'Module.TVRecruitment.Admin');
        } else {
            $this->confirmations[] = $this->trans('All images are regenerated successfully', array(), 'Module.TVRecruitment.Admin');
        }
    }
}
