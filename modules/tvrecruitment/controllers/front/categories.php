<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    BrianVo (Brian Vo)
 *  @copyright 2017 BrianVo
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

use PrestaShop\PrestaShop\Core\Product\Search\Pagination;

class TvrecruitmentCategoriesModuleFrontController extends ModuleFrontController
{
    public $pagename = 'categories';
    public $itemPerPage = 6;
    public $page = 1;
    public function __construct()
    {
        if (Tools::getIsset('page') && $page = Tools::getValue('page')) {
            $this->page = $page;
        }
        parent::__construct();
        $this->itemPerPage = Configuration::get('TVRECRUITMENT_POSTS_PER_PAGE');
    }

    public function initContent()
    {
        parent::initContent();
        $pagination = false;
        $categories = TVRecruitmentCategory::getAllFrontCategories($this->context->language->id, $this->context->shop->id, $this->context->customer->id_default_group, $this->page, $this->itemPerPage);
        if ($categories) {
            $pagination = $this->module->buildPagination(
                'cpagination',
                TVRecruitmentCategory::countFrontCategories($this->context->shop->id, $this->context->customer->id_default_group),
                $this->page,
                $this->itemPerPage
            );
        }
        $slides = TVRecruitmentPost::getAllRecruitmentsByCategory(BLOG_SLIDE_CATEGORY_ID,$this->context->language->id);
        
        $this->context->smarty->assign(
            array(
                'slides'    => $slides,
                'BLOG_SLIDE_CATEGORY_ID' => BLOG_SLIDE_CATEGORY_ID,
                'categories' => $categories,
                'pagination' => $pagination
            )
        );
        $this->setTemplate('module:tvrecruitment/views/templates/front/categories.tpl');
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array('title' => $this->trans('Blog categories', array(), 'Modules.TVRecruitment.Admin'), 'url' => '');

        return $breadcrumb;
    }
}
