<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    BrianVo (Brian Vo)
 * @copyright 2017 BrianVo
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class TvrecruitmentPostNewsModuleFrontController extends ModuleFrontController
{
    public $pagename = 'tvrecruitmentrecruitmentnews';
    public $recruitment;
    public function __construct()
    {
        parent::__construct();
        $this->recruitment = new TVRecruitmentPost(Tools::getValue('id_tvrecruitment_recruitmentnews'), $this->context->language->id);
    }

    public function initContent()
    {
        parent::initContent();
        TVRecruitmentPost::recruitmentViewed($this->recruitment->id);
        $recruitment = TVRecruitmentPost::getPost($this->recruitment->id, $this->context->language->id, $this->context->shop->id, $this->context->customer->id_default_group);
        if ($recruitment) {
            $recruitment = $recruitment[0];
        }
        $this->context->smarty->assign(
            array(
                'recruitment' => $recruitment,
                'tags' => TVRecruitmentPost::getPostTags($this->recruitment->id, $this->context->language->id),
                'displayViews' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_VIEWS'),
                'displayAuthor' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_AUTHOR')
            )
        );
        $this->setTemplate('module:tvrecruitment/views/templates/front/recruitment-news.tpl');
    }

    public function getBreadcrumbLinks()
    {
        $link = new Link();
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array('title' => $this->trans('Blog categories', array(), 'Modules.TVRecruitment.Admin'), 'url' => $link->getModuleLink('tvrecruitment', 'categories'));
        $breadcrumb['links'][] = array('title' => $this->recruitment->name, 'url' => $link->getModuleLink('tvrecruitment', 'recruitment', array('id_tvrecruitment_recruitment' => $this->recruitment->id, 'rewrite' => $this->recruitment->link_rewrite)));

        return $breadcrumb;
    }
}
