<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    BrianVo (Brian Vo)
 *  @copyright 2017 BrianVo
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class tvrecruitmentnewsModuleFrontController extends ModuleFrontController
{
    public $pagename = 'news';
    public $category;
    public $page = 1;
    public $itemPerPage = 6;

    const cate_tintuc_sukien = 8;
    const cate_khuyen_mai = 2;
    const cate_tintuc_chuyennganh= 10;
    const cate_tuyen_dung = 7;
    const cate_hot_news = 12;
    const cate_specialized_knowledge = 13;
    const cate_gems_jewelry = 14;

    public function __construct()
    {
        if (Tools::getIsset('page') && $page = Tools::getValue('page')) {
            $this->page = $page;
        }
        parent::__construct();
        $this->itemPerPage = Configuration::get('TVRECRUITMENT_POSTS_PER_PAGE');
        $id = Tools::getValue('id_tvrecruitment_category');
        $idCategory = !empty($id) ? $id : Tools::getValue('id_tvrecruitment_category');
        $this->category = new TVRecruitmentCategory($idCategory, $this->context->language->id);
    }

    public function initContent()
    {
        parent::initContent();

        $pagination = false;
        $recruitments = false;
        $recruitments = TVRecruitmentPost::getRecruitmentsByCategory($this->category->id, $this->context->language->id, $this->page, $this->itemPerPage);
        $pagination = $this->module->buildPagination(
            'npagination',
            TVRecruitmentPost::countRecruitmentsByCategory($this->category->id),
            $this->page,
            $this->itemPerPage,
            $this->category->id,
            $this->category->link_rewrite
        );

        if($this->category->id == 1){
            $cate_tintuc_sukien = new TVRecruitmentCategory(self::cate_tintuc_sukien, $this->context->language->id);
            $cate_khuyen_mai = new TVRecruitmentCategory(self::cate_khuyen_mai, $this->context->language->id);
            $cate_tintuc_chuyennganh= new TVRecruitmentCategory(self::cate_tintuc_chuyennganh, $this->context->language->id);
            $cate_tuyen_dung = new TVRecruitmentCategory(self::cate_tuyen_dung, $this->context->language->id);
            $this->context->smarty->assign(
                array(
                    'category_id' => $this->category->id,
                    'category_name' => $this->category->name,
                    'recruitments' => $recruitments,
                    'pagination' => $pagination,
                    'displayViews' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_VIEWS'),
                    'displayAuthor' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_AUTHOR'),
                    'cate_tintuc_sukien' => $cate_tintuc_sukien,
                    'cate_khuyen_mai' => $cate_khuyen_mai,
                    'cate_tintuc_chuyennganh' => $cate_tintuc_chuyennganh,
                    'cate_tuyen_dung' => $cate_tuyen_dung
                )
            );
        } else if($this->category->id == 6) {
            $cate_hot_news = new TVRecruitmentCategory(self::cate_hot_news, $this->context->language->id);
            $cate_specialized_knowledge = new TVRecruitmentCategory(self::cate_specialized_knowledge, $this->context->language->id);
            $cate_gems_jewelry= new TVRecruitmentCategory(self::cate_gems_jewelry, $this->context->language->id);
            $this->context->smarty->assign(
                array(
                    'category_id' => $this->category->id,
                    'category_name' => $this->category->name,
                    'recruitments' => $recruitments,
                    'pagination' => $pagination,
                    'displayViews' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_VIEWS'),
                    'displayAuthor' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_AUTHOR'),
                    'cate_hot_news' => $cate_hot_news,
                    'cate_specialized_knowledge' => $cate_specialized_knowledge,
                    'cate_gems_jewelry' => $cate_gems_jewelry,
                )
            );
        }
        else{
            $this->context->smarty->assign(
                array(
                    'category_id' => $this->category->id,
                    'category_name' => $this->category->name,
                    'recruitments' => $recruitments,
                    'pagination' => $pagination,
                    'displayViews' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_VIEWS'),
                    'displayAuthor' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_AUTHOR')
                )
            );
        }
        $this->setTemplate('module:tvrecruitment/views/templates/front/category-news.tpl');
    }

    public function getBreadcrumbLinks()
    {
        $link = new Link();
        $breadcrumb = parent::getBreadcrumbLinks();
//        $breadcrumb['links'][] = array('title' => $this->trans('Blog categories', array(), 'Modules.TVRecruitment.Admin'), 'url' => $link->getModuleLink('tvrecruitment', 'news'));
        $breadcrumb['links'][] = array('title' => $this->category->name, 'url' => $link->getModuleLink('tvrecruitment', 'news', array('id_tvrecruitment_category' => $this->category->id, 'rewrite' => $this->category->link_rewrite)));
        return $breadcrumb;
    }
}
