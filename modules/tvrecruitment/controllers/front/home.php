<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    BrianVo (Brian Vo)
 *  @copyright 2017 BrianVo
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class TvrecruitmentHomeModuleFrontController extends ModuleFrontController
{
    public $pagename = 'home';
    public $page = 1;
    public $itemPerPage = 6;

    public function __construct()
    {
        if (Tools::getIsset('page') && $page = Tools::getValue('page')) {
            $this->page = $page;
        }
        parent::__construct();
        $this->itemPerPage = Configuration::get('TVRECRUITMENT_POSTS_PER_PAGE');
    }

    public function initContent()
    {
        parent::initContent();
        $pagination = false;
        $recruitments = false;
        $recruitments = TVRecruitmentPost::getRecruitmentsByHome($this->context->language->id, $this->page, $this->itemPerPage);
		$pagination = $this->module->buildPagination(
			'pagination',
			TVRecruitmentPost::countRecruitmentsByHome(),
			$this->page,
			$this->itemPerPage
		);
        $this->context->smarty->assign(
            array( 
                'recruitments' => $recruitments,
                'pagination' => $pagination,
                'displayViews' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_VIEWS'),
                'displayAuthor' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_AUTHOR')
            )
        );

        $this->setTemplate('module:tvrecruitment/views/templates/front/home.tpl');
    }

    public function getBreadcrumbLinks()
    {
        $link = new Link();
        $breadcrumb = parent::getBreadcrumbLinks();
        return $breadcrumb;
    }
}
