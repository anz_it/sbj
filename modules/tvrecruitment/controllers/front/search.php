<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    BrianVo (Brian Vo)
 *  @copyright 2002-2017 BrianVo
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class TvrecruitmentSearchModuleFrontController extends ModuleFrontController
{
    public $pagename = 'search';
    public $query;
    public $category;
    public $page = 1;
    public $itemPerPage = 6;

    public function __construct()
    {
        if (Tools::getIsset('page') && $page = Tools::getValue('page')) {
            $this->page = $page;
        }
        parent::__construct();
        $this->itemPerPage = Configuration::get('TVRECRUITMENT_POSTS_PER_PAGE');
        $this->query = Tools::getValue('recruitment_search_query');
        $this->category = Tools::getValue('search_recruitment_categories');
    }

    public function initContent()
    {
        $query = Tools::replaceAccentedChars(urldecode(Tools::getValue('q')));
        if (Tools::getValue('ajax')) {
            $this->getAjaxSearch($query);
        } else {
            parent::initContent();
            $pagination = false;
            $recruitments = false;
            if ($this->query) {
                $recruitments = TVRecruitmentPost::search(
                    $this->query,
                    $this->category,
                    $this->context->language->id,
                    $this->context->shop->id,
                    $this->context->customer->id_default_group,
                    $this->page,
                    $this->itemPerPage
                );
                $pagination = $this->module->buildPagination(
                    'spagination',
                    TVRecruitmentPost::countRecruitmentsBySearch(
                        $this->query,
                        $this->category,
                        $this->context->language->id,
                        $this->context->shop->id,
                        $this->context->customer->id_default_group
                    ),
                    $this->page,
                    $this->itemPerPage,
                    '',
                    ''
                );
                $currentCategoryName = '';
                if ($this->category) {
                    $currentCategory = new TVRecruitmentCategory($this->category, $this->context->language->id);
                    $currentCategoryName = $currentCategory->name;
                }
            }
            $this->context->smarty->assign(
                array(
                    'recruitment_search_query' => $this->query,
                    'recruitments' => $recruitments,
                    'pagination' => $pagination,
                    'active_recruitment_category' => $currentCategoryName,
                    'displayViews' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_VIEWS'),
                    'displayAuthor' => Configuration::get('TVRECRUITMENT_DISPLAY_POST_AUTHOR')
                )
            );

            $this->setTemplate('module:tvrecruitment/views/templates/front/search.tpl');
        }
    }

    public function getBreadcrumbLinks()
    {
        $link = new Link();
        $breadcrumb = parent::getBreadcrumbLinks();
        $breadcrumb['links'][] = array('title' => $this->trans('Blog categories', array(), 'Modules.TVRecruitment.Admin'), 'url' => $link->getModuleLink('tvrecruitment', 'categories'));

        return $breadcrumb;
    }

    protected function getAjaxSearch($query)
    {
        $id_category = (int)Tools::getValue('category');
        $recruitments = TVRecruitmentPost::search(
            $query,
            $id_category,
            $this->context->language->id,
            $this->context->shop->id,
            $this->context->customer->id_default_group,
            1,
            1000
        );
        $output = array();
        if (count($recruitments)) {
            $jxsearch = new Jxsearch();
            $link = new Link();
            foreach ($recruitments as $recruitment) {
                $this->context->smarty->assign('recruitment', array(
                    'info' => $recruitment,
                    'url' => $link->getModuleLink('tvrecruitment', 'recruitment', array('id_tvrecruitment_recruitment' => $recruitment['id_tvrecruitment_recruitment'], 'rewrite' => $recruitment['link_rewrite'])),
                ));
                $output[] = $jxsearch->display($jxsearch->getLocalPath(), '/views/templates/hook/_items/row-recruitment.tpl');
            }
        }
        if (!count($output)) {
            die(json_encode(array('empty' => $this->trans('No recruitment found'))));
        }

        $total = count($output);

        die(json_encode(array('result' => $output, 'total' => $total)));
    }
}
