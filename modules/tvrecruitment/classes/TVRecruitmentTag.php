<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    BrianVo (Brian Vo)
 * @copyright 2017 BrianVo
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class TVRecruitmentTag extends ObjectModel
{
    public $id_lang;
    public $tag;
    public static $definition = array(
        'table'     => 'tvrecruitment_tag',
        'primary'   => 'id_tvrecruitment_tag',
        'multilang' => false,
        'fields'    => array(
            'id_lang' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'tag'     => array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'required' => true, 'size' => 64)
        )
    );

    /**
     * Check if tag is already exists to prevent duplicating
     *
     * @param $id_lang
     * @param $tag
     *
     * @return false|null|string
     */
    public static function checkTagExistence($id_lang, $tag)
    {
        return Db::getInstance()->getValue('
          SELECT `id_tvrecruitment_tag`
          FROM '._DB_PREFIX_.'tvrecruitment_tag
          WHERE `tag` = "'.pSql($tag).'"
          AND `id_lang` = '.(int)$id_lang);
    }

    /**
     * Get all recruitment tags
     *
     * @param $id_tvrecruitment_recruitment
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getPostTags($id_tvrecruitment_recruitment)
    {
        return Db::getInstance()->executeS('
          SELECT t.*
          FROM '._DB_PREFIX_.'tvrecruitment_tag t
          LEFT JOIN '._DB_PREFIX_.'tvrecruitment_tag_recruitment tp
          ON(tp.`id_tvrecruitment_tag`=t.`id_tvrecruitment_tag`)
          WHERE tp.`id_tvrecruitment_recruitment` = '.(int)$id_tvrecruitment_recruitment);
    }
}
