{*
* 2017 BrianVo
*
* TV Recruitment
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    BrianVo (Brian Vo)
*  @copyright 2017 BrianVo
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{extends file=$layout}

{block name='head_seo_description'}{$recruitment.meta_description}{/block}
{block name='head_seo_keywords'}{$recruitment.meta_keyword}{/block}

{block name='content'}
  <section id="main">
    {if $recruitment}

      {block name='recruitment_recruitment_header'}
        <h1>{$recruitment.name}</h1>
      {/block}

      <p class="recruitment-meta">
        {l s='Posted on %date% at %time% by [1"]%link%["1]%name%[/1]'
        sprintf=[
        '%date%' => {$recruitment.date_start|date_format},
        '%time%' => {$recruitment.date_start|date_format:"%H:%M"},
        '%link%' => {url entity='module' name='tvrecruitment' controller='author' params = ['author' => $recruitment.id_author]},
        '%name%' => $recruitment.author,
        '[1"]' => '<a href="',
        '["1]' => '">',
        '[/1]' => '</a>'
        ]
        d='Shop.Theme.Global'
        }{if $recruitment.views} - <small class="recruitment-views">{l s='Views' mod='tvrecruitment'} ({$recruitment.views})</small>{/if}
      </p>

      <div class="recruitment-image mb-2">
        <img class="img-fluid" src="{TVRecruitmentImageManager::getImage('recruitment', $recruitment.id_tvrecruitment_recruitment, 'recruitment_default')}" alt="{$recruitment.name}"/>
      </div>

      {if $recruitment.description}
        <div class="recruitment-description">
          {$recruitment.description nofilter}
        </div>
      {/if}

      {if $tags}
        <hr>
        <div class="recruitment-tags">
          {l s='Tagged' mod='tvrecruitment'}
          {foreach from=$tags item='tag' name="tag"}
            <a href="{$link->getModuleLink('tvrecruitment', 'tag', ['id_tvrecruitment_tag' => $tag.id_tvrecruitment_tag])}">{$tag.tag}</a>{if !$smarty.foreach.tag.last},{/if}
          {/foreach}
        </div>
      {/if}

      <hr>

      {hook h="displayTVRecruitmentPostFooter" recruitment=$recruitment}

    {else}
      <p>{l s='Post doesn\'t exist or you don\'t have permissions to access it.' mod='tvrecruitment'}</p>
    {/if}

  </section>
{/block}
