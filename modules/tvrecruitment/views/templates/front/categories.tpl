{*
* 2017 BrianVo
*
* TV Recruitment
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    BrianVo (Brian Vo)
*  @copyright 2017 BrianVo
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}


{extends file=$layout}

{block name='content'}
  <section id="main">
    <div class="container">
    {block name='recruitment_categories_header'}
      <h1>{l s='Blog categories' mod='tvrecruitment'}</h1>
    {/block}

    {if $categories}
      <section class="recruitment-categories">
        
        <div class="row">
          {foreach from=$categories item='category'}
            {block name='recruitment_category_miniature'}
                <table class="table-fee">
                <tr>
                  <th class="col-first">
                    Vị trí
                  </th>
                  <th>
                    <div class="row-line">
                      <p>Địa điểm</p> 
                    </div>
                  </th> 
                  <th>
                    <div class="row-line">
                      <p>Thời hạn ứng tuyển</p> 
                    </div>
                  </th>                  
                </tr>
                <tr>
                  <td>
                    <a href="{url entity='module' name='tvrecruitment' controller='category' params = ['id_tvrecruitment_category' => $category.id_tvrecruitment_category, 'rewrite' => $category.link_rewrite]}">
                        {$category.name}
                    </a> 
                  </td>
                  <td>
                    {$category.name}
                  </td>
                  <td>
                    {$category.name}
                  </td> 
                </tr>
               
              </table> 
            {/block}
          {/foreach}
        </div>
        
      </section>
      {if $pagination}
        {include file="module:tvrecruitment/views/templates/front/_partials/pagination.tpl"}
      {/if}

    {else}
      {l s='There are no categories in the recruitment yet' mod='tvrecruitment'}
    {/if}
    </div>
  </section>
{/block}
