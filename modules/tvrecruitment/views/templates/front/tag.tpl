{*
* 2017 BrianVo
*
* TV Recruitment
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    BrianVo (Brian Vo)
*  @copyright 2017 BrianVo
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{extends file=$layout}

{block name='content'}
  <section id="main">
    <h1>{$tag->tag}</h1>
    {if $recruitments}
      {include file="module:tvrecruitment/views/templates/front/_partials/recruitment-miniature.tpl"}
      {if $pagination}
        {include file="module:tvrecruitment/views/templates/front/_partials/pagination.tpl"}
      {/if}
    {else}
      <p>{l s='There are no recruitments' mod='tvrecruitment'}</p>
    {/if}
  </section>
{/block}
