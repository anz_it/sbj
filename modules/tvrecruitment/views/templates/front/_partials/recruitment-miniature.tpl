{*
* 2017 BrianVo
*
* TV Recruitment
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    BrianVo (Brian Vo)
*  @copyright 2017 BrianVo
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<section class="recruitment-recruitments container-table row">
  <table class="table-fee">
      <tr>
        <th class="col-first">
          Vị trí
        </th>
        <th>
          <div class="row-line">
            <p>Địa điểm</p> 
          </div>
        </th> 
        <th>
          <div class="row-line">
            <p>Thời hạn ứng tuyển</p> 
          </div>
        </th>                  
      </tr>
  
  {foreach from=$recruitments item='recruitment'}
    {block name='recruitment_recruitment_miniature'}
      <tr>
        <td>
          <a href="{url entity='module' name='tvrecruitment' controller='recruitment' params = ['id_tvrecruitment_recruitment' => $recruitment.id_tvrecruitment_recruitment, 'rewrite' => $recruitment.link_rewrite]}">
            {$recruitment.name}
          </a>
          {if $recruitment.short_description}
              <div class="bp-short-description">
                {$recruitment.short_description nofilter}
              </div>
            {/if}
        </td>
        <td>
          {$recruitment.name}
        </td>
        <td>
          {$recruitment.name}
        </td> 
      </tr> 
    {/block}
  {/foreach}
  </table>
</section>
