<?php
/**
 * 2017 BrianVo
 *
 * TV Recruitment
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    BrianVo (Brian Vo)
 *  @copyright 2017 BrianVo
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once(_PS_MODULE_DIR_.'tvrecruitment/src/TVRecruitmentRepository.php');
include_once(_PS_MODULE_DIR_.'tvrecruitment/classes/TVRecruitmentCategory.php');
include_once(_PS_MODULE_DIR_.'tvrecruitment/classes/TVRecruitmentPost.php');
include_once(_PS_MODULE_DIR_.'tvrecruitment/classes/TVRecruitmentTag.php');
include_once(_PS_MODULE_DIR_.'tvrecruitment/classes/TVRecruitmentImage.php');
include_once(_PS_MODULE_DIR_.'tvrecruitment/classes/TVRecruitmentImageManager.php');

class Tvrecruitment extends Module
{
    public $repository;
    public $mainTab = array();
    public $tabs = array();
    public $languages;
    public $modulePath;
    public $_link;
    public $imagesAutoRegenerate;
    public $imageTypes;

    public function __construct()
    {
        $this->name = 'tvrecruitment';
        $this->tab = 'content_management';
        $this->version = '0.0.1';
        $this->author = 'BrianVo (BrianVo)';
        $this->need_instance = 1;
        $this->controllers = array('author', 'categories', 'category', 'recruitment', 'search', 'tag');
        $this->bootstrap = true;
        parent::__construct();
        $this->modulePath = $this->local_path;
        $this->_link = $this->_path;

        $this->displayName = $this->l('TV Recruitment');
        $this->description = $this->l('The Recruitment extension for Prestashop platform.');
        $this->confirmUninstall = $this->l('Are you sure that you want to delete the module? All related data will be deleted forever!');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->languages = Language::getLanguages(true);
        $this->defaultLanguage = new Language(Configuration::get('PS_LANG_DEFAULT'));
        $this->repository = new TVRecruitmentRepository(Db::getInstance(), $this->context->shop, $this->context->getTranslator());
        $this->mainTab = array(
            'class_name' => 'AdminTVRecruitment',
            'module' => $this->name,
            'name' => 'TV Recruitment'
        );

        $this->tabs = array(
            array(
                'class_name' => 'AdminTVRecruitmentCategories',
                'module'     => $this->name,
                'name'       => 'Categories'
            ),
            array(
                'class_name' => 'AdminTVRecruitmentPosts',
                'module'     => $this->name,
                'name'       => 'Recruitments'
            ),
            array(
                'class_name' => 'AdminTVRecruitmentImages',
                'module'     => $this->name,
                'name'       => 'Images Manager'
            )
        );

        $this->settingsTab = array(
            'class_name' => 'AdminTVRecruitmentSettings',
            'module'     => $this->name,
            'name'       => 'Settings'
        );

        $this->settingsSubTabs = array(
            array(
                'class_name' => 'AdminTVRecruitmentMainSettings',
                'module'     => $this->name,
                'name'       => 'Main settings'
            )
        );
        $this->imageTypes = array(
            'default'        => array('default'),
            'category'       => array('c/', 'category'),
            'category_thumb' => array('ct/', 'category_thumb'),
            'recruitment'           => array('p/', 'recruitment'),
            'recruitment_thumb'     => array('pt/', 'recruitment_thumb'),
            'user'           => array('u/', 'user')
        );

        $this->imagesAutoRegenerate = Configuration::get('TVRECRUITMENT_IMAGES_AUTO_REGENERATION');
    }

    public function install()
    {
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('moduleRoutes') &&
            $this->repository->createTables() &&
            $this->setSettings() &&
            $this->addTabs();
    }

    public function uninstall()
    {
        // use this hook to delete all related module which are not useful after this module deleting
        Hook::exec('actionTVRecruitmentBeforeModuleDelete');

        return $this->repository->dropTables() &&
            $this->removeTabs() &&
            $this->removeSettings() &&
            $this->clearAllImages() &&
            parent::uninstall();
    }

    protected function addTabs()
    {
        $idMainTab = $this->addTab($this->mainTab, 0);

        foreach ($this->tabs as $item) {
            $this->addTab($item, $idMainTab);
        }

        $idSettingsTab = $this->addTab($this->settingsTab, $idMainTab);
        foreach ($this->settingsSubTabs as $item) {
            $this->addTab($item, $idSettingsTab);
        }

        return true;
    }

    public function addTab($tab, $parent)
    {
        $t = new Tab();
        $t->class_name = $tab['class_name'];
        $t->id_parent = (int)$parent;
        $t->module = $tab['module'];

        foreach ($this->languages as $lang) {
            $t->name[$lang['id_lang']] = $this->l($tab['name']);
        }

        if (!$t->save()) {
            return false;
        }

        return $t->id;
    }

    public function setSettings()
    {
        Configuration::updateValue('TVRECRUITMENT_IMAGES_AUTO_REGENERATION', 1);
        Configuration::updateValue('TVRECRUITMENT_DISPLAY_POST_AUTHOR', 1);
        Configuration::updateValue('TVRECRUITMENT_DISPLAY_POST_VIEWS', 1);
        Configuration::updateValue('TVRECRUITMENT_POSTS_PER_PAGE', 6);

        return true;
    }

    protected function removeTabs()
    {
        $tabId = TabCore::getIdFromClassName($this->mainTab['class_name']);
        $tab = new Tab($tabId);
        if (!$tab->delete()) {
            return false;
        }

        foreach (array_merge($this->tabs, $this->settingsSubTabs) as $t) {
            if ($t) {
                $t = new Tab(TabCore::getIdFromClassName($t['class_name']));
                if (!$t->delete()) {
                    return false;
                }
            }
        }

        return true;
    }

    public function removeSettings()
    {
        Configuration::deleteByName('TVRECRUITMENT_IMAGES_AUTO_REGENERATION');
        Configuration::deleteByName('TVRECRUITMENT_DISPLAY_POST_AUTHOR');
        Configuration::deleteByName('TVRECRUITMENT_DISPLAY_POST_VIEWS');
        Configuration::deleteByName('TVRECRUITMENT_POSTS_PER_PAGE');

        return true;
    }

    protected function clearAllImages()
    {
        $imagesList = array();
        $path = $this->modulePath.'/img/';
        foreach ($this->imageTypes as $type => $imageType) {
            if ($type != 'default') {
                $path = $this->modulePath.'/img/'.$imageType[0];
            }
            if ($images = Tools::scandir($path, 'jpg')) {
                foreach ($images as $image) {
                    array_push($imagesList, $path.$image);
                }
            }
        }

        if ($imagesList) {
            $imageManager = new TVRecruitmentImageManager($this);
            return $imageManager->removeImagesByList($imagesList);
        }

        return true;
    }

    public function hookModuleRoutes()
    {
        return array(
            'module-tvrecruitment-categories'  => array(
                'controller' => 'categories',
                'rule'       => 'recruitment/categories',
                'keywords'   => array(),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
			'module-tvrecruitment-home'  => array(
                'controller' => 'home',
                'rule'       => 'recruitment/home',
                'keywords'   => array(),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-cpagination' => array(
                'controller' => 'categories',
                'rule'       => 'recruitment/categories/page/{page}',
                'keywords'   => array(
                    'page' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'page'),
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-category'    => array(
                'controller' => 'category',
                'rule'       => 'recruitment/category/{id_tvrecruitment_category}/{rewrite}',
                'keywords'   => array(
                    'id_tvrecruitment_category' => array('regexp' => '[0-9]+', 'param' => 'id_tvrecruitment_category'),
                    'rewrite'            => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*')
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),

            'module-tvrecruitment-tag'         => array(
                'controller' => 'tag',
                'rule'       => 'recruitment/tag/{id_tvrecruitment_tag}',
                'keywords'   => array(
                    'id_tvrecruitment_tag' => array('regexp' => '[0-9]+', 'param' => 'id_tvrecruitment_tag')
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-tpagination' => array(
                'controller' => 'tag',
                'rule'       => 'recruitment/tag/{id_tvrecruitment_tag}/page/{page}',
                'keywords'   => array(
                    'id_tvrecruitment_tag' => array('regexp' => '[0-9]+', 'param' => 'id_tvrecruitment_tag'),
                    'page'          => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'page'),
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-author'      => array(
                'controller' => 'author',
                'rule'       => 'recruitment/author/{author}',
                'keywords'   => array(
                    'author' => array('regexp' => '[0-9]+', 'param' => 'author')
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-apagination' => array(
                'controller' => 'author',
                'rule'       => 'recruitment/author/{author}/page/{page}',
                'keywords'   => array(
                    'author' => array('regexp' => '[0-9]+', 'param' => 'author'),
                    'page'   => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'page'),
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-pagination'  => array(
                'controller' => 'category',
                'rule'       => 'recruitment/category/{id_tvrecruitment_category}/{rewrite}/page/{page}',
                'keywords'   => array(
                    'id_tvrecruitment_category' => array('regexp' => '[0-9]+', 'param' => 'id_tvrecruitment_category'),
                    'rewrite'            => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*'),
                    'page'               => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'page'),
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            ),
            'module-tvrecruitment-recruitment'        => array(
                'controller' => 'recruitment',
                'rule'       => 'recruitment/recruitment/{id_tvrecruitment_recruitment}/{rewrite}',
                'keywords'   => array(
                    'id_tvrecruitment_recruitment' => array('regexp' => '[0-9]+', 'param' => 'id_tvrecruitment_recruitment'),
                    'rewrite'        => array('regexp' => '[_a-zA-Z0-9\pL\pS-]*')
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tvrecruitment',
                )
            )
        );
    }

    /**
     * Used to access recruitment links from external modules
     *
     * @param $page
     * @param $params
     *
     * @return string
     */
    public function getBlogLink($page, $params)
    {
        $link = new Link();
        return $link->getModuleLink($this->name, $page, $params);
    }

    public function buildPagination($controller, $totalItems, $page, $itemsPerPage, $id = false, $rewrite = '')
    {
        if (!$totalItems) {
            return false;
        }
        $link = new Link();
        $pagination = array();
        $itemShowFrom = 1;
        $itemShowTo = $page * $itemsPerPage;
        if ($page > 1) {
            $itemShowFrom = ($itemsPerPage * ($page - 1)) + 1;
        }
        if ($itemShowTo > $totalItems) {
            $itemShowTo = $totalItems;
        }
        $params = array();
        if ($id) {
            $params = array('id_tvrecruitment_category' => $id, 'rewrite' => $rewrite);
        }
        if ($controller == 'tpagination') {
            $params = array('id_tvrecruitment_tag' => $id);
        }
        if ($controller == 'apagination') {
            $params = array('author' => $id);
        }
        $i = 0;
        $totalPages = ceil($totalItems/$itemsPerPage);
        $pagination['total'] = $totalItems;
        $pagination['from'] = $itemShowFrom;
        $pagination['to'] = $itemShowTo;
        if ($totalItems > $itemsPerPage) {
            $pagination['steps'][$i] = array(
                'type'   => 'previous',
                'url'    => $link->getModuleLink(
                    'tvrecruitment',
                    $controller,
                    array_merge(array('page' => $page > 1 ? $page - 1 : $page), $params)
                ),
                'name'   => $this->trans('Previous', array(), 'Modules.TVRecruitment.Admin'),
                'active' => ($page == 1) ? true : false
            );
            for ($i = &$i; $i < $totalPages; $i++) {
                $pagination['steps'][$i + 1]['type'] = 'page';
                $pagination['steps'][$i + 1]['url'] = $link->getModuleLink('tvrecruitment', $controller, array_merge(array('page' => $i + 1), $params));
                $pagination['steps'][$i + 1]['name'] = $i + 1;
                $pagination['steps'][$i + 1]['active'] = ($page == $i + 1) || (!$page && $i == 0) ? true : false;
            }
            $pagination['steps'][$i + 1] = array(
                'type'   => 'next',
                'url'    => $link->getModuleLink(
                    'tvrecruitment',
                    $controller,
                    array_merge(array('page' => $page < $totalPages ? $page + 1 : $page), $params)
                ),
                'name'   => $this->trans('Next', array(), 'Modules.TVRecruitment.Admin'),
                'active' => ($page == $totalPages) ? true : false
            );
        }

        return $pagination;
    }

    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJS('js/jquery/jquery-1.11.0.min.js');
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/tvrecruitment_admin.js');
            $this->context->controller->addCSS($this->_path.'views/css/tvrecruitment_admin.css');
        }
    }

    public function hookHeader()
    {
        $this->context->controller->requireAssets(array('font-awesome'));
        $this->context->controller->addJS($this->_path.'/views/js/tvrecruitment.js');
        $this->context->controller->addCSS($this->_path.'/views/css/tvrecruitment.css');
    }
}
