{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="inspection">
  <div class="container">
    <div class="inspection-center"
         style="background-image:url({if isset($BANNER_TTGD_IMG)} {$BANNER_TTGD_IMG} {/if} )">
      <div class="text">
        <h2>{if isset($BANNER_TTGD_NAME)} {$BANNER_TTGD_NAME} {/if} </h2>
        <div class="desc">{if isset($BANNER_TTGD_DESC)} {$BANNER_TTGD_DESC} {/if}</div>
      </div>
      <div class="bottom">
        <h3 class="text-uppercase">{l s='Trung tâm giám định & Dịch vụ kim hoàn' d='Shop.Theme.Actions'}</h3>
        <a class="btn btn-white text-uppercase" href="{$link->goPage('trung-tam-giam-dinh','')}">{l s='View More' d='Shop.Theme.Actions'}</a>
      </div>
    </div>
  </div>
</div>