{block name="news_pricegold"}
    <!-- news and price gold-->
    <div class="news-pricegold bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <!-- news-->
                    <div class="news">
                        <h2 class="title text-center">{l s='Tin tức & Khuyến mãi' d='Admin.Global'}</h2>
                        <ul class="list-unstyled">
                            {foreach $news as $item}
                                <li class="tile-news-home">
                                    <h3><a href="{url entity='module' name='jxblog' controller='postnews' params = ['id_jxblog_postnews' => $item.id_jxblog_post, 'rewrite' => $item.link_rewrite]}">{$item.name}</a></h3>
                                    <p class="time">{$item.date_add|date_format:"%d/%m/%Y"}</p>
                                    <div class="desc">
                                        {$item.short_description|strip_tags:false|truncate:120:'...'}
                                    </div>
                                </li>
                            {/foreach}
                        </ul>
                        <div class="text-center">
                            <a class="btn btn-red btn-lg text-uppercase"
                               href="{url entity='module' name='jxblog' controller='news' params = ['id_jxblog_category' => $category->id_jxblog_category, 'rewrite' => $category->link_rewrite]}">
                                {l s='Xem thêm' d='Admin.Global'}</a>
                        </div>
                    </div>
                    <!-- e: news-->
                </div>
                <div class="col-lg-6">
                    <!-- price-gold-->
                    <div class="price-gold">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" colspan="3">{l s='Giá vàng hôm nay' d='Admin.Global'}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" colspan="3">
                                    <p>{$smarty.now|date_format:"%I:%k:%S %p %d/%m/%Y"}</p>
                                    <p>(Đơn vị: ngàn đồng/chỉ)</p>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-center">{l s='Loại vàng' d='Admin.Global'} </td>
                                <td class="text-center">{l s='Mua vào' d='Admin.Global'}</td>
                                <td class="text-center">{l s='Bán ra' d='Admin.Global'}</td>
                            </tr>
                            {foreach $prices_gold as $item}
                                <tr>
                                    <td>{$item.name_gold}</td>
                                    <td>{$item.price_buy|number_format:0:".":"."}</td>
                                    <td>{$item.price_sell|number_format:0:".":"."}</td>
                                </tr>
                            {/foreach}
                            <tr>
                                <td colspan="3"><a class="btn btn-block btn-red text-uppercase" href="{$urls.shop_domain_url}/gia-vang">
                                        {l s='Xem chi tiết' d='Admin.Global'}</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- e: price-gold-->
                </div>
            </div>
        </div>
    </div>
    <!-- e: news and price gold-->
{/block}
