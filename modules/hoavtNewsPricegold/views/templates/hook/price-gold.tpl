<div id="main-content">
    <!-- title page-->
    <div class="title-page text-center" style="background-image:url({$banner_gold})">
        <div class="container">
            <h1>{$title_gold}</h1>
        </div>
    </div>

    <!-- e: title page-->
    <div class="gold-table">
        <div class="container">
             <div class="row">
                <div class="col-md-6 offset-md-3 col-lg-8 offset-lg-2">
                    <h2 class="title text-center">GIÁ VÀNG TRONG NƯỚC</h2>
                    <div class="header-table-gold d-flex flex-wrap justify-content-between">
                        <div class="info">
                            <p>{$smarty.now|date_format:"%I:%k:%S %p %d/%m/%Y"}</p>
                            <p>(Đơn vị: ngàn đồng/chỉ)</p>
                        </div>
                        <form method="get">
                            <div class="fillter-day d-flex align-items-center"><span>Lọc giá vàng theo</span>
                                <input id="day-frontend" name="date-frontend" class="form-control" type="text" value="{$keySearch}">
                                <button class="btn btn-red btn-sm text-uppercase" type="submit">Xem</button>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Loại vàng</th>
                                <th>Mua vào</th>
                                <th>Bán ra</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach $prices_gold as $item}
                                <tr>
                                    <td>{$item.name_gold}</td>
                                    <td>{$item.price_buy|number_format:0:".":"."}</td>
                                    <td>{$item.price_sell|number_format:0:".":"."}</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>

                    <div>
                        <p>
                            {$note|unescape: "html" nofilter}
                        </p>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>