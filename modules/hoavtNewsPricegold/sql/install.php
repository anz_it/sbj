<?php
/**
* 2017-2018 Zemez
*
* JX Deal of Day
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    Zemez (Sergiy Sakun)
*  @copyright 2017-2018 Zemez
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'price_gold` (
	`id_price_gold` int(11) NOT NULL AUTO_INCREMENT,
	`key_gold` varchar(25) NOT NULL,
	`price_buy` varchar(128) NOT NULL,
	`price_sell` varchar(128) NOT NULL,
	`day` int(11) NOT NULL,
	`name_gold` varchar(128),
     PRIMARY KEY  (`id_price_gold`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'price_gold_note` (
	`day` int(11) NOT NULL,
	`note` text not null,
	PRIMARY KEY  (`day`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
