<?php
/**
 * @author : hoavt write 2018
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;

include_once(_PS_MODULE_DIR_ . 'hoavtNewsPricegold/classes/priceGold.php');
include_once(_PS_MODULE_DIR_ . 'hoavtNewsPricegold/classes/priceGoldNote.php');

class hoavtNewsPricegold extends Module
{
    public function __construct()
    {
        $this->name = 'hoavtNewsPricegold';
        $this->tab = 'administration';
        $this->version = '1.0';
        $this->author = 'HoaVt (Vo Thien Hoa)';
        $this->need_instance = 0;
        $this->lang = true;
        $this->module_key = 'e9338bceb8ef0cebd52fa0db917a0928';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Tin tức - giá vàng');
        $this->description = $this->l('Cấu hình tab tin tức trang chủ + thông tin giá vàng');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        include(dirname(__FILE__) . '/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('displayHome') &&
            $this->registerHook('displayPriceGold') &&
            $this->installFixtures();
    }

    public function hookActionObjectLanguageAddAfter($params)
    {
        return $this->installFixture((int)$params['object']->id, Configuration::get('BANNER_IMG_GOLD_PRICE', (int)Configuration::get('PS_LANG_DEFAULT')));
    }

    protected function installFixtures()
    {
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $this->installFixture((int)$lang['id_lang'], 'sale70.png');
        }
        return true;
    }

    protected function installFixture($id_lang, $image = '')
    {
        $values['BANNER_IMG_GOLD_PRICE'][(int)$id_lang] = $image;
        $values['TILE_GOLD_PRICE'][(int)$id_lang] = '';
        Configuration::updateValue('BANNER_IMG_GOLD_PRICE', $values['BANNER_IMG_GOLD_PRICE']);
        Configuration::updateValue('TILE_GOLD_PRICE', $values['TILE_GOLD_PRICE']);
    }

    public function uninstall()
    {
        include(dirname(__FILE__) . '/sql/uninstall.php');
        Configuration::deleteByName('BANNER_IMG_GOLD_PRICE');
        Configuration::deleteByName('TILE_GOLD_PRICE');
        return parent::uninstall();
    }

    public function hookdisplayBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path . 'views/js/price-gold.js');
    }

    /**
     * Returns module content for home page
     */
    public function hookDisplayHome()
    {
        $keySearch = isset($_GET['date']) ? $_GET['date'] : date('d/m/Y');
        $dtime = DateTime::createFromFormat("d/m/Y", $keySearch);
        $dtime->setTime(0, 0, 0);
        $timestamp = $dtime->getTimestamp();

        $news = priceGold::getAllPost();
        $price_gold = priceGold::getAllPriceGoldFrontend();
        $this->context->smarty->assign(
            array(
                'prices_gold' => $price_gold,
                'news' => $news,
                'category'=> new JXBlogCategory(Configuration::get('ID_TIN_TUC_KHUYEN_MAI'), $this->context->language->id),
            )
        );
        return $this->display(__FILE__, 'news-price-gold.tpl');
    }

    private function updateUrl($link)
    {
        if (substr($link, 0, 7) !== "http://" && substr($link, 0, 8) !== "https://") {
            $link = "http://" . $link;
        }

        return $link;
    }

    /**
     * Returns module content for home page
     */
    public function hookDisplayPriceGold()
    {
        $keySearch = isset($_GET['date-frontend']) ? $_GET['date-frontend'] : '';
        if(!empty($keySearch)){
            $dtime = DateTime::createFromFormat("d/m/Y", $keySearch);
            $dtime->setTime(0, 0, 0);
            $timestamp = $dtime->getTimestamp();
        }else{
            $timestamp = '';
        }
        $price_gold = priceGold::getAllPriceGoldFrontend($timestamp);
        $dateSearch = date('d/m/Y',$timestamp);
        $titleGold = $imgname = Configuration::get('TILE_GOLD_PRICE', $this->context->language->id);
        $bannerGold = Configuration::get('BANNER_IMG_GOLD_PRICE', $this->context->language->id);
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $note = '';
        $keySearchNote = isset($_GET['date-frontend']) ? $_GET['date-frontend'] : date('d/m/Y');
        if (!empty($keySearchNote)) {
            $dtime_note = DateTime::createFromFormat("d/m/Y", $keySearchNote);
            $dtime_note->setTime(0, 0, 0);
            $timestampNote = $dtime_note->getTimestamp();
            $data = priceGoldNote::getNote($timestampNote);
            if( !empty($data)){
                $note = $data['note'];
            }
        }

        $this->context->smarty->assign(
            array(
                'prices_gold' => $price_gold,
                'keySearch' =>$keySearchNote,
                'title_gold' =>$titleGold,
                'note' =>$note,
                'banner_gold' =>$this->context->link->protocol_content . Tools::getMediaServer($bannerGold) . $this->_path . 'img/' . $bannerGold,
            )
        );

        return $this->display(__FILE__, 'price-gold.tpl');
    }

    /**
     * Delete tab
     */
    protected function deleteTab()
    {
        $priceGold = new priceGold(Tools::getValue('id_price_gold'));
        $priceGold->delete();
        return $this->displayConfirmation($this->l('The priceGold is successfully deleted'));

    }

    /**
     * Add tab
     */
    protected function addPriceGold()
    {
        $this->addElement();
    }

    /**
     * Add tab
     */
    protected function addElement()
    {
        if ((int)Tools::getValue('id_price_gold') > 0) {
            $priceGold = new priceGold((int)Tools::getValue('id_price_gold'));
        } else {
            $priceGold = new priceGold();
        }
        $priceGold->key_gold = Tools::getValue('key_gold');
        $priceGold->name_gold = Tools::getValue('name_gold');
        $priceGold->price_buy = Tools::getValue('price_buy');
        $priceGold->price_sell = Tools::getValue('price_sell');
        $dtime = DateTime::createFromFormat("d/m/Y", Tools::getValue('day'));
        $dtime->setTime(0, 0, 0);
        $timestamp = $dtime->getTimestamp();
        $priceGold->day = $timestamp;
        if (!Tools::getValue('id_price_gold')) {
            if (!$priceGold->add()) {
                return $this->displayError($this->l('The price gold could not be added.'));
            }
        } else {
            if (!$priceGold->update()) {
                return $this->displayError($this->l('The price gold could not be updated.'));
            }
        }
    }


    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = '';
        $checker = false;
        $key_search_day = '';
        if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL) {
            return $this->displayError($this->l('You cannot add/edit elements from \"All Shops\" or \"Group Shop\".'));
        } else {
            if ((bool)Tools::isSubmit('submitNewPriceGoldModule')) {
                if (!$result = $this->preValidateForm()) {
                    $output .= $this->addPriceGold();
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name);
                } else {
                    $checker = true;
                    $output = $result;
                }
            }
            if ((bool)Tools::isSubmit('deletehoavtNewsPricegold')) {
                $output .= $this->deleteTab();
            }
            if (Tools::getIsset('updatehoavtNewsPricegold')) {//update
                $output .= $this->renderForm();
            } elseif ((bool)Tools::isSubmit('addhoavtNewsPricegold')) {//add new
                $output .= $this->renderForm();
            } elseif (!$checker) {
                $output .= $this->renderFormConfigNews();
                $output .= $this->formSearchDay($key_search_day);
                $output .= $this->formNoteForDay($key_search_day);
                $output .= $this->formUploadExcel();
                if (!empty($key_search_day)) {
                    $dtime = DateTime::createFromFormat("d/m/Y", $key_search_day);
                    $dtime->setTime(0, 0, 0);
                    $timestamp = $dtime->getTimestamp();
                    $key_search_day = $timestamp;
                }
                $output .= $this->renderIndex($key_search_day);
            }
        }
        return $output;

    }

    protected function formSearchDay(&$key_search_day)
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Tìm kiếm theo ngày'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'day',
                        'label' => $this->l('Ngày search'),
                        'class' => 'form-control',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Tìm kiếm'),
                    'icon' => 'icon-search',
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->submit_action = 'submitSearch';
        if (Tools::isSubmit('submitSearch')) {
            $key_search_day = Tools::getValue('day');
        }
        $helper->simple_header = false;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getSearchValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'theme_url' => $this->context->link->getAdminLink('AdminNewsPriceGold')
        );
        return $helper->generateForm(array($fields_form));
    }

    protected function renderFormConfigNews()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Cấu hình trang giá vàng'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'file_lang',
                        'name' => 'BANNER_IMG_GOLD_PRICE',
                        'label' => $this->l('Baner'),
                        'class' => 'form-control',
                        'desc' => $this->trans('Upload an image for your top banner. The recommended dimensions are 1110 x 214px if you are using the default theme.', array(), 'Modules.Banner.Admin'),
                        'lang' => true,
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'TILE_GOLD_PRICE',
                        'label' => $this->l('Title'),
                        'class' => 'form-control',
                        'lang' => true,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Lưu'),
                ),
            ),
        );
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language =$lang->id ;
        $helper->submit_action = 'submitNewModule';

        if (Tools::isSubmit('submitNewModule')) {
            $this->processImageGoldPrice();
            $title = Tools::getValue('TILE_GOLD_PRICE');
            Configuration::updateValue('TILE_GOLD_PRICE', $title);
        }

        $helper->simple_header = false;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFormNewsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'theme_url' => $this->context->link->getAdminLink('AdminNewsPriceGold')
        );
        return $helper->generateForm(array($fields_form));
    }

    protected function processImageGoldPrice(){

        $languages = Language::getLanguages(false);
        $values = array();
        $update_images_values = false;

        foreach ($languages as $lang) {
            if (isset($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']])
                && isset($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']]['tmp_name'])
                && !empty($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']]['tmp_name'])) {
                if ($error = ImageManager::validateUpload($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']], 4000000)) {
                    return $error;
                } else {
                    $ext = substr($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']]['name'], strrpos($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']]['name'], '.') + 1);
                    $file_name = md5($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']]['name']).'.'.$ext;
                    if (!move_uploaded_file($_FILES['BANNER_IMG_GOLD_PRICE_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name)) {
                        return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                    } else {
                        if (Configuration::hasContext('BANNER_IMG_GOLD_PRICE', $lang['id_lang'], Shop::getContext())
                            && Configuration::get('BANNER_IMG_GOLD_PRICE', $lang['id_lang']) != $file_name) {
                            @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('BANNER_IMG_GOLD_PRICE', $lang['id_lang']));
                        }
                        $values['BANNER_IMG_GOLD_PRICE'][$lang['id_lang']] = $file_name;
                    }
                }

                $update_images_values = true;
            }
            $values['TILE_GOLD_PRICE'][$lang['id_lang']] = Tools::getValue('TILE_GOLD_PRICE_'.$lang['id_lang']);
        }
        if ($update_images_values) {
            Configuration::updateValue('BANNER_IMG_GOLD_PRICE', $values['BANNER_IMG_GOLD_PRICE']);
        }
        Configuration::updateValue('TILE_GOLD_PRICE', $values['TILE_GOLD_PRICE']);
        return $this->displayConfirmation($this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success'));
    }

    protected function getSearchValues()
    {
        if ((bool)Tools::getIsset('day')) {
            $fields_values = array(
                'day' => Tools::getValue('day', ''),
            );
        } else {
            $fields_values = array(
                'day' => ''
            );
        }
        return $fields_values;
    }

    /**
     * Set values for the tabs.
     * @return array $fields_values
     */
    protected function getConfigFormNewsValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();

        foreach ($languages as $lang) {
            $fields['BANNER_IMG_GOLD_PRICE'][$lang['id_lang']] = Tools::getValue('BANNER_IMG_GOLD_PRICE_'.$lang['id_lang'], Configuration::get('BANNER_IMG_GOLD_PRICE', $lang['id_lang']));
            $fields['TILE_GOLD_PRICE'][$lang['id_lang']] = Tools::getValue('TILE_GOLD_PRICE_'.$lang['id_lang'], Configuration::get('TILE_GOLD_PRICE', $lang['id_lang']));
        }
        return $fields;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Thêm mới / Chỉnh sửa'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'key_gold',
                        'label' => $this->l('Mã Loại vàng'),
                        'required' => true,
                        'class' => 'form-control'
                    ),

                    array(
                        'type' => 'text',
                        'name' => 'name_gold',
                        'label' => $this->l('Tên loại vàng'),
                        'required' => true,
                        'class' => 'form-control'
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Giá mua'),
                        'required' => true,
                        'name' => 'price_buy',
                        'class' => 'form-control'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Giá bán'),
                        'required' => true,
                        'name' => 'price_sell',
                        'class' => 'form-control'
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Ngày đặt giá'),
                        'required' => true,
                        'name' => 'day',
                        'class' => 'pull-right'
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Lưu'),
                ),
                'buttons' => array(
                    array(
                        'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                        'title' => $this->l('Quay lại trang danh sách'),
                        'icon' => 'process-icon-back'
                    )
                )
            ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->submit_action = 'submitNewPriceGoldModule';
        if (Tools::isSubmit('submitNewPriceGoldModule')) {
            if ($this->preValidateForm()) {
                if (Tools::getValue('id_price_gold')) {
                    $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&updatehoavtNewsPricegold&id_price_gold=' . Tools::getValue('id_price_gold');
                } else {
                    $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&addhoavtNewsPricegold';
                }
            } else {
                $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
            }
        } else {
            if ($this->preValidateForm()) {
                if (Tools::getValue('id_price_gold')) {
                    $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&updatehoavtNewsPricegold' . '&id_price_gold=' . Tools::getValue('id_price_gold');
                } else {
                    $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&addhoavtNewsPricegold';
                }
            } else {
                $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
            }
        }
        $helper->simple_header = false;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'theme_url' => $this->context->link->getAdminLink('AdminNewsPriceGold')
        );
        return $helper->generateForm(array($fields_form));
    }

    /**
     * Check for item fields validity
     * @return array $errors if invalid or false
     */
    protected function preValidateForm()
    {
        $errors = array();
        $name_gold = Tools::getValue('name_gold');
        $price_buy = Tools::getValue('price_buy');
        $price_sell = Tools::getValue('price_sell');

        if (empty($name_gold)) {
            $errors[] = $this->l('Invalid name_gold field is not null ');
        }
        if (empty($price_buy)) {
            $errors[] = $this->l('Invalid price_buy field is not null ');
        }
        if (empty($price_sell)) {
            $errors[] = $this->l('Invalid price_sell field is not null ');
        }

        if (!Validate::isPrice($price_buy)) {
            $errors[] = $this->l('Invalid price buy field');
        }
        if (!Validate::isPrice($price_sell)) {
            $errors[] = $this->l('Invalid price sell field');
        }

        foreach (Language::getLanguages(false) as $lang) {
            if (!Validate::isName(Tools::getValue('label_' . $lang['id_lang']))) {
                $errors[] = sprintf($this->l('Name is invalid: %s'), $lang['iso_code']);
            }
        }
        if (count($errors)) {
            return $this->displayError(implode('<br />', $errors));
        }
        return false;
    }

    /**
     * Set values for the tabs.
     * @return array $fields_values
     */
    protected function getConfigFormValues()
    {
        if ((bool)Tools::getIsset('updatehoavtNewsPricegold') && (int)Tools::getValue('id_price_gold') > 0) {
            $price_gold = new priceGold((int)Tools::getValue('id_price_gold'));
        } else {
            $price_gold = new priceGold();
        }
        $day_price_gold = isset($price_gold->day) ? date('d/m/Y', $price_gold->day) : $price_gold->day;
        $fields_values = array(
            'key_gold' => Tools::getValue('key_gold', $price_gold->key_gold),
            'name_gold' => Tools::getValue('name_gold', $price_gold->name_gold),
            'price_buy' => Tools::getValue('price_buy', $price_gold->price_buy),
            'price_sell' => Tools::getValue('price_sell', $price_gold->price_sell),
            'day' => Tools::getValue('day', $day_price_gold)
        );
        return $fields_values;
    }

    protected function getSettingsFieldsValues()
    {
        return array(
            'POST_LIMIT' => Tools::getValue('POST_LIMIT', Configuration::get('POST_LIMIT')),
        );
    }

    protected function updateSettingsFieldsValues()
    {
        $form_values = $this->getSettingsFieldsValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }


    /**
     * Create the structure of your form.
     * @param bool $tab
     * @return array $tabs and $fields_list
     */
    public function renderIndex($key_search)
    {
        if (!$tabs = priceGold::getAllPriceGold($key_search)) {
            $tabs = array();
        }
        $fields_list = array(
            'id_price_gold' => array(
                'title' => $this->l('Id'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            ),
            'name_gold' => array(
                'title' => $this->l('Tên loại vàng'),
                'type' => 'text',
                'search' => true,
                'orderby' => true,
            ),
            'price_buy' => array(
                'title' => $this->l('Giá mua'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            ),
            'price_sell' => array(
                'title' => $this->l('Giá bán'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            ),
            'day' => array(
                'title' => $this->l('Ngày tháng'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            ),
        );
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_price_gold';
        $helper->table = $this->name;
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->module = $this;
        $helper->title = 'Danh sách giá vàng';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex
                . '&configure=' . $this->name . '&add' . $this->name
                . '&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );
        $helper->currentIndex = AdminController::$currentIndex
            . '&configure=' . $this->name;
        return $helper->generateList($tabs, $fields_list);
    }

    protected function formNoteForDay($key_search_day)
    {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Nhập mô tả theo ngày'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'day',
                        'label' => $this->l('Ngày'),
                        'class' => 'form-control',
                        'id'=>'day-note'
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Mô tả'),
                        'name' => 'note',
                        'cols' => 8,
                        'rows' => 4,
                        'autoload_rte' => 'rte'
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Lưu'),
                    'icon' => 'icon-search',
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->submit_action = 'submitNote';



        if (Tools::isSubmit('submitNote')) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $day  = Tools::getValue('day');
            if (isset($day) && !empty($day)) {
                $dtime = DateTime::createFromFormat("d/m/Y", $day);
                $dtime->setTime(0, 0, 0);
                $timestamp = $dtime->getTimestamp();
                $priceGoldNote = new priceGoldNote($timestamp);
                $key_search_day = date('d/m/Y', $timestamp) ;
                if(!empty($priceGoldNote->day)){
                    $priceGoldNote->note = Tools::getValue('note');
                    if (!$priceGoldNote->update()) {
                        return $this->displayError($this->l('The price gold could not be updated.'));
                    }
                }else{
                    $priceGoldNote->id = $timestamp;
                    $priceGoldNote->day = $timestamp;
                    $priceGoldNote->note = Tools::getValue('note');
                    if (!$priceGoldNote->add()) {
                        return $this->displayError($this->l('The price gold could not be added.'));
                    }
                }
            }
        }
        $helper->simple_header = false;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getNoteValues($key_search_day),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'theme_url' => $this->context->link->getAdminLink('AdminNewsPriceGold')
        );
        return $helper->generateForm(array($fields_form));
    }

    function getNoteValues($key_search_day){
        $note = '';
        if (!empty($key_search_day)) {
            $dtime = DateTime::createFromFormat("d/m/Y", $key_search_day);
            $dtime->setTime(0, 0, 0);
            $timestamp = $dtime->getTimestamp();
            $data = priceGoldNote::getNote($timestamp);
            if( !empty($data)){
                $note = $data['note'];
            }
        }
        if (Tools::getIsset('day')) {
            $fields_values = array(
                'day' => Tools::getValue('day', ''),
                'note' => $note,
            );
        } else {
            $fields_values = array(
                'day' => '',
                'note' => $note
            );
        }
        return $fields_values;
    }

    protected function formUploadExcel()
    {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Upload gia vang bang File Excel'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'file',
                        'name' => 'file_excel',
                        'label' => $this->l('Upload File Excel'),
                        'class' => 'form-control',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Upload File'),
                ),
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->submit_action = 'submitExcel';

        $maxsize    = 2097152;
        if (Tools::isSubmit('submitExcel')) {
            if (isset($_FILES['file_excel'])
                && isset($_FILES['file_excel']['tmp_name'])
                && !empty($_FILES['file_excel']['tmp_name'])) {
                $mimes = array('application/vnd.ms-excel','text/csv');
                if(($_FILES['file_excel']['size'] >= $maxsize) || ($_FILES["file_excel"]["size"] == 0)) {
                    return 'File too large. File must be less than 2 megabytes.';
                }
                if(!in_array($_FILES['file_excel']['type'],$mimes)){
                    return "Sorry, mime type not allowed";
                } else {
                    $filename=$_FILES["file_excel"]["tmp_name"];
                    $this->uploadExcel($filename);
                }
            }
        }
        $helper->simple_header = true;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'theme_url' => $this->context->link->getAdminLink('AdminNewsPriceGold')
        );
        return $helper->generateForm(array($fields_form));
    }

    public function uploadExcel($filename){

        $csvData = file_get_contents($filename);
        $lines = explode(PHP_EOL, $csvData);
        $array = array();
        foreach ($lines as $key => $line) {
            if ($key !== 0) {
                $array[] = str_getcsv($line);
            }
        }
        foreach ($array as $key=>$getData){
            if(!empty($getData[0]) && !empty($getData[1]) && !empty($getData[4])){
                $key_gold = $getData[0];
                $name_gold = $getData[1];
                $day = $getData[4];
                $price_buy = !empty($getData[2]) ? $getData[2] : '';
                $price_sell = !empty($getData[3]) ? $getData[3] : '';
                $priceGold = priceGold::findPriceGold($key_gold,$this->convertDateToTimestamp($day));
                if(!empty($priceGold) && count($priceGold)>0){
                    $priceGold = new priceGold((int)$priceGold[0]['id_price_gold']);
                    $priceGold->key_gold = $key_gold;
                    $priceGold->name_gold = $name_gold;
                    $priceGold->price_buy = $price_buy;
                    $priceGold->price_sell = $price_sell;
                    $priceGold->day = $this->convertDateToTimestamp($day);
                    $priceGold->update();
                }else{
                    $priceGold = new priceGold();
                    $priceGold->key_gold = $key_gold;
                    $priceGold->name_gold = $name_gold;
                    $priceGold->price_buy = $price_buy;
                    $priceGold->price_sell = $price_sell;
                    $priceGold->day = $this->convertDateToTimestamp($day);
                    $flag = $priceGold->add();
                }
            }

        }
    }

    public function convertDateToTimestamp($date){
        $dtime = DateTime::createFromFormat("d/m/Y", $date);
        $dtime->setTime(0, 0, 0);
        return $timestamp = $dtime->getTimestamp();
    }

}
