<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class priceGold extends ObjectModel
{

    /** @var int default shop id */
    public $id_price_gold;

    /** @var string key gold */
    public $key_gold;

    /** @var string name gold */
    public $name_gold;

    /** @var string */
    public $price_buy;

    /** @var string */
    public $price_sell;

    /** @var int */
    public $day;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
     'table' => 'price_gold',
     'primary' => 'id_price_gold',
     'multilang' => false,
     'fields' => array(
        'key_gold' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
        'name_gold' => array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isGenericName', 'size' => 128),
        'price_buy' => array('type' => self::TYPE_STRING),
        'price_sell' => array('type' => self::TYPE_STRING),
        'day' => array('type' => self::TYPE_INT,  'validate' => 'isUnsignedFloat', 'required' => true),
       ),
    );

    /**
     * Get list with product id
     * return bool $result if invalid or false
     */
    public static function getTopNews()
    {
        $prefix = _DB_PREFIX_;
        $result = '';
        $sql = 'SELECT p.*`
				FROM'._DB_PREFIX_.'jxblog_post_lang p
				LEFT JOIN '._DB_PREFIX_.'jxblog_post ps
				ON(p.`id_jxblog_post` = ps.`id_jxblog_post`)
				ORDER BY ps.date_add DESC 
				LIMIT 10';
        if (!$result = Db::getInstance()->executeS($sql)) {
            return false;
        }

        return $result;
    }


    /**
     * @return array|bool|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAllPriceGold($key_search = '')
    {
        if(!empty($key_search)){
            $and_where = " WHERE day = $key_search";
        }else{
            $and_where = ' WHERE day IS NOT NULL';
        }
        $sql = '
            SELECT n.* 
            FROM ps_price_gold n
            INNER JOIN (
              SELECT key_gold, MAX(day) AS day
              FROM ps_price_gold 
               '.$and_where.' 
              GROUP BY key_gold
            ) AS max USING (key_gold, day)
            
            order by key_gold,day desc
            ';
        if (!$result = Db::getInstance()->executeS($sql)) {
            return false;
        }
        $results = [];
        foreach ($result as $value){
            $value['day'] = date("d/m/Y",$value['day']);
            $results[] = $value;
        }
        return $results;
    }

    /**
     * @return array|bool|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAllPriceGoldFrontend($key_search = '')
    {
        if(!empty($key_search)){
            $and_where = " WHERE day = $key_search";
        }else{
            $and_where = ' WHERE day IS NOT NULL';
        }
        $sql = '
            SELECT n.* 
            FROM ps_price_gold n
            INNER JOIN (
              SELECT key_gold, MAX(day) AS day
              FROM ps_price_gold 
                '.$and_where.' 
              GROUP BY key_gold
            ) AS max USING (key_gold, day)
            order by key_gold,day desc
            ';
        if (!$result = Db::getInstance()->executeS($sql)) {
            return false;
        }
        $results = [];
        foreach ($result as $value){
            $value['day'] = date("d/m/Y",$value['day']);
            $results[] = $value;
        }
        return $results;
    }

    /**
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAllPost(){
        $id_lang = (int)Context::getContext()->language->id;
        $id_cate_news = Configuration::get('ID_TIN_TUC_KHUYEN_MAI');
        $sql = 'SELECT p.*, pl.*
                FROM '._DB_PREFIX_.'jxblog_post p
                LEFT JOIN '._DB_PREFIX_.'jxblog_post_lang pl
                ON(p.`id_jxblog_post` = pl.`id_jxblog_post` AND pl.`id_lang` = '.(int)$id_lang.')
                WHERE p.`active` = 1
                AND p.id_jxblog_category_default = '.(int)$id_cate_news.'
                ORDER BY  p.`date_start` DESC
                LIMIT 3';

        return Db::getInstance()->executeS($sql);
    }

    public static function findPriceGold($key_gold,$day){
        $sql = 'SELECT pg.*
			FROM '._DB_PREFIX_.'price_gold pg'.
            ' WHERE pg.key_gold='."'". $key_gold."'". ' AND pg.day ='.$day.
            ' LIMIT 1'
        ;
        return Db::getInstance()->executeS($sql);
    }

}
