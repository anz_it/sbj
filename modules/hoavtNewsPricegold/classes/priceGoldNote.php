<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class priceGoldNote extends ObjectModel
{

    public $id;
    public $day;
    public $note;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
     'table' => 'price_gold_note',
     'primary' => 'day',
      'multilang' => false,
     'fields' => array(
        'day' => array('type' => self::TYPE_DATE),
        'note' => array('type' => self::TYPE_HTML, 'lang' => false, 'required' => true),
       ),
    );

    public static function getNote($day){
        $sql = 'SELECT p.*
				FROM '._DB_PREFIX_.'price_gold_note p
				WHERE p.day = '. $day.'
				LIMIT 1';

        if (!$result = Db::getInstance()->executeS($sql)) {
            return false;
        }

        return $result[0];
    }
}
