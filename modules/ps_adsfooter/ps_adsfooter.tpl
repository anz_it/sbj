<div class="advs">
    <div class="container">
        <div class="advs-home d-none d-md-block">
            <div class="row">
                {foreach $configs as $key=>$config}
                    <div class="col-lg-{if $key == 2}6{else}3{/if} col-md-6">
                        <a href="{$config.adsfooter_link}">
                            <img src="{$config.adsfooter_img}" alt="{$config.adsfooter_title}">
                        </a>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>
