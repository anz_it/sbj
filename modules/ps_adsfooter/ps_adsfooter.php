<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class ps_adsfooter extends Module implements WidgetInterface
{
    private $templateFile;

    public function __construct()
    {
        $this->name = 'ps_adsfooter';
        $this->version = '2.1.0';
        $this->author = 'PrestaShop';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Page Ads Footer Cấu hình', array(), 'Modules.Banner.Admin');
        $this->description = $this->trans('Cấu hình Ads ', array(), 'Modules.Banner.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);

        $this->templateFile = 'module:ps_adsfooter/ps_adsfooter.tpl';
    }

    public function install()
    {
        return (parent::install() &&
            $this->registerHook('displayHome') &&
            $this->registerHook('actionObjectLanguageAddAfter') &&
            $this->installFixtures() &&
            $this->disableDevice(Context::DEVICE_MOBILE));
    }

    public function hookActionObjectLanguageAddAfter($params)
    {
        return $this->installFixture((int)$params['object']->id, Configuration::get('ADS_FOOTER_IMG_', (int)Configuration::get('PS_LANG_DEFAULT')));
    }

    protected function installFixtures()
    {
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $this->installFixture((int)$lang['id_lang'], 'sale70.png');
        }

        return true;
    }

    protected function installFixture($id_lang, $image = null)
    {
        for ($i = 1; $i < 4; $i++) {
            $values['ADS_FOOTER_IMG_' . $i][(int)$id_lang] = $image;
            $values['ADS_FOOTER_LINK_' . $i][(int)$id_lang] = '';
            $values['ADS_FOOTER_TITLE_' . $i][(int)$id_lang] = '';

            Configuration::updateValue('ADS_FOOTER_IMG_' . $i, $values['ADS_FOOTER_IMG_' . $i]);
            Configuration::updateValue('ADS_FOOTER_LINK_' . $i, $values['ADS_FOOTER_LINK_' . $i]);
            Configuration::updateValue('ADS_FOOTER_TITLE_' . $i, $values['ADS_FOOTER_TITLE_' . $i]);
        }

    }

    public function uninstall()
    {
        for ($i = 1; $i < 4; $i++) {
            Configuration::deleteByName('ADS_FOOTER_IMG_' . $i);
            Configuration::deleteByName('ADS_FOOTER_LINK_' . $i);
            Configuration::deleteByName('ADS_FOOTER_TITLE_' . $i);
        }
        return parent::uninstall();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitStoreConf')) {
            $languages = Language::getLanguages(false);
            $values = array();
            for ($i = 1; $i < 4; $i++) {
                $update_images_values = false;
                foreach ($languages as $lang) {
                    if (isset($_FILES['ADS_FOOTER_IMG_'.$i.'_'. $lang['id_lang']])
                        && isset($_FILES['ADS_FOOTER_IMG_'.$i.'_'.$lang['id_lang']]['tmp_name'])
                        && !empty($_FILES['ADS_FOOTER_IMG_'.$i.'_'.$lang['id_lang']]['tmp_name'])
                    ) {
                        if ($error = ImageManager::validateUpload($_FILES['ADS_FOOTER_IMG_'.$i.'_'. $lang['id_lang']], 4000000)) {
                            return $error;
                        } else {
                            $ext = substr($_FILES['ADS_FOOTER_IMG_'.$i.'_'. $lang['id_lang']]['name'], strrpos($_FILES['ADS_FOOTER_IMG_'.$i.'_'. $lang['id_lang']]['name'], '.') + 1);
                            $file_name = md5($_FILES['ADS_FOOTER_IMG_' .$i.'_'. $lang['id_lang']]['name']) . '.' . $ext;

                            if (!move_uploaded_file($_FILES['ADS_FOOTER_IMG_'.$i.'_'. $lang['id_lang']]['tmp_name'], dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $file_name)) {
                                return $this->displayError($this->trans('An error occurred while attempting to upload the file.', array(), 'Admin.Notifications.Error'));
                            } else {
                                if (Configuration::hasContext('ADS_FOOTER_IMG_'.$i, $lang['id_lang'], Shop::getContext())
                                    && Configuration::get('ADS_FOOTER_IMG_'.$i, $lang['id_lang']) != $file_name
                                ) {
                                    @unlink(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . Configuration::get('ADS_FOOTER_IMG_'.$i, $lang['id_lang']));
                                }

                                $values['ADS_FOOTER_IMG_'.$i][$lang['id_lang']] = $file_name;
                            }
                        }

                        $update_images_values = true;
                    }

                    $values['ADS_FOOTER_LINK_'.$i][$lang['id_lang']] = Tools::getValue('ADS_FOOTER_LINK_'.$i.'_'. $lang['id_lang']);
                    $values['ADS_FOOTER_TITLE_'.$i][$lang['id_lang']] = Tools::getValue('ADS_FOOTER_TITLE_' .$i.'_'. $lang['id_lang']);
                }

                if ($update_images_values) {
                    Configuration::updateValue('ADS_FOOTER_IMG_'.$i, $values['ADS_FOOTER_IMG_'.$i]);
                }

                Configuration::updateValue('ADS_FOOTER_LINK_'.$i, $values['ADS_FOOTER_LINK_'.$i]);
                Configuration::updateValue('ADS_FOOTER_TITLE_'.$i, $values['ADS_FOOTER_TITLE_'.$i]);
            }


            $this->_clearCache($this->templateFile);

            return $this->displayConfirmation($this->trans('The settings have been updated.', array(), 'Admin.Notifications.Success'));
        }

        return '';
    }

    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function renderForm()
    {
        $array_form = [];
        for($i = 1; $i < 4;$i++){
            $array_temp =array(
                array(
                    'type' => 'file_lang',
                    'label' => $this->trans("Block $i Image ", array(), 'Modules.Banner.Admin'),
                    'name' => 'ADS_FOOTER_IMG_'.$i,
                    'desc' => $this->trans('Upload hình', array(), 'Modules.Banner.Admin'),
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'lang' => true,
                    'label' => $this->trans("Block $i Link", array(), 'Modules.Banner.Admin'),
                    'name' => 'ADS_FOOTER_LINK_'.$i,
                    'desc' => $this->trans('Nhập link tới page', array(), 'Modules.Banner.Admin')
                ),
                array(
                    'type' => 'text',
                    'lang' => true,
                    'label' => $this->trans('Block Title', array(), 'Modules.Banner.Admin'),
                    'name' => 'ADS_FOOTER_TITLE_'.$i,
                    'desc' => $this->trans('Nhập tiêu đề block', array(), 'Modules.Banner.Admin')
                )
            );
            $array_form = array_merge_recursive($array_form,$array_temp);
        }


        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Settings', array(), 'Admin.Global'),
                    'icon' => 'icon-cogs'
                ),
                'input' => $array_form,
                'submit' => array(
                    'title' => $this->trans('Save', array(), 'Admin.Actions')
                )
            ),
        );

        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitStoreConf';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();

        for($i = 1; $i < 4;$i++) {
            foreach ($languages as $lang) {
                $fields['ADS_FOOTER_IMG_'.$i][$lang['id_lang']] = Tools::getValue('ADS_FOOTER_IMG_'.$i.'_'. $lang['id_lang'], Configuration::get('ADS_FOOTER_IMG_'.$i, $lang['id_lang']));
                $fields['ADS_FOOTER_LINK_'.$i][$lang['id_lang']] = Tools::getValue('ADS_FOOTER_LINK_' .$i.'_'. $lang['id_lang'], Configuration::get('ADS_FOOTER_LINK_'.$i, $lang['id_lang']));
                $fields['ADS_FOOTER_TITLE_'.$i][$lang['id_lang']] = Tools::getValue('ADS_FOOTER_TITLE_' .$i.'_'. $lang['id_lang'], Configuration::get('ADS_FOOTER_TITLE_'.$i, $lang['id_lang']));
            }
        }

        return $fields;
    }

    public function renderWidget($hookName, array $params)
    {
        if (!$this->isCached($this->templateFile, $this->getCacheId('ps_adsfooter'))) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $params));
        }

        return $this->fetch($this->templateFile, $this->getCacheId('ps_adsfooter'));
    }

    public function getWidgetVariables($hookName, array $params)
    {
        $data = [];
        for($i = 1; $i < 4 ;$i ++){
            $url_img = '';
            $imgname = Configuration::get('ADS_FOOTER_IMG_'.$i, $this->context->language->id);
            if ($imgname && file_exists(_PS_MODULE_DIR_ . $this->name . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $imgname)) {
                $url_img = $this->context->link->protocol_content . Tools::getMediaServer($imgname) . $this->_path . 'img/' . $imgname;
            }

            $adsfooter_link = Configuration::get('ADS_FOOTER_LINK_'.$i, $this->context->language->id);
            if ($adsfooter_link) {
                $adsfooter_link = $this->context->link->getCMSLink($adsfooter_link);
            }
            $temp= [
                'adsfooter_img' =>$url_img,
                'adsfooter_link' => $adsfooter_link,
                'adsfooter_title'=> Configuration::get('ADS_FOOTER_TITLE_'.$i, $this->context->language->id),
            ];
            $data[] = $temp;

        }
        $this->smarty->assign('configs',$data);
    }
}
